#
# CMake packaging for Omicron
# Copyright 2019  Duncan Macleod <duncan.macleod@ligo.org>
# Copyright 2019  Florent Robinet <florent.robinet@ligo.org>
#

# -- package info -----------

# cmake required
cmake_minimum_required(VERSION 3.12.0 FATAL_ERROR)

# project definition
project(
  Omicron
  LANGUAGES CXX
  VERSION 4.2.0
  DESCRIPTION "An algorithm to detect and characterize transient events in gravitational-wave detectors"
  HOMEPAGE_URL "https://virgo.docs.ligo.org/virgoapp/Omicron/"
  )
string(TOLOWER ${PROJECT_NAME} PROJECT_NAME_LOWER)

# -- extra requirements -----

# GNU software standard directory
include(GNUInstallDirs)

# use pkgconfig
find_package(PkgConfig REQUIRED)

# compilation flags
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wundef -Wshadow -Wwrite-strings -Wunreachable-code -Wunused-result -O3 -fstack-protector")

# -- set install paths ------

set(RUNTIME_DESTINATION ${CMAKE_INSTALL_BINDIR})
set(LIBRARY_DESTINATION ${CMAKE_INSTALL_LIBDIR})

# -- build components -------

enable_testing()

add_subdirectory(src)
add_subdirectory(doc)

# -- user environment -------

configure_file(./omicron.env.sh.in ./omicron.env.sh @ONLY)

# install environment scripts
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/omicron.env.sh
  DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}
  )

install(
  DIRECTORY
  html
  DESTINATION ${CMAKE_INSTALL_DATADIR}/omicron
  )

