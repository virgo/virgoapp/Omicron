/**
 * @file 
 * @brief See OxResults.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxResults.h"

////////////////////////////////////////////////////////////////////////////////////
bool OxResults::SavePlots(const string aFormat){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxResults::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxResults::"<<__func__<<": the OxResults object is corrupted"<<endl;
    return false;
  }

  // save injection plots
  if(OxSim::SavePlots(aFormat)==false) return false;

  // output directory
  string outdir = ox_opt->GetOptionValueS("OXI", "OUTDIR", 0);

  // locals
  string plot_filename;
  TH1D *h1_cum[oxresult_n];
  TH1D* h1_err[5];
  
  // loop over results
  for(unsigned int r=0; r<oxresult_n; r++){

    // oxr_ev_rank
    plot_filename = outdir+"/oxr_ev_rank_"+OxResultTypeName[r]+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxResults::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_ev_rank[r], "HIST");
    GwollumPlot::SetLogy(1);
    GwollumPlot::Print(plot_filename);

    // oxr_ev_freq
    plot_filename = outdir+"/oxr_ev_freq_"+OxResultTypeName[r]+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxResults::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_ev_freq[r], "HIST");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(1);
    GwollumPlot::Print(plot_filename);

    // oxr_ev_rank_freq
    plot_filename = outdir+"/oxr_ev_rank_freq_"+OxResultTypeName[r]+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxResults::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    GwollumPlot::Draw(h2_ev_rank_freq[r], "COLZ");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(1);
    GwollumPlot::Print(plot_filename);

    // cumulative histogram
    plot_filename = outdir+"/oxr_ev_rank_cum_"+OxResultTypeName[r]+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxResults::"<<__func__<<": "<<plot_filename<<endl;
    h1_cum[r] = (TH1D*)h1_ev_rank[r]->GetCumulative(false, "_cum");
    h1_cum[r]->GetYaxis()->SetTitle("Cumulative number of events");
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_cum[r], "HIST");
    GwollumPlot::SetLogy(1);
    GwollumPlot::Print(plot_filename);

    // cumulative rate histogram
    plot_filename = outdir+"/oxr_ev_rank_cumrate_"+OxResultTypeName[r]+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxResults::"<<__func__<<": "<<plot_filename<<endl;
    if(livetime[r]>0) h1_cum[r]->Scale(1.0/(double)livetime[r]);
    h1_cum[r]->GetYaxis()->SetTitle("Cumulative event rate [Hz]");
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_cum[r], "HIST");
    GwollumPlot::SetLogy(1);
    GwollumPlot::Print(plot_filename);
  }

  // detection plot
  plot_filename = outdir+"/oxr_ev_detection."+aFormat;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxResults::"<<__func__<<": "<<plot_filename<<endl;
  h1_err[0] = (TH1D*)h1_cum[oxresult_background]->Clone("h1_err1");
  h1_err[1] = (TH1D*)h1_cum[oxresult_background]->Clone("h1_err2");
  h1_err[2] = (TH1D*)h1_cum[oxresult_background]->Clone("h1_err3");
  h1_err[3] = (TH1D*)h1_cum[oxresult_background]->Clone("h1_err4");
  h1_err[4] = (TH1D*)h1_cum[oxresult_background]->Clone("h1_err5");
  h1_err[4]->SetTitle("");
  for(unsigned int bx=0; bx<=(unsigned int)h1_err[0]->GetNbinsX()+1; bx++){
    if(livetime[oxresult_foreground]>0)
      h1_err[0]->SetBinError(bx, TMath::Sqrt(h1_cum[oxresult_background]->GetBinContent(bx)/(double)livetime[oxresult_foreground]));
    else
      h1_err[0]->SetBinError(bx, 0.0);
    h1_err[1]->SetBinError(bx, 2.0*h1_err[0]->GetBinError(bx));
    h1_err[2]->SetBinError(bx, 3.0*h1_err[0]->GetBinError(bx));
    h1_err[3]->SetBinError(bx, 4.0*h1_err[0]->GetBinError(bx));
    h1_err[4]->SetBinError(bx, 5.0*h1_err[0]->GetBinError(bx));
  }
  h1_err[0]->SetFillColor(GwollumPlot::GetColorPalette(25*GwollumPlot::GetNumberOfColors()/100));// 25%
  h1_err[1]->SetFillColor(GwollumPlot::GetColorPalette(20*GwollumPlot::GetNumberOfColors()/100));// 20%
  h1_err[2]->SetFillColor(GwollumPlot::GetColorPalette(15*GwollumPlot::GetNumberOfColors()/100));// 15%
  h1_err[3]->SetFillColor(GwollumPlot::GetColorPalette(10*GwollumPlot::GetNumberOfColors()/100));// 10%
  h1_err[4]->SetFillColor(GwollumPlot::GetColorPalette(5*GwollumPlot::GetNumberOfColors()/100));// 5%
  GwollumPlot::UnDrawLegend();
  GwollumPlot::SetLogx(0);
  GwollumPlot::SetLogy(0);
  GwollumPlot::Draw(h1_err[4], "E3");// background error band (5 sigma)
  GwollumPlot::Draw(h1_err[3], "E3SAME");// background error band (4 sigma)
  GwollumPlot::Draw(h1_err[2], "E3SAME");// background error band (3 sigma)
  GwollumPlot::Draw(h1_err[1], "E3SAME");// background error band (2 sigma)
  GwollumPlot::Draw(h1_err[0], "E3SAME");// background error band (1 sigma)
  GwollumPlot::Draw(h1_cum[oxresult_background], "CHISTSAME");
  GwollumPlot::Draw(h1_cum[oxresult_fakeground], "E1X0SAME");
  GwollumPlot::Draw(h1_cum[oxresult_foreground], "E1X0SAME");
  GwollumPlot::SetLogy(1);
  GwollumPlot::RedrawAxis();
  GwollumPlot::ResetLegend();
  GwollumPlot::AddLegendEntry(h1_cum[oxresult_background], "Background", "L");
  GwollumPlot::AddLegendEntry(h1_cum[oxresult_fakeground], "Fakeground", "P");
  GwollumPlot::AddLegendEntry(h1_cum[oxresult_foreground], "Foreground", "P");
  GwollumPlot::DrawLegend();
  GwollumPlot::Print(plot_filename);


  // cleaning
  delete h1_err[0];
  delete h1_err[1];
  delete h1_err[2];
  delete h1_err[3];
  delete h1_err[4];
  for(unsigned int r=0; r<oxresult_n; r++) delete h1_cum[r];


  return true;
}
