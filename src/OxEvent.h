/**
 * @file 
 * @brief Manage Ox events.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __OxEvent__
#define __OxEvent__
#include "OxCorr.h"

using namespace std;

/**
 * @brief Ox event management.
 * @details An Ox event is defined as a time-delay slice in a cross-correlation map built with OxCorr. Each event is parameterized by (see ComputeEvent()):
 * - An analysis index: 8-bit integer to associate an event to a given analysis.
 * - A slice index: see MakeSlices() for a definition.
 * - A time delay: this is the time delay between detector 0 and 1 which maximizes the integrated cross-correlation.
 * - For each Q plane, the integral of \f$\xi\f$, noted \f$\Xi\f$, over all frequencies, calculated for the event time delay.
 * - For each Q plane, an mean frequency.
 * - For each Q plane, the number of frequency rows which are rejected by the slice veto.
 * - For each Q plane, the time-delay variance around the event time delay.
 *
 * This class can be used in two different ways:
 * 1. To build a list of Ox events. After creating an Ox file with CreateOutputFile(), use the OxCorr engine to build cross-correlation maps. Then call SaveEvents() to record the events in the output Ox file.
 * 2. To process a list of Ox events. In this case, a list of Ox files (generated in 1.) must be provided in the option file (`OXI/OXFILELIST`). Then, call the events with GetEntry().
 */
class OxEvent: public OxCorr{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
   /**
   * @brief Constructor of the OxEvent class.
   * @details An option file must be provided to configure the Ox event management:
   * - `OXE SLICEDELTA` Size of slices. Two time delays [s] are expected: the calibration time uncertainty, noted \f$\delta_\mathrm{cal}\f$, and the slice veto time delay \f$\delta_\mathrm{veto}\f$.
   * - `OXE SNRGATE` OxEvent gating. When the maximum signal-to-noise ratio in the Omicron spectrogram exceeds this value, all the derived event ranks are set to impossible values (below -1). Two values are expected: one SNR threshold for each detector. By default = 1000. See also GetEventRank().
   * - `OXE RANKMETHOD` OxEvent ranking method.
   *   - "X": this is the basic cross-correlation ranking statistic (default).
   *   - "A": this is the amplitude cross-correlation ranking statistic.
   *   - "W": this is the amplitude-weighted cross-correlation ranking statistic.
   *
   * The cross-correlation map is sliced with MakeSlices().
   * @param[in] aOptionFile Path to the option file.
   * @param[in] aGpsRef0 Reference GPS time to intiate the Omicron object for the first detector.
   * @param[in] aGpsRef1 Reference GPS time to intiate the Omicron object for the second detector.
   */ 
  OxEvent(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1);

  /**
   * @brief Destructor of the OxEvent class.
   */
  virtual ~OxEvent(void);

  /**
     @}
  */


  /**
   * @brief Creates an ouput file.
   * @details A new ROOT file is opened in the output directory to save the Ox analysis results (OxInit::CreateOutputFile()). The Ox event TTree "oxe" is initialized in the ouput file.
   * @sa OxInit::CreateOutputFile() for the file management.
   * @param[out] aOutFileName Output ROOT file name.
   * @param[in] aFileId File ID used to name the ouput Ox file.
   * @param[in] aTime Time used to name the ouput file.
   */
  bool CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime);

  /**
   * @brief Constructs an Ox event.
   * @details After the cross-correlation analysis is finished (OxCorr::Process()), call this function to construct an event and its parameters. A given slice is processed with ComputeEvent() to calculate all the event parameters.
   *
   * An event can be tagged with an analysis index. This option is useful to manage multiple sets of events.
   * @param[in] aSliceIndex Slice index.
   * @param[in] aAnalysisIndex Analysis index.
   */
  bool MakeEvent(const unsigned int aSliceIndex, const UChar_t aAnalysisIndex = 0);

  /**
   * @brief Save events in the TTree.
   * @details After the cross-correlation analysis is finished (OxCorr::Process()), call this function to save all the events in the output TTree. Each slice is processed with ComputeEvent() to calculate all the event parameters.
   *
   * An event can be tagged with an analysis index. This option is useful to manage multiple sets of events.
   * @note This function can only be called after an ouput file is created with CreateOutputFile().
   * @param[in] aAnalysisIndex Analysis index.
   */
  bool SaveEvents(const UChar_t aAnalysisIndex = 0);

  /**
   * @brief Returns the number of events in the input Ox files.
   */
  inline Long64_t GetEntries(void){ return oxe_tree_read->GetEntries(); };

  /**
   * @brief Loads an entry in the event TTree (read-only).
   * @details The corresponding Omicron analysis result is also loaded.
   * @returns The function returns the number of bytes read from the input buffer. If entry does not exist the function returns 0. If an I/O error occurs, the function returns -1.
   * @param[in] aEntry Entry number.
   */
  int GetEntry(const Long64_t aEntry);

  /**
   * @brief Returns the TChain entry number for the current event (read-only).
   */
  inline Long64_t GetEventEntryNumber(void){ return oxe_tree_read->GetReadEntry(); };

  /**
   * @brief Returns the number of slices in one cross-correlation map.
   * @sa MakeSlices()
   */ 
  inline unsigned int GetSliceN(void){ return slice_n; };

  /**
   * @brief Returns the number of time-delay bins in one slice.
   * @sa MakeSlices()
   */ 
  inline unsigned int GetSliceTimeDelayBinsN(void){ return slice_nbins; };

  /**
   * @brief Returns the number of unsed time-delay bins after slicing.
   * @note It is always an even number (by construction).
   * @sa MakeSlices()
   */
  inline unsigned int GetSliceUnusedTimeDelayBinsN(void){
    return OxCorr::GetTimeDelayBinsN() - GetSliceN()*GetSliceTimeDelayBinsN();
  };
  
  /**
   * @brief Returns the half duration of one slice [s].
   * @sa MakeSlices()
   */
  inline double GetSliceHalfDuration(void){
    return (double)GetSliceTimeDelayBinsN()*OxCorr::GetTimeDelayResolution()/2.0;
  };

  /**
   * @brief Returns the time delay at the center of a slice [s].
   * @param[in] aSliceIndex Slice index.
   * @pre The slice index must be valid.
   * @sa MakeSlices()
   */
  inline double GetSliceTimeDelay(const unsigned int aSliceIndex){
    return OxCorr::GetTimeDelay(slice_firstbin[aSliceIndex] + slice_nbins/2);
  };

  /**
   * @brief Returns the index of the slice at the center of the cross-correlation map.
   * @details This is the slice indexed by \f$s= n_s = (N_s-1)/2\f$.
   */
  inline unsigned int GetSliceIndexCenter(void){
    return (GetSliceN()-1)/2;
  };

  /**
   * @brief Returns the slice index for the current event.
   */  
  inline unsigned int GetEventSliceIndex(void){ return oxe_sindex; };

  /**
   * @brief Returns the analysis index for the current event.
   */  
  inline UChar_t GetEventAnalysisIndex(void){ return oxe_aindex; };

  /**
   * @brief Returns the rank for the current event.
   * @details The event rank is the weighted-average \f$\Xi\f$ over all the Omicron Q planes. The weighted method is selected with the `OXE/RANKMETHOD` option.
   */  
  double GetEventRank(void);

  /**
   * @brief Returns the frequency for the current event [Hz].
   */  
  inline double GetEventFrequency(void){ return oxe_frequency; };

  /**
   * @brief Returns the GW time delay for the current event [s].
   * @details This is the time delay measured from the center of the slice.
   */  
  inline double GetEventTimeDelayGw(void){ return GetEventTimeDelay() - GetSliceTimeDelay(oxe_sindex); };

  /**
   * @brief Returns the time delay for the current event [s].
   */  
  inline double GetEventTimeDelay(void){ return oxe_dt; };

  /**
   * @brief Returns the time delay variance for the current event [s^2].
   */  
  inline double GetEventTimeDelayVariance(void){ return oxe_dt_var; };

  /**
   * @brief Returns the integrated cross-correlation \f$\Xi\f$ for the the current event.
   */  
  inline double GetEventIntegratedXi(void){ return oxe_Xi; };

  /**
   * @brief Returns the number of rejected frequency rows for the the current event.
   * @details Frequency rows are rejected by the slice veto when integrating the cross-correlation \f$\Xi\f$.
   * @param[in] aQindex Q plane index. It must be valid.
   */  
  inline unsigned int GetEventFrequencyRowRejectedN(const unsigned int aQindex){ return oxe_n_sveto_q[aQindex]; };

  /**
   * @brief Returns the amplitude ratio for the the current event.
   */  
  inline double GetEventAmplitudeRatio(void){ return oxe_aratio; };

  /**
   * @brief Returns the minimum antenna factor ratio for the the current event.
   */  
  inline double GetEventAntennaFactorRatioMin(void){ return oxe_afratio_min; };

  /**
   * @brief Returns the maximum antenna factor ratio for the the current event.
   */  
  inline double GetEventAntennaFactorRatioMax(void){ return oxe_afratio_max; };

  /**
   * @brief Writes sky maps in the output directory as png files for the current event.
   * @details Three sky maps are written to disk for the current event:
   * - "OXSKYMAP0-[TIME]-s[SLICE_INDEX].png": the map shows the antenna factors for the first detector at the start time.
   * - "OXSKYMAP1-[TIME]-s[SLICE_INDEX].png": the map shows the antenna factors for the second detector at the start time (same as first detector!).
   * - "OXSKYMAPR-[TIME]-s[SLICE_INDEX].png": the map shows the ratio of the two prevois map, second detector / first detector.
   *
   * On top of each map, a ring is drawn, corresponding to the gravitational-wave time delay for the current event (using the center of the slice as a reference).
   *
   * @note The same time is used for both maps, the time of the first detector. We only use a configuration of foreground events, where gravitational waves can be detected.
   */  
  void WriteSkyMaps(void);

protected:

  /**
   * @brief Fills the output tree with the current event.
   * @details It is assumed that both OxOmicron trees are already filled with OxOmicron::FillTree(). It is necessary to map the event with the OxOmicron entries.
   * @returns The function returns the number of bytes committed to the individual branches.
   */  
  int FillTree(void);

  /**
   * @brief Returns the current tree number (or file number) in the list of input Ox files.
   */  
  inline unsigned int GetTreeNumber(void){ return oxe_tree_read->GetTreeNumber(); };

  /**
   * @brief Returns the number of entries in the output tree.
   */  
  inline Long64_t GetEntriesOut(void){ return oxe_tree->GetEntries(); };

  /**
   * @brief Generates an html table listing the current event parameters.
   * @param[in] aReport Output file stream.
   */  
  void MakeHtmlEventTable(ofstream &aReport);

  /**
   * @brief Generates an html table showing sky maps for the current event.
   * @sa WriteSkyMaps()
   * @param[in] aReport Output file stream.
   */  
  void MakeHtmlSkyMapTable(ofstream &aReport);

  /**
   * @brief Generates an html table listing the Ox configuration.
   * @param[in] aReport Output file stream.
   * @param[in] aOutDir Output directory where to save option file. It must be the same as the one hosting the main html page.
   */  
  void MakeHtmlConfigTable(ofstream &aReport, const string aOutDir);


private:

  // TTree
  TTree *oxe_tree;             ///< Event tree. Do not delete as it is owned by the output file.
  TChain *oxe_tree_read;       ///< TChain: Ox events from input Ox files.
  UChar_t oxe_aindex;          ///< Event tree: analysis index.
  unsigned int oxe_sindex;     ///< Event tree: slice index.
  double oxe_dt;               ///< Event tree: time delay [s].
  double oxe_dt_var;           ///< Event tree: time-delay variance [s^2].
  Long64_t oxe_oxo_0;          ///< Event tree: Omicron TTree entry (det 0).
  Long64_t oxe_oxo_1;          ///< Event tree: Omicron TTree entry (det 1).
  double oxe_frequency;        ///< Event tree: frequency [Hz].
  double oxe_aratio;           ///< Event tree: mean amplitude ratio.
  double oxe_afratio_min;      ///< Event tree: minimum antenna factor ratio.
  double oxe_afratio_max;      ///< Event tree: maximum antenna factor ratio.
  double oxe_Xi;               ///< Event tree: \f$\Xi\f$.
  unsigned int *oxe_n_sveto_q; ///< Event tree: number of frequency rows rejected by the slice veto / Q.

  // SLICES
  unsigned int slice_n;        ///< Number of slices in a cross-correlation map.
  unsigned int slice_nbins;    ///< Number of time-delay bins in a slice.
  unsigned int *slice_firstbin;///< First time-delay bin index of each slice.
  int **slice_ctn_binmax;      ///< Slice container: time-delay bin where xi is max / f row / Q.
  double slice_delta_cal;      /// Slice calibration uncertainty (to go faster) [s]. 
  double slice_delta_veto;     /// Slice calibration uncertainty (to go faster) [s]. 

  double snrsq_gate_0;         ///< SNR-squared gate for detector 0 (to go faster).
  double snrsq_gate_1;         ///< SNR-squared gate for detector 1 (to go faster).
  string rank_method;          ///< Rank method (to go faster).
  double *aratio_q;            ///< Amplitude ratio / Q.

  /**
   * @brief Computes the event parameters.
   * @details TBC.
   * @param[in] aSliceIndex Slice index.
   * @pre The slice index must be valid.
   */
  void ComputeEvent(const unsigned int aSliceIndex);

  /**
   * @brief Slices a cross-correlation map.
   * @details A slice in the cross-correlation map is a time-delay window the size of which is \f$\pm (\delta_\mathrm{det} + \delta_\mathrm{cal} + \delta_\mathrm{veto})\f$. The maximum light travel time between the two detectors, \f$\delta_\mathrm{det}\f$, is automatically determined with the GetLightTravelTime() function from GWOLLUM. The calibration time uncertainty is noted \f$\delta_\mathrm{cal}\f$. An additional time delay \f$\delta_\mathrm{veto}\f$ can be provided. The slice width includes all correlated signals between the 2 detectors.
   *
   * A sequence of slices is constructed with the following conditions:
   * - Slices are contiguous.
   * - The number of slices is maximal given the size of the cross-correlation map.
   * - There must be an odd number of slices in a map: \f$N_s = 2n_s+1\f$.
   * - Slices in a map are indexed by \f$s\f$ running from \f$0\f$ to \f$N_s-1\f$. This is called the "slice index".
   * - The slice \f$s = n_s = (N_s-1)/2\f$ is centered at the center of the cross-correlation map.
   *
   * @note Some part of the map may be left unused at both edges of the cross-correlation map.
   *
   * The following image shows an example of slicing. There is \f$N_s=7\f$ slices. The slices are positioned symmetrically with respect to the center of the map.
   * @image html ox_slices.png "The cross-correlation map is sliced" width=700
   */
  void MakeSlices(void);

  
  ClassDef(OxEvent,0)
};


#endif


