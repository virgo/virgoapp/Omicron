/**
 * @file 
 * @brief Ox cross-correlation engine.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __OxCorr__
#define __OxCorr__
#include <FFT.h>
#include "OxOmicron.h"

using namespace std;

/**
 * @brief Ox cross-correlation engine.
 * @details This class is designed to compute the cross-correlation of Omicron spectrograms. The one-dimensional cross-correlation is performed in the Fourier domain along the time direction. 
 *
 * The Omicron analysis is managed by the OxOmicron class. Use it to compute the the spectrograms for both detectors. Then, call Transform() to Fourier-transform the spectrograms. Finally, the cross-correlation is computed with Process().
 */
class OxCorr: public OxOmicron, public GwollumPlot{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
  /**
   * @brief Constructor of the OxCorr class.
   * @details FFT plans are created. They cover the duration of the Omicron spectrograms excluding half the overlap at both sides.
   *
   * An option file must be provided to set the cross-correlation parameters:
   * - `OXC TIMERES` Time resolution [s] of the cross-correlation. The time resolution cannot be higher than the resolution of the \f$Q=0\f$ Omicron time-frequency plane. Set this parameter to a negative value to use the maximal time resolution (default).
   * - `OXC AMPLITUDE` Two values are expected to construct the amplitude spectrograms: \f$(A/A_0)^{\alpha}\f$. The first parameter, \f$\alpha\f$, is the power index applied to the spectrogram amplitudes. The second parameter, \f$A_0\f$, is used to normalize the amplitude to preserve the double-precision calculation. Default = 1.0 for both parameters.
   *
   * @param[in] aOptionFile Path to the option file.
   * @param[in] aGpsRef0 Reference GPS time to intiate the Omicron object for the first detector.
   * @param[in] aGpsRef1 Reference GPS time to intiate the Omicron object for the second detector.
   */
  OxCorr(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1);

  /**
   * @brief Destructor of the OxCorr class.
   */
  virtual ~OxCorr(void);

  /**
     @}
  */

  /**
   * @brief Transforms the Omicron amplitude spectrogram in the Fourier domain.
   * @details The Omicron time-frequency maps are resampled at the OxCorr time resolution and the Omicron overlap is removed at both sides of the map. Two maps are prepared:
   * - With a normalized amplitude: \f$(A/A_0)\f$
   * - With a power law: \f$(A/A_0)^{\alpha}\f$
   *
   * The resulting time-frequency maps are Fourier-transform along the time direction. This is done one frequency row at a time.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   */
  bool Transform(const unsigned int aDetectorIndex);

  /**
   * @brief Runs the cross-correlation analysis.
   * @details After the Omicron spectrograms have been transformed in the Fourier domain (Transform()), this function runs the cross-correlation analysis. The cross-correlation is performed in the Fourier domain. After Fourier-transforming back the data, the cross-correlation maps are obtained.
   * @warning After calling this function, the FFT structure for the second detector is used to store the cross-correlation coefficients \f$\xi\f$. It can no longer be used to compute new cross-correlation maps. This is not the case for the first detector: this function can be called again to process a new spectrogram of detector 1.
   */
  bool Process(void);

  /**
   * @brief Writes maps in the output directory as png files.
   * @details Omicron SNR spectrograms (not amplitude!) are printed as png files in the ouput directory: see OxOmicron::WriteMaps(). The cross-correlation maps (\f$xi\f$) are also printed as png files for each Q values.
   *
   * It is possible to draw a vertical line in the cross-correlation map at a given time-delay value. When the line is inside the map, a zoomed plot centered on the line is also printed. It includes markers to identified rejected frequency rows.
   *
   * This function also plots the amplitude variance as a function of the frequency for both detectors.
   * @param[in] aTimeDelay Time-delay value at which to draw a map.
   */
  bool WriteMaps(const double aTimeDelay=1.0e20);

  /**
   * @brief Returns the \f$\bar{\zeta}(Q,f,dt)\f$ coefficient.
   * @note The amplitude normalization implemented in Transform() is removed here.
   * @param[in] aQindex Q plane index \f$q\f$ (must be valid!).
   * @param[in] aFrequencyIndex Frequency row index \f$f\f$ (must be valid!).
   * @param[in] aTimeDelayIndex Time-delay index \f$n\f$ (must be valid!).
   */
  inline double GetZetaBar(const unsigned int aQindex, const unsigned int aFrequencyIndex, const unsigned int aTimeDelayIndex){
    return map_fft[0][1][aQindex][aFrequencyIndex]->GetRe_t(aTimeDelayIndex)*spec_norm*spec_norm;
  };

  /**
   * @brief Returns the cross-correlation coefficient \f$\xi(Q,f,dt)=\xi[\lambda][q][l][n]\f$.
   * @details This is the cross-correlation computed with the amplitude power law.
   * @param[in] aQindex Q plane index \f$q\f$ (must be valid!).
   * @param[in] aFrequencyIndex Frequency row index \f$f\f$ (must be valid!).
   * @param[in] aTimeDelayIndex Time-delay index \f$n\f$ (must be valid!).
   */
  inline double GetXi(const unsigned int aQindex, const unsigned int aFrequencyIndex, const unsigned int aTimeDelayIndex){
    return map_fft[1][1][aQindex][aFrequencyIndex]->GetRe_t(aTimeDelayIndex);
  };

  /**
   * @brief Returns the Omicron amplitude variance in a frequency row.
   * @details The frequency-row amplitude variance is computed in Transform().
   * @note The amplitude normalization implemented in Transform() is removed here. The original Omicron spectrogram is recovered.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector (must be valid!).
   * @param[in] aQindex Q plane index \f$q\f$ (must be valid!).
   * @param[in] aFrequencyIndex Frequency row index \f$f\f$ (must be valid!).
   */
  inline double GetAmplitudeVariance(const unsigned int aDetectorIndex, const unsigned int aQindex, const unsigned int aFrequencyIndex){
    return variance[aDetectorIndex][aQindex][aFrequencyIndex]*spec_norm*spec_norm;
  }

  /**
   * @brief Returns the number of time-delay bins \f$N\f$.
   */
  inline unsigned int GetTimeDelayBinsN(void){ return map_fft[0][0][0][0]->GetSize_t(); };

  /**
   * @brief Returns the time delay of a bin \f$n\f$ in the cross-correlation map [s].
   * @details The time delay for bin \f$n\f$ is given by:
   * \f[
   \delta t = \left (-n+\frac{N}{2}\right) \times \frac{\Delta_t}{N}\quad + (t_1-t_0),
   \f]
   * where \f$(t_1-t_0)\f$ is the time-shift between the two Omicron spectrograms.
   * @param[in] aTimeDelayBinIndex Time-delay bin index \f$n\f$ (starts at 0).
   */
  inline double GetTimeDelay(const unsigned int aTimeDelayBinIndex){
    return (double)OxOmicron::GetOmicronChunkStartTime(1) - (double)OxOmicron::GetOmicronChunkStartTime(0)
      +(double)OxOmicron::GetAnalysisDuration()/2.0 - (double)aTimeDelayBinIndex*(double)GetTimeDelayResolution();
  };

  /**
   * @brief Returns the time-delay resolution [s].
   * @details This is the size of one time-delay bin: \f$\Delta_t/N\f$.
   */
  inline double GetTimeDelayResolution(void){
    return (double)OxOmicron::GetAnalysisDuration()/(double)(GetTimeDelayBinsN());
  };

  /**
   * @brief Returns the time-delay bin index \f$n\f$ for a given time delay.
   * @details The time delay bin is given by:
   * \f[
   n = \lfloor \frac{N}{2} - \frac{N}{\Delta_t}\times(\delta t-t_1+t_0) \rfloor,
   \f]
   * where \f$(t_1-t_0)\f$ is the time-shift between the two Omicron spectrograms.
   * @param[in] aTimeDelay Time delay \f$\delta t\f$ between detector 1 and detector 0 [s].
   */
  inline unsigned int GetTimeDelayBinIndex(const double aTimeDelay){
    return (double)GetTimeDelayBinsN()/2.0 - (aTimeDelay - ((double)OxOmicron::GetOmicronChunkStartTime(1) - (double)OxOmicron::GetOmicronChunkStartTime(0)))/GetTimeDelayResolution();
  };

protected:

  TGraph **g_timedelay_valid;  ///< Graph to mark valid frequency rows: OxEvent::ComputeEvent().

  /**
   * @brief Generates an html table displaying the cross-correlation maps.
   * @details The cross-correlation maps for all Q values are displayed in a single row.
   * @warning The cross-correlation maps must be previously generated with WriteMaps(). The requested map times must match the time of generated cross-correlation maps.
   *
   * @param[in] aReport Output file stream.
   * @param[in] aMapTime_0 Central GPS time for the first detector.
   * @param[in] aMapTime_1 Central GPS time for the second detector.
   */  
  void MakeHtmlMapTable(ofstream &aReport, const unsigned int aMapTime_0, const unsigned int aMapTime_1);

  
private:

  fft ***map_fft[2][2];  ///< Map FFT plans: [zeta_bar, xi], [detector 0, detector 1].
  double **variance[2];  ///< Cross-correlation amplitude variance / Q plane / frequency band.
  double spec_power;     ///< Amplitude power index.
  double spec_norm;      ///< Amplitude normalization.

  /**
   * @brief Returns the Omicron tile index matching the time bin of OxCorr maps.
   * @param[in] aQindex Q plane index \f$q\f$ (must be valid!).
   * @param[in] aFrequencyIndex Frequency row index \f$f\f$ (must be valid!).
   * @param[in] aMapTimeIndex OxCorr time bin index.
   */  
  unsigned int GetOmicronTileIndex(const unsigned int aQindex, const unsigned int aFrequencyIndex, const unsigned int aMapTimeIndex);



  ClassDef(OxCorr,0)  
};


#endif


