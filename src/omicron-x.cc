/**
 * @file 
 * @brief Program to run an Omicron cross-correlation analysis.
 * @details TBC.
 * @snippet this omicron-x-usage
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxAnalysis.h"


using namespace std;

/**
 * @brief Prints the program usage message.
 */
void PrintUsage(void){
  //! [omicron-x-usage]
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"omicron-x [option file] ([timing])"<<endl; 
  cerr<<endl;
  cerr<<"[option file]"<<endl;
  cerr<<"Path to the Ox option file"<<endl;
  cerr<<endl;
  cerr<<"[timing]"<<endl;
  cerr<<"To generate Ox ouput files, a timing must be provided."<<endl;
  cerr<<"The timing is given either as 2 GPS times or 2 segment text files, one for each detector"<<endl;
  cerr<<"When the timing is not provided, Ox files are processed to produce results (plots and html report)"<<endl;
  cerr<<endl;
  cerr<<"See also: https://virgo.docs.ligo.org/virgoapp/Omicron/omicron-x_8cc.html#details"<<endl;
  cerr<<endl;
  //! [omicron-x-usage]
  return;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return OmicronPrintVersion();
  }

  // number of arguments
  if(argc<2){
    PrintUsage();
    return -1;
  }

  // get list of options
  Segments *Seg0 = NULL;
  Segments *Seg1 = NULL;
  string segmentfile1 = "";
  string segmentfile2 = "";
  string optionfile = "";
  unsigned int gps_ref_0 = 0;
  unsigned int gps_ref_1 = 0;

  // option file
  optionfile = (string)argv[1];
  if(filesystem::is_regular_file(optionfile)==false){
    cerr<<"The option file "<<optionfile<<" cannot be found"<<endl;
    return -2;
  }

  // timing
  if(argc>3){
    // [segment file1] [segment file2]
    if(filesystem::is_regular_file((string)argv[2])){
      Seg0 = new Segments((string)argv[2]);
      Seg1 = new Segments((string)argv[3]);
    }
    // [GPS start] [GPS stop]
    else{
      Seg0 = new Segments((unsigned int)floor(atof(argv[2])), (unsigned int)ceil(atof(argv[3])));
      Seg1 = new Segments((unsigned int)floor(atof(argv[2])), (unsigned int)ceil(atof(argv[3])));
    }
  
    // no segments
    if(Seg0->GetLiveTime(0)==0.0){
      cerr<<"No live time for detector 1"<<endl;
      return -2;
    }
    if(Seg1->GetLiveTime(0)==0.0){
      cerr<<"No live time for detector 2"<<endl;
      return -2;
    }

    gps_ref_0 = Seg0->GetFirst();
    gps_ref_1 = Seg1->GetFirst();
  }


  // run the analysis
  OxAnalysis *ox = new OxAnalysis(optionfile, gps_ref_0, gps_ref_1);

  // Cross-correlation processing
  if((Seg0!=NULL)&&(Seg1!=NULL)){
    if(ox->Process(Seg0, Seg1) == false) cerr<<"OxAnalysis::Process() failed"<<endl;
  }

  // Post-processing
  else{
    if(ox->PostProcess()==false) cerr<<"OxAnalysis::PostProcess() failed"<<endl;
  }
  
  // cleaning
  delete Seg0;
  delete Seg1;
  delete ox;
    
  return 0;
}
