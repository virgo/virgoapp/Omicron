/**
 * @file 
 * @brief See Omap.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Omap.h"

ClassImp(Omap)

////////////////////////////////////////////////////////////////////////////////////
Omap::Omap(const double aQ, const unsigned int aSampleFrequency,
           const double aFrequencyMin, const double aFrequencyMax,
           const unsigned int aTimeRange, const double aMaximumMismatch): q(aQ){ 
////////////////////////////////////////////////////////////////////////////////////

  // monitoring
  mon = new Monitor("omap");
  
  // adjust time range
  unsigned int trange = aTimeRange;
  if(trange<4){
    mon->SetMessageType(monitor_warning);
    *mon<<"Omap::"<<__func__<<": the time range must be at least 4s --> set it to 4s"<<endl;
    trange=4;
  }
  if(trange%2 == 1){
    trange++;
    mon->SetMessageType(monitor_warning);
    *mon<<"Omap::"<<__func__<<": the time range must be an even number of seconds --> set it to "<<trange<<endl;
  }

  // derived parameters
  double qprime = q / TMath::Sqrt(11.0);
  unsigned int NyquistFrequency    = aSampleFrequency/2;
  double MinimumAllowableFrequency = 4.0 * q / (2.0 * TMath::Pi() * trange);
  double MaximumAllowableFrequency = (double)NyquistFrequency/(1.0 + 1.0/qprime);
  
  // adjust frequency range
  double fmin = aFrequencyMin;
  double fmax = aFrequencyMax;
  if(fmin<MinimumAllowableFrequency){
    fmin = MinimumAllowableFrequency;
    mon->SetMessageType(monitor_warning);
    *mon<<"Omap::"<<__func__<<": the minimum frequency is too low --> set it to "<<fmin<<endl;
  }
  if(fmax>MaximumAllowableFrequency){
    fmax = MaximumAllowableFrequency;
    mon->SetMessageType(monitor_warning);
    *mon<<"Omap::"<<__func__<<": the maximum frequency is too high --> set it to "<<fmax<<endl;
  }
  if(fmax<=fmin){
    fmin=MinimumAllowableFrequency;
    fmax=MaximumAllowableFrequency;
    mon->SetMessageType(monitor_warning);
    *mon<<"Omap::"<<__func__<<": the frequency range is incorrect --> set it to "<<fmin<<" - "<<fmax<<endl;
  }

  // mismatch step
  double mismatchstep=2.0*TMath::Sqrt(aMaximumMismatch/3.0);

  // number of frequency bands
  double FrequencyCumulativeMismatch =
    TMath::Log(fmax/fmin)*TMath::Sqrt(2.0 + q*q) / 2.0;
  int Nf = (int)ceil(FrequencyCumulativeMismatch / mismatchstep);
  if(Nf<=0) Nf = 1;

  // force an even number of rows
  if(Nf%2==1) Nf+=1;
  
  // frequency bands (log)
  double FrequencyLogStep = TMath::Log(fmax/fmin) / (double)Nf;
  double *fbins = new double [Nf+1];
  for(int f=0; f<=Nf; f++) fbins[f] = fmin * TMath::Exp((double)f*FrequencyLogStep);
  
  // number of time bins
  double TimeCumulativeMismatch =
    (double)trange * 2.0*TMath::Pi() * TMath::Sqrt(fbins[Nf-1]*fbins[Nf]) / q;
  unsigned int Nt = NextPowerOfTwo(TimeCumulativeMismatch / mismatchstep);

  // time bins (linear)
  double *tbins = new double [Nt+1];
  for(unsigned int t=0; t<=Nt; t++) tbins[t] = -(double)trange/2.0 + (double)t/(double)Nt*(double)trange;

  // TF map
  tfmap = new TH2D("tfmap", "tfmap", Nt,tbins, Nf,fbins);
  delete [] fbins;
  delete [] tbins;
  gwl_ss<<"tfmap_"<<setprecision(5)<<fixed<<q;
  tfmap->SetTitle(gwl_ss.str().c_str());
  gwl_ss<<"_"<<gRandom->Integer(10000000);
  tfmap->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();
  tfmap->GetXaxis()->SetTitle("Time [s]");
  tfmap->GetYaxis()->SetTitle("Frequency [Hz]");
  tfmap->GetZaxis()->SetTitle("Tile content [?]");
  tfmap->GetXaxis()->SetNoExponent();
  tfmap->GetYaxis()->SetMoreLogLabels();
  tfmap->GetXaxis()->SetNdivisions(4,5,0);
  tfmap->GetXaxis()->SetLabelSize(0.045);
  tfmap->GetYaxis()->SetLabelSize(0.045);
  tfmap->GetZaxis()->SetLabelSize(0.045);
  tfmap->GetXaxis()->SetTitleSize(0.045);
  tfmap->GetYaxis()->SetTitleSize(0.045);
  tfmap->GetZaxis()->SetTitleSize(0.045);
  tfmap->GetYaxis()->SetTitleOffset(1.4);

  // band parameters
  bandMultiple = new unsigned int [GetBandN()];
  Ntiles=0;
  for(unsigned int f=0; f<GetBandN(); f++){
    TimeCumulativeMismatch = (double)trange * 2.0*TMath::Pi() * GetBandFrequency(f) / q;
    Nt = NextPowerOfTwo(TimeCumulativeMismatch / mismatchstep);
    bandMultiple[f] = (unsigned int)tfmap->GetNbinsX() / Nt;
    Ntiles+=(long unsigned int)Nt;
  }
}

////////////////////////////////////////////////////////////////////////////////////
Omap::~Omap(void){
////////////////////////////////////////////////////////////////////////////////////
  delete [] bandMultiple;
  delete tfmap;
  delete mon;
}

////////////////////////////////////////////////////////////////////////////////////
double* Omap::GetBands(void){
////////////////////////////////////////////////////////////////////////////////////

  double *bins = new double [tfmap->GetNbinsY()+1];
  for(unsigned int b=0; b<(unsigned int)tfmap->GetNbinsY(); b++) 
    bins[b] = tfmap->GetYaxis()->GetBinLowEdge(b+1);
  bins[tfmap->GetNbinsY()] = tfmap->GetYaxis()->GetBinUpEdge(tfmap->GetNbinsY());
  return bins;
}

////////////////////////////////////////////////////////////////////////////////////
long unsigned int Omap::GetTileN(const double aPadding){
////////////////////////////////////////////////////////////////////////////////////

  if(aPadding==0.0) return GetTileN();

  long unsigned int nt = 0;
  for(unsigned int f=0; f<GetBandN(); f++){
    nt+=GetTimeTileIndex(f, GetTimeMax()-aPadding)-GetTimeTileIndex(f, GetTimeMin()+aPadding)+1;
  }
  
  return nt;  
}

////////////////////////////////////////////////////////////////////////////////////
void Omap::PrintParameters(void){
////////////////////////////////////////////////////////////////////////////////////
  cout<<"Oqplane::PrintParameters:"<<endl;
  cout<<"\t- Q                         = "<<GetQ()<<endl;
  cout<<"\t- Time range                = "<<GetTimeRange()<<" s"<<endl;
  cout<<"\t- Time resolution           = "<<GetTileDuration(GetBandN()-1)<<" s - "<<GetTileDuration(0)<<" s"<<endl;
  cout<<"\t- Frequency range           = "<<GetFrequencyMin()<<"-"<<GetFrequencyMax()<<" Hz"<<endl;
  cout<<"\t- Number of frequency bands = "<<GetBandN()<<endl;
  cout<<"\t- Frequency resolution      = "<<GetBandWidth(0)<<" Hz - "<<GetBandWidth(GetBandN()-1)<<" Hz"<<endl;
  cout<<"\t- Number of tiles           = "<<GetTileN()<<endl;
  cout<<"\t- Number of bins (internal) = "<<tfmap->GetNbinsX()*GetBandN()<<endl;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void MapApplyOffset(TH2D *aMap, const double aOffset){
////////////////////////////////////////////////////////////////////////////////////

  // get X bins
  TArrayD X(*(aMap->GetXaxis()->GetXbins()));

  // apply offset
  for(int i = 0; i<X.GetSize(); i++) X[i] += aOffset;

  // reset bins
  aMap->GetXaxis()->Set((X.GetSize() - 1), X.GetArray());
  
  return;
}


