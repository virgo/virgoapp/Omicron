/**
 * @file 
 * @brief See OxResults.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxResults.h"

ClassImp(OxResults)

////////////////////////////////////////////////////////////////////////////////////
OxResults::OxResults(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1):
OxSim(aOptionFile, aGpsRef0, aGpsRef1){
////////////////////////////////////////////////////////////////////////////////////

  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxResults::"<<__func__<<endl;

  // analysis time
  for(unsigned int r=0; r<oxresult_n; r++){
    livetime[r] = 0;
    segments[0][r] = new Segments();
    segments[1][r] = new Segments();
  }

  // list of options
  OxInit::DefineOption("OXR", "FAKEGROUND", (int)-1, 2);
  OxInit::DefineOption("OXR", "DETECTIONTHRESHOLD", 0.5, 2);
  OxInit::DefineOption("OXR", "NLOUDEST", (unsigned int)5, oxresult_n);
  OxInit::DefineOption("OXR", "NLOUDESTMAP", (unsigned int)0, oxresult_n);
  OxInit::DefineOption("OXR", "NEVENTBINS", (unsigned int)100, 1);

  // add options from the text file
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxResults::"<<__func__<<": add OXR options"<<endl;
  OxInit::AddOptions();

  // read results in the input Ox files
  // note: local histograms are created if results are found: CreateHistograms()
  // note: local arrays are created if results are found: CreateArrays()
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxResults::"<<__func__<<": extract results from Ox files"<<endl;
  results_found = new bool[OxInit::GetOxFileN()];
  bool found = ExtractResults();

  // there is no results in the input Ox files
  if(found==false){

    // overload some of the options (from text file)
    OxInit::OverloadOption("OXR", "FAKEGROUND");
    OxInit::OverloadOption("OXR", "DETECTIONTHRESHOLD");
    OxInit::OverloadOption("OXR", "NLOUDEST");
    OxInit::OverloadOption("OXR", "NLOUDESTMAP");
    OxInit::OverloadOption("OXR", "NEVENTBINS");

    // local histograms
    CreateHistograms();

    // local arrays
    CreateArrays();
    
    // fakeground definition
    if(ox_opt->GetOptionValueI("OXR", "FAKEGROUND", 0)<0){
      // random draw
      DrawFakeground();
      // save new fakeground definition in the options
      ox_opt->SetOptionValue("OXR", "FAKEGROUND", 0, (int)fake_slice_index);
      ox_opt->SetOptionValue("OXR", "FAKEGROUND", 1, fake_lag);
    }
  }

  // save fake slice
  fake_slice_index = (unsigned int)ox_opt->GetOptionValueI("OXR", "FAKEGROUND", 0);
  fake_lag = ox_opt->GetOptionValueI("OXR", "FAKEGROUND", 1);
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxResults::"<<__func__<<": fakeground slice index = "<<fake_slice_index<<", lag = "<<fake_lag<<" s"<<endl;

  // output
  outfilename = "";
  oxr_tree = NULL;

  // fill option tree
  ox_opt->FillTree();
}

////////////////////////////////////////////////////////////////////////////////////
OxResults::~OxResults(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxResults::"<<__func__<<endl;
  delete [] results_found;
  for(unsigned int r=0; r<oxresult_n; r++){
    delete h1_ev_rank[r];
    delete h1_ev_freq[r];
    delete h2_ev_rank_freq[r];
    delete segments[0][r];
    delete segments[1][r];
    delete ev_loudest_entry[r];
    delete ev_loudest_rank[r];
  }
}

////////////////////////////////////////////////////////////////////////////////////
bool OxResults::Process(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxResults::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxResults::"<<__func__<<": the OxResults object is corrupted"<<endl;
    return false;
  }

  if(!outfilename.compare("")){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxResults::"<<__func__<<": an output file must be created first"<<endl;
    return false;
  }

  // locals
  OxResultType restype;
  unsigned int progress_percentage = 0;

  // loop over events
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxResults::"<<__func__<<": read events"<<endl;
  for(Long64_t e=0; e<OxEvent::GetEntries(); e++){
    if(OxEvent::GetEntry(e)<=0) continue;

    // analysis progress
    if((e*100) / OxEvent::GetEntries() > progress_percentage){
      progress_percentage = (e*100) / OxEvent::GetEntries();
      monit->Monitor::SetMessageType(monitor_status);
      *monit<<"OxResults::"<<__func__<<": fraction of processed events = "<<e<<"/"<<OxEvent::GetEntries()<<" = "<<progress_percentage<<"%"<<endl;
    }
       
    // do not process events from files with results
    if(results_found[OxEvent::GetTreeNumber()]==true) continue;

    // result type
    restype = GetEventResultType();

    // save time segments
    segments[0][restype]->AddSegment(OxOmicron::GetOmicronStartTime(0), OxOmicron::GetOmicronEndTime(0));
    segments[1][restype]->AddSegment(OxOmicron::GetOmicronStartTime(1), OxOmicron::GetOmicronEndTime(1));
    livetime[restype] += (unsigned long int)OxOmicron::GetAnalysisDuration();

    // fill histograms
    FillHistograms(restype);

    // save loudest events
    SaveLoudest(restype, e, OxEvent::GetEventRank());

    // match injections in the foreground
    if(restype==oxresult_foreground) OxSim::Match();
  }
  monit->Monitor::SetMessageType(monitor_status);
  *monit<<"OxResults::"<<__func__<<": fraction of processed events = "<<OxEvent::GetEntries()<<"/"<<OxEvent::GetEntries()<<" = 100%"<<endl;

  // fill result tree
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxResults::"<<__func__<<": fill result tree"<<endl;
  TArrayL *loudest_entry_tmp = NULL;
  for(unsigned int r=0; r<oxresult_n; r++){

    oxr_livetime = livetime[r];
    oxr_ev_rank = h1_ev_rank[r];
    oxr_ev_freq = h1_ev_freq[r];
    oxr_ev_rank_freq = h2_ev_rank_freq[r];

    // save loudest events in the output event tree
    SaveLoudestTree((OxResultType)r);

    // loudest event entry: re-map the OxEvent tree with the latest entries (see SaveLoudestTree())
    loudest_entry_tmp = new TArrayL(ev_loudest_entry[r]->GetSize());
    for(int l=0; l<loudest_entry_tmp->GetSize(); l++){
      loudest_entry_tmp->AddAt(OxEvent::GetEntriesOut()-loudest_entry_tmp->GetSize()+l, l);
    }
  
    oxr_ev_loudest_entry = loudest_entry_tmp;
    oxr_ev_loudest_rank = ev_loudest_rank[r];    
    oxr_tree->Fill();
    delete loudest_entry_tmp;
  }
  
  // save segment trees (attached to the output file)
  TTree *seg_tree;
  for(unsigned int r=0; r<oxresult_n; r++){
    seg_tree = segments[0][r]->GetTree();
    seg_tree->SetName(("seg_0_"+OxResultTypeName[r]).c_str());
    OxInit::AttachTree(seg_tree);
    seg_tree = segments[1][r]->GetTree();
    seg_tree->SetName(("seg_1_"+OxResultTypeName[r]).c_str());
    OxInit::AttachTree(seg_tree);
  }

  // detect injections and make associated histograms
  // CHECKME: the memory of injection history is lost when working with Ox results --> possible bugs in corner cases
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxResults::"<<__func__<<": detect injections"<<endl;
  OxSim::Process(GetRankDetectionThreshold());

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
double OxResults::RankToRate(OxResultType aResultType, const double aRank, const int aType){
////////////////////////////////////////////////////////////////////////////////////
  
  // no livetime
  if(livetime[aResultType]==0) return 0.0;

  // find bin with that rank
  int bin = h1_ev_rank[aResultType]->GetXaxis()->FindBin(aRank);

  // number of events in that bin
  double n_event = h1_ev_rank[aResultType]->GetBinContent(bin);

  // check for the range of the histogram
  if(bin==0){// underflow
    if(aType==0) return n_event/(double)livetime[aResultType];
    else if(aType>0) return h1_ev_rank[aResultType]->GetEntries()/(double)livetime[aResultType];
    else return 0.0;
  }
  if(bin==h1_ev_rank[aResultType]->GetNbinsX()+1){// overflow
    if(aType==0) return n_event/(double)livetime[aResultType];
    else if(aType<0) return h1_ev_rank[aResultType]->GetEntries()/(double)livetime[aResultType];
    else return 0.0;
  }

  // now, aRank is inside the histogram limits
  
  // rate above that rank
  if(aType>0){

    // fraction of bin content
    n_event *= (h1_ev_rank[aResultType]->GetXaxis()->GetBinUpEdge(bin) - aRank) / (h1_ev_rank[aResultType]->GetXaxis()->GetBinUpEdge(bin) - h1_ev_rank[aResultType]->GetXaxis()->GetBinLowEdge(bin));

    // integrate forward
    for(int b=bin+1; b<=h1_ev_rank[aResultType]->GetNbinsX()+1; b++) n_event += h1_ev_rank[aResultType]->GetBinContent(b);
  }

  // rate below that rank
  if(aType<0){

    // fraction of bin content
    n_event *= (aRank - h1_ev_rank[aResultType]->GetXaxis()->GetBinLowEdge(bin)) / (h1_ev_rank[aResultType]->GetXaxis()->GetBinUpEdge(bin) - h1_ev_rank[aResultType]->GetXaxis()->GetBinLowEdge(bin));
    
    // integrate forward
    for(int b=bin-1; b>=0; b--) n_event += h1_ev_rank[aResultType]->GetBinContent(b);
  }
  
  return n_event/(double)livetime[aResultType];
}

////////////////////////////////////////////////////////////////////////////////////
double OxResults::RateToRank(OxResultType aResultType, const double aRate){
////////////////////////////////////////////////////////////////////////////////////
  
  // no livetime: use the highest possible rate value
  if(livetime[aResultType]==0) return h1_ev_rank[aResultType]->GetXaxis()->GetBinUpEdge(h1_ev_rank[aResultType]->GetNbinsX());
  
  // number of events
  double n_event = aRate*(double)livetime[aResultType];

  // get minimum number of events
  double n_event_min = 0.0;
  int b;
  for(b=h1_ev_rank[aResultType]->GetNbinsX()+1; b>=0; b--){
    n_event_min = h1_ev_rank[aResultType]->GetBinContent(b);
    if(n_event_min>0.0) break;
  }

  // the requested rate is too low and cannot be achieved
  // --> use the highest known rank value (approximated by the bin center)
  if(n_event<=n_event_min) return h1_ev_rank[aResultType]->GetXaxis()->GetBinCenter(b);
  
  // use cumulative distribution
  double rank = h1_ev_rank[aResultType]->GetXaxis()->GetBinLowEdge(1);// min achievable rank value
  double n_event_cum = h1_ev_rank[aResultType]->GetBinContent(b);
  b--;
  for(; b>=0; b--){

    // cumulative number in current bin
    n_event_cum += h1_ev_rank[aResultType]->GetBinContent(b);

    // log-interpolate between b and b+1
    if(n_event_cum>n_event){
      rank = h1_ev_rank[aResultType]->GetXaxis()->GetBinCenter(b) // starting point
        + (h1_ev_rank[aResultType]->GetXaxis()->GetBinCenter(b+1)-h1_ev_rank[aResultType]->GetXaxis()->GetBinCenter(b))/(-h1_ev_rank[aResultType]->GetBinContent(b)) // slope
        * (n_event-n_event_cum); // stride
      break;
    }
  }
  
  return rank;
}

////////////////////////////////////////////////////////////////////////////////////
OxResultType OxResults::GetEventResultType(void){
////////////////////////////////////////////////////////////////////////////////////
  
  // foreground
  if(OxEvent::GetEventSliceIndex()==OxEvent::GetSliceIndexCenter()){
    if(OxOmicron::GetOmicronStartTime(0) == OxOmicron::GetOmicronStartTime(1))
      return oxresult_foreground;
  }

  // fakeground
  if(OxEvent::GetEventSliceIndex()==fake_slice_index){
    if((int)OxOmicron::GetOmicronStartTime(1)-(int)OxOmicron::GetOmicronStartTime(0)==fake_lag)
      return oxresult_fakeground;
  }  

  // background
  return oxresult_background;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxResults::CreateOutputFile(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxResults::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxResults::"<<__func__<<": the OxResults object is corrupted"<<endl;
    return false;
  }
  
  // create output file
  if(OxSim::CreateOutputFile(outfilename, "results", 0)==false) return false;

  // create result tree
  oxr_ev_rank = 0;
  oxr_ev_freq = 0;
  oxr_ev_rank_freq = 0;
  oxr_ev_loudest_entry = 0;
  oxr_ev_loudest_rank = 0;
  oxr_tree = new TTree("oxr", "oxr");
  oxr_tree->Branch("oxr_livetime", &oxr_livetime, "oxr_livetime/l");
  oxr_tree->Branch("oxr_ev_rank", "TH1D", &oxr_ev_rank);
  oxr_tree->Branch("oxr_ev_freq", "TH1D", &oxr_ev_freq);
  oxr_tree->Branch("oxr_ev_rank_freq", "TH2D", &oxr_ev_rank_freq);
  oxr_tree->Branch("oxr_ev_loudest_entry", "TArrayL", &oxr_ev_loudest_entry);
  oxr_tree->Branch("oxr_ev_loudest_rank", "TArrayD", &oxr_ev_loudest_rank);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxResults::ExtractResults(void){
////////////////////////////////////////////////////////////////////////////////////

  // create result chain
  TChain *oxr_chain = new TChain("oxr");

  // load oxr trees and identify result files
  bool found = false;
  gErrorIgnoreLevel = kBreak;// not to print warning about files w/o oxr trees
  for(unsigned int f=0; f<OxInit::GetOxFileN(); f++){
    if(oxr_chain->AddFile(OxInit::GetOxFileName(f).c_str(), 0)==1){
      results_found[f] = true;
      found = true;
    }
    else results_found[f] = false;
  }

  // no results anywhere: exit
  if(found==false){
    if(OxInit::GetOxFileN()>0){
      monit->Monitor::SetMessageType(monitor_warning);
      *monit<<"OxResults::"<<__func__<<": no Ox results in the input Ox files"<<endl;
    }
    delete oxr_chain;
    return false;
  }

  // number of entries (fore/fake/back) mismatch
  if((oxr_chain->GetEntries()%oxresult_n)!=0){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxResults::"<<__func__<<": corrupted oxr tree --> do not use"<<endl;
    delete oxr_chain;
    return false;
  }

  // attach branches
  TH1D *oxr_ev_rank_read = 0;
  TH1D *oxr_ev_freq_read = 0;
  TH2D *oxr_ev_rank_freq_read = 0;
  TArrayL *oxr_ev_loudest_entry_read = 0;
  TArrayD *oxr_ev_loudest_rank_read = 0;
  unsigned long int oxr_livetime_read = 0;
  oxr_chain->SetBranchAddress("oxr_livetime", &oxr_livetime_read);
  oxr_chain->SetBranchAddress("oxr_ev_rank", &oxr_ev_rank_read);
  oxr_chain->SetBranchAddress("oxr_ev_freq", &oxr_ev_freq_read);
  oxr_chain->SetBranchAddress("oxr_ev_rank_freq", &oxr_ev_rank_freq_read);
  oxr_chain->SetBranchAddress("oxr_ev_loudest_entry", &oxr_ev_loudest_entry_read);
  oxr_chain->SetBranchAddress("oxr_ev_loudest_rank", &oxr_ev_loudest_rank_read);
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxResults::"<<__func__<<": results are found in "<<oxr_chain->GetNtrees()<<" input Ox files"<<endl;

  // we have sane results in the input files --> create local histograms
  CreateHistograms();

  // we have sane results in the input files --> create local histograms
  CreateArrays();

  // read oxr
  for(Long64_t e=0; e<oxr_chain->GetEntries(); e++){
    if(oxr_chain->GetEntry(e)<=0) continue;

    // cumulate livetime
    livetime[e%oxresult_n] += oxr_livetime_read;

    // cumulate histograms
    monit->UpdateStatus(h1_ev_rank[e%oxresult_n]->Add(oxr_ev_rank_read));
    monit->UpdateStatus(h1_ev_freq[e%oxresult_n]->Add(oxr_ev_freq_read));
    monit->UpdateStatus(h2_ev_rank_freq[e%oxresult_n]->Add(oxr_ev_rank_freq_read));

    // save loudest events
    for(int l=0; l<oxr_ev_loudest_entry_read->GetSize(); l++){
      SaveLoudest((OxResultType)(e%oxresult_n), oxr_ev_loudest_entry_read->At(l), oxr_ev_loudest_rank_read->At(l));
    }

  }
  delete oxr_chain;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxResults::"<<__func__<<": number of Ox events (fore/back/fake) = "<<h1_ev_rank[oxresult_foreground]->GetEntries()<<"/"<<h1_ev_rank[oxresult_background]->GetEntries()<<"/"<<h1_ev_rank[oxresult_fakeground]->GetEntries()<<endl;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxResults::"<<__func__<<": analysis livetime [s] (fore/back/fake) = "<<livetime[oxresult_foreground]<<"/"<<livetime[oxresult_background]<<"/"<<livetime[oxresult_fakeground]<<endl;

  // read analysis segments
  Segments *seg;
  for(unsigned int f=0; f<OxInit::GetOxFileN(); f++){

    // no results --> skip
    if(results_found[f]==false) continue;

    // read and cumulate segments
    for(unsigned int t=0; t<oxresult_n; t++){
      seg = new Segments(OxInit::GetOxFileName(f), "", "seg_0_"+OxResultTypeName[t]);
      monit->UpdateStatus(segments[0][t]->AddSegments(seg));
      delete seg;
      seg = new Segments(OxInit::GetOxFileName(f), "", "seg_1_"+OxResultTypeName[t]);
      monit->UpdateStatus(segments[1][t]->AddSegments(seg));
      delete seg;
    }
  }
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxResults::"<<__func__<<": analysis segment livetime for detector 0 [s] (fore/back/fake) = "<<segments[0][oxresult_foreground]->GetLiveTime()<<"/"<<segments[0][oxresult_background]->GetLiveTime()<<"/"<<segments[0][oxresult_fakeground]->GetLiveTime()<<endl;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxResults::"<<__func__<<": analysis segment livetime for detector 1 [s] (fore/back/fake) = "<<segments[1][oxresult_foreground]->GetLiveTime()<<"/"<<segments[1][oxresult_background]->GetLiveTime()<<"/"<<segments[1][oxresult_fakeground]->GetLiveTime()<<endl;

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void OxResults::SaveLoudest(const OxResultType aResultType, const Long64_t aEntry, const double aRank){
////////////////////////////////////////////////////////////////////////////////////

  unsigned int asize = ev_loudest_rank[aResultType]->GetSize();

  // no loudest events
  if(asize==0) return;

  // the event rank is too low to be saved
  if(aRank<ev_loudest_rank[aResultType]->At(asize-1)) return;

  // loop over event in the array
  for(unsigned int l=0; l<asize; l++){

    // the new event is louder than this event
    if(aRank>=ev_loudest_rank[aResultType]->At(l)){

      // shift events toward the end
      for(unsigned int ll=asize-1; ll>l; ll--){
        ev_loudest_rank[aResultType]->AddAt(ev_loudest_rank[aResultType]->At(ll-1), ll);
        ev_loudest_entry[aResultType]->AddAt(ev_loudest_entry[aResultType]->At(ll-1), ll);
      }

      // insert new event
      ev_loudest_rank[aResultType]->AddAt(aRank, l);
      ev_loudest_entry[aResultType]->AddAt(aEntry, l);
      break;
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxResults::SaveLoudestTree(const OxResultType aResultType){
////////////////////////////////////////////////////////////////////////////////////

  // loop over events in the list
  for(int l=0; l<ev_loudest_entry[aResultType]->GetSize(); l++){
    
    // load loudest event
    if(OxEvent::GetEntry(ev_loudest_entry[aResultType]->At(l))<=0) break;      
    
    // save event in the output tree
    OxOmicron::FillTree(0);
    OxOmicron::FillTree(1);
    OxEvent::FillTree();
  }
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxResults::CreateArrays(void){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int r=0; r<oxresult_n; r++){
    ev_loudest_entry[r] = new TArrayL(ox_opt->GetOptionValueU("OXR", "NLOUDEST", r));
    ev_loudest_rank[r] = new TArrayD(ox_opt->GetOptionValueU("OXR", "NLOUDEST", r));
    ev_loudest_entry[r]->Reset(-1);
    ev_loudest_rank[r]->Reset(-99.0);
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxResults::FillHistograms(const OxResultType aResultType){
////////////////////////////////////////////////////////////////////////////////////

  // event parameters
  double ev_rank = OxEvent::GetEventRank();
  double ev_freq = OxEvent::GetEventFrequency();

  // fill histograms
  h1_ev_rank[aResultType]->Fill(ev_rank);
  h1_ev_freq[aResultType]->Fill(ev_freq);
  h2_ev_rank_freq[aResultType]->Fill(ev_freq, ev_rank);

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxResults::CreateHistograms(void){
////////////////////////////////////////////////////////////////////////////////////

  // number of bins
  unsigned int nbins = ox_opt->GetOptionValueU("OXR", "NEVENTBINS", 0);
  if(nbins==0) nbins = 1;

  // log bins
  double *freq_bins = new double [nbins+1];
  for(unsigned int b=0; b<nbins+1; b++){
    freq_bins[b] = OxOmicron::omicron[0]->GetTileFrequencyMin()*pow(10.0, (double)b*log10(1.1*OxOmicron::omicron[0]->GetTileFrequencyMax()/OxOmicron::omicron[0]->GetTileFrequencyMin())/(double)nbins);
  }

  // create histograms (attached to the output file)
  for(unsigned int r=0; r<oxresult_n; r++){

    // event rank (max ~ snr=50)
    h1_ev_rank[r] = new TH1D(("h1_ev_rank_"+OxResultTypeName[r]).c_str(), OxResultTypeName[r].c_str(), nbins, 0.0, 1.0);
    h1_ev_rank[r]->GetXaxis()->SetTitle("Event rank R");
    h1_ev_rank[r]->GetYaxis()->SetTitle("Number of events");
    h1_ev_rank[r]->Sumw2(true);

    // event frequency
    h1_ev_freq[r] = new TH1D(("h1_ev_freq_"+OxResultTypeName[r]).c_str(), OxResultTypeName[r].c_str(), nbins, freq_bins);
    h1_ev_freq[r]->GetXaxis()->SetTitle("Event frequency [Hz]");
    h1_ev_freq[r]->GetYaxis()->SetTitle("Number of events");
    h1_ev_freq[r]->Sumw2(true);

    // event rank/frequency
    h2_ev_rank_freq[r] = new TH2D(("h2_ev_rank_freq_"+OxResultTypeName[r]).c_str(), OxResultTypeName[r].c_str(), nbins, freq_bins, nbins, 0.0, 1.0);
    h2_ev_rank_freq[r]->GetXaxis()->SetTitle("Event frequency [Hz]");
    h2_ev_rank_freq[r]->GetYaxis()->SetTitle("Event rank R");
    h2_ev_rank_freq[r]->GetZaxis()->SetTitle("Number of events");
    h2_ev_rank_freq[r]->Sumw2(true);

  }

  // special color for the foreground
  h1_ev_rank[oxresult_foreground]->SetLineColor(GwollumPlot::GetColorPalette(GwollumPlot::GetNumberOfColors()-1));
  h1_ev_freq[oxresult_foreground]->SetLineColor(GwollumPlot::GetColorPalette(GwollumPlot::GetNumberOfColors()-1));
  h2_ev_rank_freq[oxresult_foreground]->SetLineColor(GwollumPlot::GetColorPalette(GwollumPlot::GetNumberOfColors()-1));
  h1_ev_rank[oxresult_foreground]->SetMarkerColor(GwollumPlot::GetColorPalette(GwollumPlot::GetNumberOfColors()-1));
  h1_ev_freq[oxresult_foreground]->SetMarkerColor(GwollumPlot::GetColorPalette(GwollumPlot::GetNumberOfColors()-1));
  h2_ev_rank_freq[oxresult_foreground]->SetMarkerColor(GwollumPlot::GetColorPalette(GwollumPlot::GetNumberOfColors()-1));

  // special markers for the fore/fakeground
  h1_ev_rank[oxresult_foreground]->SetMarkerStyle(4);
  h1_ev_freq[oxresult_foreground]->SetMarkerStyle(4);
  h2_ev_rank_freq[oxresult_foreground]->SetMarkerStyle(4);
  h1_ev_rank[oxresult_fakeground]->SetMarkerStyle(25);
  h1_ev_freq[oxresult_fakeground]->SetMarkerStyle(25);
  h2_ev_rank_freq[oxresult_fakeground]->SetMarkerStyle(25);

  delete [] freq_bins;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxResults::DrawFakeground(void){
////////////////////////////////////////////////////////////////////////////////////

  // foreground slice index
  unsigned int fore_slice_index = OxEvent::GetSliceIndexCenter();

  // init with foreground event
  fake_lag = 0;
  fake_slice_index = fore_slice_index;

  // no events --> random slice index
  if(OxEvent::GetEntries()==0){
    while(fake_slice_index==fore_slice_index){
      fake_slice_index = (unsigned int)gRandom->Integer(OxEvent::GetSliceN());
    }
    return;
  }

  // max entry number
  int int_max = (int)TMath::Min(OxEvent::GetEntries(), (Long64_t)1000000000);

  // loop as long the foreground is found
  while((fake_slice_index==fore_slice_index)&&(fake_lag==0)){

    // random event
    int random_entry = gRandom->Integer(int_max);
    if(OxEvent::GetEntry(random_entry)<=0) continue;

    // lag
    fake_lag = (int)OxOmicron::GetOmicronStartTime(1) - (int)OxOmicron::GetOmicronStartTime(0);
    
    // random slice index
    fake_slice_index = (unsigned int)gRandom->Integer(OxEvent::GetSliceN());
  }

  return;
}

