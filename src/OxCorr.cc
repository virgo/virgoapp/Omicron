/**
 * @file 
 * @brief See OxCorr.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxCorr.h"

ClassImp(OxCorr)

////////////////////////////////////////////////////////////////////////////////////
OxCorr::OxCorr(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1): OxOmicron(aOptionFile, aGpsRef0, aGpsRef1),
  GwollumPlot("ox", "OX"){
////////////////////////////////////////////////////////////////////////////////////

  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxCorr::"<<__func__<<endl;

  // gwollum plots
  GwollumPlot::ResizePlot(2*OxOmicron::omicron[0]->GetOptionValueU("OUTPUT", "PLOTDIMENSIONS", 0),
                          2*OxOmicron::omicron[0]->GetOptionValueU("OUTPUT", "PLOTDIMENSIONS", 1));
  GwollumPlot::SetGridx(1);
  GwollumPlot::SetGridy(1);

  // list of options
  OxInit::DefineOption("OXC", "TIMERES", -1.0, 1);
  OxInit::DefineOption("OXC", "AMPLITUDE", 1.0, 2);

  // add options from the text file
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxCorr::"<<__func__<<": add OXC options"<<endl;
  OxInit::AddOptions();

  // overload some of the options (from text file)
  // OXC/TIMERES keep the TTree value
  // OXC/AMPLITUDE keep the TTree value

  // to go faster
  spec_power = ox_opt->GetOptionValueD("OXC", "AMPLITUDE", 0);
  spec_norm = ox_opt->GetOptionValueD("OXC", "AMPLITUDE", 1);

  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxCorr::"<<__func__<<": create FFT plans"<<endl;

  // time resolution
  double tres = ox_opt->GetOptionValueD("OXC", "TIMERES", 0);
  double tres_min = (double)(OxOmicron::omicron[0]->GetChunkDuration()) / (double)(OxOmicron::omicron[0]->GetBandTileN(0, OxOmicron::omicron[0]->GetBandN(0)-1));
  if(tres<tres_min) tres = tres_min;

  // number of FFT bins
  unsigned int fft_n = (unsigned int)TMath::Ceil((double)OxOmicron::GetAnalysisDuration()/tres);
  fft_n += (fft_n%2); // to make sure it is an even number!

  // FFT plans
  for(unsigned int c=0; c<2; c++){
    for(unsigned int d=0; d<2; d++){
      map_fft[c][d] = new fft** [OxOmicron::omicron[0]->GetQN()];
      for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
        map_fft[c][d][q] = new fft* [OxOmicron::omicron[0]->GetBandN(q)];
        for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){
          map_fft[c][d][q][f] = new fft(fft_n, "FFTW_MEASURE", "r2c");
        }
      }
    }
  }

  // variance function (for zeta_bar)
  for(unsigned int d=0; d<2; d++){
    variance[d] = new double* [OxOmicron::omicron[0]->GetQN()];
    for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
      variance[d][q] = new double [OxOmicron::omicron[0]->GetBandN(q)];
      for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){
        variance[d][q][f] = 0.0;
      }
    }
  }
  
  // fill option tree
  ox_opt->FillTree();

  // TGraph for event display
  g_timedelay_valid = new TGraph* [OxOmicron::omicron[0]->GetQN()];
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
    gwl_ss<<q;
    g_timedelay_valid[q] = new TGraph(OxOmicron::omicron[0]->GetBandN(q));
    g_timedelay_valid[q]->SetName(("g_timedelay_valid_"+gwl_ss.str()).c_str());
    g_timedelay_valid[q]->SetMarkerStyle(4);
    g_timedelay_valid[q]->SetMarkerColor(2);
    gwl_ss.clear(); gwl_ss.str("");
  }

  // print info
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxCorr::"<<__func__<<": number of time-delay bins = "<<GetTimeDelayBinsN()<<endl;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxCorr::"<<__func__<<": time-delay resolution = "<<GetTimeDelayResolution()<<endl;

}

////////////////////////////////////////////////////////////////////////////////////
OxCorr::~OxCorr(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxCorr::"<<__func__<<endl;
  for(unsigned int d=0; d<2; d++){
    for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
      delete [] variance[d][q];
      for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){
        delete map_fft[0][d][q][f];
        delete map_fft[1][d][q][f];
      }
      delete [] map_fft[0][d][q];
      delete [] map_fft[1][d][q];
    }
    delete [] map_fft[0][d];
    delete [] map_fft[1][d];
    delete [] variance[d];
  }
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
    delete g_timedelay_valid[q];
  }
  delete [] g_timedelay_valid;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxCorr::Transform(const unsigned int aDetectorIndex){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxCorr::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxCorr::"<<__func__<<": the OxCorr object is corrupted"<<endl;
    return false;
  }
  
  unsigned int d = aDetectorIndex%2;

  // locals
  double n_snr = 0.0;
  double mean_snr = 0.0;
  double mean;
  double mean_powerlaw;
  double variance_powerlaw;
  double tilevalue;
  double tilesnr;

  OxOmicron::oxo_snrvar[d] = 0.0;

  // loop over Q planes
  for(unsigned int q=0; q<OxOmicron::omicron[d]->GetQN(); q++){
    
    // loop over frequency rows
    for(unsigned int f=0; f<OxOmicron::omicron[d]->GetBandN(q); f++){
      
      // init mean/variance over the frequency row
      mean = 0.0;
      mean_powerlaw = 0.0;
      variance[d][q][f] = 0.0;
      variance_powerlaw = 0.0;
      
      // fill time-domain vectors
      for(unsigned int t=0; t<GetTimeDelayBinsN(); t++){

        // calculate the SNR variance
        tilesnr = TMath::Sqrt(OxOmicron::omicron[d]->GetTileSnrSq(q, f, GetOmicronTileIndex(q, f, t)));
        mean_snr += tilesnr;
        OxOmicron::oxo_snrvar[d] += tilesnr*tilesnr;
        n_snr += 1.0;

        // get amplitude
        tilevalue = OxOmicron::omicron[d]->GetTileAmplitude(q, f, GetOmicronTileIndex(q, f, t))/spec_norm;

        // amplitude xcorr
        mean += tilevalue;
	variance[d][q][f] += tilevalue*tilevalue;
        map_fft[0][d][q][f]->SetRe_t(t, tilevalue);

        // amplitude power law xcorr
        tilevalue = TMath::Power(tilevalue, spec_power);
	mean_powerlaw += tilevalue;
 	variance_powerlaw += tilevalue*tilevalue;
        map_fft[1][d][q][f]->SetRe_t(t, tilevalue);
      }

      // zeta_bar: compute mean and variance for that frequency row 
      mean /= (double)GetTimeDelayBinsN();
      variance[d][q][f] = variance[d][q][f]/(double)GetTimeDelayBinsN() - mean*mean;
      if(variance[d][q][f]<0.0) variance[d][q][f] = 0.0;

      // power law: compute mean and variance for that frequency row
      mean_powerlaw /= (double)GetTimeDelayBinsN();
      variance_powerlaw = variance_powerlaw/(double)GetTimeDelayBinsN() - mean_powerlaw*mean_powerlaw;
      if(variance_powerlaw<=0.0) variance_powerlaw = 0.0;
      
      // shift input map by the mean value + FFT norm (/N)
      for(unsigned int t=0; t<GetTimeDelayBinsN(); t++){

        // for zeta_bar (no variance)
        map_fft[0][d][q][f]->SetRe_t(t, (map_fft[0][d][q][f]->GetRe_t(t) - mean)/(double)GetTimeDelayBinsN());

        // for power law (/std dev)
        if(variance_powerlaw==0.0) map_fft[1][d][q][f]->SetRe_t(t, 0.0);
        else
          map_fft[1][d][q][f]->SetRe_t(t, (map_fft[1][d][q][f]->GetRe_t(t) - mean_powerlaw)/(double)GetTimeDelayBinsN()/TMath::Sqrt(variance_powerlaw));
      }

      // fft forward
      map_fft[0][d][q][f]->Forward();
      map_fft[1][d][q][f]->Forward();
    }
  }

  // overall SNR variance
  mean_snr /= n_snr;
  OxOmicron::oxo_snrvar[d] = OxOmicron::oxo_snrvar[d]/n_snr - mean_snr*mean_snr;
  if(OxOmicron::oxo_snrvar[d]<0.0) OxOmicron::oxo_snrvar[d] = 0.0;

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxCorr::Process(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxCorr::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxCorr::"<<__func__<<": the OxCorr object is corrupted"<<endl;
    return false;
  }
  
  double norm, realpart;

  // loop over Q planes
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
    
    // loop over frequency rows
    for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){

      // cross-correlation in the fourier domain
      for(unsigned int k=0; k<map_fft[0][0][q][f]->GetSize_f(); k++){
        
        // phase factor
        if(k%2==1) norm = -1.0;
        else       norm = 1.0;
        
        // complex-conjugate multiplication (zeta_bar)
        realpart = map_fft[0][1][q][f]->GetRe_f(k);
        map_fft[0][1][q][f]->SetRe_f(k, norm*(map_fft[0][0][q][f]->GetRe_f(k)*realpart
                                           + map_fft[0][0][q][f]->GetIm_f(k)*map_fft[0][1][q][f]->GetIm_f(k)));
        map_fft[0][1][q][f]->SetIm_f(k, norm*(map_fft[0][0][q][f]->GetIm_f(k)*realpart
                                           - map_fft[0][0][q][f]->GetRe_f(k)*map_fft[0][1][q][f]->GetIm_f(k)));
        // complex-conjugate multiplication (power law)
        realpart = map_fft[1][1][q][f]->GetRe_f(k);
        map_fft[1][1][q][f]->SetRe_f(k, norm*(map_fft[1][0][q][f]->GetRe_f(k)*realpart
                                           + map_fft[1][0][q][f]->GetIm_f(k)*map_fft[1][1][q][f]->GetIm_f(k)));
        map_fft[1][1][q][f]->SetIm_f(k, norm*(map_fft[1][0][q][f]->GetIm_f(k)*realpart
                                           - map_fft[1][0][q][f]->GetRe_f(k)*map_fft[1][1][q][f]->GetIm_f(k)));
      }
      
      // fft backward
      map_fft[0][1][q][f]->Backward();
      map_fft[1][1][q][f]->Backward();
    }
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxCorr::WriteMaps(const double aTimeDelay){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxCorr::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxCorr::"<<__func__<<": the OxCorr object is corrupted"<<endl;
    return false;
  }

  // first write Omicron maps
  if(OxOmicron::WriteMaps(0)==false) return false;
  if(OxOmicron::WriteMaps(1)==false) return false;

  // locals
  TH2D *oxmap;
  TGraph *varamp0;
  TGraph *varamp0_whitenoise;
  TGraph *varamp1;
  TGraph *varamp1_whitenoise;
  TLine *td_line;
  double *fbands;
  double time_delay;
  double varmin, varmax;
  double dt_omicron = (double)OxOmicron::GetOmicronChunkStartTime(1) - (double)OxOmicron::GetOmicronChunkStartTime(0);
  string det0 = OxOmicron::detector[0]->GetDetectorPrefix();
  string det1 = OxOmicron::detector[1]->GetDetectorPrefix();

  // loop over Q planes
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){

    // create map
    fbands = OxOmicron::omicron[0]->GetBands(q);
    gwl_ss<<"#xi(#delta t, f), Q = "<<OxOmicron::omicron[0]->GetQ(q);
    oxmap = new TH2D("oxmap", gwl_ss.str().c_str(),
                     GetTimeDelayBinsN(), -(double)GetAnalysisDuration()/2.0+dt_omicron, (double)GetAnalysisDuration()/2.0+dt_omicron,
                     OxOmicron::omicron[0]->GetBandN(q), fbands);
    oxmap->GetXaxis()->SetTitle(("Time delay: t("+det1+") - t("+det0+") [s]").c_str());
    oxmap->GetYaxis()->SetTitle("Frequency [Hz]");
    oxmap->GetZaxis()->SetTitle("Cross-correlation #xi");
    gwl_ss.clear(); gwl_ss.str("");
    delete fbands;

    // fill Xi map
    // There is a shift of one bin (this is due to the asymmetry of the cross-correlation:
    // n=0 --> bin N+1 (histo overflow)
    // n=N-1 --> bin 2 (bin 1 is empty)
    for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){
      for(unsigned int t=0; t<GetTimeDelayBinsN(); t++){
        oxmap->SetBinContent(GetTimeDelayBinsN()-t+1, f+1, GetXi(q, f, t));
      }
    }

    // draw maps
    GwollumPlot::UnDrawLegend();
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    GwollumPlot::Draw(oxmap, "COLZ");
    GwollumPlot::SetLogy(1);

    // draw time-delay line if within the map
    time_delay = aTimeDelay;
    td_line = new TLine(time_delay, oxmap->GetYaxis()->GetBinLowEdge(1), time_delay, oxmap->GetYaxis()->GetBinUpEdge(OxOmicron::omicron[0]->GetBandN(q)));
    td_line->SetLineColor(kRed);
    td_line->SetLineStyle(7);
    td_line->SetLineWidth(2);
    if((time_delay>oxmap->GetXaxis()->GetBinLowEdge(1))&&(time_delay<oxmap->GetXaxis()->GetBinUpEdge(oxmap->GetNbinsX()))){
      GwollumPlot::Draw(td_line, "LSAME");
    }

    // write map
    gwl_ss<<OxInit::GetOutputDirectory()<<"/OXXIMAPQ"<<q<<"-"<<OxOmicron::GetOmicronTime(0)<<"-"<<OxOmicron::GetOmicronTime(1)<<".png";
    GwollumPlot::Print(gwl_ss.str());
    gwl_ss.clear(); gwl_ss.str("");

    // zoom around the time-delay line
    if((time_delay>oxmap->GetXaxis()->GetBinLowEdge(1))&&(time_delay<oxmap->GetXaxis()->GetBinUpEdge(oxmap->GetNbinsX()))){
      GwollumPlot::Draw(g_timedelay_valid[q], "PSAME");
      oxmap->GetXaxis()->SetRangeUser(time_delay-2.0*OxOmicron::GetLightTravelTime(), time_delay+2.0*OxOmicron::GetLightTravelTime());
      gwl_ss<<OxInit::GetOutputDirectory()<<"/OXXIMAPZOOMQ"<<q<<"-"<<OxOmicron::GetOmicronTime(0)<<"-"<<OxOmicron::GetOmicronTime(1)<<".png";
      GwollumPlot::Print(gwl_ss.str());
      gwl_ss.clear(); gwl_ss.str("");
    }

    delete oxmap;
    delete td_line;

    // amplitude variance plots
    gwl_ss<<"g_variance_0_"<<q;
    varamp0 = new TGraph(OxOmicron::omicron[0]->GetBandN(q));
    varamp0->SetName(gwl_ss.str().c_str());
    varamp0_whitenoise = new TGraph(OxOmicron::omicron[0]->GetBandN(q));
    varamp0_whitenoise->SetName((gwl_ss.str()+"_whitenoise").c_str());
    gwl_ss.clear(); gwl_ss.str("");
    gwl_ss<<"g_variance_1_"<<q;
    varamp1 = new TGraph(OxOmicron::omicron[0]->GetBandN(q));
    varamp1->SetName(gwl_ss.str().c_str());
    varamp1_whitenoise = new TGraph(OxOmicron::omicron[0]->GetBandN(q));
    varamp1_whitenoise->SetName((gwl_ss.str()+"_whitenoise").c_str());
    gwl_ss.clear(); gwl_ss.str("");

    // fill variance plots
    varmin = GetAmplitudeVariance(0, q, 0);
    varmax = GetAmplitudeVariance(0, q, 0);
    for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){
      varamp0->GetX()[f] = OxOmicron::omicron[0]->GetBandFrequency(q, f);
      varamp0->GetY()[f] = GetAmplitudeVariance(0, q, f);
      varamp0_whitenoise->GetX()[f] = OxOmicron::omicron[0]->GetBandFrequency(q, f);
      varamp0_whitenoise->GetY()[f] = OxOmicron::GetWhiteNoiseAmplitudeVariance(0, q, f);
      varamp1->GetX()[f] = OxOmicron::omicron[0]->GetBandFrequency(q, f);
      varamp1->GetY()[f] = GetAmplitudeVariance(1, q, f);
      varamp1_whitenoise->GetX()[f] = OxOmicron::omicron[0]->GetBandFrequency(q, f);
      varamp1_whitenoise->GetY()[f] = OxOmicron::GetWhiteNoiseAmplitudeVariance(1, q, f);
      
      if((varamp0->GetY()[f]>varmax)&&(varamp0->GetY()[f]>0.0)) varmax = varamp0->GetY()[f];
      if((varamp0->GetY()[f]<varmin)&&(varamp0->GetY()[f]>0.0)) varmin = varamp0->GetY()[f];
      if((varamp0_whitenoise->GetY()[f]>varmax)&&(varamp0_whitenoise->GetY()[f]>0.0)) varmax = varamp0_whitenoise->GetY()[f];
      if((varamp0_whitenoise->GetY()[f]<varmin)&&(varamp0_whitenoise->GetY()[f]>0.0)) varmin = varamp0_whitenoise->GetY()[f];
      if((varamp1->GetY()[f]>varmax)&&(varamp1->GetY()[f]>0.0)) varmax = varamp1->GetY()[f];
      if((varamp1->GetY()[f]<varmin)&&(varamp1->GetY()[f]>0.0)) varmin = varamp1->GetY()[f];
      if((varamp1_whitenoise->GetY()[f]>varmax)&&(varamp1_whitenoise->GetY()[f]>0.0)) varmax = varamp1_whitenoise->GetY()[f];
      if((varamp1_whitenoise->GetY()[f]<varmin)&&(varamp1_whitenoise->GetY()[f]>0.0)) varmin = varamp1_whitenoise->GetY()[f];
    }

    // plot style
    varamp0->SetTitle("Amplitude variance;Frequency [Hz];Amplitude variance");
    varamp0->SetMinimum(0.9*varmin);
    varamp0->SetMaximum(1.1*varmax);
    varamp0->SetMarkerColor(kYellow-7);
    varamp0->SetLineColor(kYellow-7);
    varamp0->SetLineStyle(1);
    varamp0->SetLineWidth(2);
    varamp0_whitenoise->SetMarkerColor(kYellow-7);
    varamp0_whitenoise->SetLineColor(kYellow-7);
    varamp0_whitenoise->SetLineStyle(7);
    varamp0_whitenoise->SetLineWidth(2);
    varamp1->SetMarkerColor(kRed-7);
    varamp1->SetLineColor(kRed-7);
    varamp1->SetLineStyle(1);
    varamp1->SetLineWidth(2);
    varamp1_whitenoise->SetMarkerColor(kRed-7);
    varamp1_whitenoise->SetLineColor(kRed-7);
    varamp1_whitenoise->SetLineStyle(7);
    varamp1_whitenoise->SetLineWidth(2);

    // write variance plots
    GwollumPlot::UnDrawLegend();
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    
    GwollumPlot::Draw(varamp0, "APL");
    GwollumPlot::Draw(varamp0_whitenoise, "PLSAME");
    GwollumPlot::Draw(varamp1, "PLSAME");
    GwollumPlot::Draw(varamp1_whitenoise, "PLSAME");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(1);
    GwollumPlot::ResetLegend();
    GwollumPlot::AddLegendEntry(varamp0, "Data (0)", "PL");
    GwollumPlot::AddLegendEntry(varamp0_whitenoise, "Noise (0)", "PL");
    GwollumPlot::AddLegendEntry(varamp1, "Data (1)", "PL");
    GwollumPlot::AddLegendEntry(varamp1_whitenoise, "Noise (1)", "PL");
    GwollumPlot::DrawLegend();
    gwl_ss<<OxInit::GetOutputDirectory()<<"/OXVARIANCEQ"<<q<<"-"<<OxOmicron::GetOmicronTime(0)<<"-"<<OxOmicron::GetOmicronTime(1)<<".png";
    GwollumPlot::Print(gwl_ss.str());
    gwl_ss.clear(); gwl_ss.str("");

    delete varamp0;
    delete varamp0_whitenoise;
    delete varamp1;
    delete varamp1_whitenoise;
  }

  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxCorr::"<<__func__<<": cross-correlation map at GPS0="<<OxOmicron::GetOmicronStartTime(0)<<", GPS1="<<OxOmicron::GetOmicronStartTime(1)<<" is produced"<<endl;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void OxCorr::MakeHtmlMapTable(ofstream &aReport, const unsigned int aMapTime_0, const unsigned int aMapTime_1){
////////////////////////////////////////////////////////////////////////////////////

  // map time must exist (first detector)
  unsigned int m = 0;
  for(m=0; m<OxOmicron::proc_maps[0].size(); m++){
    if(OxOmicron::proc_maps[0][m]==aMapTime_0) break;
  }
  if(m==OxOmicron::proc_maps[0].size()) return;

  // map time must exist (second detector)
  for(m=0; m<OxOmicron::proc_maps[1].size(); m++){
    if(OxOmicron::proc_maps[1][m]==aMapTime_1) break;
  }
  if(m==OxOmicron::proc_maps[1].size()) return;

  aReport<<"<table class=\"oxmaps\">"<<endl;

  // **** xi map
  
  // add a row with a link to a zoom plot, if any
  gwl_ss<<OxInit::GetOutputDirectory()<<"/OXXIMAPZOOMQ0-"<<aMapTime_0<<"-"<<aMapTime_1<<".png";
  if(filesystem::is_regular_file(gwl_ss.str())==true){
    aReport<<"  <tr>"<<endl;
    for(unsigned int q=0; q<omicron[0]->GetQN(); q++){
      aReport<<"    <td><a href=\"./OXXIMAPZOOMQ"<<q<<"-"<<aMapTime_0<<"-"<<aMapTime_1<<".png\">Click here for a zoomed plot</a></td>"<<endl;
    }
    aReport<<"  </tr>"<<endl;
  }
  gwl_ss.clear(); gwl_ss.str("");

  aReport<<"  <tr>"<<endl;
  for(unsigned int q=0; q<omicron[0]->GetQN(); q++){
    aReport<<"    <td><a href=\"./OXXIMAPQ"<<q<<"-"<<aMapTime_0<<"-"<<aMapTime_1<<".png\"><img src=\"./OXXIMAPQ"<<q<<"-"<<aMapTime_0<<"-"<<aMapTime_1<<".png\" alt=\"Cross-correlation xi map Q"<<q<<", "<<aMapTime_0<<", "<<aMapTime_1<<"\"/></a></td>"<<endl;
  }
  aReport<<"  </tr>"<<endl;

  // **** variance plots
    aReport<<"  <tr>"<<endl;
  for(unsigned int q=0; q<omicron[0]->GetQN(); q++){
    aReport<<"    <td><a href=\"./OXVARIANCEQ"<<q<<"-"<<aMapTime_0<<"-"<<aMapTime_1<<".png\"><img src=\"./OXVARIANCEQ"<<q<<"-"<<aMapTime_0<<"-"<<aMapTime_1<<".png\" alt=\"variance Q"<<q<<", "<<aMapTime_0<<", "<<aMapTime_1<<"\"/></a></td>"<<endl;
  }
  aReport<<"  </tr>"<<endl;

  aReport<<"</table>"<<endl;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int OxCorr::GetOmicronTileIndex(const unsigned int aQindex, const unsigned int aFrequencyIndex, const unsigned int aMapTimeIndex){
////////////////////////////////////////////////////////////////////////////////////

  // map time (at bin center)
  double map_time = ((double)aMapTimeIndex+0.5)*GetTimeDelayResolution();
    
  // add omicron overlap/2
  map_time += (double)OxOmicron::omicron[0]->GetOverlapDuration()/2.0;
    
  // Omicron map tile index (use det 0 as a reference)
  unsigned int tile_index = (unsigned int)TMath::Floor(map_time/(double)OxOmicron::omicron[0]->GetChunkDuration()*(double)OxOmicron::omicron[0]->GetBandTileN(aQindex, aFrequencyIndex));

  return tile_index;
}

