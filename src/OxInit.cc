/**
 * @file 
 * @brief See OxInit.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxInit.h"

ClassImp(OxInit)

////////////////////////////////////////////////////////////////////////////////////
OxInit::OxInit(const string aOptionFile){
////////////////////////////////////////////////////////////////////////////////////

  ox_opt_filepath = aOptionFile;
  ox_nfiles = 0;

  // init monitoring
  monit = new Monitor("omicronx");

  // options
  ox_opt = new GwollumOptions();

  // Ox file list options
  ox_opt_text = new GwollumOptions();
  DefineOption("OXI", "OXFILELIST", "", 0);

  //****** OXFILELIST only ******
  // read options from the text file
  ox_opt_text->ReadTextFile(aOptionFile);

  // get options from Ox files (if any)
  ox_nfiles = 0;
  ox_filename = NULL; // intialized in GetOptionsFromOxFiles()
  GetOptionsFromOxFiles();
  ox_opt_text->ResetOption("OXI", "OXFILELIST");
  //*****************************

  // additional options
  DefineOption("OXI", "NAME", "ox", 1);
  DefineOption("OXI", "OUTDIR", ".", 1);
  DefineOption("OXI", "VERBOSITY", (unsigned int)0, 1);

  // add options from the text file
  AddOptions();

  // overload some of the options
  OverloadOption("OXI", "OXFILELIST");
  // OXI/NAME keep the TTree value
  OverloadOption("OXI", "OUTDIR");
  OverloadOption("OXI", "VERBOSITY");

  // configure monitoring
  monit->Monitor::SetVerbosityLevel(ox_opt->GetOptionValueU("OXI", "VERBOSITY", 0));
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxInit::"<<__func__<<endl;

  // fix name
  string name_fixed = ReplaceAll(ReplaceAll(ox_opt->GetOptionValueS("OXI", "NAME", 0), "-", "_"), " ", "_");
  ox_opt->SetOptionValue("OXI", "NAME", 0, name_fixed);

  // option optimization
  ox_outdir = ox_opt->GetOptionValueS("OXI", "OUTDIR", 0);
  ox_name = ox_opt->GetOptionValueS("OXI", "NAME", 0);
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxInit::"<<__func__<<": analysis name = "<<GetName()<<endl;

  // output directory
  error_code errc;
  if(CreateDirectory(errc, ox_outdir)==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxInit::"<<__func__<<": the output directory cannot be created = "<<errc<<endl;
  }

  // output file
  ox_outfile = NULL;

  // fill option tree
  ox_opt->FillTree();
}

////////////////////////////////////////////////////////////////////////////////////
OxInit::~OxInit(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxInit::"<<__func__<<endl;
  delete ox_opt;
  delete ox_opt_text;
  delete [] ox_filename;
  CloseOutputFile();
  delete monit;
}

////////////////////////////////////////////////////////////////////////////////////
void OxInit::CloseOutputFile(void){
  ////////////////////////////////////////////////////////////////////////////////////
  if(ox_outfile!=NULL){
    if(ox_outfile->IsOpen()){
      ox_outfile->Write();
      delete ox_outfile;
    }
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxInit::CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxInit::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxInit::"<<__func__<<": the OxInit object is corrupted"<<endl;
    return false;
  }
  
  // a file already exists --> close it first
  CloseOutputFile();

  // output filename
  gwl_ss<<aTime;
  aOutFileName = ox_outdir+"/"+ox_name+"_"+aFileId+"-"+gwl_ss.str()+".root";
  gwl_ss.clear(); gwl_ss.str("");

  // special case if time=0
  unsigned int time_inc = aTime;
  if(time_inc==0){
    while(filesystem::is_regular_file(aOutFileName)){
      time_inc++;
      gwl_ss<<time_inc;
      aOutFileName = ox_outdir+"/"+ox_name+"_"+aFileId+"-"+gwl_ss.str()+".root";
      gwl_ss.clear(); gwl_ss.str("");
    }
  }
  
  // create a new file
  ox_outfile = new TFile(aOutFileName.c_str(), "recreate");
  if(ox_outfile->IsZombie()){
    aOutFileName = "";
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxInit::"<<__func__<<": root file is zombie"<<endl;
    return false;
  }
  ox_outfile->cd();
  
  // write option tree
  ox_opt->WriteTree("ox_options");
  monit->Monitor::SetMessageType(monitor_status);
  *monit<<"OxInit::"<<__func__<<": the ouput file "<<aOutFileName<<" is created"<<endl;

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
ofstream OxInit::CreateHtml(const string aIndexFileName, const string aTitle){
////////////////////////////////////////////////////////////////////////////////////

  // output directory
  string outdir = OxInit::GetOutputDirectory();

  // import material
  error_code errcode;
  char *omicron_html_c = getenv("OMICRON_HTML");
  char *gwollum_pix_c = getenv("GWOLLUM_PIX");
  string omicron_html = ".";
  string gwollum_pix = ".";
  if(omicron_html_c!=NULL) omicron_html= (string)omicron_html_c;
  if(gwollum_pix_c!=NULL) gwollum_pix= (string)gwollum_pix_c;
  if(!CopyFile(errcode, omicron_html+"/style/style.OX.css", outdir+"/style.css", true)){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxInit::"<<__func__<<": cannot copy style css sheet ("<<errcode<<")"<<endl;
  }
  if(!CopyFile(errcode, omicron_html+"/pics/omicronlogo.OX.gif", outdir+"/logo.gif", true)){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxInit::"<<__func__<<": cannot copy omicron logo ("<<errcode<<")"<<endl;
  }
  if(!CopyFile(errcode, gwollum_pix+"/gwollum_logo_min_trans.gif", outdir+"/icon.gif", true)){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxInit::"<<__func__<<": cannot copy gwollum logo  ("<<errcode<<")"<<endl;
  }

  // timing
  time_t timenow;
  time(&timenow);
  struct tm *ptm = gmtime(&timenow);

  // login
  char *login = getlogin();

  // index header & scripts
  ofstream report((outdir+"/"+aIndexFileName).c_str());
  report<<"<!DOCTYPE html>"<<endl;
  report<<"<html>"<<endl;
  report<<"<head>"<<endl;
  report<<"<title>"<<aTitle<<"</title>"<<endl;
  report<<"<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"<<endl;
  report<<"<link rel=\"icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function toggle(anId) {"<<endl;
  report<<"  node = document.getElementById(anId);"<<endl;
  report<<"  if (node.style.visibility==\"hidden\") {"<<endl;
  report<<"    node.style.visibility = \"visible\";"<<endl;
  report<<"    node.style.height = \"auto\";"<<endl;
  report<<"  }"<<endl;
  report<<"  else {"<<endl;
  report<<"    node.style.visibility = \"hidden\";"<<endl;
  report<<"    node.style.height = \"0\";"<<endl;
  report<<"  }"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"</head>"<<endl;
  report<<"<body>"<<endl;
  report<<endl;

  // index title
  report<<"<h1>"<<aTitle<<"</h1>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;

  //****************** summary *********************
  report<<"<h2>Summary</h2>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td>Omicron version:</td><td>"<<(string)O_PROJECT_VERSION<<": <a href=\"https://virgo.docs.ligo.org/virgoapp/Omicron/\" target=\"_blank\">documentation</a> <a href=\"https://git.ligo.org/virgo/virgoapp/Omicron\" target=\"_blank\">gitlab repository</a></td></tr>"<<endl;
  if(login!=NULL)
    report<<"  <tr><td>Omicron run by:</td><td>"<<(string)login<<"</td></tr>"<<endl;
  else
    report<<"  <tr><td>Omicron run by:</td><td>unknown</td></tr>"<<endl;
  report<<"  <tr><td>Report produced on:</td><td>"<<asctime(ptm)<<" UTC</td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;
  //************************************************

  return report;
}

////////////////////////////////////////////////////////////////////////////////////
void OxInit::CloseHtml(ofstream &aReport){
////////////////////////////////////////////////////////////////////////////////////

  // index footer
  aReport<<"<!-- Begin footer -->"<<endl;
  aReport<<"<table>"<<endl;
  aReport<<"  <tr><td><a href=\"mailto:florent.robinet@ijclab.in2p3.fr\">florent.robinet@ijclab.in2p3.fr</a></td></tr>"<<endl;
  aReport<<"</table>"<<endl;
  aReport<<"<!-- End footer -->"<<endl;
  aReport<<"</body>"<<endl;
  aReport<<"</html>"<<endl;
  aReport.close();

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxInit::AddOptions(void){
////////////////////////////////////////////////////////////////////////////////////
  
  // update options from the text file
  // (in case new options were defined)
  ox_opt_text->ReadTextFile(ox_opt_filepath);
  
  // check text options and add missing options
  for(unsigned int o=0; o<ox_opt_text->GetOptionsN(); o++){

    // the option is missing: get it from the text file
    if(!ox_opt->Is(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o))){

      // as an integer
      if(ox_opt_text->GetOptionType(o)=='i'){
        ox_opt->Add(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionDefaultI(o), ox_opt_text->GetOptionSize(o));
        for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o)); v++){
          ox_opt->AddOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionValueI(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
          ox_opt->SetOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v, ox_opt_text->GetOptionValueI(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
        }
      }

      // as an unsigned integer
      else if(ox_opt_text->GetOptionType(o)=='u'){
        ox_opt->Add(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionDefaultU(o), ox_opt_text->GetOptionSize(o));
        for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o)); v++){
          ox_opt->AddOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionValueU(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
          ox_opt->SetOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v, ox_opt_text->GetOptionValueU(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
        }
      }

      // as a double
      else if(ox_opt_text->GetOptionType(o)=='d'){
        ox_opt->Add(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionDefaultD(o), ox_opt_text->GetOptionSize(o));
        for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o)); v++){
          ox_opt->AddOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionValueD(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
          ox_opt->SetOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v, ox_opt_text->GetOptionValueD(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
        }
      }

      // as a string
      else if(ox_opt_text->GetOptionType(o)=='s'){
        ox_opt->Add(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionDefaultS(o), ox_opt_text->GetOptionSize(o));
        for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o)); v++){
          ox_opt->AddOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), ox_opt_text->GetOptionValueS(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
          ox_opt->SetOptionValue(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v, ox_opt_text->GetOptionValueS(ox_opt_text->GetOptionTag(o), ox_opt_text->GetOptionKey(o), v));
        }
      }
      else continue;
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxInit::OverloadOption(const string aTag, const string aKey){
////////////////////////////////////////////////////////////////////////////////////

  // option index and type
  unsigned int oindex = ox_opt->GetOptionIndex(aTag, aKey);
  if(oindex==ox_opt->GetOptionsN()){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxInit::"<<__func__<<": cannot overload option "<<aTag<<"/"<<aKey<<endl;
    return;
  }
  char otype = ox_opt->GetOptionType(oindex);
  
  // variable-size option 
  if(ox_opt->GetOptionSize(oindex)==0){

    // reset option
    ox_opt->ResetOption(aTag, aKey);

    // save values
    if(otype=='i'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->AddOptionValue(aTag, aKey, ox_opt_text->GetOptionValueI(aTag, aKey, v));
    }
    else if(otype=='u'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->AddOptionValue(aTag, aKey, ox_opt_text->GetOptionValueU(aTag, aKey, v));
    }
    else if(otype=='d'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->AddOptionValue(aTag, aKey, ox_opt_text->GetOptionValueD(aTag, aKey, v));
    }
    else if(otype=='s'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->AddOptionValue(aTag, aKey, ox_opt_text->GetOptionValueS(aTag, aKey, v));
    }
  }
  
  // fix-size option
  else{
    
    // replace values
    if(otype=='i'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->SetOptionValue(aTag, aKey, v, ox_opt_text->GetOptionValueI(aTag, aKey, v));
    }
    else if(otype=='u'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->SetOptionValue(aTag, aKey, v, ox_opt_text->GetOptionValueU(aTag, aKey, v));
    }
    else if(otype=='d'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->SetOptionValue(aTag, aKey, v, ox_opt_text->GetOptionValueD(aTag, aKey, v));
    }
    else if(otype=='s'){
      for(unsigned int v=0; v<ox_opt_text->GetOptionNumberOfValues(aTag, aKey); v++)
        ox_opt->SetOptionValue(aTag, aKey, v, ox_opt_text->GetOptionValueS(aTag, aKey, v));
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxInit::GetOptionsFromOxFiles(void){
////////////////////////////////////////////////////////////////////////////////////

  // Ox file pattern
  unsigned int n_patterns = ox_opt_text->GetOptionNumberOfValues("OXI", "OXFILELIST");
  if((n_patterns>0)&&(ox_opt_text->GetOptionValueS("OXI", "OXFILELIST", 0).compare(""))){
    monit->Monitor::SetMessageType(monitor_warning);
    *monit<<"OxInit::"<<__func__<<": Ox files are provided --> they are used for initialization"<<endl;
  }

  // read option TChain from input files (if any)
  TChain *c_opt = new TChain("ox_options");
  for(unsigned int p=0; p<n_patterns; p++){
    if(ox_opt_text->GetOptionValueS("OXI", "OXFILELIST", p).compare("")){
      // only adding files with options
      ox_nfiles += c_opt->Add(ox_opt_text->GetOptionValueS("OXI", "OXFILELIST", p).c_str(), 0);
    }
  }

  // list of ox files
  ox_filename = new string [ox_nfiles];
  string filename_tmp;
  for(unsigned int f=0; f<ox_nfiles; f++){

    // load tree (must exist)
    if(c_opt->LoadTree(c_opt->GetTreeOffset()[f])<0){
      monit->Monitor::SetMessageType(monitor_fatal);
      *monit<<"OxInit::"<<__func__<<": cannot read option tree in "<<c_opt->GetFile()->GetPath()<<endl;
    }

    // get filename
    filename_tmp = (string)(c_opt->GetFile()->GetPath());
    ox_filename[f] = filename_tmp.substr(0, filename_tmp.size()-2);// remove ":/" at the end
  }

  // load options from TChain
  bool uniform = false;
  ox_opt->ReadChain(uniform, c_opt);
  if(uniform==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxInit::"<<__func__<<": the list of options is not uniform across the Ox files"<<endl;
  }
  delete c_opt;

  return;
}

