/**
 * @file 
 * @brief See OxAnalysis.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxAnalysis.h"

ClassImp(OxAnalysis)

////////////////////////////////////////////////////////////////////////////////////
OxAnalysis::OxAnalysis(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1){
////////////////////////////////////////////////////////////////////////////////////

  // read options
  ox_opt = new GwollumOptions();
  ox_opt->Add("OX", "NPERFILE", (unsigned int)10, 1);
  ox_opt->Add("OX", "LAGS", (int)0, 0);
  ox_opt->ReadTextFile(aOptionFile);

  // Ox events
  ox_results = new OxResults(aOptionFile, aGpsRef0, aGpsRef1);

  // init monitoring
  monit = new Monitor("ox");
  monit->Monitor::SetVerbosityLevel(ox_results->GetVerbosityLevel());
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxAnalysis::"<<__func__<<endl;
}

////////////////////////////////////////////////////////////////////////////////////
OxAnalysis::~OxAnalysis(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxInit::"<<__func__<<endl;
  delete ox_results;
  delete ox_opt;
  delete monit;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxAnalysis::Process(Segments *aInSeg0, Segments *aInSeg1){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxAnalysis::"<<__func__<<endl;
  if(GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxAnalysis::"<<__func__<<": the OxAnalysis object is corrupted"<<endl;
    return false;
  }

  // check input segments
  if((aInSeg0==NULL)||(!aInSeg0->GetStatus())){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxAnalysis::"<<__func__<<": the input segment list (detector 0) is corrupted"<<endl;
    return false;
  }
  if((aInSeg1==NULL)||(!aInSeg1->GetStatus())){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxAnalysis::"<<__func__<<": the input segment list (detector 1) is corrupted"<<endl;
    return false;
  }

  // no segments --> nothing to do
  if(aInSeg0->GetN()==0) return true;
  if(aInSeg1->GetN()==0) return true;

  monit->Monitor::SetMessageType(monitor_status);
  *monit<<"OxAnalysis::"<<__func__<<": "<<aInSeg0->GetFirst()<<"-"<<aInSeg0->GetLast()<<" (det 0)"<<endl;
  monit->Monitor::SetMessageType(monitor_status);
  *monit<<"OxAnalysis::"<<__func__<<": "<<aInSeg1->GetFirst()<<"-"<<aInSeg1->GetLast()<<" (det 1)"<<endl;
 
  // create output directory
  error_code errcode;
  if(CreateDirectory(errcode, ox_results->OxInit::GetOutputDirectory())==false){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxAnalysis::"<<__func__<<": the output directory cannot be created ("<<errcode<<")"<<endl;
    return false;
  }

  // save options
  unsigned int n_per_file = ox_opt->GetOptionValueU("OX", "NPERFILE", 0);

  // list of lags
  vector<int> lag_list;
  if(ox_opt->GetOptionNumberOfValues("OX", "LAGS")==0)
    lag_list.push_back(0);
  else if(ox_opt->GetOptionNumberOfValues("OX", "LAGS")==2){
    for(int l=ox_opt->GetOptionValueI("OX", "LAGS", 0); l<=ox_opt->GetOptionValueI("OX", "LAGS", 1); l++)
      lag_list.push_back(l);
  }
  else{
    for(unsigned int l=0; l<ox_opt->GetOptionNumberOfValues("OX", "LAGS"); l++)
      lag_list.push_back(ox_opt->GetOptionValueI("OX", "LAGS", l));
  }

  // locals
  unsigned int psd_length0 = 0, psd_length1 = 0;
  unsigned int gstart0, gstart1;
  unsigned int seg_index_s = 0, seg_index_0 = 0, seg_index_1 = 0;
  UChar_t analysis_index = 0;
  string outfilename = "";
  string lag_name = "";
  unsigned int nproc = 0;

  // loop over lags
  for(unsigned int l=0; l<lag_list.size(); l++){

    // lag name
    if(lag_list[l]>=0) lag_name = "p"+gwl_ss.str();
    else lag_name = "m";
    gwl_ss<<TMath::Abs(lag_list[l]);
    lag_name += gwl_ss.str();
    gwl_ss.clear(); gwl_ss.str("");

    // init counter for this lag
    nproc = 0;

    // init segment index for PSD reset
    seg_index_0 = aInSeg0->GetN();
    seg_index_1 = aInSeg1->GetN();
    
    // loop over segments of the first detector
    for(unsigned int s=0; s<aInSeg0->GetN(); s++){

      // detector 0: init segment
      gstart0 = (unsigned int)(aInSeg0->GetStart(s));

      // detector 0: loop over analysis windows
      while(gstart0 + ox_results->OxOmicron::GetOmicronChunkDuration() <= (unsigned int)(aInSeg0->GetEnd(s))){

        // check status
        if(GetStatus()==false){
          monit->Monitor::SetMessageType(monitor_fatal);
          *monit<<"OxAnalysis::"<<__func__<<": the OxAnalysis object is corrupted"<<endl;
          return false;
        }
        
        // detector 1: init segment
        gstart1 = gstart0 + lag_list[l]*(ox_results->OxOmicron::GetOmicronChunkDuration() - ox_results->OxOmicron::GetOmicronOverlapDuration());
        
        // detector 1: start GPS is not in a valid segment
        if(aInSeg1->IsInsideSegment(seg_index_s, gstart1)==false){
          gstart0 += ox_results->OxOmicron::GetOmicronChunkDuration()/2 - ox_results->OxOmicron::GetOmicronOverlapDuration()/2;
          continue;
        }

        // detector 1: start and end GPS are not in the same segment
        if(gstart1+ox_results->OxOmicron::GetOmicronChunkDuration()>(unsigned int)(aInSeg1->GetEnd(seg_index_s))){
          gstart0 += ox_results->OxOmicron::GetOmicronChunkDuration()/2 - ox_results->OxOmicron::GetOmicronOverlapDuration()/2;
          continue;
        }

        // detector 0: new segment --> reset PSD
        // initialize PSD buffer at maximum
        if(s!=seg_index_0)
          psd_length0 = (unsigned int)(aInSeg0->GetEnd(s)-gstart0);
        else
          psd_length0 = 0;
        seg_index_0 = s;

        // detector 1: new segment --> reset PSD
        // initialize PSD buffer at maximum
        if(seg_index_s!=seg_index_1)
          psd_length1 = (unsigned int)(aInSeg1->GetEnd(seg_index_s)-gstart1);
        else
          psd_length1 = 0;
        seg_index_1 = seg_index_s;

        // new output file
        if(nproc%n_per_file==0) ox_results->OxEvent::CreateOutputFile(outfilename, lag_name, gstart0+ox_results->OxOmicron::GetOmicronOverlapDuration()/2);

        monit->Monitor::SetMessageType(monitor_status);
        *monit<<"OxAnalysis::"<<__func__<<": lag = "<<lag_list[l]<<", det0 = "<<gstart0+ox_results->OxOmicron::GetOmicronOverlapDuration()/2<<"-"<<gstart0 + ox_results->OxOmicron::GetOmicronOverlapDuration()/2 + ox_results->OxOmicron::GetAnalysisDuration()<<", det1 = "<<gstart1+ox_results->OxOmicron::GetOmicronOverlapDuration()/2<<"-"<<gstart1 + ox_results->OxOmicron::GetOmicronOverlapDuration()/2 + ox_results->OxOmicron::GetAnalysisDuration()<<endl;
       
        // run Omicron for detector 0
        if(ox_results->OxOmicron::Process(0, gstart0, psd_length0)==false){
          monit->Monitor::SetMessageType(monitor_fatal);
          *monit<<"OxAnalysis::"<<__func__<<": process failed"<<endl;
          return false;
        }
        psd_length0 = 0;

        // Fourier transform 0
        if(ox_results->OxCorr::Transform(0)==false){
          monit->Monitor::SetMessageType(monitor_fatal);
          *monit<<"OxAnalysis::"<<__func__<<": transform failed"<<endl;
          return false;
        }
      
        // run Omicron for detector 1        
        if(ox_results->OxOmicron::Process(1, gstart1, psd_length1)==false){
          monit->Monitor::SetMessageType(monitor_fatal);
          *monit<<"OxAnalysis::"<<__func__<<": process failed"<<endl;
          return false;
        }
        
        // Fourier transform 1
        if(ox_results->OxCorr::Transform(1)==false){
          monit->Monitor::SetMessageType(monitor_fatal);
          *monit<<"OxAnalysis::"<<__func__<<": transform failed"<<endl;
          return false;
        }
        
        // run the cross-correlation analysis
        if(ox_results->OxCorr::Process()==false){
          monit->Monitor::SetMessageType(monitor_fatal);
          *monit<<"OxAnalysis::"<<__func__<<": process failed"<<endl;
          return false;
        }
        
        // analysis index
        // 0 = from the start of the segment
        // 1 = shifted by 50%
        analysis_index = (UChar_t)((gstart0 + ox_results->OxOmicron::GetOmicronOverlapDuration()/2 - (unsigned int)(aInSeg0->GetStart(s)))
                                   % (ox_results->OxOmicron::GetAnalysisDuration()/2));
        

	// extract cross-correlation events
        if(ox_results->SaveEvents(analysis_index)==false){
          monit->Monitor::SetMessageType(monitor_fatal);
          *monit<<"OxAnalysis::"<<__func__<<": process failed"<<endl;
          return false;
        }
        
        // next chunk (1)
        gstart0 += ox_results->OxOmicron::GetOmicronChunkDuration()/2 - ox_results->OxOmicron::GetOmicronOverlapDuration()/2;
        nproc++;
      }
    }
  }

  return true;
}


////////////////////////////////////////////////////////////////////////////////////
bool OxAnalysis::PostProcess(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxAnalysis::"<<__func__<<endl;
  if(GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxAnalysis::"<<__func__<<": the OxAnalysis object is corrupted"<<endl;
    return false;
  }
  
  // create output file
  if(ox_results->OxResults::CreateOutputFile()==false){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxAnalysis::"<<__func__<<": failed to create output result file"<<endl;
    return false;
  }

  // post-process events
  if(ox_results->OxResults::Process()==false){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxAnalysis::"<<__func__<<": failed to post-process events"<<endl;
    return false;
  }
  
  // make plots
  if(ox_results->OxResults::SavePlots()==false){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxAnalysis::"<<__func__<<": failed to produce plots"<<endl;
    return false;
  }

  // make html report
  if(ox_results->OxResults::MakeHtml()==false){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxAnalysis::"<<__func__<<": failed to produce web report"<<endl;
    return false;
  }


  return true;
}
