/**
 * @file 
 * @brief Manage Omicron options.
 * @sa Oomicron.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oomicron.h"

/**
 * @details Omicron parameters must be provided with an option file. 
 * If the option file is a text file, each parameter is identified with a tag and a keyword:
 * @verbatim
TAG  KEYWORD  [PARAMETERS]
@endverbatim
 * The combination of tag/keyword/parameters is called an option.
 * @note For some options, multiple parameters can be used. They are separated by white spaces or tabs. 
 *
 * @include parameters.txt
 *
 * The list of options can also be provided in a ROOT file (extension: `.root`). In that case, the options are listed in a TTree named `gwloptions` which must be generated with the `GwollumOptions` class.
 * @note It is possible to access the `gwloptions` TTree in a ROOT sub-directory. The sub-directory must be given after the path to the ROOT file using `|` as a separator. For example: `/path/to/my/root/file.root|subdir1/subdir2`
 * 
 * Here we list all the options for Omicron.
 *
 * @section omicron_readoptions_output OUTPUT
 * 
 * @subsection omicron_readoptions_output_directory Output directory 
 * @verbatim
OUTPUT  DIRECTORY  [PARAMETER]
@endverbatim
 * `[PARAMETER]` is the directory path (relative or absolute).
 * The output directory must be provided and it must exist.
 *
 * @subsection omicron_readoptions_output_products Output products
 * @verbatim
OUTPUT  PRODUCTS  [PARAMETERS]
@endverbatim
 * `[PARAMETERS]` is the list of products computed by Omicron.
 * Possible parameters are:
 * - `triggers`: Omicron tiles above a SNR threshold are saved in a file.
 * - `asd`: The amplitude spectral density function is saved in a file.
 * - `psd`: The power spectral density function is saved in a file.
 * - `html`: A html report is produced in the output directory specified with the @ref omicron_readoptions_output_directory "directory option".
 * - `timeseries`: The condition time series is saved in a file.
 * - `white`: The time series after whitening is saved in a file.
 * - `whitepsd`: The power spectral density after whitening is saved in a file.
 * - `mapsnr`: The snr spectrograms are saved in a file.
 * - `mapamplitude`: The amplitude spectrograms are saved in a file.
 * - `mapphase`: The phase spectrograms are saved in a file.
 * - `summary`: A channel summary is written in a text file.
 * - `options`: The list of options is saved in a ROOT file.
 *
 * By default, only `triggers` are produced.
 * The ouput file format is specified with the @ref omicron_readoptions_output_format "format option".
 *
 * @subsection omicron_readoptions_output_format File format
 * @verbatim
OUTPUT  FORMAT  [PARAMETERS]
@endverbatim
 * `[PARAMETERS]` is the list of file formats to save @ref omicron_readoptions_output_products "output products".
 * Supported formats: `root` (native), `hdf5` (for triggers only), and all the usual graphical formats (`svg`, `gif`, `pdf`, `png`, `eps` and so on).
 * By default = `root`.
 * @note Use `wav` to produce sound files for time series.
 *
 * @subsection omicron_readoptions_output_verbosity Verbosity level
 * @verbatim
OUTPUT  VERBOSITY  [PARAMETER]
@endverbatim
 * `[PARAMETER]` is the verbosity level: 0, 1, 2, or 3.
 * By default = 0.
 *
 * @subsection omicron_readoptions_output_style Output style
 * @verbatim
OUTPUT  STYLE  [PARAMETER]
@endverbatim
 * `[PARAMETER]` defines the css style for the web report (if requested with the @ref omicron_readoptions_output_products "output products" option). It also defines the graphical color palette for the plots. Several styles are supported:
 * - `GWOLLUM` (default): black background, blue style.
 * - `FIRE`: black background, color palette from red to yellow
 * - `STANDARD`: light background, rainbow color palette
 * - `PINK`: black background, pink style.
 *
 * @subsection omicron_readoptions_output_nologo No logo flag
 * @verbatim
OUTPUT  NOLOGO  [PARAMETER]
@endverbatim
 * If `[PARAMETER]` is set to a non-zero value, the omicron background logo does not appear on the web reports (if requested with the @ref omicron_readoptions_output_products "output products" option). By default, the logo is present (`[PARAMETER] = 0`).
 *
 * @subsection omicron_readoptions_output_plotdimensions Plot dimensions
 * @verbatim
OUTPUT  PLOTDIMENSIONS  [PARAMETERS]
@endverbatim
 * This option sets the graphical plot dimensions (if requested with the @ref omicron_readoptions_output_format "OUTPUT/FORMAT" option). Two integer numbers should be provided with `[PARAMETERS]`: the width and the height measured in a number of pixels.
 *
 * @subsection omicron_readoptions_output_htmlmainpage HTML main page name
 * @verbatim
OUTPUT  HTMLMAINPAGE  [PARAMETER]
@endverbatim
 * This option specifies the file name of the html main page (if required with the @ref omicron_readoptions_output_products "OUTPUT/PRODUCTS" option). By default = `index.html`.
 *
 * @section omicron_readoptions_data DATA
 *
 * @subsection omicron_readoptions_data_ffl Frame file list
 * @verbatim
DATA  FFL  [PARAMETERS]
@endverbatim
 * This option specifies the list of frame files to be processed by omicron. It van be provided as a frame file list, a LAL cache file, or a mix of both. 
 * `[PARAMETERS]` must provide the path to the FFL file (absolute or relative). It can be complemented by 2 optional parameters which are used to load the FFL file multiple times in a row.
 * - Number of retries to load the FFL,
 * - Time in [s] between two retries.
 *
 * By default, no FFL file is used.
 *
 * @subsection omicron_readoptions_data_samplefrequency Working sampling frequency
 * @verbatim
DATA  SAMPLEFREQUENCY  [PARAMETER]
@endverbatim
 * This option specifies the working sampling frequency of omicron in [Hz]. All channel time series are sampled to a common frequency before being processed. Only downsampling is supported. The working sampling frequency must be a power of 2 and must be at least 16 Hz.
 *
 * @subsection omicron_readoptions_data_channels List of channels
 * @verbatim
DATA  CHANNELS  [PARAMETERS]
@endverbatim
 * This option specifies the list of channels to process, e.g. `V1:Hrec_hoft_16384Hz V1:LSC_DARM`. Multiple channel names can be provided using wildcards. This option can also be used on multiple lines. This option is mandatory.
 *
 * @subsection omicron_readoptions_data_blacklistedchannels List of black-listed channels
 * @verbatim
DATA  BLACKLISTEDCHANNELS  [PARAMETERS]
@endverbatim
 * This option specifies the list of channels to exclude from the processing, e.g. `V1:Hrec_hoft_16384Hz V1:LSC_DARM`. Multiple channel names can be provided using wildcards. This option can also be used on multiple lines.
 *
 * @section omicron_readoptions_parameter PARAMETER
 *
 * @subsection omicron_readoptions_parameter_timing Analysis timing
 * @verbatim
PARAMETER  TIMING  [PARAMETERS]
@endverbatim
 * This option specifies the timing parameters to perform the analysis. It includes 2 parameters:
 * - The analysis window size in [s]. It must be an even number of seconds.
 * - Overlap duration [s]. Two consecutive analysis windows overlap by this amount.
 * This option is mandatory.
 *
 * @subsection omicron_readoptions_parameter_frequencyrange Frequency range
 * @verbatim
PARAMETER  FREQUENCYRANGE  [PARAMETERS]
@endverbatim
 * This option specifies the search frequency range with a lower bound and a higher bound, both in [Hz].
 * The higher bound must be compatible with the @ref omicron_readoptions_data_samplefrequency "sampling frequency" option.
 * This option is mandatory.
 *
 * @subsection omicron_readoptions_parameter_qrange Q range
 * @verbatim
PARAMETER  QRANGE  [PARAMETERS]
@endverbatim
 * This option specifies the search Q range with a lower bound and a higher bound. The lower bound must be at least \f$\sqrt{11}\f$.
 * This option is mandatory.
 *
 * @subsection omicron_readoptions_parameter_mismatchmax Maximum mismatch between tiles
 * @verbatim
PARAMETER  MISMATCHMAX  [PARAMETER]
@endverbatim
 * This option specifies the maximum energy mismatch between time-frequency tiles. This value should be a number between 0 and 1.
 * By default, max mismatch = 0.25.
 *
 * @subsection omicron_readoptions_parameter_fullmapnt Full map time resolution
 * @verbatim
PARAMETER  FULLMAPNT  [PARAMETER]
@endverbatim
 * This option specifies the time resolution of the full time-frequency map, where all Q planes are combined.
 * The given parameter is the number of bins used to cover the chunk time range.
 * If this parameter is set to 0, the maximal resolution is used, which can be expensive in memory usage.
 * By default, number of time bins = 301.
 *
 * @subsection omicron_readoptions_parameter_snrthreshold SNR thresholds
 * @verbatim
PARAMETER  SNRTHRESHOLD  [PARAMETERS]
@endverbatim
 * This option specifies the signal-to-noise ratio. There are 2 SNR thresholds:
 * - The first SNR threshold applies to the individual time-frequency tiles to define a trigger.
 * - The second SNR threshold determines if spectogram plots must be generated. The plots are produced if at least one tile in the first plot time window is above this threshold. 
 * 
 * By default, both thresholds are set to 7.
 *
 * @subsection omicron_readoptions_parameter_psdlength Power spectrum density length
 * @verbatim
PARAMETER  PSDLENGTH  [PARAMETERS]
@endverbatim
 * This option specifies how the PSD is calculated. Two parameters are expected:
 * - The duration [s] over which the power spectrum density is estimated.
 * - A flag indicating if the PSD buffer should be filled whatever the input segment is: 0=NO, 1=YES.  
 * By default, the @ref omicron_readoptions_parameter_timing "analysis window duration" minus the overlap is used to calculate the PSD and the PSD buffer flag is set to 0 (NO). 
 *
 * @subsection omicron_readoptions_parameter_highpass High-pass filter
 * @verbatim
PARAMETER  HIGHPASS  [PARAMETER]
@endverbatim
 * This option specifies the frequency cutoff [Hz] at which to high-pass the data before processing.
 * By default, do not high-pass the data.
 *
 * @subsection omicron_readoptions_parameter_clustering Clustering algorithm
 * @verbatim
PARAMETER  CLUSTERING  [PARAMETER]
@endverbatim
 * This option selects the clustering method. Only one clustering method is currently supported: "TIME".
 * By default, no clustering.
 * @note This option is useless for triggers saved in a ROOT format. Clusters are nevered saved in ROOT files.
 *
 * @subsection omicron_readoptions_parameter_clusterdt Clustering time window
 * @verbatim
PARAMETER  CLUSTERDT  [PARAMETER]
@endverbatim
 * This option specifies the time window duration to cluster triggers in [s].
 * By default, = 0.1 s
 *
 * @subsection omicron_readoptions_parameter_windows Time windows for plots
 * @verbatim
PARAMETER  WINDOWS  [PARAMETERS]
@endverbatim
 * This option specifies the list of time windows for plots. There is no limit on the number of parameters.
 * By default, the @ref omicron_readoptions_parameter_timing "chunk duration" minus the "overlap duration" is used.
 *
 * @subsection omicron_readoptions_parameter_maplogscale Log scale for SNR scales in plots
 * @verbatim
PARAMETER  MAPLOGSCALE  [PARAMETER]
@endverbatim
 * This option, when different from 0, activates the log scale for the color scale of spectrograms.
 * By default, a log scale is used.
 *
 * @subsection omicron_readoptions_parameter_mapvrange Vertical range for spectrograms
 * @verbatim
PARAMETER  MAPVRANGE  [PARAMETERS]
@endverbatim
 * This option specifies the vertical range for spectrogram plots. Use an impossible range (e.g. 1 0) for an automatic scaling.
 *
 * @subsection omicron_readoptions_parameter_thumb Use thumbnails for web pages
 * @verbatim
PARAMETER  THUMB  [PARAMETER]
@endverbatim
 * When producing a web report (`html` in the @ref omicron_readoptions_output_products "OUTPUT/PRODUCTS"), spectrogram thumbnails can be produced. This improves the resolutions of images in the web page. It also speed-up the loading of the page. Producing thumbnails has a cost on the processing speed. If thumbnails are not produced, a simple browser rescaling of the image is used. Use a non zero value to activate the thumbnail production. By default, thumbnails are produced.
 *
 * @subsection omicron_readoptions_parameter_fftplan FFT plan
 * @verbatim
PARAMETER  FFTPLAN  [PARAMETER]
@endverbatim
 * This option specifies the plan to perform Fourier transforms with FFTW.
 * By default = "FFTW_MEASURE".
 *
 * @subsection omicron_readoptions_parameter_triggerratemax Maximum trigger rate
 * @verbatim
PARAMETER  TRIGGERRATEMAX [PARAMETER]
@endverbatim
 * This option specifies maximum trigger rate limit [Hz] when saving triggers to files. If the trigger rate is above this value (over the @ref omicron_readoptions_parameter_timing "analysis window"), the file is not saved.
 * By default = 5000 Hz.
 *
 * @subsection omicron_readoptions_parameter_triggerbuffersize Trigger buffer size
 * @verbatim
PARAMETER  TRIGGERBUFFERSIZE  [PARAMETER]
@endverbatim
 * This option is used for online applications. Triggers produced by omicron can be buffered. This parameter defines the size of the buffer.
 * By default, there is no buffer (size=0).
 *
 * @subsection omicron_readoptions_parameter_chirp Newtonian chirp
 * @verbatim
PARAMETER  CHIRP [PARAMETERS]
@endverbatim
 * With this option, it is possible to draw a Newtonian chirp on top of omicron spectrograms. There can be one or two parameters:
 * - A chirp mass in solar masses: the chirp is drawn with an end time positioned at the center of the @ref omicron_readoptions_parameter_timing "analysis window".
 * - A chirp mass in solar masses + a GPS time [s]: the chirp is drawn with an end time positioned at the requested GPS time.
 *
 * By default: no chirp.
 *
 * @subsection omicron_readoptions_injection INJECTION
 *
 * @subsection omicron_readoptions_injection_channels Injection channel list 
 * @verbatim
INJECTION  CHANNELS [PARAMETERS]
@endverbatim
 * This option specifies the list of channels to be used as injection signals. There should be as many channels as listed in the @ref omicron_readoptions_data_channels "main channel list".
 *
 * @subsection omicron_readoptions_injection_factors Injection scaling factors
 * @verbatim
INJECTION  FACTORS [PARAMETERS]
@endverbatim
 * This option specifies the list of amplitude scaling factors used to inject signals. There should be as many factors as the number of @ref omicron_readoptions_injection_channels "injection channels".
 *
 * @subsection omicron_readoptions_injection_ffl Frame file list 
 * @verbatim
INJECTION  FFL [PARAMETERS]
@endverbatim
 * If the @ref omicron_readoptions_injection_channels "injection channel(s)" are not included in the @ref omicron_readoptions_data_ffl "main ffl file", this option specifies an additional ffl file. `[PARAMETER]` is the relative or absolute path to the ffl/lcf file. This parameter can be complemented by 2 optional parameters which are used to load the FFL file multiple times in a row:
 * - Number of retries to load the FFL,
 * - Time in [s] between two retries.
 *
 * @subsection omicron_readoptions_injection_injgen InjGen injections
 * @verbatim
INJECTION  INJGEN [PARAMETERS]
@endverbatim
 * Injections can also be performed with injection files listing the source/waveform parameters. The injection files must be ROOT files generated with the GWOLLUM/InjGen class. This option provides a list of file patterns to the injection files.
 *
 * @subsection omicron_readoptions_injection_tukeyfrac Tukey window
 * @verbatim
INJECTION  TUKEYFRAC [PARAMETER]
@endverbatim
 * When the @ref omicron_readoptions_injection_injgen "InjGen injections" option is active, the injected waveform is Tukey-windowed before being added to the data. With this option, you can set the fraction of the time used to transition from 0 to 1 and then from 1 to 0. By default, this parameter is set to 0.1. It means that it takes 5% of the waveform to transition from 0 to 1 and 5% of the waveform to transition from 1 to 0.
 *
 * @subsection omicron_readoptions_injection_sg Sine-Gauss injections
 * @verbatim
INJECTION  SG [PARAMETER]
@endverbatim
 * Sine-gauss injections can be performed by setting `[PARAMETER]` to a value different from 0. One waveform is injected in every @ref omicron_readoptions_parameter_timing "analysis window".
 *
 * @subsection omicron_readoptions_injection_sgtime Sine-Gauss injection time
 * @verbatim
INJECTION  SGTIME [PARAMETERS]
@endverbatim
* By default, sine-Gaussian waveforms are always injected at the center of the @ref omicron_readoptions_parameter_timing "analysis window". With this option, the time of the injection can be taken as a random value in a given time range. The time range is defined with 2 values: a negative and a positive range from the center (in seconds).
 *
 * @subsection omicron_readoptions_injection_sgfrequency Sine-Gauss injection frequency
 * @verbatim
INJECTION  SGFREQUENCY [PARAMETERS]
@endverbatim
* With this option, the frequency of the injection is taken as a random value in a given frequency range, following a logarithmic distribution. If only one value is provided, the injection frequency is fixed at that value.
 *
 * @subsection omicron_readoptions_injection_sgq Sine-Gauss injection Q
 * @verbatim
INJECTION  SGQ [PARAMETERS]
@endverbatim
* With this option, the quality factor of the injection is taken as a random value in a given range, following a logarithmic distribution. If only one value is provided, the injection Q is fixed at that value.
 *
 * @subsection omicron_readoptions_injection_sgamplitude Sine-Gauss amplitude
 * @verbatim
INJECTION  SGAMPLITUDE [PARAMETERS]
@endverbatim
* With this option, the amplitude of the injection is taken as a random value in a given range, following a logarithmic distribution. If only one value is provided, the injection amplitude is fixed at that value.
 *
 */
void Omicron::ReadOptions(const string aOptFile, const bool aStrict){

  // get subdirectory if any
  vector<string> subdir = SplitString(aOptFile, '|');

  // temporary status
  bool status_tmp = true;
  
  // the option file is a root file
  if(!subdir[0].substr(subdir[0].length()-5).compare(".root")){
    if(subdir.size()>1)
      status_tmp = GwollumOptions::ReadTree(subdir[0], subdir[1]+"/gwloptions") && status_tmp;
    else
      status_tmp = GwollumOptions::ReadTree(subdir[0], "gwloptions") && status_tmp;
  }

  // number of options
  unsigned nopt = GwollumOptions::GetOptionsN();

  // add options if necessary (to complete the input TTree)
  if(!GwollumOptions::Is("OUTPUT", "DIRECTORY")) GwollumOptions::Add("OUTPUT", "DIRECTORY", ".", 1);
  if(!GwollumOptions::Is("OUTPUT", "PRODUCTS")) GwollumOptions::Add("OUTPUT", "PRODUCTS", "triggers", 0);
  if(!GwollumOptions::Is("OUTPUT", "FORMAT")) GwollumOptions::Add("OUTPUT", "FORMAT", "root", 0);
  if(!GwollumOptions::Is("OUTPUT", "VERBOSITY")) GwollumOptions::Add("OUTPUT", "VERBOSITY", (unsigned int)0, 1);
  if(!GwollumOptions::Is("OUTPUT", "STYLE")) GwollumOptions::Add("OUTPUT", "STYLE", "GWOLLUM", 1);
  if(!GwollumOptions::Is("OUTPUT", "NOLOGO")) GwollumOptions::Add("OUTPUT", "NOLOGO", (unsigned int)0, 1);
  if(!GwollumOptions::Is("OUTPUT", "PLOTDIMENSIONS")) GwollumOptions::Add("OUTPUT", "PLOTDIMENSIONS", (unsigned int)800, 2);
  if(!GwollumOptions::Is("OUTPUT", "HTMLMAINPAGE")) GwollumOptions::Add("OUTPUT", "HTMLMAINPAGE", "index.html", 1);
  if(!GwollumOptions::Is("DATA", "FFL")) GwollumOptions::Add("DATA", "FFL", "1", 3);
  if(!GwollumOptions::Is("DATA", "LCF")) GwollumOptions::Add("DATA", "LCF", "1", 3);// deprecated
  if(!GwollumOptions::Is("DATA", "SAMPLEFREQUENCY")) GwollumOptions::Add("DATA", "SAMPLEFREQUENCY", (unsigned int)16, 1);
  if(!GwollumOptions::Is("DATA", "CHANNELS")) GwollumOptions::Add("DATA", "CHANNELS", "M1:MISSING", 0);
  if(!GwollumOptions::Is("DATA", "BLACKLISTEDCHANNELS")) GwollumOptions::Add("DATA", "BLACKLISTEDCHANNELS", "", 0);
  if(!GwollumOptions::Is("PARAMETER", "TIMING")) GwollumOptions::Add("PARAMETER", "TIMING", (unsigned int)64, 2);
  if(!GwollumOptions::Is("PARAMETER", "FREQUENCYRANGE")) GwollumOptions::Add("PARAMETER", "FREQUENCYRANGE", 2.0, 2);
  if(!GwollumOptions::Is("PARAMETER", "QRANGE")) GwollumOptions::Add("PARAMETER", "QRANGE", 4.0, 2);
  if(!GwollumOptions::Is("PARAMETER", "MISMATCHMAX")) GwollumOptions::Add("PARAMETER", "MISMATCHMAX", 0.25, 1);
  if(!GwollumOptions::Is("PARAMETER", "FULLMAPNT")) GwollumOptions::Add("PARAMETER", "FULLMAPNT", (unsigned int)301, 1);
  if(!GwollumOptions::Is("PARAMETER", "SNRTHRESHOLD")) GwollumOptions::Add("PARAMETER", "SNRTHRESHOLD", 7.0, 2);
  if(!GwollumOptions::Is("PARAMETER", "PSDLENGTH")) GwollumOptions::Add("PARAMETER", "PSDLENGTH", (unsigned int)0, 2);
  if(!GwollumOptions::Is("PARAMETER", "HIGHPASS")) GwollumOptions::Add("PARAMETER", "HIGHPASS", 0.0, 1);
  if(!GwollumOptions::Is("PARAMETER", "CLUSTERING")) GwollumOptions::Add("PARAMETER", "CLUSTERING", "", 1);
  if(!GwollumOptions::Is("PARAMETER", "CLUSTERDT")) GwollumOptions::Add("PARAMETER", "CLUSTERDT", 0.1, 1);
  if(!GwollumOptions::Is("PARAMETER", "WINDOWS")) GwollumOptions::Add("PARAMETER", "WINDOWS", (unsigned int)0, 0);
  if(!GwollumOptions::Is("PARAMETER", "MAPLOGSCALE")) GwollumOptions::Add("PARAMETER", "MAPLOGSCALE", (unsigned int)1, 1);
  if(!GwollumOptions::Is("PARAMETER", "MAPVRANGE")) GwollumOptions::Add("PARAMETER", "MAPVRANGE", 0.0, 2);
  if(!GwollumOptions::Is("PARAMETER", "FFTPLAN")) GwollumOptions::Add("PARAMETER", "FFTPLAN", "FFTW_MEASURE", 1);
  if(!GwollumOptions::Is("PARAMETER", "TRIGGERRATEMAX")) GwollumOptions::Add("PARAMETER", "TRIGGERRATEMAX", 5000.0, 1);
  if(!GwollumOptions::Is("PARAMETER", "TRIGGERBUFFERSIZE")) GwollumOptions::Add("PARAMETER", "TRIGGERBUFFERSIZE", (unsigned int)0, 1);
  if(!GwollumOptions::Is("PARAMETER", "CHIRP")) GwollumOptions::Add("PARAMETER", "CHIRP", -1.0, 2);
  if(!GwollumOptions::Is("PARAMETER", "THUMB")) GwollumOptions::Add("PARAMETER", "THUMB", (unsigned int)1, 1);
  if(!GwollumOptions::Is("PARAMETER", "STRICTMODE")) GwollumOptions::Add("PARAMETER", "STRICTMODE", (unsigned int)0, 1);
  if(!GwollumOptions::Is("INJECTION", "CHANNELS")) GwollumOptions::Add("INJECTION", "CHANNELS", "", 0);
  if(!GwollumOptions::Is("INJECTION", "FACTORS")) GwollumOptions::Add("INJECTION", "FACTORS", 1.0, 0);
  if(!GwollumOptions::Is("INJECTION", "FFL")) GwollumOptions::Add("INJECTION", "FFL", "1", 3);
  if(!GwollumOptions::Is("INJECTION", "LCF")) GwollumOptions::Add("INJECTION", "LCF", "1", 3);// deprecated
  if(!GwollumOptions::Is("INJECTION", "INJGEN")) GwollumOptions::Add("INJECTION", "INJGEN", "", 0);
  if(!GwollumOptions::Is("INJECTION", "TUKEYFRAC")) GwollumOptions::Add("INJECTION", "TUKEYFRAC", 0.1, 1);
  if(!GwollumOptions::Is("INJECTION", "SG")) GwollumOptions::Add("INJECTION", "SG", (unsigned int)0, 1);
  if(!GwollumOptions::Is("INJECTION", "SGTIME")) GwollumOptions::Add("INJECTION", "SGTIME", 0.0, 2);
  if(!GwollumOptions::Is("INJECTION", "SGFREQUENCY")) GwollumOptions::Add("INJECTION", "SGFREQUENCY", 123.4, 2);
  if(!GwollumOptions::Is("INJECTION", "SGQ")) GwollumOptions::Add("INJECTION", "SGQ", 4.0, 2);
  if(!GwollumOptions::Is("INJECTION", "SGAMPLITUDE")) GwollumOptions::Add("INJECTION", "SGAMPLITUDE", 1.0e-21, 2);

  // the option file is a txt file
  if(subdir[0].substr(subdir[0].length()-5).compare(".root")){
    nopt = GwollumOptions::ReadTextFile(subdir[0]);
  }

  // init monitoring
  mon = new Monitor("omicron");
  mon->Monitor::SetVerbosityLevel(GwollumOptions::GetOptionValueU("OUTPUT", "VERBOSITY", 0));
  mon->Monitor::SetStatus(status_tmp);
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;

  // print the number of options
  mon->Monitor::SetMessageType(monitor_info_4);
  *mon<<"Omicron::"<<__func__<<": number of options = "<<nopt<<endl;
  
  // test options which MUST be provided
  if(!GwollumOptions::IsProvided("DATA", "CHANNELS")){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the list of channels must be provided (DATA/CHANNELS)"<<endl;
  }
  if(!GwollumOptions::IsProvided("OUTPUT","DIRECTORY")){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the output directory must be provided (OUTPUT/DIRECTORY)"<<endl;
  }
  if(!GwollumOptions::IsProvided("PARAMETER", "TIMING")){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the chunk timing must be provided (PARAMETER/TIMING)"<<endl;
  }
  if(!GwollumOptions::IsProvided("PARAMETER", "FREQUENCYRANGE")){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the frequency range must be provided (PARAMETER/FREQUENCYRANGE)"<<endl;
  }
  if(!GwollumOptions::IsProvided("PARAMETER", "QRANGE")){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the Q range must be provided (PARAMETER/QRANGE)"<<endl;
  }
  if(aStrict){
    if(!GwollumOptions::IsProvided("OUTPUT", "PRODUCTS")){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": the list of output products be provided (OUTPUT/PRODUCTS) - STRICT MODE"<<endl;
    }
    if(!GwollumOptions::IsProvided("OUTPUT", "FORMAT")){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": the list of output formats must be provided (OUTPUT/FORMAT) - STRICT MODE"<<endl;
    }
    if(!GwollumOptions::IsProvided("DATA", "SAMPLEFREQUENCY")){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": the sampling frequency must be provided (DATA/SAMPLEFREQUENCY) - STRICT MODE"<<endl;
    }
    if(!GwollumOptions::IsProvided("PARAMETER", "TRIGGERRATEMAX")){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": the maximum trigger rate must be provided (PARAMETER/TRIGGERRATEMAX) - STRICT MODE"<<endl;
    }
    if(!GwollumOptions::IsProvided("PARAMETER", "PSDLENGTH")){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": the PSD length must be provided (PARAMETER/PSDLENGTH) - STRICT MODE"<<endl;
    }
  }

  // save the strict-mode option
  GwollumOptions::SetOptionValue("PARAMETER", "STRICTMODE", 0, (unsigned int)aStrict);

  // sampling frequency: must be at least 16 Hz
  if(GwollumOptions::GetOptionValueU("DATA", "SAMPLEFREQUENCY", 0)<16){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the working sampling frequency (DATA/SAMPLEFREQUENCY) should be at least 16 Hz"<<endl;
    GwollumOptions::SetDefault("DATA", "SAMPLEFREQUENCY");
  }

  // the output directory must exist
  if(!filesystem::is_directory(GwollumOptions::GetOptionValueS("OUTPUT", "DIRECTORY", 0))){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the output directory (OUTPUT/DIRECTORY) must exist"<<endl;
    GwollumOptions::SetDefault("OUTPUT", "DIRECTORY");
  }

  // adjust plot dimensions if not given
  if(!GwollumOptions::IsProvided("OUTPUT", "PLOTDIMENSIONS")){
    GwollumOptions::SetOptionValue("OUTPUT", "PLOTDIMENSIONS", 0, (unsigned int)(GwollumOptions::GetOptionValueU("OUTPUT", "PLOTDIMENSIONS",1)*GOLDENNUMBER));
  }

  // check frequency range
  if(GwollumOptions::GetOptionValueD("PARAMETER", "FREQUENCYRANGE", 0) > GwollumOptions::GetOptionValueD("PARAMETER", "FREQUENCYRANGE", 1)){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the frequency range (PARAMETER/FREQUENCYRANGE) is not valid"<<endl;
  }
  
  // check Q range
  if(GwollumOptions::GetOptionValueD("PARAMETER", "QRANGE", 0) > GwollumOptions::GetOptionValueD("PARAMETER", "QRANGE", 1)){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the Q range (PARAMETER/QRANGE) is not valid"<<endl;
  }

  // optimize speed if maps are not requested
  vector<string> list_of_output_products = GwollumOptions::GetOptionValuesS("OUTPUT", "PRODUCTS");
  unsigned int found=0;
  for(found=0; found<list_of_output_products.size(); found++){
    if(list_of_output_products[found].find("map")!=string::npos) break;
  }
  if(found==list_of_output_products.size())
    GwollumOptions::SetOptionValue("PARAMETER", "FULLMAPNT", 0, (unsigned int)1);
  list_of_output_products.clear();

  return;
}


////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeOptions(void){
////////////////////////////////////////////////////////////////////////////////////

  // output directory
  GO_MainDir = GwollumOptions::GetOptionValueS("OUTPUT", "DIRECTORY", 0);
  MainDir = GO_MainDir;
  
  // output product string
  vector<string> svec = GwollumOptions::GetOptionValuesS("OUTPUT", "PRODUCTS");
  for(unsigned int i=0; i<svec.size(); i++)
    GO_OutProducts += (svec[i] + " ");
  svec.clear();

  // output product formats
  svec = GwollumOptions::GetOptionValuesS("OUTPUT", "FORMAT");
  for(unsigned int i=0; i<svec.size(); i++)
    GO_OutFormat += (svec[i] + " ");
  svec.clear();

  // list of injections
  GO_InjChan = GwollumOptions::GetOptionValuesS("INJECTION", "CHANNELS");
  GO_InjFact = GwollumOptions::GetOptionValuesD("INJECTION", "FACTORS");
  while(GO_InjFact.size()<GO_InjChan.size()) GO_InjFact.push_back(1.0);
  if(!GO_InjChan[0].compare("")){
    GO_InjChan.clear();
    GO_InjFact.clear();
  }
  
  // flag for SG injections
  GO_InjSg = (bool)(GwollumOptions::GetOptionValueU("INJECTION", "SG", 0));
  
  // maximum trigger rate
  GO_RateMax = GwollumOptions::GetOptionValueD("PARAMETER", "TRIGGERRATEMAX", 0);
    
  // PSD flag for initialization
  GO_psd_init_force = GwollumOptions::GetOptionValueD("PARAMETER", "PSDLENGTH", 1);
    
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeFfl(const unsigned int aGpsRef){
////////////////////////////////////////////////////////////////////////////////////

  // a FFL/LCF is provided
  string sval1 = GwollumOptions::GetOptionValueS("DATA","FFL", 0);
  string sval2 = GwollumOptions::GetOptionValueS("DATA","LCF", 0);// deprecated

  // a FFL is provided
  if(sval1.compare("1")){
    FFL = new ffl(sval1,
                  GwollumOptions::GetOptionValueS("OUTPUT", "STYLE", 0),
                  mon->GetVerbosityLevel(),
                  GO_MainDir,
                  stoul(GwollumOptions::GetOptionValueS("DATA","FFL", 1)),
                  stoul(GwollumOptions::GetOptionValueS("DATA","FFL", 2)));
  }
  // a LCF option is provided (deprecated)
  else if(sval2.compare("1")){
    FFL = new ffl(sval2,
                  GwollumOptions::GetOptionValueS("OUTPUT", "STYLE", 0),
                  mon->GetVerbosityLevel(),
                  GO_MainDir,
                  stoul(GwollumOptions::GetOptionValueS("DATA","LCF", 1)),
                  stoul(GwollumOptions::GetOptionValueS("DATA","LCF", 2)));
  }
  // no FFL
  else FFL = NULL;
  
  // configure the FFL object
  if(FFL!=NULL){
    FFL->SetPlotName("mainffl");
    mon->UpdateStatus(FFL->ExtractChannels(aGpsRef));
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeTriggers(void){
////////////////////////////////////////////////////////////////////////////////////

  unsigned int bl;
  
  // get list of channels
  vector<string> list_of_input_channels = GwollumOptions::GetOptionValuesS("DATA", "CHANNELS");
  vector<string> list_of_bl_channels = GwollumOptions::GetOptionValuesS("DATA", "BLACKLISTEDCHANNELS");
  vector<string> list_of_filtered_channels;

  // get options
  unsigned int sampling = GwollumOptions::GetOptionValueU("DATA", "SAMPLEFREQUENCY", 0);
  double highpass = GwollumOptions::GetOptionValueD("PARAMETER", "HIGHPASS", 0);
  double cluster_dt = GwollumOptions::GetOptionValueD("PARAMETER", "CLUSTERDT", 0);
  unsigned int bufsize = GwollumOptions::GetOptionValueU("PARAMETER", "TRIGGERBUFFERSIZE", 0);
  
  // no FFL: save all channels
  if(FFL==NULL){

    for(unsigned int c=0; c<list_of_input_channels.size(); c++){

      // check if the channel is blacklisted
      for(bl=0; bl<list_of_bl_channels.size(); bl++){
	if(!fnmatch(list_of_bl_channels[bl].c_str(), list_of_input_channels[c].c_str(), FNM_NOESCAPE)){ // black-listed
          mon->Monitor::SetMessageType(monitor_info_4);
          *mon<<"Omicron::"<<__func__<<": "<<list_of_input_channels[c]<<" is blacklisted --> ignore"<<endl;
          break;
	}
      }

      // channel is blacklisted --> remove
      if(bl<list_of_bl_channels.size()) continue;

      // save channel
      channels.push_back(new Ochannel(bufsize, list_of_input_channels[c], mon->GetVerbosityLevel()));

      // configure
      channels.back()->GwollumOptions::Copy((GwollumOptions*)this);
      channels.back()->SetProcessName("OMICRON");
      channels.back()->SetProcessVersion((string)O_PROJECT_VERSION);
      channels.back()->FillTree();
      channels.back()->SetDCRemoval(true);
      if(channels.back()->SetFrequencies(sampling, sampling, 0.0)==false){
        mon->Monitor::SetMessageType(monitor_fatal);
        *mon<<"Omicron::"<<__func__<<": cannot set frequencies for "<<channels.back()->GetName()<<endl;
      }
      if(channels.back()->SetHighPassFrequency(highpass)==false){
        mon->Monitor::SetMessageType(monitor_fatal);
        *mon<<"Omicron::"<<__func__<<": cannot set high-pass frequency for "<<channels.back()->GetName()<<endl;
      }
      channels.back()->SetClusterizeDt(cluster_dt);
    }
  }

  // use the FFL file to test if the channel exists
  else{
    
    for(unsigned int c1=0; c1<list_of_input_channels.size(); c1++){

      // get list of existing channels
      list_of_filtered_channels = FFL->GetChannelList(list_of_input_channels[c1]);

      // no matching channels
      if(list_of_filtered_channels.size()==0){
        mon->Monitor::SetMessageType(monitor_fatal);
        *mon<<"Omicron::"<<__func__<<": no channels matching "<<list_of_input_channels[c1]<<" --> ignore"<<endl;
        continue;
      }

      // check if channel is valid
      for(unsigned int c2=0; c2<list_of_filtered_channels.size(); c2++){

        // check sampling frequency
	if(FFL->GetChannelSampling(list_of_filtered_channels[c2])<sampling){
          mon->Monitor::SetMessageType(monitor_warning);
          *mon<<"Omicron::"<<__func__<<": "<<list_of_filtered_channels[c2]<<" sampling frequency is too low --> ignore"<<endl;
	  continue;
	}

        // check if the channel is blacklisted
        for(bl=0; bl<list_of_bl_channels.size(); bl++){
          if(!fnmatch(list_of_bl_channels[bl].c_str(), list_of_filtered_channels[c2].c_str(), FNM_NOESCAPE)){ // black-listed
            mon->Monitor::SetMessageType(monitor_info_4);
            *mon<<"Omicron::"<<__func__<<": "<<list_of_filtered_channels[c2]<<" is blacklisted --> ignore"<<endl;
            break;
          }
        }

        // channel is blacklisted --> remove
        if(bl<list_of_bl_channels.size()) continue;

        // save channel
        channels.push_back(new Ochannel(bufsize, list_of_filtered_channels[c2], mon->GetVerbosityLevel()));

        // configure
        channels.back()->GwollumOptions::Copy((GwollumOptions*)this);
	channels.back()->SetProcessName("OMICRON");
	channels.back()->SetProcessVersion((string)O_PROJECT_VERSION);
        channels.back()->FillTree();
        channels.back()->SetDCRemoval(true);
        if(channels.back()->SetFrequencies(FFL->GetChannelSampling(list_of_filtered_channels[c2]), sampling, 0.0)==false){
          mon->Monitor::SetMessageType(monitor_fatal);
          *mon<<"Omicron::"<<__func__<<": cannot set frequencies for "<<channels.back()->GetName()<<endl;
        }
        if(channels.back()->SetHighPassFrequency(highpass)==false){
          mon->Monitor::SetMessageType(monitor_fatal);
          *mon<<"Omicron::"<<__func__<<": cannot set high-pass frequency for "<<channels.back()->GetName()<<endl;
        }
        channels.back()->SetClusterizeDt(cluster_dt);
      }
    }
  }

  // cleaning
  list_of_input_channels.clear();
  list_of_bl_channels.clear();
  list_of_filtered_channels.clear();

  // empty list --> default channel (not to crash)
  if(channels.size()==0){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": the list of channels is empty"<<endl;
    channels.push_back(new Ochannel(bufsize, "M1:MISSING", mon->GetVerbosityLevel()));
    channels.back()->GwollumOptions::Copy((GwollumOptions*)this);
    channels.back()->FillTree();
    channels.back()->SetDCRemoval(true);
    mon->UpdateStatus(channels.back()->SetFrequencies(sampling, sampling, 0.0));
    mon->UpdateStatus(channels.back()->SetHighPassFrequency(highpass));
    channels.back()->SetClusterizeDt(cluster_dt);
  }

  // check the match with injection channels
  if(GO_InjChan.size()>0){
    if(channels.size()!=GO_InjChan.size()){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": mismatch with the list of injection channels"<<endl;
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeTiling(void){
////////////////////////////////////////////////////////////////////////////////////

  // create tiling
  tile = new Otile(GwollumOptions::GetOptionValueU("PARAMETER", "TIMING", 0), GwollumOptions::GetOptionValueU("PARAMETER", "TIMING", 1),
                   GwollumOptions::GetOptionValueD("PARAMETER", "QRANGE", 0), GwollumOptions::GetOptionValueD("PARAMETER", "QRANGE", 1),
                   GwollumOptions::GetOptionValueD("PARAMETER", "FREQUENCYRANGE", 0), GwollumOptions::GetOptionValueD("PARAMETER", "FREQUENCYRANGE", 1),
                   channels[0]->GetWorkingFrequency(),
                   GwollumOptions::GetOptionValueD("PARAMETER", "MISMATCHMAX", 0),
                   GwollumOptions::GetOptionValueU("PARAMETER", "FULLMAPNT", 0),
                   GwollumOptions::GetOptionValueS("OUTPUT", "STYLE", 0),
                   mon->GetVerbosityLevel());

  // tiling plot size
  tile->ResizePlot(GwollumOptions::GetOptionValueU("OUTPUT","PLOTDIMENSIONS",0), GwollumOptions::GetOptionValueU("OUTPUT","PLOTDIMENSIONS",1));

  // tiling type
  if(GO_OutProducts.find("mapsnr")!=string::npos) tile->SetMapFill("snr");
  else if(GO_OutProducts.find("mapamplitude")!=string::npos) tile->SetMapFill("amplitude");
  else if(GO_OutProducts.find("mapphase")!=string::npos) tile->SetMapFill("phase");
  else tile->SetMapFill("snr");

  // SNR threshold
  tile->SetSnrThr(GwollumOptions::GetOptionValueD("PARAMETER", "SNRTHRESHOLD", 1), GwollumOptions::GetOptionValueD("PARAMETER", "SNRTHRESHOLD", 0));

  // time windows
  tile->SetPlotTimeWindows(GwollumOptions::GetOptionValuesU("PARAMETER", "WINDOWS"));

  // vertical scale
  if(GwollumOptions::GetOptionValueD("PARAMETER", "MAPVRANGE", 0)==GwollumOptions::GetOptionValueD("PARAMETER", "MAPVRANGE", 1)){
    if(!tile->GetMapFill().compare("snr")) tile->SetRangez(1.0, 20.0);
    else if(!tile->GetMapFill().compare("amplitude")) tile->SetRangez(-1.0, -1.0);// automatic
    else tile->SetRangez(-TMath::Pi(), TMath::Pi());
  }
  else{
    tile->SetRangez(GwollumOptions::GetOptionValueD("PARAMETER", "MAPVRANGE", 0), GwollumOptions::GetOptionValueD("PARAMETER", "MAPVRANGE", 1));
  }

  // log scale
  tile->SetLogz(GwollumOptions::GetOptionValueU("PARAMETER", "MAPLOGSCALE", 0));
  if(tile->GetRangezMin()<=0.0) tile->SetLogz(0);

  // thumbnails
  GO_thumb = true;
  if(GwollumOptions::GetOptionValueU("PARAMETER", "THUMB", 0)==0) GO_thumb = false;

  // add chirp
  tile->SetChirp(GwollumOptions::GetOptionValueD("PARAMETER", "CHIRP", 0), GwollumOptions::GetOptionValueD("PARAMETER", "CHIRP", 1));

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeSpectrum(const bool aOneChannel){
////////////////////////////////////////////////////////////////////////////////////

  // PSD length
  unsigned int psdlength = GwollumOptions::GetOptionValueU("PARAMETER","PSDLENGTH", 0);
  if(psdlength==0){
    psdlength = tile->GetTimeRange() - tile->GetOverlapDuration();
    mon->Monitor::SetMessageType(monitor_warning);
    *mon<<"Omicron::"<<__func__<<": set data length for spectrum to "<<psdlength<<" s"<<endl;
  }

  // warning if the PSD length is shorter than the analysis window
  if(psdlength<tile->GetTimeRange() - tile->GetOverlapDuration()){
    mon->Monitor::SetMessageType(monitor_warning);
    *mon<<"Omicron::"<<__func__<<": the PSD length ("<<psdlength<<" s) is shorter than the analysis window ("<<tile->GetTimeRange() - tile->GetOverlapDuration()<<" s)"<<endl;
  }

  // channel optimization
  one_channel = channels.size();
  if(aOneChannel==false) one_channel=1;

  // create spectrum
  spectrum1 = new Spectrum* [channels.size()/one_channel];
  spectrum2 = new Spectrum* [channels.size()/one_channel];
  if(tile->GetFrequencyMin()>1.0){ // resolution = 0.5 Hz above 1 Hz

    for(unsigned int c=0; c<channels.size()/one_channel; c++){
      spectrum1[c] = new Spectrum(channels[c]->GetWorkingFrequency(),
                                  psdlength,
                                  channels[c]->GetWorkingFrequency(),
                                  mon->GetVerbosityLevel());
      spectrum2[c] = new Spectrum(channels[c]->GetWorkingFrequency(),
                                  psdlength,
                                  channels[c]->GetWorkingFrequency(),
                                  mon->GetVerbosityLevel());
    }
    spectrumw = new Spectrum(channels[0]->GetWorkingFrequency(),
                             tile->GetTimeRange()-tile->GetOverlapDuration(),
                             channels[0]->GetWorkingFrequency(),
                             mon->GetVerbosityLevel());
  }
  else{ // increase the resolution not to extrapolate the PSD.
    
    unsigned int psd_length_min = 2*NextPowerOfTwo((double)channels[0]->GetWorkingFrequency()/tile->GetFrequencyMin())/channels[0]->GetWorkingFrequency();
    if(psdlength<psd_length_min) psdlength = psd_length_min;
    
    for(unsigned int c=0; c<channels.size()/one_channel; c++){
      spectrum1[c] = new Spectrum(2*NextPowerOfTwo((double)channels[c]->GetWorkingFrequency()/tile->GetFrequencyMin()),
                                  psdlength,
                                  channels[c]->GetWorkingFrequency(),
                                  mon->GetVerbosityLevel());
      spectrum2[c] = new Spectrum(2*NextPowerOfTwo((double)channels[c]->GetWorkingFrequency()/tile->GetFrequencyMin()),
                                  psdlength,
                                  channels[c]->GetWorkingFrequency(),
                                  mon->GetVerbosityLevel());
    }
    
    // FIXME this will crash if tile->GetTimeRange()-tile->GetOverlapDuration() is too small
    spectrumw = new Spectrum(2*NextPowerOfTwo((double)channels[0]->GetWorkingFrequency()/tile->GetFrequencyMin()),
                             tile->GetTimeRange()-tile->GetOverlapDuration(),
                             channels[0]->GetWorkingFrequency(),
                             mon->GetVerbosityLevel());
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeInjections(const unsigned int aGpsRef){
////////////////////////////////////////////////////////////////////////////////////

  FFL_inject = NULL;

  // a FFL/LCF option is provided
  string sval1 = GwollumOptions::GetOptionValueS("INJECTION","FFL", 0);
  string sval2 = GwollumOptions::GetOptionValueS("INJECTION","LCF", 0);

  // a FFL is provided
  if(sval1.compare("1")){
    FFL_inject = new ffl(sval1,
                         GwollumOptions::GetOptionValueS("OUTPUT", "STYLE", 0),
                         mon->GetVerbosityLevel(),
                         GO_MainDir,
                         stoul(GwollumOptions::GetOptionValueS("INJECTION","FFL", 1)),
                         stoul(GwollumOptions::GetOptionValueS("INJECTION","FFL", 2)));
    FFL_inject->SetPlotName("injffl");
    mon->UpdateStatus(FFL_inject->ExtractChannels(aGpsRef));
  }
  // a LCF option is provided (deprecated)
  else if(sval2.compare("1")){
    FFL_inject = new ffl(sval2,
                         GwollumOptions::GetOptionValueS("OUTPUT", "STYLE", 0),
                         mon->GetVerbosityLevel(),
                         GO_MainDir,
                         stoul(GwollumOptions::GetOptionValueS("INJECTION","LCF", 1)),
                         stoul(GwollumOptions::GetOptionValueS("INJECTION","LCF", 2)));
    FFL_inject->SetPlotName("injffl");
    mon->UpdateStatus(FFL_inject->ExtractChannels(aGpsRef));
  }
  // no FFL: use the data FFL
  else{
    if(GO_InjChan.size()>0) FFL_inject = FFL;
  }
  
  // software injections
  sval1 = "";
  for(unsigned int i=0; i<GwollumOptions::GetOptionNumberOfValues("INJECTION", "INJGEN"); i++)
    sval1 += GwollumOptions::GetOptionValueS("INJECTION", "INJGEN", i)+" ";
  inject = new InjEct(sval1, GwollumOptions::GetOptionValueS("OUTPUT", "STYLE", 0), mon->GetVerbosityLevel());
  inject->SetTukeyFraction(GwollumOptions::GetOptionValueD("INJECTION", "TUKEYFRAC", 0));
    
  // Omicron injections
  oinj = new Oinject(tile->GetTimeRange());
  oinj->SetTimeRange(GwollumOptions::GetOptionValueD("INJECTION", "SSGTIME", 0), GwollumOptions::GetOptionValueD("INJECTION", "SSGTIME", 1));
  oinj->SetFrequencyRange(GwollumOptions::GetOptionValueD("INJECTION", "SGFREQUENCY", 0), GwollumOptions::GetOptionValueD("INJECTION", "SGFREQUENCY", 1));
  oinj->SetQRange(GwollumOptions::GetOptionValueD("INJECTION", "SGQ", 0), GwollumOptions::GetOptionValueD("INJECTION", "SGQ", 1));
  oinj->SetAmplitudeRange(GwollumOptions::GetOptionValueD("INJECTION", "SGAMPLITUDE", 0), GwollumOptions::GetOptionValueD("INJECTION", "SGAMPLITUDE", 1));

  return;
}



/**
 * @example parameters.txt
 * This is an example of a parameter file to run omicron (omicron.cc).
 */
