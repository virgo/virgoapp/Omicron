/**
 * @file 
 * @brief See Osequence.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Osequence.h"

ClassImp(Osequence)

////////////////////////////////////////////////////////////////////////////////////
Osequence::Osequence(const unsigned int aTimeRange, const unsigned int aTimeOverlap,
                     const unsigned int aVerbosity){
////////////////////////////////////////////////////////////////////////////////////
 
  // monitoring
  mon = new Monitor("osequence");
  mon->SetVerbosityLevel(aVerbosity);
  mon->SetMessageType(monitor_info_2);
  *mon<<"Osequence::"<<__func__<<endl;

  // init sequence
  SeqInSegments = new Segments();
  SeqOutSegments = new Segments();
  SeqTimeRange = aTimeRange;
  if(SeqTimeRange<4) SeqTimeRange = 4;
  if(SeqTimeRange%2) SeqTimeRange++;
  SeqOverlap = aTimeOverlap;
  if(SeqOverlap%2) SeqTimeRange++;
  SeqOverlapCurrent = SeqOverlap;
  SeqT0 = 0;
  SeqSeg = 0;
}

////////////////////////////////////////////////////////////////////////////////////
Osequence::~Osequence(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Osequence::"<<__func__<<endl;
  delete SeqInSegments;
  delete SeqOutSegments;
}

////////////////////////////////////////////////////////////////////////////////////
void Osequence::ResetSequence(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Osequence::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Osequence::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // reset sequence
  SeqOverlapCurrent = SeqOverlap;
  SeqT0 = 0;// for initialization in NewChunk()
  SeqSeg = 0;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
unsigned int Osequence::SetSegments(Segments *aInSeg, Segments *aOutSeg){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Osequence::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Osequence::"<<__func__<<": corrupted object"<<endl;
    return 0;
  }

  // reset sequence
  SeqInSegments->Reset(); SeqOutSegments->Reset();
  ResetSequence();

  // check input segments
  if(aInSeg==NULL || !aInSeg->GetStatus() || (aInSeg->GetN()==0)){
    mon->SetMessageType(monitor_error);
    *mon<<"Osequence::"<<__func__<<": input segments are corrupted"<<endl;
    return 0;
  }

  // monitor print
  mon->SetMessageType(monitor_info_4);
  *mon<<"Osequence::"<<__func__<<": input segments ("<<aInSeg->GetFirst()<<"-"<<aInSeg->GetLast()<<")"<<endl;
  if((aOutSeg!=NULL)&&(aInSeg->GetN()!=0)){
    mon->SetMessageType(monitor_info_4);
    *mon<<"Osequence::"<<__func__<<": output segments ("<<aOutSeg->GetFirst()<<"-"<<aOutSeg->GetLast()<<")"<<endl;
  }

  // update sequence segments
  unsigned int nchunks = 0;
  double st, en;
  for(unsigned int s=0; s<aInSeg->GetN(); s++){
    st = TMath::Floor(aInSeg->GetStart(s));
    en = TMath::Floor(aInSeg->GetEnd(s));
    if(!SeqInSegments->AddSegment(st, en)) return 0;
    if(en-st<(double)SeqTimeRange) continue;
    nchunks+=(unsigned int)TMath::Ceil((en-st-(double)SeqOverlap)/(double)(SeqTimeRange-SeqOverlap));
  }

  // ouput segments
  if(aOutSeg==NULL){// no selection
    if(!SeqOutSegments->Append(aInSeg)) return nchunks;
  }
  else{
    if(!SeqOutSegments->Append(aOutSeg)) return nchunks;
  }
  
  return nchunks;
}

////////////////////////////////////////////////////////////////////////////////////
bool Osequence::NewChunk(bool &aNewSegFlag){
////////////////////////////////////////////////////////////////////////////////////
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Osequence::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // no more segments
  if(SeqSeg>=SeqInSegments->GetN()){
    mon->SetMessageType(monitor_info_4);
    *mon<<"Osequence::"<<__func__<<": end of segments"<<endl;
    return false;
  }
  
  // current segment is too short
  if((unsigned int)SeqInSegments->GetLiveTime(SeqSeg)<GetTimeRange()){
    SeqSeg++; //  --> move to next segment
    SeqT0=0;  // flag a new segment start
    return NewChunk(aNewSegFlag);
  }

  // end of current segment
  if(SeqT0+SeqTimeRange/2==(unsigned int)SeqInSegments->GetEnd(SeqSeg)){
    SeqSeg++; //  --> move to next segment
    SeqT0=0;  // flag a new segment start
    return NewChunk(aNewSegFlag);
  }

  // initialization = start of current segment
  if(SeqT0==0){
    SeqT0=(unsigned int)SeqInSegments->GetStart(SeqSeg)-SeqTimeRange/2+SeqOverlap;
    aNewSegFlag=true;
  }
  else aNewSegFlag=false;

  // new test chunk
  SeqOverlapCurrent = SeqOverlap;// reset current overlap
  unsigned int start_test = SeqT0+SeqTimeRange/2-SeqOverlap;
  unsigned int stop_test  = start_test+SeqTimeRange;

  // chunk ends after current segment end --> adjust overlap
  if(stop_test>(unsigned int)SeqInSegments->GetEnd(SeqSeg)){
    SeqT0=(unsigned int)SeqInSegments->GetEnd(SeqSeg)-SeqTimeRange/2;
    SeqOverlapCurrent=start_test+SeqOverlap-SeqT0+SeqTimeRange/2;// --> adjust overlap
    return true;
  }

  // OK  
  mon->SetMessageType(monitor_info_4);
  *mon<<"Osequence::"<<__func__<<": "<<start_test<<"-"<<start_test+SeqTimeRange<<endl;
  SeqT0 = start_test+SeqTimeRange/2;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
Segments* Osequence::GetChunkOut(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->SetMessageType(monitor_info_2);
  *mon<<"Osequence::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->SetMessageType(monitor_fatal);
    *mon<<"Osequence::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }

  // output segments (remove 1/2 overlap)
  Segments *seg = new Segments((double)(SeqT0-SeqTimeRange/2+SeqOverlapCurrent-SeqOverlap/2),
                               (double)(SeqT0+SeqTimeRange/2-SeqOverlap/2));

  // apply user-defined output selection
  if(!seg->Intersect(SeqOutSegments)){
    delete seg;
    return NULL;
  }

  return seg;
}
