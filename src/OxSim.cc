/**
 * @file 
 * @brief See OxSim.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxSim.h"

ClassImp(OxSim)

////////////////////////////////////////////////////////////////////////////////////
OxSim::OxSim(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1):
OxEvent(aOptionFile, aGpsRef0, aGpsRef1){
////////////////////////////////////////////////////////////////////////////////////
  
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;

  // list of options
  OxInit::DefineOption("OXS", "NBINS", (unsigned int)20, 1);

  // add options from the text file
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxSim::"<<__func__<<": add OXS options"<<endl;
  OxInit::AddOptions();

  // overload some of the options (from text file)
  OxInit::OverloadOption("OXS", "NBINS");

  // maximum time delay between detectors
  double time_delay_max = OxOmicron::GetLightTravelTime()+ox_opt->GetOptionValueD("OXE", "SLICEDELTA", 0);

  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxSim::"<<__func__<<": prepare injection histograms"<<endl;

  // binning
  unsigned int nbins = ox_opt->GetOptionValueU("OXS", "NBINS", 0);
  double *fbins = new double [nbins+1];
  double *abins = new double [nbins+1];
  double *arbins = new double [nbins+1];

  // list of Omicron injections
  inject = OxOmicron::omicron[0]->GetInjectionEngine();
        
  // event parameters for each injection
  inj_rk = new double [inject->InjRea::GetN()];
  inj_entry = new Long64_t [inject->InjRea::GetN()];
    
  // injection plots / injection name
  h1_inj_time_tot = new TH1D* [inject->InjRea::GetInjectionNameN()];
  h1_inj_time_det = new TH1D* [inject->InjRea::GetInjectionNameN()];
  h1_inj_freq_tot = new TH1D* [inject->InjRea::GetInjectionNameN()];
  h1_inj_freq_det = new TH1D* [inject->InjRea::GetInjectionNameN()];
  h1_inj_amp_tot = new TH1D* [inject->InjRea::GetInjectionNameN()];
  h1_inj_amp_det = new TH1D* [inject->InjRea::GetInjectionNameN()];
    
  // reconstruction plots / injection name
  h2_rec_rank_amp = new TH2D* [inject->InjRea::GetInjectionNameN()];
  h1_rec_timedelay = new TH1D* [inject->InjRea::GetInjectionNameN()];
  h2_rec_timedelay = new TH2D* [inject->InjRea::GetInjectionNameN()];
  h2_rec_timedelay_amp = new TH2D* [inject->InjRea::GetInjectionNameN()];
  h1_rec_aratio = new TH1D* [inject->InjRea::GetInjectionNameN()];
  h2_rec_aratio = new TH2D* [inject->InjRea::GetInjectionNameN()];
  h2_rec_aratio_amp = new TH2D* [inject->InjRea::GetInjectionNameN()];

  // loop over injection names
  for(unsigned int n=0; n<inject->InjRea::GetInjectionNameN(); n++){
    
    // parameter bins
    for(unsigned int b=0; b<nbins+1; b++){
      fbins[b] = inject->InjRea::GetInjectionFrequencyMin(n)*pow(10.0, (double)b*log10(1.1*inject->InjRea::GetInjectionFrequencyMax(n)/inject->InjRea::GetInjectionFrequencyMin(n))/(double)nbins);
      abins[b] = inject->InjRea::GetInjectionAmplitudeMin(n)*pow(10.0, (double)b*log10(1.1*inject->InjRea::GetInjectionAmplitudeMax(n)/inject->InjRea::GetInjectionAmplitudeMin(n))/(double)nbins);
      arbins[b] = 0.02*pow(10.0, (double)b*log10(50.0/0.02)/(double)nbins);
    }

    // injection parameters
    h1_inj_time_tot[n] = new TH1D(("h1_inj_time_tot_"+inject->InjRea::GetInjectionName(n)).c_str(), "Injection time", nbins, inject->InjRea::GetInjectionTimeMin(n), inject->InjRea::GetInjectionTimeMax(n)+1);
    h1_inj_time_tot[n]->GetXaxis()->SetNoExponent(true);
    h1_inj_time_tot[n]->GetXaxis()->SetTitle("Injection GPS time [s]");
    h1_inj_time_tot[n]->GetYaxis()->SetTitle("Number of injections");
    h1_inj_time_tot[n]->GetYaxis()->SetMoreLogLabels(true);
    h1_inj_time_det[n] = new TH1D(("h1_inj_time_det_"+inject->InjRea::GetInjectionName(n)).c_str(), "Injection time", nbins, inject->InjRea::GetInjectionTimeMin(n), inject->InjRea::GetInjectionTimeMax(n)+1);
    h1_inj_time_det[n]->SetFillColor(h1_inj_time_det[n]->GetLineColor());
    h1_inj_time_det[n]->GetXaxis()->SetNoExponent(true);
    h1_inj_time_det[n]->GetXaxis()->SetTitle("Injection GPS time [s]");
    h1_inj_time_det[n]->GetYaxis()->SetTitle("Number of injections");
    h1_inj_time_det[n]->GetYaxis()->SetMoreLogLabels(true);
    h1_inj_freq_tot[n] = new TH1D(("h1_inj_freq_tot_"+inject->InjRea::GetInjectionName(n)).c_str(), "Injection frequency", nbins, fbins);
    h1_inj_freq_tot[n]->GetXaxis()->SetMoreLogLabels(true);
    h1_inj_freq_tot[n]->GetXaxis()->SetTitle("Injection frequency [Hz]");
    h1_inj_freq_tot[n]->GetYaxis()->SetTitle("Number of injections");
    h1_inj_freq_tot[n]->GetYaxis()->SetMoreLogLabels(true);
    h1_inj_freq_det[n] = new TH1D(("h1_inj_freq_det_"+inject->InjRea::GetInjectionName(n)).c_str(), "Injection frequency", nbins, fbins);
    h1_inj_freq_det[n]->SetFillColor(h1_inj_freq_det[n]->GetLineColor());
    h1_inj_freq_det[n]->GetXaxis()->SetMoreLogLabels(true);
    h1_inj_freq_det[n]->GetXaxis()->SetTitle("Injection frequency [Hz]");
    h1_inj_freq_det[n]->GetYaxis()->SetTitle("Number of injections");
    h1_inj_freq_det[n]->GetYaxis()->SetMoreLogLabels(true);
    h1_inj_amp_tot[n] = new TH1D(("h1_inj_amp_tot_"+inject->InjRea::GetInjectionName(n)).c_str(), "Injection amplitude", nbins, abins);
    h1_inj_amp_tot[n]->GetXaxis()->SetMoreLogLabels(true);
    h1_inj_amp_tot[n]->GetXaxis()->SetTitle("Injection amplitude");
    h1_inj_amp_tot[n]->GetYaxis()->SetTitle("Number of injections");
    h1_inj_amp_tot[n]->GetYaxis()->SetMoreLogLabels(true);
    h1_inj_amp_det[n] = new TH1D(("h1_inj_amp_det_"+inject->InjRea::GetInjectionName(n)).c_str(), "Injection amplitude", nbins, abins);
    h1_inj_amp_det[n]->SetFillColor(h1_inj_amp_det[n]->GetLineColor());
    h1_inj_amp_det[n]->GetXaxis()->SetMoreLogLabels(true);
    h1_inj_amp_det[n]->GetXaxis()->SetTitle("Injection amplitude");
    h1_inj_amp_det[n]->GetYaxis()->SetTitle("Number of injections");
    h1_inj_amp_det[n]->GetYaxis()->SetMoreLogLabels(true);
      
    // reconstruction
    h2_rec_rank_amp[n] = new TH2D(("h2_rec_rank_amp_"+inject->InjRea::GetInjectionName(n)).c_str(), "Rank of injections", nbins, abins, nbins, 0.0, 1.0);
    h2_rec_rank_amp[n]->GetXaxis()->SetMoreLogLabels(true);
    h2_rec_rank_amp[n]->GetZaxis()->SetMoreLogLabels(true);
    h2_rec_rank_amp[n]->GetXaxis()->SetTitle("Injection amplitude");
    h2_rec_rank_amp[n]->GetYaxis()->SetTitle("Event rank");
    h2_rec_rank_amp[n]->GetZaxis()->SetTitle("Number of valid injections");
    h1_rec_timedelay[n] = new TH1D(("h1_rec_timedelay_"+inject->InjRea::GetInjectionName(n)).c_str(), "Time-delay reconstruction", nbins, -time_delay_max, time_delay_max);
    h1_rec_timedelay[n]->SetStats(true);
    h1_rec_timedelay[n]->GetXaxis()->SetTitle("Time delay between detectors: (reconstructed - injected) [s]");
    h1_rec_timedelay[n]->GetYaxis()->SetTitle("Number of detected injections");
    h2_rec_timedelay[n] = new TH2D(("h2_rec_timedelay_"+inject->InjRea::GetInjectionName(n)).c_str(), "Time-delay reconstruction", nbins, -time_delay_max, time_delay_max, nbins, -time_delay_max, time_delay_max);
    h2_rec_timedelay[n]->GetZaxis()->SetMoreLogLabels(true);
    h2_rec_timedelay[n]->GetXaxis()->SetTitle("Injection time delay between detectors [s]");
    h2_rec_timedelay[n]->GetYaxis()->SetTitle("Reconstructed time delay between detectors [s]");
    h2_rec_timedelay[n]->GetZaxis()->SetTitle("Number of detected injections");
    h2_rec_timedelay_amp[n] = new TH2D(("h2_rec_timedelay_amp_"+inject->InjRea::GetInjectionName(n)).c_str(), "Time-delay reconstruction", nbins, abins, nbins, -time_delay_max, time_delay_max);
    h2_rec_timedelay_amp[n]->GetXaxis()->SetMoreLogLabels(true);
    h2_rec_timedelay_amp[n]->GetZaxis()->SetMoreLogLabels(true);
    h2_rec_timedelay_amp[n]->GetXaxis()->SetTitle("Injection amplitude");
    h2_rec_timedelay_amp[n]->GetYaxis()->SetTitle("Time delay between detectors: (reconstructed - injected) [s]");
    h2_rec_timedelay_amp[n]->GetZaxis()->SetTitle("Number of injections");
    h1_rec_aratio[n] = new TH1D(("h1_rec_aratio_"+inject->InjRea::GetInjectionName(n)).c_str(), "Amplitude ratio reconstruction", nbins, arbins);
    h1_rec_aratio[n]->SetStats(true);
    h1_rec_aratio[n]->GetXaxis()->SetTitle("Amplitude ratio: (reconstructed / injected)");
    h1_rec_aratio[n]->GetYaxis()->SetTitle("Number of detected injections");
    h2_rec_aratio[n] = new TH2D(("h2_rec_aratio_"+inject->InjRea::GetInjectionName(n)).c_str(), "Amplitude ratio reconstruction", nbins, arbins, nbins, arbins);
    h2_rec_aratio[n]->GetZaxis()->SetMoreLogLabels(true);
    h2_rec_aratio[n]->GetXaxis()->SetTitle("Injection amplitude ratio");
    h2_rec_aratio[n]->GetYaxis()->SetTitle("Reconstructed amplitude ratio");
    h2_rec_aratio[n]->GetZaxis()->SetTitle("Number of detected injections");
    h2_rec_aratio_amp[n] = new TH2D(("h2_rec_aratio_amp_"+inject->InjRea::GetInjectionName(n)).c_str(), "Amplitude ratio  reconstruction", nbins, abins, nbins, arbins);
    h2_rec_aratio_amp[n]->GetXaxis()->SetMoreLogLabels(true);
    h2_rec_aratio_amp[n]->GetZaxis()->SetMoreLogLabels(true);
    h2_rec_aratio_amp[n]->GetXaxis()->SetTitle("Injection amplitude");
    h2_rec_aratio_amp[n]->GetYaxis()->SetTitle("Amplitude ratio: (reconstructed / injected)");
    h2_rec_aratio_amp[n]->GetZaxis()->SetTitle("Number of injections");
  }
  
  delete [] fbins;
  delete [] abins;
  delete [] arbins;

  // reset all
  Reset();

  // extract injection results from Ox files
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxSim::"<<__func__<<": extract injection results from Ox files"<<endl;
  ExtractResults();

  // output
  oxs_tree = NULL;
}

////////////////////////////////////////////////////////////////////////////////////
OxSim::~OxSim(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;
  delete [] inj_rk;
  delete [] inj_entry;
  for(unsigned int n=0; n<inject->InjRea::GetInjectionNameN(); n++){
    delete h1_inj_time_tot[n];
    delete h1_inj_time_det[n];
    delete h1_inj_freq_tot[n];
    delete h1_inj_freq_det[n];
    delete h1_inj_amp_tot[n];
    delete h1_inj_amp_det[n];
    delete h2_rec_rank_amp[n];
    delete h1_rec_timedelay[n];
    delete h2_rec_timedelay[n];
    delete h2_rec_timedelay_amp[n];
    delete h1_rec_aratio[n];
    delete h2_rec_aratio[n];
    delete h2_rec_aratio_amp[n];
  }
  delete [] h1_inj_time_tot;
  delete [] h1_inj_time_det;
  delete [] h1_inj_freq_tot;
  delete [] h1_inj_freq_det;
  delete [] h1_inj_amp_tot;
  delete [] h1_inj_amp_det;
  delete [] h2_rec_rank_amp;
  delete [] h1_rec_timedelay;
  delete [] h2_rec_timedelay;
  delete [] h2_rec_timedelay_amp;
  delete [] h1_rec_aratio;
  delete [] h2_rec_aratio;
  delete [] h2_rec_aratio_amp;
}


////////////////////////////////////////////////////////////////////////////////////
void OxSim::Reset(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxSim::"<<__func__<<": the OxSim object is corrupted"<<endl;
    return;
  }

  // init: all injections are invalid
  for(Long64_t i=0; i<inject->InjRea::GetN(); i++){
    inject->InjRea::SetInjectionTag(i, false);
    inj_rk[i] = -1.0e20;
    inj_entry[i] = -1;
  }
  
  for(unsigned int n=0; n<inject->InjRea::GetInjectionNameN(); n++){
    h1_inj_amp_tot[n]->Reset();
    h1_inj_amp_det[n]->Reset();
    h1_inj_time_tot[n]->Reset();
    h1_inj_time_det[n]->Reset();
    h1_inj_freq_tot[n]->Reset();
    h1_inj_freq_det[n]->Reset();
    h2_rec_rank_amp[n]->Reset();
    h1_rec_timedelay[n]->Reset();
    h2_rec_timedelay[n]->Reset();
    h2_rec_timedelay_amp[n]->Reset();
    h1_rec_aratio[n]->Reset();
    h2_rec_aratio[n]->Reset();
    h2_rec_aratio_amp[n]->Reset();
  }
    
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxSim::Match(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxSim::"<<__func__<<": the OxSim object is corrupted"<<endl;
    return;
  }

  // no injections
  if(inject->InjRea::GetN()==0) return;
    
  // NOTE: we consider the geocentric time => we neglect the time propagation to the detector sites
    
  // search the first injection to consider
  Long64_t inj_index_first = inject->InjRea::GetInjectionIndexAfter(OxOmicron::GetOmicronStartTime(0));
  
  // loop over injections
  for(Long64_t i=inj_index_first; i<inject->InjRea::GetN(); i++){
    inject->InjRea::LoadInjection(i);
    
    // went too far --> stop
    if(inject->InjRea::GetInjectionTime()>=OxOmicron::GetOmicronEndTime(0)) break;
    
    // the injection is valid
    inject->InjRea::SetInjectionTag(true);
    
    // save the event entry for this injection (use the best rank)
    if(OxEvent::GetEventRank()>inj_rk[i]){
      inj_rk[i] = OxEvent::GetEventRank();
      inj_entry[i] = OxEvent::GetEventEntryNumber();
    }
  }
    
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxSim::Process(const double aRankDetectionThreshold){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxSim::"<<__func__<<": the OxSim object is corrupted"<<endl;
    return;
  }

  unsigned int inj_name_index = 0;
  double timedelay = 0.0;
  double local_time_0 = 0.0;
  double local_time_1 = 0.0;
  double Fp = 0.0;
  double Fc = 0.0;
  double Fr = 0.0;
  double gmst = 0.0;
  
  // list of missed injections
  ofstream ofs_missed[inject->InjRea::GetInjectionNameN()];
  for(unsigned int n=0; n<inject->InjRea::GetInjectionNameN(); n++){
    ofs_missed[n].open((OxInit::GetOutputDirectory()+"/"+inject->InjRea::GetInjectionName(n)+"_missed.txt").c_str(), ios_base::out);
    if(ofs_missed[n].is_open()){
      ofs_missed[n]<<"# injection amplitude"<<endl;
      ofs_missed[n]<<"# injection geocentric time (GPS)"<<endl;
      ofs_missed[n]<<"# injection local time (GPS) for detector "<<detector[0]->GetNamePrefix()<<endl;
      ofs_missed[n]<<"# injection local time (GPS) for detector "<<detector[1]->GetNamePrefix()<<endl;
      ofs_missed[n]<<"# injection time delay between detectors [s]"<<endl;
      ofs_missed[n]<<"# injection frequency [Hz]"<<endl;
      ofs_missed[n]<<"# injection right ascension [rad]"<<endl;
      ofs_missed[n]<<"# injection declination [rad]"<<endl;
      ofs_missed[n]<<"# event rank R"<<endl;
    }
  }
  
  // loop over injections
  for(Long64_t i=0; i<inject->InjRea::GetN(); i++){
    
    // only consider valid injections
    if(inject->InjRea::GetInjectionTag(i)==false) continue;
    
    // load injection
    if(inject->InjRea::LoadInjection(i)==0) continue;
    
    // load matching event
    if(OxEvent::GetEntry(inj_entry[i])<=0) continue;
    
    // get injection name index
    inj_name_index = inject->InjRea::GetInjectionNameIndex();
    
    // get injection time delay between detectors (true value)
    local_time_0 = OxOmicron::detector[0]->GetLocalTime(inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), inject->InjRea::GetInjectionTime());
    local_time_1 = OxOmicron::detector[1]->GetLocalTime(inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), inject->InjRea::GetInjectionTime());
    timedelay = local_time_1 - local_time_0;

    // get injection amplitude ratio (true value)
    gmst = GreenwichMeanSiderealTime(inject->InjRea::GetInjectionTime());
    OxOmicron::detector[1]->GetDetectorAMResponse(Fp, Fc, inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), 1.0, gmst);
    Fr = Fp*Fp + Fc*Fc;
    OxOmicron::detector[0]->GetDetectorAMResponse(Fp, Fc, inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), 1.0, gmst);
    Fr /= (Fp*Fp + Fc*Fc);
    Fr = TMath::Sqrt(Fr);

    // all valid injections:
    h1_inj_amp_tot[inj_name_index]->Fill(inject->InjRea::GetInjectionAmplitude());
    h1_inj_time_tot[inj_name_index]->Fill(inject->InjRea::GetInjectionTime());
    h1_inj_freq_tot[inj_name_index]->Fill(inject->InjRea::GetInjectionFrequency());
    h2_rec_rank_amp[inj_name_index]->Fill(inject->InjRea::GetInjectionAmplitude(), OxEvent::GetEventRank());
    h2_rec_timedelay_amp[inj_name_index]->Fill(inject->InjRea::GetInjectionAmplitude(), OxEvent::GetEventTimeDelay()-timedelay);
    h2_rec_aratio_amp[inj_name_index]->Fill(inject->InjRea::GetInjectionAmplitude(), OxEvent::GetEventAmplitudeRatio()/Fr);

    // the injection is recovered above the detection threshold
    if(OxEvent::GetEventRank()>aRankDetectionThreshold){
      h1_inj_amp_det[inj_name_index]->Fill(inject->InjRea::GetInjectionAmplitude());
      h1_inj_time_det[inj_name_index]->Fill(inject->InjRea::GetInjectionTime());
      h1_inj_freq_det[inj_name_index]->Fill(inject->InjRea::GetInjectionFrequency());
      h1_rec_timedelay[inj_name_index]->Fill(OxEvent::GetEventTimeDelay()-timedelay);
      h2_rec_timedelay[inj_name_index]->Fill(timedelay, OxEvent::GetEventTimeDelay());
      h1_rec_aratio[inj_name_index]->Fill(OxEvent::GetEventAmplitudeRatio()/Fr);
      h2_rec_aratio[inj_name_index]->Fill(Fr, OxEvent::GetEventAmplitudeRatio());
    }
  
    // missed injection
    else{
      if(ofs_missed[inj_name_index].is_open()){
        ofs_missed[inj_name_index]<<scientific<<setprecision(5)<<inject->InjRea::GetInjectionAmplitude()<<" "
                                  <<fixed<<setprecision(5)<<inject->InjRea::GetInjectionTime()<<" "
                                  <<fixed<<setprecision(5)<<local_time_0<<" "
                                  <<fixed<<setprecision(5)<<local_time_1<<" "
                                  <<fixed<<setprecision(5)<<timedelay<<" "
                                  <<fixed<<setprecision(5)<<inject->InjRea::GetInjectionFrequency()<<" "
                                  <<fixed<<setprecision(5)<<inject->InjRea::GetInjectionRa()<<" "
                                  <<fixed<<setprecision(5)<<inject->InjRea::GetInjectionDec()<<" "
                                  <<fixed<<setprecision(5)<<OxEvent::GetEventRank()<<" "
                                  <<endl;
      }
    }
  }  

  // loop over injection names
  for(unsigned int n=0; n<inject->InjRea::GetInjectionNameN(); n++){

    // close ouput files
    if(ofs_missed[n].is_open()) ofs_missed[n].close();

    // fill output tree
    *oxs_inj_name = inject->InjRea::GetInjectionName(n);
    oxs_h1_inj_amp_tot = h1_inj_amp_tot[n];
    oxs_h1_inj_amp_det = h1_inj_amp_det[n];
    oxs_h1_inj_time_tot = h1_inj_time_tot[n];
    oxs_h1_inj_time_det = h1_inj_time_det[n];
    oxs_h1_inj_freq_tot = h1_inj_freq_tot[n];
    oxs_h1_inj_freq_det = h1_inj_freq_det[n];
    oxs_h2_rec_rank_amp = h2_rec_rank_amp[n];
    oxs_h1_rec_timedelay = h1_rec_timedelay[n];
    oxs_h2_rec_timedelay = h2_rec_timedelay[n];
    oxs_h2_rec_timedelay_amp = h2_rec_timedelay_amp[n];
    oxs_h1_rec_aratio = h1_rec_aratio[n];
    oxs_h2_rec_aratio = h2_rec_aratio[n];
    oxs_h2_rec_aratio_amp = h2_rec_aratio_amp[n];
    if(oxs_tree!=NULL) oxs_tree->Fill();
  }


  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxSim::CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxSim::"<<__func__<<": the OxSim object is corrupted"<<endl;
    return false;
  }
  
  // create output file
  if(OxEvent::CreateOutputFile(aOutFileName, aFileId, aTime)==false) return false;

  // create output tree
  oxs_inj_name = 0;
  oxs_h1_inj_amp_tot = 0;
  oxs_h1_inj_amp_det = 0;
  oxs_h1_inj_time_tot = 0;
  oxs_h1_inj_time_det = 0;
  oxs_h1_inj_freq_tot = 0;
  oxs_h1_inj_freq_det = 0;
  oxs_h2_rec_rank_amp = 0;
  oxs_h1_rec_timedelay = 0;
  oxs_h2_rec_timedelay = 0;
  oxs_h2_rec_timedelay_amp = 0;
  oxs_h1_rec_aratio = 0;
  oxs_h2_rec_aratio = 0;
  oxs_h2_rec_aratio_amp = 0;

  oxs_tree = new TTree("oxs", "oxs");
  oxs_tree->Branch("oxs_inj_name", &oxs_inj_name);
  oxs_tree->Branch("oxs_h1_inj_amp_tot", &oxs_h1_inj_amp_tot);
  oxs_tree->Branch("oxs_h1_inj_amp_det", &oxs_h1_inj_amp_det);
  oxs_tree->Branch("oxs_h1_inj_time_tot", &oxs_h1_inj_time_tot);
  oxs_tree->Branch("oxs_h1_inj_time_det", &oxs_h1_inj_time_det);
  oxs_tree->Branch("oxs_h1_inj_freq_tot", &oxs_h1_inj_freq_tot);
  oxs_tree->Branch("oxs_h1_inj_freq_det", &oxs_h1_inj_freq_det);
  oxs_tree->Branch("oxs_h2_rec_rank_amp", &oxs_h2_rec_rank_amp);
  oxs_tree->Branch("oxs_h1_rec_timedelay", &oxs_h1_rec_timedelay);
  oxs_tree->Branch("oxs_h2_rec_timedelay", &oxs_h2_rec_timedelay);
  oxs_tree->Branch("oxs_h2_rec_timedelay_amp", &oxs_h2_rec_timedelay_amp);
  oxs_tree->Branch("oxs_h1_rec_aratio", &oxs_h1_rec_aratio);
  oxs_tree->Branch("oxs_h2_rec_aratio", &oxs_h2_rec_aratio);
  oxs_tree->Branch("oxs_h2_rec_aratio_amp", &oxs_h2_rec_aratio_amp);

  return true;
}



////////////////////////////////////////////////////////////////////////////////////
double OxSim::GetInjectionAmplitudeAtEfficiency(const unsigned int aNameIndex, const double aEfficiency){
////////////////////////////////////////////////////////////////////////////////////

  // unknown name index
  if(aNameIndex>=inject->InjRea::GetInjectionNameN()) return -1.0;
  
  double amplitude = -1.0;
  double eff = 0.0;
  double eff_plusone = 0.0;
  double eff_max = 0.0;
  int bin_max = 0;

  // search the bin with the maximum efficiency
  for(int b=1; b<=h1_inj_amp_tot[aNameIndex]->GetNbinsX(); b++){

    // detection efficiency in this bin
    if(h1_inj_amp_tot[aNameIndex]->GetBinContent(b)>0.0)
      eff = h1_inj_amp_det[aNameIndex]->GetBinContent(b) / h1_inj_amp_tot[aNameIndex]->GetBinContent(b);
    else
      eff = 0.0;

    // max
    if(eff>eff_max){
      eff_max = eff;
      bin_max = b;
    }
  }

  // no efficiency
  if(bin_max==0) return -1.0;

  // never reach the expected level of efficiency
  if(eff_max<aEfficiency) return -1.0;
  
  // check bins by decreasing amplitude
  for(int b=bin_max-1; b>0; b--){

    // detection efficiency in this bin
    if(h1_inj_amp_tot[aNameIndex]->GetBinContent(b)>0.0)
      eff = h1_inj_amp_det[aNameIndex]->GetBinContent(b) / h1_inj_amp_tot[aNameIndex]->GetBinContent(b);
    else
      eff = 0.0;

    // detection efficiency in previous bin
    if(h1_inj_amp_tot[aNameIndex]->GetBinContent(b+1)>0.0)
      eff_plusone = h1_inj_amp_det[aNameIndex]->GetBinContent(b+1) / h1_inj_amp_tot[aNameIndex]->GetBinContent(b+1);
    else
      eff_plusone = 0.0;

    // interpolate with the previous bin
    if(eff<aEfficiency){
      amplitude = h1_inj_amp_tot[aNameIndex]->GetXaxis()->GetBinCenterLog(b) // start point
        + (h1_inj_amp_tot[aNameIndex]->GetXaxis()->GetBinCenterLog(b+1)-h1_inj_amp_tot[aNameIndex]->GetXaxis()->GetBinCenterLog(b))/(eff_plusone-eff) // slope
        *(aEfficiency-eff); // stride
      break;
    }
  }


  return amplitude;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxSim::SavePlots(const string aFormat){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxSim::"<<__func__<<": the OxSim object is corrupted"<<endl;
    return false;
  }

  // output directory
  string outdir = ox_opt->GetOptionValueS("OXI", "OUTDIR", 0);

  // locals
  string plot_filename;
  
  // injection parameters
  GwollumPlot::UnDrawLegend();
  for(unsigned int n=0; n<inject->GetInjectionNameN(); n++){

    // amplitude
    plot_filename = outdir+"/oxr_inj_amplitude_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    h1_inj_amp_tot[n]->GetYaxis()->SetRangeUser(0.8, 1.1*(h1_inj_amp_tot[n]->GetMaximum()+1.0));
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_inj_amp_tot[n], "HIST");
    GwollumPlot::Draw(h1_inj_amp_det[n], "HISTSAME");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::ResetLegend();
    GwollumPlot::AddLegendEntry(h1_inj_amp_tot[n], "Valid", "L");
    GwollumPlot::AddLegendEntry(h1_inj_amp_det[n], "Detected", "F");
    GwollumPlot::DrawLegend();
    GwollumPlot::Print(plot_filename);

    // time
    plot_filename = outdir+"/oxr_inj_time_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    h1_inj_time_tot[n]->GetYaxis()->SetRangeUser(0.8, 1.1*(h1_inj_time_tot[n]->GetMaximum()+1.0));
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_inj_time_tot[n], "HIST");
    GwollumPlot::Draw(h1_inj_time_det[n], "HISTSAME");
    GwollumPlot::SetLogy(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);

    // frequency
    plot_filename = outdir+"/oxr_inj_freq_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    h1_inj_freq_tot[n]->GetYaxis()->SetRangeUser(0.8, 1.1*(h1_inj_freq_tot[n]->GetMaximum()+1.0));
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_inj_freq_tot[n], "HIST");
    GwollumPlot::Draw(h1_inj_freq_det[n], "HISTSAME");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);
  }

  // detection efficiency
  TH1D *h1_inj_eff;
  GwollumPlot::UnDrawLegend();
  for(unsigned int n=0; n<inject->GetInjectionNameN(); n++){
    h1_inj_eff = (TH1D*)h1_inj_amp_det[n]->Clone("h1_inj_eff");
    h1_inj_eff->Divide(h1_inj_amp_tot[n]);
    h1_inj_eff->SetTitle(inject->GetInjectionName(n).c_str());
    h1_inj_eff->GetYaxis()->SetTitle("Detection efficiency");
    h1_inj_eff->GetYaxis()->SetRangeUser(0, 1.1);
    plot_filename = outdir+"/oxr_inj_efficiency_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_inj_eff, "HIST");
    GwollumPlot::SetLogx(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);
    delete h1_inj_eff;
  }

  // injection reconstruction
  GwollumPlot::UnDrawLegend();
  for(unsigned int n=0; n<inject->GetInjectionNameN(); n++){

    // event rank vs injection amplitude
    h2_rec_rank_amp[n]->GetZaxis()->SetRangeUser(0.8, 1.1*(h2_rec_rank_amp[n]->GetMaximum()+1.0));
    plot_filename = outdir+"/oxr_rec_rank_amplitude_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    GwollumPlot::Draw(h2_rec_rank_amp[n], "COLZ");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogz(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);

    // time delay 1D
    h1_rec_timedelay[n]->GetYaxis()->SetRangeUser(0.8, 1.1*(h1_rec_timedelay[n]->GetMaximum()+1.0));
    plot_filename = outdir+"/oxr_rec_timedelay1D_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    gStyle->SetOptStat(111100);
    h1_rec_timedelay[n]->SetStats(true);
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_rec_timedelay[n], "HIST");
    GwollumPlot::SetLogy(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);
    gStyle->SetOptStat(0);

    // time delay 2D
    h2_rec_timedelay[n]->GetZaxis()->SetRangeUser(0.8, 1.1*(h2_rec_timedelay[n]->GetMaximum()+1.0));
    plot_filename = outdir+"/oxr_rec_timedelay2D_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    GwollumPlot::Draw(h2_rec_timedelay[n], "COLZ");
    GwollumPlot::SetLogz(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);

    // time delay vs amplitude
    h2_rec_timedelay_amp[n]->GetZaxis()->SetRangeUser(0.8, 1.1*(h2_rec_timedelay_amp[n]->GetMaximum()+1.0));
    plot_filename = outdir+"/oxr_rec_timedelay_amplitude_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    GwollumPlot::Draw(h2_rec_timedelay_amp[n], "COLZ");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogz(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);

    // amplitude ratio 1D
    h1_rec_aratio[n]->GetYaxis()->SetRangeUser(0.8, 1.1*(h1_rec_aratio[n]->GetMaximum()+1.0));
    plot_filename = outdir+"/oxr_rec_aratio1D_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    gStyle->SetOptStat(111100);
    h1_rec_aratio[n]->SetStats(true);
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(0);
    GwollumPlot::Draw(h1_rec_aratio[n], "HIST");
    GwollumPlot::SetLogy(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);
    gStyle->SetOptStat(0);

    // amplitude ratio 2D
    h2_rec_aratio[n]->GetZaxis()->SetRangeUser(0.8, 1.1*(h2_rec_aratio[n]->GetMaximum()+1.0));
    plot_filename = outdir+"/oxr_rec_aratio2D_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    GwollumPlot::Draw(h2_rec_aratio[n], "COLZ");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(1);
    GwollumPlot::SetLogz(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);

    // amplitude ratio vs amplitude
    h2_rec_aratio_amp[n]->GetZaxis()->SetRangeUser(0.8, 1.1*(h2_rec_aratio_amp[n]->GetMaximum()+1.0));
    plot_filename = outdir+"/oxr_rec_aratio_amplitude_"+inject->GetInjectionName(n)+"."+aFormat;
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxSim::"<<__func__<<": "<<plot_filename<<endl;
    GwollumPlot::SetLogx(0);
    GwollumPlot::SetLogy(0);
    GwollumPlot::SetLogz(0);
    GwollumPlot::Draw(h2_rec_aratio_amp[n], "COLZ");
    GwollumPlot::SetLogx(1);
    GwollumPlot::SetLogy(1);
    GwollumPlot::SetLogz(1);
    GwollumPlot::RedrawAxis();
    GwollumPlot::Print(plot_filename);
   }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxSim::MakeHtml(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxSim::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxSim::"<<__func__<<": the OxSim object is corrupted"<<endl;
    return false;
  }

  // initiate web report
  gwl_ss<<OxOmicron::GetOmicronStartTime(0)<<"-"<<OxOmicron::GetOmicronStartTime(1)<<"-"<<OxEvent::GetEventSliceIndex()<<".html";
  ofstream report = OxInit::CreateHtml(gwl_ss.str(), "Omicron-X event display");
  gwl_ss.clear(); gwl_ss.str("");

  // output directory
  string outdir = OxInit::GetOutputDirectory();

  //****************** configuration *********************
  report<<"<h2>Ox analysis configuration</h2>"<<endl;
  OxEvent::MakeHtmlConfigTable(report, outdir);
  report<<"<hr />"<<endl;
  report<<endl;
  //******************************************************

  //****************** event analysis *********************
  report<<"<h2>Ox event characterization</h2>"<<endl;

  // analysis results
  OxEvent::MakeHtmlEventTable(report);
  report<<"<hr />"<<endl;

  // maps
  OxCorr::MakeHtmlMapTable(report, (OxOmicron::GetOmicronStartTime(0) + OxOmicron::GetOmicronEndTime(0))/2, (OxOmicron::GetOmicronStartTime(1) + OxOmicron::GetOmicronEndTime(1))/2);
  OxOmicron::MakeHtmlMapTable(report, 0, (OxOmicron::GetOmicronStartTime(0) + OxOmicron::GetOmicronEndTime(0))/2);
  OxOmicron::MakeHtmlMapTable(report, 1, (OxOmicron::GetOmicronStartTime(1) + OxOmicron::GetOmicronEndTime(1))/2);
  report<<"<hr />"<<endl;

  // sky maps
  OxEvent::MakeHtmlSkyMapTable(report);
  report<<"<hr />"<<endl;

  // injection tables
  report<<"<h2>Injections</h2>"<<endl;
  MakeHtmlInjectionTable(report);
  report<<"<hr />"<<endl;

  OxInit::CloseHtml(report);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void OxSim::MakeHtmlResults(ofstream &aReport){
////////////////////////////////////////////////////////////////////////////////////

  // loop over injection names
  for(unsigned int n=0; n<inject->InjRea::GetInjectionNameN(); n++){
    aReport<<"<h3>"<<inject->InjRea::GetInjectionName(n)<<"</h3>"<<endl;

    // injection summary
    aReport<<"<table>"<<endl;
    aReport<<"  <tr><td>Number of injections (total/valid/detected):</td><td>"<<inject->InjRea::GetN(n)<<" / "<<GetValidN(n)<<" / "<<GetDetectedN(n)<<"</td></tr>"<<endl;
    aReport<<"  <tr><td>List of missed injections:</td><td><a href=\"./"<<inject->InjRea::GetInjectionName(n)<<"_missed.txt\">./"<<inject->InjRea::GetInjectionName(n)<<"_missed.txt</a></td></tr>"<<endl;
    aReport<<"  <tr><td>Injection amplitude at 90% detection efficiency:</td><td>"<<GetInjectionAmplitudeAtEfficiency(n, 0.9)<<"</td></tr>"<<endl;
    aReport<<"  <tr><td>Injection amplitude at 50% detection efficiency:</td><td>"<<GetInjectionAmplitudeAtEfficiency(n, 0.5)<<"</td></tr>"<<endl;
    aReport<<"  <tr><td>Injection amplitude at 10% detection efficiency:</td><td>"<<GetInjectionAmplitudeAtEfficiency(n, 0.1)<<"</td></tr>"<<endl;
    aReport<<"</table>"<<endl;

    // injection detection efficiency
    aReport<<"<table>"<<endl;
    aReport<<"  <tr>"<<endl;
    aReport<<"    <td><a href=\"./oxr_inj_efficiency_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_inj_efficiency_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"800\"></a></td>"<<endl;
    aReport<<"  </tr>"<<endl;
    aReport<<"</table>"<<endl;

    // injection parameters
    aReport<<"<h4>Injection parameters</h4>"<<endl;
    aReport<<"<table>"<<endl;
    aReport<<"  <tr>"<<endl;
    aReport<<"    <td><a href=\"./oxr_inj_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_inj_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"    <td><a href=\"./oxr_inj_time_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_inj_time_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"    <td><a href=\"./oxr_inj_freq_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_inj_freq_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"  </tr>"<<endl;
    aReport<<"</table>"<<endl;

    // injection reconstruction
    aReport<<"<h4>Injection reconstruction</h4>"<<endl;
    aReport<<"<table>"<<endl;
    aReport<<"  <tr>"<<endl;
    aReport<<"    <td><a href=\"./oxr_rec_rank_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_rec_rank_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"    <td></td>"<<endl;
    aReport<<"    <td></td>"<<endl;
    aReport<<"  </tr>"<<endl;
    aReport<<"  <tr>"<<endl;
    aReport<<"    <td><a href=\"./oxr_rec_timedelay_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_rec_timedelay_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"    <td><a href=\"./oxr_rec_timedelay2D_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_rec_timedelay2D_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"    <td><a href=\"./oxr_rec_timedelay1D_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_rec_timedelay1D_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"  </tr>"<<endl;
    aReport<<"  <tr>"<<endl;
    aReport<<"    <td><a href=\"./oxr_rec_aratio_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_rec_aratio_amplitude_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"    <td><a href=\"./oxr_rec_aratio2D_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_rec_aratio2D_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"    <td><a href=\"./oxr_rec_aratio1D_"<<inject->InjRea::GetInjectionName(n)<<".png\"><img src=\"./oxr_rec_aratio1D_"<<inject->InjRea::GetInjectionName(n)<<".png\" width=\"100%\"></a></td>"<<endl;
    aReport<<"  </tr>"<<endl;
    aReport<<"</table>"<<endl;

  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxSim::MakeHtmlInjectionTable(ofstream &aReport){
////////////////////////////////////////////////////////////////////////////////////
  double local_time_0 = 0.0;
  double local_time_1 = 0.0;

  // search the first injection to consider
  Long64_t inj_index_first = inject->InjRea::GetInjectionIndexAfter(OxOmicron::GetOmicronStartTime(0));

  // antenna factors
  double Fp, Fc;
  double F0, F1;

  // search for the relevant injections
  for(Long64_t i=inj_index_first; i<inject->InjRea::GetN(); i++){
    inject->InjRea::LoadInjection(i);
    
    // went too far --> stop
    if(inject->InjRea::GetInjectionTime()>=OxOmicron::GetOmicronEndTime(0)) break;
    
    // get injection local times
    local_time_0 = detector[0]->GetLocalTime(inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), inject->InjRea::GetInjectionTime());
    local_time_1 = detector[1]->GetLocalTime(inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), inject->InjRea::GetInjectionTime());
    
    // get antenna factors
    detector[0]->GetDetectorAMResponse(Fp, Fc, inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), 0.0, GreenwichMeanSiderealTime(inject->InjRea::GetInjectionTime()));
    F0 = TMath::Sqrt(Fp*Fp+Fc*Fc);
    detector[1]->GetDetectorAMResponse(Fp, Fc, inject->InjRea::GetInjectionRa(), inject->InjRea::GetInjectionDec(), 0.0, GreenwichMeanSiderealTime(inject->InjRea::GetInjectionTime()));
    F1 = TMath::Sqrt(Fp*Fp+Fc*Fc);
    
    aReport<<"<table>"<<endl;
    aReport<<"  <tr><td>Name:</td><td>"<<inject->InjRea::GetInjectionName()<<"</td></tr>"<<endl;
    aReport<<"  <tr><td>GPS time:</td><td>"<<fixed<<setprecision(4)<<inject->InjRea::GetInjectionTime()<<" (geocentric), "<<local_time_0<<" ("<<OxOmicron::detector[0]->GetNamePrefix()<<"), "<<local_time_1<<" ("<<OxOmicron::detector[1]->GetNamePrefix()<<")</td></tr>"<<endl;
    aReport<<"  <tr><td>Time delay between detectors:</td><td>"<<local_time_1-local_time_0<<" s</td></tr>"<<endl;
    aReport<<"  <tr><td>Amplitude:</td><td>"<<inject->InjRea::GetInjectionAmplitude()<<"</td></tr>"<<endl;
    aReport<<"  <tr><td>Frequency:</td><td>"<<inject->InjRea::GetInjectionFrequency()<<" Hz</td></tr>"<<endl;
    aReport<<"  <tr><td>Sky position [rad]:</td><td> ra = "<<inject->InjRea::GetInjectionRa()<<", dec = "<<inject->InjRea::GetInjectionDec()<<"</td></tr>"<<endl;
    aReport<<"  <tr><td>Sky position [deg]:</td><td> ra = "<<inject->InjRea::GetInjectionRa()*180.0/TMath::Pi()<<", dec = "<<inject->InjRea::GetInjectionDec()*180.0/TMath::Pi()<<"</td></tr>"<<endl;
    aReport<<"  <tr><td>Antenna factors (F+&sup2; + Fx&sup2;)^{1/2}:</td><td>"<<F0<<" ("<<detector[0]->GetNamePrefix()<<"), "<<F1<<" ("<<detector[1]->GetNamePrefix()<<")</td></tr>"<<endl;
    aReport<<"  <tr><td>Ratio of antenna factors ("<<detector[1]->GetNamePrefix()<<"/"<<detector[0]->GetNamePrefix()<<"):</td><td>"<<F1/F0<<"</td></tr>"<<endl;
    aReport<<"</table>"<<endl; 
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxSim::ExtractResults(void){
////////////////////////////////////////////////////////////////////////////////////

  // create TChain
  TChain *oxs_chain = new TChain("oxs");
  
  // load oxs trees
  bool found = false;
  gErrorIgnoreLevel = kBreak;// not to print warning about files w/o oxs trees
  for(unsigned int f=0; f<OxInit::GetOxFileN(); f++){
    if(oxs_chain->AddFile(OxInit::GetOxFileName(f).c_str(), 0)==1) found = true;
  }

  // no results anywhere: exit
  if(found==false){
    if(OxInit::GetOxFileN()>0){
      monit->Monitor::SetMessageType(monitor_warning);
      *monit<<"OxSim::"<<__func__<<": no Ox injection results in the input Ox files"<<endl;
    }
    delete oxs_chain;
    return;
  }

  // attach branches
  oxs_inj_name = 0;
  oxs_h1_inj_amp_tot = 0;
  oxs_h1_inj_amp_det = 0;
  oxs_h1_inj_time_tot = 0;
  oxs_h1_inj_time_det = 0;
  oxs_h1_inj_freq_tot = 0;
  oxs_h1_inj_freq_det = 0;
  oxs_h2_rec_rank_amp = 0;
  oxs_h1_rec_timedelay = 0;
  oxs_h2_rec_timedelay = 0;
  oxs_h2_rec_timedelay_amp = 0;
  oxs_h1_rec_aratio = 0;
  oxs_h2_rec_aratio = 0;
  oxs_h2_rec_aratio_amp = 0;
  oxs_chain->SetBranchAddress("oxs_inj_name", &oxs_inj_name);
  oxs_chain->SetBranchAddress("oxs_h1_inj_amp_tot", &oxs_h1_inj_amp_tot);
  oxs_chain->SetBranchAddress("oxs_h1_inj_time_tot", &oxs_h1_inj_time_tot);
  oxs_chain->SetBranchAddress("oxs_h1_inj_freq_tot", &oxs_h1_inj_freq_tot);
  oxs_chain->SetBranchAddress("oxs_h1_inj_amp_det", &oxs_h1_inj_amp_det);
  oxs_chain->SetBranchAddress("oxs_h1_inj_time_det", &oxs_h1_inj_time_det);
  oxs_chain->SetBranchAddress("oxs_h1_inj_freq_det", &oxs_h1_inj_freq_det);
  oxs_chain->SetBranchAddress("oxs_h2_rec_rank_amp", &oxs_h2_rec_rank_amp);
  oxs_chain->SetBranchAddress("oxs_h1_rec_timedelay", &oxs_h1_rec_timedelay);
  oxs_chain->SetBranchAddress("oxs_h2_rec_timedelay", &oxs_h2_rec_timedelay);
  oxs_chain->SetBranchAddress("oxs_h2_rec_timedelay_amp", &oxs_h2_rec_timedelay_amp);
  oxs_chain->SetBranchAddress("oxs_h1_rec_aratio", &oxs_h1_rec_aratio);
  oxs_chain->SetBranchAddress("oxs_h2_rec_aratio", &oxs_h2_rec_aratio);
  oxs_chain->SetBranchAddress("oxs_h2_rec_aratio_amp", &oxs_h2_rec_aratio_amp);
  
  // read chain
  for(Long64_t i=0; i<oxs_chain->GetEntries(); i++){
    if(oxs_chain->GetEntry(i)<=0) continue;

    // loop over injection names to find a right match
    for(unsigned int n=0; n<inject->InjRea::GetInjectionNameN(); n++){

      // name match --> add if the number of bins is also a match
      if((*oxs_inj_name).compare(inject->InjRea::GetInjectionName(n))==0){

        if(oxs_h1_inj_amp_tot->GetNbinsX()==h1_inj_amp_tot[n]->GetNbinsX())
          h1_inj_amp_tot[n]->Add(oxs_h1_inj_amp_tot);
        else{
          monit->Monitor::SetMessageType(monitor_error);
          *monit<<"OxSim::"<<__func__<<": number of bin mismatch"<<endl;
        }
        if(oxs_h1_inj_time_tot->GetNbinsX()==h1_inj_time_tot[n]->GetNbinsX())
          h1_inj_time_tot[n]->Add(oxs_h1_inj_time_tot);
        
        if(oxs_h1_inj_freq_tot->GetNbinsX()==h1_inj_freq_tot[n]->GetNbinsX())
          h1_inj_freq_tot[n]->Add(oxs_h1_inj_freq_tot);

        if(oxs_h1_inj_amp_det->GetNbinsX()==h1_inj_amp_det[n]->GetNbinsX())
          h1_inj_amp_det[n]->Add(oxs_h1_inj_amp_det);

        if(oxs_h1_inj_time_det->GetNbinsX()==h1_inj_time_det[n]->GetNbinsX())
          h1_inj_time_det[n]->Add(oxs_h1_inj_time_det);

        if(oxs_h1_inj_freq_det->GetNbinsX()==h1_inj_freq_det[n]->GetNbinsX())
          h1_inj_freq_det[n]->Add(oxs_h1_inj_freq_det);

        if(oxs_h2_rec_rank_amp->GetNbinsX()==h2_rec_rank_amp[n]->GetNbinsX())
          h2_rec_rank_amp[n]->Add(oxs_h2_rec_rank_amp);

        if(oxs_h1_rec_timedelay->GetNbinsX()==h1_rec_timedelay[n]->GetNbinsX())
          h1_rec_timedelay[n]->Add(oxs_h1_rec_timedelay);

        if(oxs_h2_rec_timedelay->GetNbinsX()==h2_rec_timedelay[n]->GetNbinsX())
          h2_rec_timedelay[n]->Add(oxs_h2_rec_timedelay);

        if(oxs_h2_rec_timedelay_amp->GetNbinsX()==h2_rec_timedelay_amp[n]->GetNbinsX())
          h2_rec_timedelay_amp[n]->Add(oxs_h2_rec_timedelay_amp);

        if(oxs_h1_rec_aratio->GetNbinsX()==h1_rec_aratio[n]->GetNbinsX())
          h1_rec_aratio[n]->Add(oxs_h1_rec_aratio);

        if(oxs_h2_rec_aratio->GetNbinsX()==h2_rec_aratio[n]->GetNbinsX())
          h2_rec_aratio[n]->Add(oxs_h2_rec_aratio);

        if(oxs_h2_rec_aratio_amp->GetNbinsX()==h2_rec_aratio_amp[n]->GetNbinsX())
          h2_rec_aratio_amp[n]->Add(oxs_h2_rec_aratio_amp);
      }         
    }
  }

  delete oxs_chain;
  return;
}
