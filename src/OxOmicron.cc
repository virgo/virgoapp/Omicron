/**
 * @file 
 * @brief See OxOmicron.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxOmicron.h"

ClassImp(OxOmicron)

static bool ForceOmicronOptions(const string aInFile, const string aOutFile, const string aOutDir);


////////////////////////////////////////////////////////////////////////////////////
OxOmicron::OxOmicron(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1): OxInit(aOptionFile){
////////////////////////////////////////////////////////////////////////////////////

  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxOmicron::"<<__func__<<endl;

  // raise the omicron error level
  gErrorIgnoreLevel = kBreak;

  // list of options
  OxInit::DefineOption("OXO", "OPTIONS", "", 2);
  OxInit::DefineOption("OXO", "FFL0", "", 1);
  OxInit::DefineOption("OXO", "FFL1", "", 1);
  OxInit::DefineOption("OXO", "INJGEN", "", 0);
  OxInit::DefineOption("OXO", "SKYRINGS", (unsigned int)100, 2);
  OxInit::DefineOption("OXO", "WHITENOISECORR", 1.0, 1);

  // add options from the text file
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxOmicron::"<<__func__<<": add OXO options"<<endl;
  OxInit::AddOptions();

  // overload some of the options (from text file)
  // OXO/OPTIONS keep the TTree value
  OxInit::OverloadOption("OXO", "FFL0");
  OxInit::OverloadOption("OXO", "FFL1");
  OxInit::OverloadOption("OXO", "INJGEN");
  // OXO/SKYRINGS keep the TTree value
  // OXO/WHITENOISECORR keep the TTree value

  // create omicron objects
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxOmicron::"<<__func__<<": create Omicron objects"<<endl;
  omicron[0] = CreateOmicron(0, aGpsRef0);
  omicron[1] = CreateOmicron(1, aGpsRef1);

  // fill Omicron option tree
  omicron[0]->GwollumOptions::FillTree();
  omicron[1]->GwollumOptions::FillTree();

  // detector streams
  detector[0] = new Streams(omicron[0]->GetChannelName(0), 0);
  detector[1] = new Streams(omicron[1]->GetChannelName(0), 0);

  // white noise SNR variance
  whitenoise_snr_variance = ox_opt->GetOptionValueD("OXO", "WHITENOISECORR", 0) * WHITE_NOISE_VARIANCE;

  // maximum light travel time between detectors
  double light_travel_time = ::GetLightTravelTime(omicron[0]->GetChannelName(0).substr(0,2),
                                                  omicron[1]->GetChannelName(0).substr(0,2));

  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxOmicron::"<<__func__<<": create sky rings"<<endl;

  // number sky rings
  skyring_n = ox_opt->GetOptionValueU("OXO", "SKYRINGS", 0);
  if(skyring_n==0) skyring_n = 1;

  // sky rings: list of time delays
  skyring_timedelays = new double [GetRingN()];
  for(unsigned int r=0; r<GetRingN(); r++){// from negative to positive
    skyring_timedelays[r] = -light_travel_time*TMath::Cos((double)r/(double)skyring_n * TMath::Pi()/2.0);
  }

  // number of points in the central sky ring (time delay = 0)
  skyring_np = ox_opt->GetOptionValueU("OXO", "SKYRINGS", 1);
  if(skyring_np==0) skyring_np = 1;

  // min/max antenna factors along rings
  skyring_af_ratio_min = new double [GetRingN()];
  skyring_af_ratio_max = new double [GetRingN()];
  skyring_start_time = 0;
 
  // antenna factor ratio histograms
  skyring_af_ratio = new TH1D* [GetRingN()];
  for(unsigned int r=0; r<GetRingN(); r++){
    gwl_ss<<r;
    skyring_af_ratio[r] = new TH1D(("skyring_af_ratio_"+gwl_ss.str()).c_str(), ("Antenna factor ratio 1/0, ring="+gwl_ss.str()).c_str(), 500, 0.0, 10.0);
    skyring_af_ratio[r]->GetXaxis()->SetTitle("Antenna factor ratio (1/0)");
    skyring_af_ratio[r]->GetYaxis()->SetTitle("Number of sky points");
    // FIXME: find a better binning!!!
    gwl_ss.clear(); gwl_ss.str("");

    skyring_af_ratio_min[r] = 0.0;
    skyring_af_ratio_max[r] = 1.0e20;
  }

  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxOmicron::"<<__func__<<": create output TTrees"<<endl;

  // Omicron output TTree
  oxo_tree[0] = NULL;
  oxo_tree[1] = NULL;
  oxo_start_time[0] = 0;
  oxo_start_time[1] = 0;
  oxo_snrsq[0] = 0.0;
  oxo_snrsq[1] = 0.0;
  oxo_snrvar[0] = 0.0;
  oxo_snrvar[1] = 0.0;
  
  // get Omicron TChain (read-only)
  Long64_t nentries_0 = 0;
  Long64_t nentries_1 = 0;
  oxo_tree_read[0] = new TChain("oxo_0");
  oxo_tree_read[1] = new TChain("oxo_1");
  for(unsigned int f=0; f<OxInit::GetOxFileN(); f++){

    // add file
    if(oxo_tree_read[0]->AddFile(OxInit::GetOxFileName(f).c_str(), 0)!=1) monit->Monitor::SetStatus(false);
    if(oxo_tree_read[1]->AddFile(OxInit::GetOxFileName(f).c_str(), 0)!=1) monit->Monitor::SetStatus(false);

    // if new entries are found in oxo_0, there must be new entries in oxo_1 as well
    if((oxo_tree_read[0]->GetEntries()>nentries_0)&&(oxo_tree_read[1]->GetEntries()==nentries_1)){
      monit->Monitor::SetMessageType(monitor_fatal);
      *monit<<"OxOmicron::"<<__func__<<": missing oxo_1 entries in "<<OxInit::GetOxFileName(f)<<endl;
    }
    
    // if new entries are found in oxo_1, there must be new entries in oxo_0 as well
    if((oxo_tree_read[1]->GetEntries()>nentries_1)&&(oxo_tree_read[0]->GetEntries()==nentries_0)){
      monit->Monitor::SetMessageType(monitor_fatal);
      *monit<<"OxOmicron::"<<__func__<<": missing oxo_0 entries in "<<OxInit::GetOxFileName(f)<<endl;
    }

    // for next round
    nentries_0 = oxo_tree_read[0]->GetEntries();
    nentries_1 = oxo_tree_read[1]->GetEntries();
  }
  oxo_tree_read[0]->SetBranchAddress("oxo_start_time_0", &(oxo_start_time[0]));
  oxo_tree_read[1]->SetBranchAddress("oxo_start_time_1", &(oxo_start_time[1]));
  oxo_tree_read[0]->SetBranchAddress("oxo_snrsq_0", &(oxo_snrsq[0]));
  oxo_tree_read[1]->SetBranchAddress("oxo_snrsq_1", &(oxo_snrsq[1]));
  oxo_tree_read[0]->SetBranchAddress("oxo_snrvar_0", &(oxo_snrvar[0]));
  oxo_tree_read[1]->SetBranchAddress("oxo_snrvar_1", &(oxo_snrvar[1]));
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxOmicron::"<<__func__<<": number of entries (det 0) = "<<GetEntries(0)<<endl;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxOmicron::"<<__func__<<": number of entries (det 1) = "<<GetEntries(1)<<endl;
  
  // fill option tree
  ox_opt->FillTree();

  // list of produced maps
  proc_maps[0].clear();
  proc_maps[1].clear();

  // check omicron setting
  monit->Monitor::UpdateStatus(CheckOmicron());
}

////////////////////////////////////////////////////////////////////////////////////
OxOmicron::~OxOmicron(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxOmicron::"<<__func__<<endl;
  delete omicron[0];
  delete omicron[1];
  delete detector[0];
  delete detector[1];
  delete oxo_tree_read[0];
  delete oxo_tree_read[1];
  proc_maps[0].clear();
  proc_maps[1].clear();
  delete [] skyring_timedelays;
  delete [] skyring_af_ratio_min;
  delete [] skyring_af_ratio_max;
  for(unsigned int r=0; r<GetRingN(); r++){
    delete skyring_af_ratio[r];
  }
  delete [] skyring_af_ratio;

  // do not delete TTree
}

////////////////////////////////////////////////////////////////////////////////////
bool OxOmicron::Process(const unsigned int aDetectorIndex, const unsigned int aTimeStart, const unsigned int aPsdLengthReset){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxOmicron::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxOmicron::"<<__func__<<": the OxOmicron object is corrupted"<<endl;
    return false;
  }
  
  unsigned int datasize;
  double *data;
  bool is_flat;// not used
  unsigned int d = aDetectorIndex%2;

  // load channel
  while(omicron[d]->NewChannel()==false);

  // define analysis chunk
  if(omicron[d]->DefineNewChunk(aTimeStart, aTimeStart + omicron[d]->GetChunkDuration(), aPsdLengthReset)==false) return false;
      
  // load data
  if(omicron[d]->LoadData(&data, datasize)==false) return false;

  // condition data
  if(omicron[d]->Condition(datasize, data, is_flat)!=0){
    delete [] data;
    return false;
  }
  delete [] data;

  // project data
  omicron[d]->Project();

  // save results
  oxo_start_time[d] = omicron[d]->GetChunkTimeStart() + omicron[d]->GetOverlapDuration()/2;
  oxo_snrsq[d] = omicron[d]->GetSnrSqMax(0);
  for(unsigned int q=1; q<omicron[d]->GetQN(); q++){
    if(omicron[d]->GetSnrSqMax(q)>oxo_snrsq[d]) oxo_snrsq[d] = omicron[d]->GetSnrSqMax(q);
  }
  // note: snrvar parameters are calculated in OxCorr

  // fill the tree
  FillTree(d);

  // update sky rings
  if(d==0) ScanSkyRings();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxOmicron::WriteMaps(const unsigned int aDetectorIndex){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxOmicron::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxOmicron::"<<__func__<<": the OxOmicron object is corrupted"<<endl;
    return false;
  }
  
  unsigned int d = aDetectorIndex%2;
  unsigned int tmap = (omicron[d]->GetChunkTimeStart()+omicron[d]->GetChunkTimeEnd())/2;

  // check previous maps
  for(unsigned int m=0; m<proc_maps[d].size(); m++){
    // already produced --> exit
    if(tmap==proc_maps[d][m]){
      monit->Monitor::SetMessageType(monitor_info_4);
      *monit<<"OxOmicron::"<<__func__<<": Omicron spectrogram for detector "<<d<<" at GPS="<<GetOmicronStartTime(d)<<" was already produced"<<endl;
      return true;
    }
  }

  // write map to disk
  if(omicron[d]->WriteOutput()==true){
    monit->Monitor::SetMessageType(monitor_info_4);
    *monit<<"OxOmicron::"<<__func__<<": Omicron spectrogram for detector "<<d<<" at GPS="<<GetOmicronStartTime(d)<<" is produced"<<endl;
    proc_maps[d].push_back(tmap);
    return true;
  }

  return false;
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* OxOmicron::GetSkyRing(const double aTimeDelay){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxOmicron::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxOmicron::"<<__func__<<": the OxOmicron object is corrupted"<<endl;
    return NULL;
  }

  // search for the best ring (closest time delay)
  unsigned int best_r = 0;
  double best_ddt = TMath::Abs(GetRingTimeDelay(0)-aTimeDelay);
  for(unsigned int r=1; r<GetRingN(); r++){
    if(TMath::Abs(GetRingTimeDelay(r)-aTimeDelay)<best_ddt){
      best_ddt = TMath::Abs(GetRingTimeDelay(r)-aTimeDelay);
      best_r = r;
    }
  }
  
  return GetSkyRing(best_r);
}

////////////////////////////////////////////////////////////////////////////////////
TGraph* OxOmicron::GetSkyRing(const unsigned int aSkyRingIndex){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxOmicron::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxOmicron::"<<__func__<<": the OxOmicron object is corrupted"<<endl;
    return NULL;
  }

  double gmst = GreenwichMeanSiderealTime((double)GetOmicronStartTime(0));

  // ring index:
  unsigned int r = aSkyRingIndex%GetRingN();
  
  // sky ring
  TGraph *g_ring = new TGraph(GetRingPointsN(r));
  for(int p=0; p<g_ring->GetN(); p++){
    detector[0]->GetSkyRingPointRaDec(g_ring->GetY()[p], g_ring->GetX()[p], gmst, GetRingTimeDelay(r), (double)p/(double)g_ring->GetN()*2.0*TMath::Pi(), detector[1]);
  }

  return g_ring;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxOmicron::CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxOmicron::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxOmicron::"<<__func__<<": the OxOmicron object is corrupted"<<endl;
    return false;
  }
  
  // create file
  if(OxInit::CreateOutputFile(aOutFileName, aFileId, aTime)==false) return false;

  // write Omicron options
  omicron[0]->GwollumOptions::WriteTree("omicron0_options");
  omicron[1]->GwollumOptions::WriteTree("omicron1_options");

  // create omicron tree so it is owned by the file
  // note: from now on, the file is managed by OxInit (saving and closing)
  oxo_tree[0] = new TTree("oxo_0", "oxo_0");
  oxo_tree[0]->Branch("oxo_start_time_0", &(oxo_start_time[0]), "oxo_start_time_0/i");
  oxo_tree[0]->Branch("oxo_snrsq_0",      &(oxo_snrsq[0]),      "oxo_snrsq_0/D");
  oxo_tree[0]->Branch("oxo_snrvar_0",     &(oxo_snrvar[0]),     "oxo_snrvar_0/D");
  oxo_tree[1] = new TTree("oxo_1", "oxo_1");
  oxo_tree[1]->Branch("oxo_start_time_1", &(oxo_start_time[1]), "oxo_start_time_1/i");
  oxo_tree[1]->Branch("oxo_snrsq_1",      &(oxo_snrsq[1]),      "oxo_snrsq_1/D");
  oxo_tree[1]->Branch("oxo_snrvar_1",     &(oxo_snrvar[1]),     "oxo_snrvar_1/D");

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void OxOmicron::GetAntennaFactorRatioMinMax(double &aRmin, double &aRmax,
                                            const double aTimeDelay, const double aTimeDelayUncertainty){
////////////////////////////////////////////////////////////////////////////////////

  aRmin = -1.0;
  aRmax = -1.0;
  
  // loop over sky rings
  for(unsigned int r=0; r<GetRingN(); r++){

    // the ring time delay must be within uncertainties
    if(GetRingTimeDelay(r)<aTimeDelay-aTimeDelayUncertainty) continue;
    if(GetRingTimeDelay(r)>aTimeDelay+aTimeDelayUncertainty) continue;

    if(aRmin==-1.0){
      aRmin = skyring_af_ratio_min[r];
      aRmax = skyring_af_ratio_max[r];
    }
    else{
      // find overall min max
      if(skyring_af_ratio_min[r]<aRmin) aRmin = skyring_af_ratio_min[r];
      if(skyring_af_ratio_max[r]>aRmax) aRmax = skyring_af_ratio_max[r];
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxOmicron::MakeHtmlMapTable(ofstream &aReport, const unsigned int aDetectorIndex, const unsigned int aMapTime){
////////////////////////////////////////////////////////////////////////////////////

  // detector index
  unsigned int d = aDetectorIndex%2;

  // map time must exist
  unsigned int m = 0;
  for(m=0; m<proc_maps[d].size(); m++){
    if(proc_maps[d][m]==aMapTime) break;
  }
  if(m==proc_maps[d].size()) return;

  aReport<<"<table class=\"oxmaps\">"<<endl;
  aReport<<"  <tr>"<<endl;
  for(unsigned int q=0; q<omicron[d]->GetQN(); q++){
    aReport<<"    <td><a href=\"./"<<detector[d]->GetNameConv()<<"_OMICRONMAPQ"<<q<<"-"<<aMapTime<<"-"<<GetAnalysisDuration()<<".png\"><img src=\"./"<<detector[d]->GetNameConv()<<"_OMICRONMAPQ"<<q<<"-"<<aMapTime<<"-"<<GetAnalysisDuration()<<".png\" alt=\"Omicron map Q"<<q<<", det"<<d<<", "<<aMapTime<<"\"/></a></td>"<<endl;
  }
  aReport<<"  </tr>"<<endl;
  aReport<<"</table>"<<endl;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxOmicron::ScanSkyRings(void){
////////////////////////////////////////////////////////////////////////////////////

  // already done
  // FIXME: add some tolerance on time to be even more efficient?
  if(oxo_start_time[0]==skyring_start_time) return;

  // locals
  double af_ratio;
  double Fp, Fc, Fsq;
  double ra, decl;
  double gmst = GreenwichMeanSiderealTime((double)GetOmicronStartTime(0));
  unsigned int np;
  
  // loop over sky rings
  for(unsigned int r=0; r<GetRingN(); r++){

    // init min/max
    skyring_af_ratio_min[r] = 1.0e20;
    skyring_af_ratio_max[r] = 0.0;
    
    // number of points in the ring
    np = GetRingPointsN(r);

    // loop over points along the ring
    for(unsigned int p=0; p<np; p++){

      // get point on the ring
      if(detector[0]->GetSkyRingPointRaDec(decl, ra, gmst, GetRingTimeDelay(r), (double)p/(double)np*2.0*TMath::Pi(), detector[1])==false)// same detectors!
        break;
      
      // get F+ and Fx for detector 1 (psi=1.0)
      detector[1]->GetDetectorAMResponse(Fp, Fc, ra, decl, 1.0, gmst);

      // quadratic sum
      af_ratio = TMath::Sqrt(Fp*Fp + Fc*Fc);

      // get F+ and Fx for detector 0 (psi=1.0)
      detector[0]->GetDetectorAMResponse(Fp, Fc, ra, decl, 1.0, gmst);

      // get antenna factor ratio
      Fsq = Fp*Fp + Fc*Fc;
      if(Fsq>1.0e-20)
        af_ratio /= TMath::Sqrt(Fsq);
      else
        af_ratio = 1.0e20;// + infty

      // fill histogram
      skyring_af_ratio[r]->Fill(af_ratio);
      
      // min/max
      if(af_ratio<skyring_af_ratio_min[r]) skyring_af_ratio_min[r] = af_ratio;
      if(af_ratio>skyring_af_ratio_max[r]) skyring_af_ratio_max[r] = af_ratio;
    }
  }
  
  skyring_start_time = oxo_start_time[0];
  return;
}

////////////////////////////////////////////////////////////////////////////////////
Omicron* OxOmicron::CreateOmicron(const unsigned int aDetectorIndex, const unsigned int aGpsRef){
////////////////////////////////////////////////////////////////////////////////////

  // detector index string
  gwl_ss<<aDetectorIndex;
  string sdet = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");

  // gps ref string (+ process id for unicity)
  gwl_ss<<aGpsRef<<"."<<getpid();
  string sgps = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
 
  // option file (output)
  string out_optfile = OxInit::GetOutputDirectory()+"/"+OxInit::GetName()+".omicron."+sdet+"."+sgps+".opt";

  // Omicron option file (if given)
  string in_optfile = ox_opt->GetOptionValueS("OXO", "OPTIONS", aDetectorIndex);

  // by default, copy option text file to the output directory
  // it will be overwritten if a list of Ox files is provided
  error_code errcode;
  if(filesystem::is_regular_file(in_optfile)==true){
    // copy option file and force some of the options
    if(ForceOmicronOptions(in_optfile, out_optfile, OxInit::GetOutputDirectory())==false){
      monit->Monitor::SetMessageType(monitor_error);
      *monit<<"OxOmicron::"<<__func__<<": cannot copy Omicron option files in the output directory ("<<errcode<<")"<<endl;
    }
  }
  
  // Omicron option chain
  TChain *c_opt;
  c_opt = new TChain(("omicron"+sdet+"_options").c_str());

  // read option TChain from input files (if any)
  for(unsigned int f=0; f<OxInit::GetOxFileN(); f++){
    if(c_opt->AddFile(OxInit::GetOxFileName(f).c_str(), 0)!=1) monit->Monitor::SetStatus(false);
  }

  // no Ox files or no Omicron options => use text files to create Omicron object
  if(c_opt->GetEntries()==0){
    delete c_opt;
    return new Omicron(out_optfile, true, aGpsRef);
  }

  // GWOLLUM options
  GwollumOptions *go = new GwollumOptions();
  bool go_uniform = false;

  // the options must be uniform
  // FIXME: we only check the list options not the values!!!
  if((go->ReadChain(go_uniform, c_opt)==false) || (go_uniform==false)){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxOmicron::"<<__func__<<": omicron options are not valid"<<endl;
    delete c_opt;
    delete go;
    return new Omicron(ox_opt->GetOptionValueS("OXO", "OPTIONS", aDetectorIndex), true, aGpsRef);
  }

  delete c_opt;

  // overload output directory
  go->SetOptionValue("OUTPUT", "DIRECTORY", 0, OxInit::GetOutputDirectory());
  
  // overload FFL path
  if(OxInit::ox_opt->GetOptionValueS("OXO", "FFL"+sdet, 0).compare("")){
    go->SetOptionValue("DATA", "FFL", 0, OxInit::ox_opt->GetOptionValueS("POST", "FFL"+sdet, 0));
  }

  // overload injection path
  if((OxInit::ox_opt->GetOptionNumberOfValues("OXO", "INJGEN")>0) && (OxInit::ox_opt->GetOptionValueS("OXO", "INJGEN", 0).compare(""))){
    go->ResetOption("INJECTION", "INJGEN");
    for(unsigned int p=0; p<OxInit::ox_opt->GetOptionNumberOfValues("OXO", "INJGEN"); p++)
      go->AddOptionValue("INJECTION", "INJGEN", OxInit::ox_opt->GetOptionValueS("OXO", "INJGEN", p));
  }

  // dump options in a text file
  go->WriteTextFile(out_optfile);
  delete go;

  return new Omicron(out_optfile, true, aGpsRef);
}

////////////////////////////////////////////////////////////////////////////////////
bool OxOmicron::CheckOmicron(void){
////////////////////////////////////////////////////////////////////////////////////

  // check status
  if(omicron[0]==NULL) return false;
  if(omicron[1]==NULL) return false;
  if(!omicron[0]->GetStatus()) return false;
  if(!omicron[1]->GetStatus()) return false;
  
  // there must be only one channel
  if(omicron[0]->GetChannelsN() != 1){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxOmicron::"<<__func__<<": only one channel can be processed"<<endl;
    return false;
  }
  if(omicron[1]->GetChannelsN() != 1){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxOmicron::"<<__func__<<": only one channel can be processed"<<endl;
    return false;
  }
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxOmicron::"<<__func__<<": channels = "<<omicron[0]->GetChannelName(0)<<" - "<<omicron[1]->GetChannelName(0)<<endl;
  
  // check that the tiling is the same
  if(omicron[0]->GetChunkDuration() != omicron[1]->GetChunkDuration()){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxOmicron::"<<__func__<<": analysis window mismatch"<<endl;
    return false;
  }
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxOmicron::"<<__func__<<": chunk duration = "<<omicron[0]->GetChunkDuration()<<" s"<<endl;
  if(omicron[0]->GetOverlapDuration() != omicron[1]->GetOverlapDuration()){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxOmicron::"<<__func__<<": window overlap mismatch"<<endl;
    return false;
  } 
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxOmicron::"<<__func__<<": overlap duration = "<<omicron[0]->GetOverlapDuration()<<" s"<<endl;
  if(omicron[0]->GetQN() != omicron[1]->GetQN()){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxOmicron::"<<__func__<<": number of Q planes mismatch"<<endl;
    return false;
  }
  else{
    for(unsigned int q=0; q<omicron[0]->GetQN(); q++){
      if(omicron[0]->GetBandN(q) != omicron[1]->GetBandN(q)){
        monit->Monitor::SetMessageType(monitor_error);
        *monit<<"OxOmicron::"<<__func__<<": number of frequency rows mismatch"<<endl;
        return false;
      }
      // we do not check tiles in a row as this should be automatically OK
    }
  }
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxOmicron::"<<__func__<<": number of Q planes = "<<omicron[0]->GetQN()<<endl;

  if(omicron[0]->GetSampleFrequency()!=omicron[1]->GetSampleFrequency()){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxOmicron::"<<__func__<<": sampling frequency mismatch"<<endl;
    return false;
  }
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxOmicron::"<<__func__<<": sampling frequency = "<<omicron[0]->GetSampleFrequency()<<" Hz"<<endl;

  // print a warning if the injections are different
  if(omicron[0]->GetInjectionChannelN() != omicron[1]->GetInjectionChannelN()){
    monit->Monitor::SetMessageType(monitor_warning);
    *monit<<"OxOmicron::"<<__func__<<": the number of injection channels is not the same"<<endl;
  }
  
  // print a warning if the only one stream of SG injections
  if(omicron[0]->GetSgInjectionFlag() != omicron[1]->GetSgInjectionFlag()){
    monit->Monitor::SetMessageType(monitor_warning);
    *monit<<"OxOmicron::"<<__func__<<": the SG injections are only performed in one detector"<<endl;
  }
  
  // print a warning if the InjGen injections are not the same
  if(omicron[0]->GetInjectionEngine()->InjRea::GetN() != omicron[1]->GetInjectionEngine()->InjRea::GetN()){
    monit->Monitor::SetMessageType(monitor_warning);
    *monit<<"OxOmicron::"<<__func__<<": the InjGen injections are not the same"<<endl;
  }

  return true;
}

/**
 * @brief Forces some Omicron options for the processing of Ox files.
 * @param[in] aInFile Input option file.
 * @param[in] aOutFile Output option file.
 * @param[in] aOutDir Output directory.
 */
static bool ForceOmicronOptions(const string aInFile, const string aOutFile, const string aOutDir){

  // input file
  ifstream ifs;
  ifs.open(aInFile.c_str(), ios_base::in);
  if(ifs.is_open()==false) return false;

  // output file
  ofstream ofs;
  ofs.open(aOutFile.c_str(), ios_base::out);
  if(ofs.is_open()==false) return false;

  // read the input file line by line
  string line;
  while(getline(ifs, line)){

    // remove options to force them after
    if(line.find("PARAMETER")!=string::npos){
      if(line.find("FULLMAPNT")!=string::npos) continue;
      else if(line.find("SNRTHRESHOLD")!=string::npos) continue;
      else if(line.find("WINDOWS")!=string::npos) continue;
      else if(line.find("MAPLOGSCALE")!=string::npos) continue;
      else if(line.find("MAPVRANGE")!=string::npos) continue;
      else if(line.find("THUMB")!=string::npos) continue;
      else ofs<<line<<endl;
    }
    else if(line.find("OUTPUT")!=string::npos){
      if(line.find("DIRECTORY")!=string::npos) continue;
      else if(line.find("PRODUCTS")!=string::npos) continue;
      else if(line.find("FORMAT")!=string::npos) continue;
      else if(line.find("STYLE")!=string::npos) continue;
      else if(line.find("PLOTDIMENSIONS")!=string::npos) continue;
      else ofs<<line<<endl;
    }

    // otherwise keep the line as is
    else{
      ofs<<line<<endl;
    }
  }

  // close input file
  ifs.close();

  // force options
  ofs<<"PARAMETER FULLMAPNT     300"<<endl;
  ofs<<"PARAMETER SNRTHRESHOLD  1000.0 0.0"<<endl;
  ofs<<"PARAMETER MAPLOGSCALE   1"<<endl;
  ofs<<"PARAMETER MAPVRANGE     1.0 100.0"<<endl;
  ofs<<"PARAMETER THUMB         0"<<endl;

  ofs<<"OUTPUT DIRECTORY       "<<aOutDir<<endl;
  ofs<<"OUTPUT PRODUCTS        mapsnr"<<endl;
  ofs<<"OUTPUT FORMAT          png"<<endl;
  ofs<<"OUTPUT STYLE           OX"<<endl;

  // close output file
  ofs.close();


  return true;
}

