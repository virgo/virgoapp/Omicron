/**
 * @file 
 * @brief Ox analysis results.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __OxResults__
#define __OxResults__

#include "OxSim.h"
#include <TArrayL.h>

using namespace std;

/**
 * @brief List of Ox result types to classify Ox events.
 */
enum OxResultType{
             oxresult_foreground = 0,///< Foreground events: these Ox events are associated to the time-delay slice where gravitational-wave events can be detected (time delay below the maximum light-travel time between detectors).
             oxresult_background,    ///< Background events: these Ox events are associated to time-delay slices where gravitational-waves cannot be detected because the time delay between detectors is too large.
             oxresult_fakeground,    ///< Fake foreground events: one slice is isolated from the background slices to simulate a foreground slice where no gravitational-wave event can be detected. It is used to test the statistical agreement between the background dataset and a foreground dataset without gravitational-wave events.
             oxresult_n              ///< Number of Ox event types.
};

/**
 * @brief List of Ox result type names.
 */
const string OxResultTypeName[oxresult_n] = {"foreground", "background", "fakeground"};

/**
 * @brief Ox analysis results.
 * @details This class is designed to collect Ox events and present results of an Ox analysis. Ox events are provided with a list of Ox files pointed by the `OXI/OXFILELIST` option (see OxInit::OxInit()). 
 *
 * This class can also be initialized with a list of Ox files already containing results (also pointed by `OXI/OXFILELIST`). These Ox result files were previously produced with this class. When Ox result files are provided, all the options are defined by the content of the files and can no longer be changed.
 *
 * The results consist of a set of histograms and arrays of numbers to characterize a set of Ox events. This class provides many functions to generate plots and html pages to display the results.
 *
 * The results are separated into 3 categories:
 * - Foreground events: see ::oxresult_foreground
 * - Background events: see ::oxresult_background
 * - Fakeground events: see ::oxresult_fakeground
 */
class OxResults: public OxSim{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
   /**
   * @brief Constructor of the OxResults class.
   * @details OxResults options are:
   * - `OXR FAKEGROUND`: Select a slice to represent fakeground events. Two integer number are expected. The first one is the slice index. The second one is the time difference between the second and first detector. Use a negative slice index to randomly choose the fakeground slice.
   * - `OXR DETECTIONTHRESHOLD`: Detection thresholds. Two parameters are expected: a threshold on the event rank and a threshold on the false-alarm rate (Hz). If the threshold on the event rank is negative, it is ignored and the threshold on the false-alarm rate is considered. By default, both thresholds are set to 0.5.
   * - `OXR NLOUDEST`: Number of loudest events to save in each category: foreground, background, fakeground. Default = 5 for each category.
   * - `OXR NLOUDESTMAP`: Number of loudest events for which to print maps in each category: foreground, background, fakeground. Default = 0 for each category. Note that this number cannot exceed the number of loudest events provided with the `OXR/NLOUDEST` option. This option only applies when generating a web report with MakeHtml().
   * - `OXR NEVENTBINS`: Number of bins to histogram the event parameters. Default = 100.
   *
   * @warning If Ox results are found in the list of input Ox files (`OXI/OXFILELIST`), all OXR options found in the option file are ignored. Instead, all OXR options are derived from the Ox files. Moreover, the class is initialized with the results found in the input Ox files: see ReadInputFiles().
   *
   * @param[in] aOptionFile Path to the option file.
   * @param[in] aGpsRef0 Reference GPS time to intiate the Omicron object for the first detector.
   * @param[in] aGpsRef1 Reference GPS time to intiate the Omicron object for the second detector.
   */ 
  OxResults(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1);

  /**
   * @brief Destructor of the OxResults class.
   */
  virtual ~OxResults(void);

  /**
     @}
  */

  /**
   * @brief Processes the list of Ox events.
   * @details The list of events is processed. For each event, the result type (OxResultType) is identified. Processed segments are saved. Result plots are filled with FillHistograms(). The list of loudest events is updated with SaveLoudest(). For foreground events, injections are matched with OxInj::Match().
   *
   * The injections are detected above the rank threshold (GetRankDetectionThreshold) and the associated parameter histograms are filled.
   *
   * @warning Ox events from input Ox files with results are not processed!
   *
   * @note This function can only be called after an output file is opened with CreateOutputFile(). 
   */
  bool Process(void);

  /**
   * @brief Returns the number of processed events.
   * @param[in] aResultType Result type.
   */
  unsigned int GetProcessedEventsN(OxResultType aResultType){ return (unsigned int)h1_ev_rank[aResultType]->GetEntries(); };

  /**
   * @brief Returns the event rate at a given rank [Hz].
   * @details The rank histogram is used to derive the event rate at a given rank. Three types of event rates can be returned:
   * - if aType=0, the event rate is calculated over the rank bin.
   * - if atype>0, the event rate is calculated above that rank value.
   * - if atype<0, the event rate is calculated below that rank value.
   *
   * @warning The calculated event rate can be wrong for rank values outside the histogram limits.
   * @param[in] aResultType Result type.
   * @param[in] aRank Rank value.
   * @param[in] aType Calculation type.
   */
  double RankToRate(OxResultType aResultType, const double aRank, const int aType=0);

  /**
   * @brief Returns the event rank at a given rate.
   * @details The rank cummulative event distribution is used to derive a rank value from a rate value. The rate value is defined as the rate of events above that rank value. The cumultive distribution histogram is interpolated.
   *
   * If there is no events, the highest possible rank value is returned.
   * If the provided rate value is smaller than the lowest rate achieved by the histogram, the highest achievable rank value is returned.
   * @param[in] aResultType Result type.
   * @param[in] aRate Event rate [Hz].
   */
  double RateToRank(OxResultType aResultType, const double aRate);

  /**
   * @brief Returns the detection threshold on the event rank.
   * @details The rank threshold is defined with the first parameter of the `OXR/DETECTIONTHRESHOLD` option. If the threshold on the event rank (first parameter) is negative, it is ignored and the threshold on the false-alarm rate is considered. It is converted in to a threshold on the event rank using the current ::oxresult_background distribution.
   * @warning The background distribution must be computed first.
   */
  inline double GetRankDetectionThreshold(void){
    double threshold = ox_opt->GetOptionValueD("OXR", "DETECTIONTHRESHOLD", 0);
    if(threshold<0.0) return RateToRank(oxresult_background, ox_opt->GetOptionValueD("OXR", "DETECTIONTHRESHOLD", 1));
    return threshold;
  };

  /**
   * @brief Returns the result type of the current event.
   * @details A foreground event (::oxresult_foreground) is identified if the event slice is at the center of the cross-correlation map (OxEvent::GetSliceIndexCenter()) and the two detector Omicron spectrogram times match.
   * A fakeground event (::oxresult_fakeground) is identified if the event slice matches the fakeground definition.
   * Otherwise, the result type is ::oxresult_background.
   */
  OxResultType GetEventResultType(void);

  /**
   * @brief Creates an ouput Ox file to save results.
   * @details A new ROOT file is opened in the output directory to save the Ox analysis results (OxInit::CreateOutputFile()). All the result plots are initialized in the ouput file.
   * @sa OxInit::CreateOutputFile() for the file management.
   */
  bool CreateOutputFile(void);

  /**
   * @brief Saves all the result plots in the output directory.
   * @note Injection result plots are also saved with OxSim::SavePlots().
   * @param[in] aFormat ROOT-supported formats.
   */
  bool SavePlots(const string aFormat="png");

  /**
   * @brief Generates an html report in the output directory.
   */
  bool MakeHtml(void);

private:

  bool *results_found;                   ///< Flags input files when OXR results are found.
  unsigned int fake_slice_index;         ///< Fakeground definition: slice index.
  int fake_lag;                          ///< Fakeground definition: time lag [s].
  string outfilename;                    ///< Output file name.

  // OUTPUT TTREE
  TTree *oxr_tree;                       ///< Ox result tree.
  unsigned long int oxr_livetime;        ///< Result tree: cumulative livetime [s].
  TH1D* oxr_ev_rank;                     ///< Result tree: event rank distribution.
  TH1D* oxr_ev_freq;                     ///< Result tree: event frequency distribution.
  TH2D* oxr_ev_rank_freq;                ///< Result tree: event rank/frequency distribution.
  TArrayL *oxr_ev_loudest_entry;         ///< Result tree: list of event entries for loudest events: loudest first.
  TArrayD *oxr_ev_loudest_rank;          ///< Result tree: list of ranks for loudest events: loudest first.

  // TIMING
  unsigned long int livetime[oxresult_n];///< Analysis livetime [s].
  Segments *segments[2][oxresult_n];     ///< Analysis segments for the two detectors.
  
  // LOCALS
  TH1D *h1_ev_rank[oxresult_n];          ///< Event rank distribution.
  TH1D *h1_ev_freq[oxresult_n];          ///< Event frequency distribution.
  TH2D *h2_ev_rank_freq[oxresult_n];     ///< Event rank/frequency distribution.
  TArrayL *ev_loudest_entry[oxresult_n]; ///< List of event entries for loudest events: loudest first.
  TArrayD *ev_loudest_rank[oxresult_n];  ///< List of ranks for loudest events: loudest first.

  /**
   * @brief Extracts Ox results from input Ox files.
   * @details Input Ox files are scanned. Ox results are extracted from "oxr" trees. Result files are tagged, not to be processed with Process().
   * The Ox results are cumulated from all the input files.
   * @returns true if at least one "oxr" tree is found in the input files and if the tree is formatted as expected.
   *
   * @note If results are found in the input files, this function creates local histograms with CreateHistograms() and local arrays with CreateArrays().
   */
  bool ExtractResults(void);

  /**
   * @brief Saves the loudest events.
   * @details The list of loudest events is sorted by decreasing rank values. The input event is saved if it is louder than the last event in the list. It is inserted to maintain the event order.
   * @param[in] aResultType Result type for this event.
   * @param[in] aEntry Event entry number.
   * @param[in] aRank Event rank.
   */
  void SaveLoudest(const OxResultType aResultType, const Long64_t aEntry, const double aRank);

  /**
   * @brief Saves the list of loudest events in output tree.
   * @details The list of loudest events is scanned. Events are loaded and are saved in the output event tree.
   * @param[in] aResultType Result type for this event.
   */
  void SaveLoudestTree(const OxResultType aResultType);

  /**
   * @brief Creates the result arrays.
   */
  void CreateArrays(void);

  /**
   * @brief Fills the result histograms.
   * @param[in] aResultType Result type for this event.
   */
  void FillHistograms(const OxResultType aResultType);

  /**
   * @brief Creates the result histograms.
   */
  void CreateHistograms(void);

  /**
   * @brief Draws a random slice to represent the fakeground.
   */
  void DrawFakeground(void);

  /**
   * @brief Prints the list of loudest events in the html report.
   * @param[in] aReport Output stream for the html report.
   * @param[in] aResultType Result type.
   */
  void MakeHtmlLoudest(ofstream &aReport, const OxResultType aResultType);
  
  ClassDef(OxResults,0)  
};


#endif


