/**
 * @file 
 * @brief See OmicronUtils.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OmicronUtils.h"

static string GetFiles(unsigned int &aEffectiveTimeEnd, const string aDir,
                       const unsigned int aTimeStart, const unsigned int aTimeEnd);

static string GetFilesHpss(unsigned int &aEffectiveTimeEnd, const string aDir,
                           const unsigned int aTimeStart, const unsigned int aTimeEnd);

////////////////////////////////////////////////////////////////////////////////////
string GetOmicronFilePattern(const string aChannelName,
                             const unsigned int aTimeStart, const unsigned int aTimeEnd){
////////////////////////////////////////////////////////////////////////////////////

  // trigger directories
  char *stmp = getenv("OMICRON_TRIGGERS");
  string trigdir;
  if(stmp==NULL) trigdir=".";
  else trigdir = (string)stmp;
  stmp = getenv("OMICRON_TRIGGERS_ONLINE");
  string onlinedir;
  if(stmp==NULL) onlinedir=".";
  else onlinedir = (string)stmp;

  string filelist="";
  unsigned int effend = 0;
  
  // channel stream
  Streams *S = new Streams(aChannelName, 0);
  
  // LIGO-Virgo OFFLINE directory
  gwl_ss<< trigdir << "/" << S->GetNamePrefix() << "/" << S->GetNameSuffixUnderScore() << "_OMICRON";
  if(trigdir.find("hpss")!=string::npos)
    filelist += GetFilesHpss(effend, gwl_ss.str(), aTimeStart, aTimeEnd);
  else
    filelist += GetFiles(effend, gwl_ss.str(), aTimeStart, aTimeEnd);
  gwl_ss.clear(); gwl_ss.str("");

  // LIGO-Virgo ONLINE directory
  // (starting where we stopped in the offline directory)
  if(effend<aTimeEnd){
    gwl_ss<< onlinedir << "/" << S->GetNamePrefix() << "/" << S->GetNameSuffixUnderScore() << "_OMICRON";
    filelist+=GetFiles(effend, gwl_ss.str(), TMath::Max(effend, aTimeStart), aTimeEnd);
    gwl_ss.clear(); gwl_ss.str("");
  }
  
  delete S;
  return filelist;
}

/**
 * @brief Returns the list of Omicron trigger file patterns.
 * @param[out] aEffectiveTimeEnd Effective GPS end time of the last time directory used.
 * @param[in] aDir Directory.
 * @param[in] aTimeStart GPS start time.
 * @param[in] aTimeEnd GPS end time.
 */
static string GetFiles(unsigned int &aEffectiveTimeEnd, const string aDir,
                       const unsigned int aTimeStart, const unsigned int aTimeEnd){

  aEffectiveTimeEnd = aTimeStart;
  string filelist="";
  vector <string> timedir;
  vector <string> vfiles;
  vector <string> vfilefrag;
  unsigned int fstart, fstop;
  unsigned int g_start = aTimeStart/100000;
  unsigned int g_end = aTimeEnd/100000;
  vector<string> dir_vector;
  unsigned int dir_time;
 
  if(!filesystem::is_directory(aDir)) return "";
  
  // get list of time directories
  timedir = Glob((aDir+"/*").c_str());
  if(timedir.size()==0) return "";

  // find the first relevant directory
  unsigned int g=0;
  for(g=0; g<timedir.size(); g++){

    // get time index from dir string
    dir_vector = SplitString(timedir[g], '/');
    if(dir_vector.size()==0) continue;
    dir_time = stoul(dir_vector.back());
    dir_vector.clear();

    if(dir_time>=g_start) break;
  }

  // start with the directory before the requested start
  if(g>0) g--;

  // loop over time directories
  for(; g<timedir.size(); g++){

    // get time index from dir string
    dir_vector = SplitString(timedir[g], '/');
    if(dir_vector.size()==0) continue;
    dir_time = stoul(dir_vector.back());
    dir_vector.clear();

    // **** The time directory matches the start time or end time
    // --> select files individually
    if((dir_time<=g_start) || (dir_time==g_end)){

      // get list of files in this time directory
      vfiles = Glob((timedir[g]+"*.root").c_str());

      // loop over root files
      for(unsigned int v=0; v<vfiles.size(); v++){

	// get file fragments
	vfilefrag.clear();
	vfilefrag = SplitString(GetFileNameFromPath(vfiles[v]),'-');

	// check file naming convention (4 fragments)
	if(vfilefrag.size()!=4) continue;

	// file start
	fstart = stoul(vfilefrag[2].c_str());
	if(fstart>=aTimeEnd) break;

	// file stop
	fstop = fstart+stoul(vfilefrag[3].substr(0,vfilefrag[3].size()-5).c_str());
	if(fstop<=aTimeStart) continue;

	// select file
	filelist += (vfiles[v] + " ");
        if(fstop>aEffectiveTimeEnd) aEffectiveTimeEnd = fstop;
      }
    }

    // **** The time directory is entirely covered
    else if((dir_time>g_start) && (dir_time<g_end)){

      // select the entire directory
      filelist += (timedir[g] + "*.root ");

      // effective stop
      aEffectiveTimeEnd= (dir_time+1)*100000;
    }

    // **** The time directory is after --> stop
    else if(dir_time>g_end) break;

    // **** Should never happen
    else break;
  }

  timedir.clear();

  return filelist;
}

/**
 * @brief Returns the list of Omicron trigger files (in HPSS).
 * @details The standard omicron trigger directory (HPSS) is scanned.
 * The list of files matching the input time range are returned.
 * @param[out] aEffectiveTimeEnd Effective GPS end time of the last returned file.
 * @param[in] aDir Directory.
 * @param[in] aTimeStart GPS start time.
 * @param[in] aTimeEnd GPS end time.
 */
static string GetFilesHpss(unsigned int &aEffectiveTimeEnd, const string aDir,
                           const unsigned int aTimeStart, const unsigned int aTimeEnd){

  string filelist="";
  aEffectiveTimeEnd = aTimeStart;
  
  // random id
  int randid = rand();

  // tmp file
  gwl_ss<<ReplaceAll(filesystem::temp_directory_path(),"\"","")<<"/omf-"<<aTimeStart<<"-"<<aTimeEnd<<"."<<randid<<".tmp";
  string tmpfile = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");

  // rfdir command
  gwl_ss<<"rfdir "<<aDir<<" | grep -v \"\\.\" | awk '{print $9}' | sort -u> "<<tmpfile;

  // dump list of channel sub-directories in a tmp file
  if(system(gwl_ss.str().c_str())!=0) return filelist;
  gwl_ss.clear(); gwl_ss.str("");

  ReadAscii *R = new ReadAscii(tmpfile,"u");
  if(!R->GetNRow()) { delete R; return filelist; }

  // list of gps directories
  unsigned int stoproot = aTimeEnd / 100000;
  vector <unsigned int> gpsdir; unsigned int gps;
  for(unsigned int g=0; g<R->GetNRow(); g++){
    R->GetElement(gps,g,0);
    if(gps<=stoproot) gpsdir.push_back(gps);
  }

  delete R;
  
  // list relevant files in gps directories
  vector <string> vfilefrag; string filename;
  unsigned int fstart, fstop;
  for(unsigned int g=0; g<gpsdir.size(); g++){
    gwl_ss<<"rfdir "<<aDir<<"/"<<gpsdir[g]<<" | grep OMICRON | awk '{print $9}' | sort -u> "<<tmpfile;
    if(system(gwl_ss.str().c_str())!=0) return "";
    gwl_ss.clear(); gwl_ss.str("");

    R = new ReadAscii(tmpfile,"s");

    // select relevant files in that gps directories
    for(unsigned int gg=0; gg<R->GetNRow(); gg++){
      R->GetElement(filename,gg,0);
     
      // get file fragments
      vfilefrag.clear();
      vfilefrag = SplitString(filename,'-');
    
      // check file naming convention (4 fragments)
      if(vfilefrag.size()!=4) continue;
    
      // file timing
      fstart = atoi(vfilefrag[2].c_str());
      fstop = fstart+atoi(vfilefrag[3].substr(0,vfilefrag[3].size()-5).c_str());
    
      // check file start time
      if(fstart>=aTimeEnd) continue;
    
      // check file stop time
      if(fstop<=aTimeStart) continue;

      // save effective end
      if(fstop>aEffectiveTimeEnd)
        aEffectiveTimeEnd = fstop;
      
      // full file name
      gwl_ss<<"root://ccxroot:1999/"<<aDir<<"/"<<gpsdir[g]<<"/"<<filename;

      // select file
      filelist += (gwl_ss.str()+" ");
      gwl_ss.clear(); gwl_ss.str("");

    }

    error_code ec;
    if(RemoveFileOrDirectory(ec, tmpfile)!=1)
      cerr<<"GetOmicronFilePatternFromHpss: clean-up failed ("<<ec<<")"<<endl;

    delete R;
  }

  return filelist;
}

