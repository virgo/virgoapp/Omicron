/**
 * @file 
 * @brief Omicron channels.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Ochannel__
#define __Ochannel__
#include "Oconfig.h"
#include <TriggerBuffer.h>

using namespace std;

/**
 * @brief Manage Omicron channels.
 * @details This class is designed to manage Omicron triggers of a given channel. This is achieved with the TriggerBuffer class. On top of this, the trigger production is monitored with chunk counters.
 */
class Ochannel: public TriggerBuffer{
  
public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
  /**
   * @brief Constructor of the Ochannel class.
   * @param[in] aSize Trigger buffer size.
   * @param[in] aStreamName Channel name.
   * @param[in] aVerbose Verbosity level.
   */
  Ochannel(const unsigned int aSize, const string aStreamName, const int unsigned aVerbose=0);
  
  /**
   * @brief Destructor of the Ochannel class.
   */
  virtual ~Ochannel(void);
  
  /**
     @}
  */

  /**
   * @brief Resets the history of saved chunks.
   */
  void ResetHistory(void);

  /**
   * @brief Resets the current chunk.
   */
  void ResetChunk(void);

  /**
   * @brief Sets the number of triggers for the current chunk.
   * @param[in] aN Number of triggers.
   */
  inline void SetChunkTriggersN(const unsigned int aN){ chunk_trig_n = aN; };

  /**
   * @brief Returns the number of triggers for the current chunk.
   */
  inline unsigned int GetChunkTriggersN(void){ return chunk_trig_n; };

  /**
   * @brief Add a new chunk SNR.
   * @details The maximum SNR over all chunks is saved.
   * @param[in] aSnr New SNR value to add.
   */
  inline void AddMapSNR(const double aSnr){
    if(aSnr>chunk_map_snrmax) chunk_map_snrmax = aSnr;
  };
  
  /**
   * @brief Returns the map maximum SNR for the all chunks.
   */
  inline double GetMapSnr(void){ return chunk_map_snrmax; };

  /**
   * @brief Adds a new chunk to load.
   * @param[in] aChunkTime Chunk GPS time.
   */
  inline void ChunkLoad(const unsigned int aChunkTime){
    chunk_load.push_back(aChunkTime);
  };

  /**
   * @brief Returns the number of loaded chunks.
   */
  inline unsigned int GetChunkLoadN(void){
    return chunk_load.size();
  };
  
  /**
   * @brief Returns the GPS time of a loaded chunk.
   * @param[in] aChunkIndex Chunk index: must be valid.
   */
  inline unsigned int GetChunkLoad(const unsigned int aChunkIndex){
    return chunk_load[aChunkIndex];
  };
  
  /**
   * @brief Adds a new chunk to condition.
   * @param[in] aChunkTime Chunk GPS time.
   */
  inline void ChunkCondition(const unsigned int aChunkTime){
    chunk_condition.push_back(aChunkTime);
  };
  
  /**
   * @brief Returns the number of conditioned chunks.
   */
  inline unsigned int GetChunkConditionN(void){
    return chunk_condition.size();
  };
  
  /**
   * @brief Adds a new chunk to project.
   * @param[in] aChunkTime Chunk GPS time.
   */
  inline void ChunkProject(const unsigned int aChunkTime){
    chunk_project.push_back(aChunkTime);
  };
  
  /**
   * @brief Returns the number of projected chunks.
   */
  inline unsigned int GetChunkProjectN(void){
    return chunk_project.size();
  };
  
  /**
   * @brief Adds a new chunk to write.
   * @param[in] aChunkTime Chunk GPS time.
   * @param[in] aChunkTriggerFile Chunk trigger file path.
   */
  inline void ChunkWrite(const unsigned int aChunkTime, const string aChunkTriggerFile){
    chunk_write.push_back(aChunkTime);
    chunk_write_tfile.push_back(aChunkTriggerFile);
  };
  
  /**
   * @brief Returns the number of written chunks.
   */
  inline unsigned int GetChunkWriteN(void){
    return chunk_write.size();
  };
  
  /**
   * @brief Searches and returns a chunk trigger file path.
   * @param[in] aChunkTime Chunk GPS time.
   */
  string GetChunkTriggerFile(const unsigned int aChunkTime);
  
  /**
   * @brief Adds a new processed chunk segment.
   * @param[in] aTimeStart GPS start time.
   * @param[in] aTimeEnd GPS end time.
   */
  inline bool AddChunkProcSegment(const unsigned int aTimeStart, const unsigned int aTimeEnd){
    return proc_segments->AddSegment((double)aTimeStart, (double)aTimeEnd);
  };
  
  /**
   * @brief Returns the processed chunk segments.
   */
  inline Segments* GetChunkProcSegment(void){
    return proc_segments;
  };
  
  /**
   * @brief Sets the channel output directory.
   * @pre It is assumed the directory is valid.
   * @param[in] aPath Path to the output directory.
   */
  inline void SetOutputDirectory(const string aPath){ outdir = aPath; };

  /**
   * @brief Returns the channel output directory.
   */
  inline string GetOutputDirectory(void){ return outdir; };

private:

  string outdir;                        ///< Output directory.
  unsigned int chunk_trig_n;            ///< Number of triggers in the current chunk above the SNR threshold.
  double chunk_map_snrmax;              ///< Maximum SNR value within the first map window (over all chunks).
  vector<unsigned int> chunk_load;      ///< List of chunk times (load).
  vector<unsigned int> chunk_condition; ///< List of chunk times (condition).
  vector<unsigned int> chunk_project;   ///< List of chunk times (project).
  vector<unsigned int> chunk_write;     ///< List of chunk times (write).
  vector<string> chunk_write_tfile;     ///< List of trigger files (write).
  Segments *proc_segments;              ///< Processed segments.

};


#endif


