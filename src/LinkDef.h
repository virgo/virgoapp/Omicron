/**
 * @file 
 * @brief Omicron class linker.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Omap;
#pragma link C++ class Oqplane;
#pragma link C++ class Osequence;
#pragma link C++ class Otile;
#pragma link C++ class Oinject;
#pragma link C++ class Omicron;

#endif

