/**
 * @file 
 * @brief Ox simulation results.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __OxSim__
#define __OxSim__
#include "OxEvent.h"

using namespace std;

/**
 * @brief Analyze injection results for Ox analyses.
 * @details This class is used to recover injections in a set of Ox events. After constructing the object:
 * - Call Match() to associate Ox events to injections.
 * - Call Process() to detect injections, fixing the false-alarm rate threshold.
 */
class OxSim: public OxEvent{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
  /**
   * @brief Constructor of the OxSim class.
   * @details OxSim options are:
   * - `OXS NBINS`: Provide a number of bins to histogram injection parameters. Default = 20.
   *
   * In this constructor, histograms are initialized for each waveform name. If Ox files results are provided, injection results are checked in these files. If they are found, injection results are loaded: ExtractResults().
   * @note It is assumed that the same injection list is used for both detectors. Only the injections for detector 0 are considered.
   * @param[in] aOptionFile Path to the option file.
   * @param[in] aGpsRef0 Reference GPS time to intiate the Omicron object for the first detector.
   * @param[in] aGpsRef1 Reference GPS time to intiate the Omicron object for the second detector.
   */
  OxSim(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1);

  /**
   * @brief Destructor of the OxSim class.
   */
  virtual ~OxSim(void);

  /**
     @}
  */

  /**
   * @brief Resets the injection set.
   * @details The event matching is reset and all histograms are reset.
   */
  void Reset(void);

  /**
   * @brief Matches injections with the current Ox event.
   * @details An injection is set as valid (injection tag = true) if the injection time is found in the event time range. If more than one event matches an injection, the event with the highest rank is associated with the injection.
   *
   * @warning The geocentric time of the injection is considered. In other words, we neglect the time propagation to the detector sites.
   */
  void Match(void);
  
  /**
   * @brief Processes injections.
   * @details After injections have been matched to Ox events with Match(), call this function to detect injections and fill plots. An injection is detected if the associated event rank value is above the detection threshold.
   *
   * The list of missed injections is dumped in a text file in the output directory.  All the histograms are saved in the output TTree if there is one open with CreateOutputFile().
   *
   * @warning The internal histograms are not reset. Do not use this function several times.
   *
   * @param[in] aRankDetectionThreshold Detection threshold on the event rank.
   */
  void Process(const double aRankDetectionThreshold);
  
  /**
   * @brief Creates an ouput file.
   * @details A new ROOT file is opened in the output directory to save the Ox analysis results (OxInit::CreateOutputFile()). The Ox injection TTree "oxs" is initialized in the ouput file.
   * @sa OxInit::CreateOutputFile() for the file management.
   * @param[out] aOutFileName Output ROOT file name.
   * @param[in] aFileId File ID used to name the ouput Ox file.
   * @param[in] aTime Time used to name the ouput file.
   */
  bool CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime);

  /**
   * @brief Returns the injection amplitude at a given detection efficiency. 
   * @details The detection efficiency is defined as the ratio between the number of detected injections and the number of valid injections. It is measured as a function of the injection amplitude. This function returns the injection amplitude at a given detection efficiency. It is estimated by a simple interpolation between neighboring bins in injection amplitude.
   * @returns -1 is the requested detection efficiency cannot be achieved. 
   * @todo Improve the calculation, not to rely on a basic interpolation. Fit?  
   * @param[in] aNameIndex Injection name index.
   * @param[in] aEfficiency Detection efficiency value (between 0 and 1).
   */
  double GetInjectionAmplitudeAtEfficiency(const unsigned int aNameIndex, const double aEfficiency);
  
  /**
   * @brief Returns the number of valid injections of a given name.
   * @param[in] aNameIndex Injection name index.
   * @pre The injection name index must be valid.
   */
  inline unsigned int GetValidN(const unsigned int aNameIndex){ return (unsigned int)h1_inj_amp_tot[aNameIndex]->GetEntries(); };
  
  /**
   * @brief Returns the number of detected injections of a given name.
   * @param[in] aNameIndex Injection name index.
   * @pre The injection name index must be valid.
   */
  inline unsigned int GetDetectedN(const unsigned int aNameIndex){ return (unsigned int)h1_inj_amp_det[aNameIndex]->GetEntries(); };

  /**
   * @brief Saves all the injection result plots in the output directory.
   * @param[in] aFormat ROOT-supported formats.
   */
  bool SavePlots(const string aFormat="png");
  
  /**
   * @brief Generates an html report for the current event.
   * @details Am html report is generated in the output directory. It provides all the information (numbers and plots) to characterize the current event.
   */  
  bool MakeHtml(void);

protected:

  /**
   * @brief Prints the results of injection studies in the html report.
   * @param[in] aReport Output stream for the html report.
   */
  void MakeHtmlResults(ofstream &aReport);

private:

  InjEct *inject;              ///< Pointer to the list of Omicron injections (detector 0): do not delete.
  double *inj_rk;              ///< List of event ranks.
  Long64_t *inj_entry;         ///< List of event entries.

  // INJECTION PARAMETERS
  TH1D** h1_inj_amp_tot;       ///< Injection amplitude distribution /injection name.
  TH1D** h1_inj_amp_det;       ///< Detected injection amplitude distribution /injection name.
  TH1D** h1_inj_time_tot;      ///< Injection time distribution /injection name.
  TH1D** h1_inj_time_det;      ///< Detected time amplitude distribution /injection name.
  TH1D** h1_inj_freq_tot;      ///< Injection frequency distribution /injection name.
  TH1D** h1_inj_freq_det;      ///< Detected frequency amplitude distribution /injection name.

  // INJECTION RECONSTRUCTION
  TH2D** h2_rec_rank_amp;      ///< Reconstruction: rank-amplitude /injection name.
  TH1D** h1_rec_timedelay;     ///< Reconstruction: time-delay (rec-inj) /injection name.
  TH2D** h2_rec_timedelay;     ///< Reconstruction: time-delay (rec vs inj) /injection name.
  TH2D** h2_rec_timedelay_amp; ///< Reconstruction: time-delay (rec-inj) vs amplitude /injection name.
  TH1D** h1_rec_aratio;        ///< Reconstruction: amplitude ratio (rec-inj) /injection name.
  TH2D** h2_rec_aratio;        ///< Reconstruction: amplitude ratio (rec vs inj) /injection name.
  TH2D** h2_rec_aratio_amp;    ///< Reconstruction: amplitude ratio (rec-inj) vs amplitude /injection name.

  // RESULT TREE
  TTree* oxs_tree;             ///< Ox injection tree.
  string *oxs_inj_name;        ///< Injection name.
  TH1D* oxs_h1_inj_amp_tot;    ///< Injection amplitude distribution.
  TH1D* oxs_h1_inj_amp_det;    ///< Detected injection amplitude distribution.
  TH1D* oxs_h1_inj_time_tot;   ///< Injection time distribution.
  TH1D* oxs_h1_inj_time_det;   ///< Detected time amplitude distribution.
  TH1D* oxs_h1_inj_freq_tot;   ///< Injection frequency distribution.
  TH1D* oxs_h1_inj_freq_det;   ///< Detected frequency amplitude distribution.
  TH2D* oxs_h2_rec_rank_amp;   ///< Reconstruction: rank-amplitude.
  TH1D* oxs_h1_rec_timedelay;  ///< Reconstruction: time-delay (rec-inj).
  TH2D* oxs_h2_rec_timedelay;  ///< Reconstruction: time-delay (rec vs inj).
  TH2D* oxs_h2_rec_timedelay_amp; ///< Reconstruction: time-delay (rec-inj) vs amplitude.
  TH1D* oxs_h1_rec_aratio;     ///< Reconstruction: amplitude ratio (rec-inj).
  TH2D* oxs_h2_rec_aratio;     ///< Reconstruction: amplitude ratio (rec vs inj).
  TH2D* oxs_h2_rec_aratio_amp; ///< Reconstruction: amplitude ratio (rec-inj) vs amplitude.

  /**
   * @brief Prints a table with injection parameters in the html report.
   * @details One table is printed for each injection name.
   * @param[in] aReport Output stream for the html report.
   */
  void MakeHtmlInjectionTable(ofstream &aReport);

  /**
   * @brief Reads injection analysis results from input Ox files.
   * @details The injection results are saved in Ox files. These results can be read back by calling this function. All TTree are read and all histograms are filled.
   */
  void ExtractResults(void);
  

  ClassDef(OxSim,0)  
};


#endif


