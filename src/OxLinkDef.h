/**
 * @file 
 * @brief Omicron class linker.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class OxInit;
#pragma link C++ class OxOmicron;
#pragma link C++ class OxCorr;
#pragma link C++ class OxEvent;
#pragma link C++ class OxSim;
#pragma link C++ class OxResults;
#pragma link C++ class OxAnalysis;

#endif

