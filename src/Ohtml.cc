/**
 * @file 
 * @brief See Oomicron.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oomicron.h"


////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeHtml(void){
////////////////////////////////////////////////////////////////////////////////////

  // import material
  gwl_ss.clear(); gwl_ss.str("");
  char *omicron_html_c = getenv("OMICRON_HTML");
  char *gwollum_pix_c = getenv("GWOLLUM_PIX");
  string omicron_html = ".";
  string gwollum_pix = ".";
  if(omicron_html_c!=NULL) omicron_html= (string)omicron_html_c;
  if(gwollum_pix_c!=NULL) gwollum_pix= (string)gwollum_pix_c;
  error_code ec;
  if(!CopyFile(ec, omicron_html+"/style/style."+tile->GetCurrentStyle()+".css", MainDir+"/style.css", true)){
    cerr<<"Omicron::MakeHtml: cannot copy style css sheet ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, gwollum_pix+"/gwollum_logo_min_trans.gif", MainDir+"/icon.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy gwollum logo ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, omicron_html+"/pics/led-blue.gif", MainDir+"/led-blue.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy blue led ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, omicron_html+"/pics/led-green.gif", MainDir+"/led-green.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy green led ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, omicron_html+"/pics/led-red.gif", MainDir+"/led-red.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy red led ("<<ec<<")"<<endl;
  }
  RemoveFileOrDirectory(ec, MainDir+"/logo.gif");
  if(!GwollumOptions::GetOptionValueU("OUTPUT", "NOLOGO", 0)){
    if(!CopyFile(ec, omicron_html+"/pics/omicronlogo."+tile->GetCurrentStyle()+".gif", MainDir+"/logo.gif", true)){
      cerr<<"Omicron::MakeHtml: cannot copy omicron logo ("<<ec<<")"<<endl;
    }
  }
  GwollumOptions::WriteTextFile(MainDir+"/omicron.parameters.txt");
  
  // select format
  string form;
  if(GO_OutFormat.find("png")!=string::npos) form="png";
  else if(GO_OutFormat.find("gif")!=string::npos) form="gif";
  else if(GO_OutFormat.find("jpg")!=string::npos) form="jpg";
  else form="";

  // thumbnail size
  unsigned int th_w = (unsigned int)((double)(GwollumOptions::GetOptionValueU("OUTPUT", "PLOTDIMENSIONS",0))*(double)O_THUMBNAIL_RATIO);

  // window set
  vector <unsigned int> windows = tile->GetPlotTimeWindows();
  stringstream tmpstream;
  tmpstream<<"[";
  for(unsigned int w=0; w<windows.size(); w++){
    if(w) tmpstream<<",";
    tmpstream<<"'"<<windows[w]<<"'";

    // sound images
    if(GO_OutFormat.find("wav")!=string::npos){
      gwl_ss<<windows[w];
      tile->Clear();
      tile->AddText(("- "+gwl_ss.str()+" s -").c_str(), 0.1, 0.3, 0.35);
      tile->Print(MainDir+"/listen-"+gwl_ss.str()+".gif", (double)O_THUMBNAIL_RATIO);
      gwl_ss.clear(); gwl_ss.str("");
    }
  }
  tmpstream<<"]";
  string windowset = tmpstream.str();
  tmpstream.clear(); tmpstream.str("");

  // index header & scripts
  ofstream report((MainDir+"/"+GwollumOptions::GetOptionValueS("OUTPUT", "HTMLMAINPAGE", 0)).c_str());
  report<<"<html>"<<endl;
  report<<"<!-- Begin header -->"<<endl;
  report<<"<head>"<<endl;
  if(inSegments->GetN()>0)
    report<<"<title>Omicron Report "<<(unsigned int)inSegments->GetFirst()<<"-"<<(unsigned int)inSegments->GetLast()<<"</title>"<<endl;
  else
    report<<"<title>Omicron Report</title>"<<endl;
  report<<"<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"<<endl;
  report<<"<link rel=\"icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function showImage(channel, channelconv, type, timerange, format) {"<<endl;
  report<<"  for (var dt in timerange) {"<<endl;
  report<<"    var basename ="<<endl;
  report<<"      channelconv + \"_\" + type + \"-\" + timerange[dt];"<<endl;
  report<<"    document.getElementById(\"a_\" + channelconv + \"-\" + timerange[dt]).href ="<<endl;
  report<<"      \"./\" + channel + \"/\" + basename + \".\" + format;"<<endl;
  report<<"    document.getElementById(\"img_\" + channelconv + \"-\" + timerange[dt]).src ="<<endl;
  report<<"      \"./\" + channel + \"/";
  if(GO_thumb){
    report<<"th\" + basename + \".\" + format;"<<endl;
  }
  else{
    report<<"\" + basename + \".\" + format;"<<endl;
    report<<"    document.getElementById(\"img_\" + channelconv + \"-\" + timerange[dt]).width ="<<endl;
    report<<"      \""<<th_w<<"\";"<<endl;
  }
  report<<"  }"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function showSound(channel, channelconv, type, timerange) {"<<endl;
  report<<"  for (var dt in timerange) {"<<endl;
  report<<"    var basename ="<<endl;
  report<<"      channelconv + \"_\" + type + \"-\" + timerange[dt];"<<endl;
  report<<"    document.getElementById(\"a_\" + channelconv + \"-\" + timerange[dt]).href ="<<endl;
  report<<"      \"./\" + channel + \"/\" + basename + \".wav\";"<<endl;
  report<<"    document.getElementById(\"img_\" + channelconv + \"-\" + timerange[dt]).src ="<<endl;
  report<<"      \"./listen-\" + timerange[dt] + \".gif\";"<<endl;
  report<<"  }"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function toggle(anId) {"<<endl;
  report<<"  node = document.getElementById(anId);"<<endl;
  report<<"  if (node.style.visibility==\"hidden\") {"<<endl;
  report<<"    node.style.visibility = \"visible\";"<<endl;
  report<<"    node.style.height = \"auto\";"<<endl;
  report<<"  }"<<endl;
  report<<"  else {"<<endl;
  report<<"    node.style.visibility = \"hidden\";"<<endl;
  report<<"    node.style.height = \"0\";"<<endl;
  report<<"  }"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"</head>"<<endl;
  report<<"<!-- End header -->"<<endl;
  report<<"<body>"<<endl;
  report<<endl;

  // index title
  report<<"<h1>Omicron Report </h1>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;

  // index summary
  report<<"<!-- Begin summary -->"<<endl;
  report<<"<h2>Summary</h2>"<<endl;
  tm utc; unsigned int gps;
  report<<"<table>"<<endl;
  if(inSegments->GetN()>0)
    gps = (unsigned int)inSegments->GetFirst();
  else
    gps = 0;
  GPSToUTC(&utc, gps);
  char *login = getlogin();
  report<<"  <tr><td>Omicron version:</td><td>"<<(string)O_PROJECT_VERSION<<": <a href=\"https://virgo.docs.ligo.org/virgoapp/Omicron/\" target=\"_blank\">documentation</a>, <a href=\"https://git.ligo.org/virgo/virgoapp/Omicron\" target=\"_blank\">gitlab repository</a></td></tr>"<<endl;
  if(login!=NULL)
    report<<"  <tr><td>Omicron run by:</td><td>"<<(string)login<<"</td></tr>"<<endl;
  else
    report<<"  <tr><td>Omicron run by:</td><td>unknown</td></tr>"<<endl;
  report<<"  <tr><td>Omicron processing time:</td><td>"<<mon->GetProcessingTimeString()<<"</td></tr>"<<endl;
  report<<"  <tr><td>Processing Date:</td><td>"<<mon->GetProcessDate()<<"</td></tr>"<<endl;
  report<<"  <tr><td>Requested start:</td><td>"<<gps<<" &rarr; "<<asctime(&utc)<<" (UTC)</td></tr>"<<endl;
  if(inSegments->GetN()>0)
    gps = (unsigned int)inSegments->GetLast();
  else
    gps = 0;
  GPSToUTC(&utc, gps);
  report<<"  <tr><td>Requested stop:</td><td>"<<gps<<" &rarr; "<<asctime(&utc)<<" (UTC)</td></tr>"<<endl;
  report<<"  <tr><td>Requested livetime:</td><td>"<<(unsigned int)inSegments->GetLiveTime()<<" sec &rarr; "<<setprecision(3)<<fixed<<inSegments->GetLiveTime()/3600.0/24<<" days</td></tr>"<<endl;
  inSegments->Dump(2, MainDir+"/omicron.segments.txt");
  report<<"  <tr><td>Requested segments:</td><td><a href=\"./omicron.segments.txt\">omicron.segments.txt</a></td></tr>"<<endl;
  report<<"  <tr><td>Number of chunks:</td><td>"<<inChunksN<<"</td></tr>"<<endl;
  report<<"  <tr><td>Number of channels:</td><td>"<<channels.size()<<"</td></tr>"<<endl;
  if(GwollumOptions::GetOptionValueU("PARAMETER", "STRICTMODE", 0)==1)
    report<<"  <tr><td>Run in STRICT mode:</td><td>YES</td></tr>"<<endl;
  else
    report<<"  <tr><td>Run in STRICT mode:</td><td>NO</td></tr>"<<endl;
  report<<"  <tr><td>Configuration:</td><td><a href=\"./omicron.parameters.txt\">omicron.parameters.txt</a>";
  if(GO_OutProducts.find("option")!=string::npos)
    report<<", <a href=\"./omicron.parameters.root\">omicron.parameters.root</a>";
  report<<"</td></tr>"<<endl;
  if(GO_OutProducts.find("summary")!=string::npos)
    report<<"  <tr><td>Summary file:</td><td><a href=\"./summary.txt\">summary.txt</a></td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<"<!-- End summary -->"<<endl;
  report<<endl;

  // search parameters
  report<<"<!-- Begin parameters -->"<<endl;
  report<<"<h2>Parameters</h2>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td>Timing:</td><td> chunks of "<<tile->GetTimeRange()<<" sec, overlapping by "<<tile->GetOverlapDuration()<<" sec</td></tr>"<<endl;
  report<<"  <tr><td>Sampling frequency:</td><td>"<<channels[0]->GetWorkingFrequency()<<" Hz</td></tr>"<<endl;
  report<<"  <tr><td>Frequency range:</td><td>"<<tile->GetFrequencyMin()<<" &rarr; "<<tile->GetFrequencyMax()<<" Hz</td></tr>"<<endl;
  report<<"  <tr><td>Q range:</td><td>"<<tile->GetQ(0)<<" &rarr; "<<tile->GetQ(tile->GetQN()-1)<<"</td></tr>"<<endl;
  report<<"  <tr><td>Tiling maximal mismatch:</td><td>"<<tile->GetMismatchMax()*100.0<<" %</td></tr>"<<endl;
  if(GO_OutProducts.find("triggers")!=string::npos)
    report<<"  <tr><td>SNR threshold (triggers):</td><td>SNR &gt; "<<tile->GetSnrTriggerThr()<<"</td></tr>"<<endl;
  if(GO_OutProducts.find("map")!=string::npos)
    report<<"  <tr><td>SNR threshold (maps):</td><td>SNR &gt; "<<tile->GetSnrMapThr()<<"</td></tr>"<<endl;
  if(GwollumOptions::GetOptionValueS("PARAMETER", "CLUSTERING", 0).compare(""))
    report<<"  <tr><td>Trigger clustering:</td><td> dt = "<<channels[0]->GetClusterizeDt()<<" sec</td></tr>"<<endl;
  else
    report<<"  <tr><td>Trigger clustering:</td><td>NONE</td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<"<!-- End parameters -->"<<endl;
  report<<endl;

  // Injection parameters
  report<<"<!-- Begin injections -->"<<endl;
  report<<"<h2>Injections</h2>"<<endl;
  if(GO_InjSg){
    report<<"<h3>Sinusoidal Gaussian waveforms:</h3>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><td>Frequency range:</td><td>"<<oinj->GetFrequencyMin()<<" &rarr; "<<oinj->GetFrequencyMax()<<" Hz</td></tr>"<<endl;
    report<<"  <tr><td>Q range:</td><td>"<<oinj->GetQMin()<<" &rarr; "<<oinj->GetQMax()<<"</td></tr>"<<endl;
    report<<"  <tr><td>Amplitude range:</td><td>"<<scientific<<oinj->GetAmplitudeMin()<<" &rarr; "<<oinj->GetAmplitudeMax()<<"</td></tr>"<<endl;
    report<<"  <tr><td>Time range /chunk center:</td><td>"<<oinj->GetTimeMin()<<" &rarr; "<<oinj->GetTimeMax()<<" s</td></tr>"<<endl;
    report<<"  <tr><td>Parameters:</td><td><a href=\"./oinjections.txt\">injection parameters</a></td></tr>"<<endl;
    report<<"</table>"<<endl;
  }
  if(FFL_inject!=NULL){
    report<<"<h3>Injection channels:</h3>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><td>FFL data:</td><td>"<<FFL_inject->GetInputFfl()<<"</td></tr>"<<endl;
    report<<"</table>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><th>Main channel</th><th>Injection channel</th><th>Injection factor</th></tr>"<<endl;
    for(unsigned int c=0; c<channels.size(); c++) report<<"  <tr><td>"<<channels[c]->GetName()<<"</td><td>"<<GO_InjChan[c]<<"</td><td>"<<GO_InjFact[c]<<"</td></tr>"<<endl;
    report<<"</table>"<<endl;
  }
  if(inject!=NULL){
    report<<"<h3>Software injections:</h3>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><td>Injection file(s):</td><td>"<<inject->GetInputFilePattern()<<"</td></tr>"<<endl;
    report<<"</table>"<<endl;
  }
  if((!GO_InjSg) && (FFL_inject==NULL) && (inject==NULL))
    report<<"<p>No injection was performed</p>"<<endl;
  report<<"<hr />"<<endl;
  report<<"<!-- End injections -->"<<endl;
  report<<endl;

  // channel index
  report<<"<!-- Begin channel index -->"<<endl;
  report<<"<h2>Channel index</h2>"<<endl;
  if(GO_OutProducts.find("map")!=string::npos){// color scale for glitchiness
    report<<"<table><tr>"<<endl;
    for(unsigned int c=0; c<17; c++) report<<"<td bgcolor=\""<<GetColorCode(c*0.1)<<"\"></td>"<<endl;
    report<<"<td>event strength</td>"<<endl;
    report<<"</tr></table>"<<endl;
  }
  report<<"<table class=\"omicronindex\">"<<endl;
  string colcode;
  for(unsigned int c=0; c<channels.size(); c++){
    colcode="";
    if(tile->GetSnrMapThr()>0) colcode=GetColorCode((channels[c]->GetMapSnr()-tile->GetSnrMapThr())/tile->GetSnrMapThr());
    if(!(c%6)) report<<"  <tr>"<<endl;
    if(colcode.compare("")) report<<"    <td style=\"border:2px solid "<<colcode<<"\"><a href=\"#"<<channels[c]->GetNameConv()<<"\">"<<channels[c]->GetName()<<"</a></td>"<<endl;
    else report<<"    <td><a href=\"#"<<channels[c]->GetNameConv()<<"\">"<<channels[c]->GetName()<<"</a></td>"<<endl;
    if(!((c+1)%6)) report<<"  </tr>"<<endl;
  }
  for(unsigned int c=0; c<6-(channels.size())%6; c++) report<<"    <td></td>"<<endl;
  report<<"  </tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<"<!-- End channel index -->"<<endl;
  report<<endl;

  //**** channel report *********
  string type_first="", led, led_h;
  string chunktfile = "";
  report<<"<!-- Begin channel report -->"<<endl;
  for(unsigned int c=0; c<channels.size(); c++){

    // select processing led
    if(channels[c]->GetChunkWriteN()==inChunksN){ led="green"; led_h="Processing OK"; }
    else if(channels[c]->GetChunkLoadN()!=inChunksN){ led="blue"; led_h="Data access failed"; }
    else{ led="red"; led_h="Processing failed"; }
    
    // write processed segments
    channels[c]->GetChunkProcSegment()->Dump(2, channels[c]->GetOutputDirectory()+"/omicron.segments.txt");

    // processing report
    report<<"<!-- Begin channel "<<channels[c]->GetName()<<" -->"<<endl;
    if((GO_OutProducts.find("map")!=string::npos)&&(channels[c]->GetMapSnr()<tile->GetSnrMapThr())){
      report<<"<h2 class=\"off\"><img src=\"./led-"<<led<<".gif\" alt=\""<<led_h<<"\" title=\""<<led_h<<"\"/>&nbsp;"<<channels[c]->GetName()<<" <a href=\"javascript:void(0)\" name=\""<<channels[c]->GetNameConv()<<"\" onclick=\"toggle('id_"<<channels[c]->GetNameConv()<<"')\">[click here to expand/hide]</a></h2>"<<endl;
      report<<"<div class=\"omicronchannel\" id=\"id_"<<channels[c]->GetNameConv()<<"\" style=\"visibility:hidden;height:0;\">"<<endl;
    }
    else{
      report<<"<h2 class=\"on\"><img src=\"./led-"<<led<<".gif\" />&nbsp;"<<channels[c]->GetName()<<" <a href=\"javascript:void(0)\" name=\""<<channels[c]->GetNameConv()<<"\" onclick=\"toggle('id_"<<channels[c]->GetNameConv()<<"')\">[click here to expand/hide]</a></h2>"<<endl;
      report<<"<div class=\"omicronchannel\" id=\"id_"<<channels[c]->GetNameConv()<<"\" style=\"visibility:visible;height:auto;\">"<<endl;
    }
    report<<"<h3>Processing</h3>"<<endl;
    report<<"  <table class=\"omicronsummary\">"<<endl;
    report<<"    <tr><td>Number of calls [load/condition/project/write]:</td><td>"<<channels[c]->GetChunkLoadN()<<"/"<<channels[c]->GetChunkConditionN()<<"/"<<channels[c]->GetChunkProjectN()<<"/"<<channels[c]->GetChunkWriteN()<<"</td></tr>"<<endl;
    report<<"    <tr><td>Processed livetime:</td><td>"<<(unsigned int)channels[c]->GetChunkProcSegment()->GetLiveTime()<<" sec ("<<setprecision(3)<<fixed<<channels[c]->GetChunkProcSegment()->GetLiveTime()/inSegments->GetLiveTime()*100.0<<"%) &rarr; "<<setprecision(3)<<fixed<<channels[c]->GetChunkProcSegment()->GetLiveTime()/3600.0/24<<" days <a href=\"./"<<channels[c]->GetName()<<"/omicron.segments.txt\">segments</a></td></tr>"<<endl;
    report<<"  </table>"<<endl;

    // output products
    report<<"<h3>Output</h3>"<<endl;
    report<<"<table class=\"omicronsummary\">"<<endl;
      
    // loop over loaded chunks
    for(unsigned int s=0; s<channels[c]->GetChunkLoadN(); s++){
      report<<"  <tr>"<<endl;
      report<<"    <td>"<<channels[c]->GetChunkLoad(s)<<":</td>"<<endl;

      // triggers
      if(GO_OutProducts.find("triggers")!=string::npos){
        chunktfile = channels[c]->GetChunkTriggerFile(channels[c]->GetChunkLoad(s));
	if(chunktfile.compare("")) report<<"    <td><a href=\"./"<<channels[c]->GetName()<<"/"<<chunktfile<<"\">Triggers</a></td>"<<endl;
	else report<<"    <td>Triggers</td>"<<endl;
      }

      // maps
      if(GO_OutProducts.find("map")!=string::npos){
	gwl_ss<<channels[c]->GetOutputDirectory()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONMAP-"<<channels[c]->GetChunkLoad(s)<<"-"<<windows[0]<<"."<<form;
	if(filesystem::is_regular_file(gwl_ss.str())){
	  for(unsigned int q=0; q<=tile->GetQN(); q++){
	    if(q){
	      report<<"    <td><a href=\"javascript:showImage('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONMAPQ"<<q-1<<"-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<", '"<<form<<"');\">mapQ="<<setprecision(1)<<fixed<<tile->GetQ(q-1)<<"</a>";
	      if(tile->GetChirpMass()>0) report<<"<a href=\"javascript:showImage('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONMAPQ"<<q-1<<"C-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<", '"<<form<<"');\">(C)</a>";
	      report<<"</td>"<<endl;
	    }
	    else{
	      report<<"    <td><a href=\"javascript:showImage('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONMAP"<<"-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<", '"<<form<<"');\">Full map</a>";
	      if(tile->GetChirpMass()>0) report<<"<a href=\"javascript:showImage('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONMAPC-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<", '"<<form<<"');\">(C)</a>";
	      report<<"</td>"<<endl;
	    }
	  }
	}
	else{
	  for(unsigned int q=0; q<=tile->GetQN(); q++){
	    if(q) report<<"    <td>mapQ="<<setprecision(1)<<fixed<<tile->GetQ(q-1)<<"</td>"<<endl;
 	    else  report<<"    <td>Full map</td>"<<endl;
	  }
	}
        gwl_ss.clear(); gwl_ss.str("");
      }

      // ASD
      if(GO_OutProducts.find("asd")!=string::npos)
	report<<"    <td><a href=\"./"<<channels[c]->GetName()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONASD-"<<channels[c]->GetChunkLoad(s)-tile->GetTimeRange()/2<<"-"<<tile->GetTimeRange()<<"."<<form<<"\" target=\"_blank\">ASD</a></td>"<<endl;
	
      // PSD
      if(GO_OutProducts.find("psd")!=string::npos)
	report<<"    <td><a href=\"./"<<channels[c]->GetName()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONPSD-"<<channels[c]->GetChunkLoad(s)-tile->GetTimeRange()/2<<"-"<<tile->GetTimeRange()<<"."<<form<<"\" target=\"_blank\">PSD</a></td>"<<endl;
	
      // PSD variance
      if(GO_OutProducts.find("psdvariance")!=string::npos)
	report<<"    <td><a href=\"./"<<channels[c]->GetName()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONPSDV-"<<channels[c]->GetChunkLoad(s)-tile->GetTimeRange()/2<<"-"<<tile->GetTimeRange()<<"."<<form<<"\" target=\"_blank\">PSD variance</a></td>"<<endl;
	
      // whitened PSD
      if(GO_OutProducts.find("whitepsd")!=string::npos)
	report<<"    <td><a href=\"./"<<channels[c]->GetName()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONWPSD-"<<channels[c]->GetChunkLoad(s)-tile->GetTimeRange()/2<<"-"<<tile->GetTimeRange()<<"."<<form<<"\" target=\"_blank\">Whitened PSD</a></td>"<<endl;
      
      // conditioned time-series
      if(GO_OutProducts.find("timeseries")!=string::npos){
	report<<"    <td><a href=\"javascript:showImage('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONCONDTS"<<"-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<", '"<<form<<"');\">Conditioned data"<<"</a>";
 	if(GO_OutFormat.find("wav")!=string::npos)
	  report<<" <a href=\"javascript:showSound('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONCONDTS"<<"-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<");\">(.wav)</a>";
        report<<"    </td>"<<endl;
      }
      
      // whitened time-series
      if(GO_OutProducts.find("white")!=string::npos){
	report<<"    <td><a href=\"javascript:showImage('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONWHITETS"<<"-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<", '"<<form<<"');\">Whitened data"<<"</a>";
	if(GO_OutFormat.find("wav")!=string::npos)
	  report<<" <a href=\"javascript:showSound('"<<channels[c]->GetName()<<"', '"<<channels[c]->GetNameConv()<<"', 'OMICRONWHITETS"<<"-"<<channels[c]->GetChunkLoad(s)<<"', "<<windowset<<");\">(.wav)</a>";
        report<<"    </td>"<<endl;
      }

    }
    
    report<<"</table>"<<endl;

    // PLOTS
    if(channels[c]->GetChunkLoadN()>0){
      report<<"<table>"<<endl;
      report<<"  <tr>"<<endl;

      // add window plots
      if(GO_OutProducts.find("map")!=string::npos){
        
        for(unsigned int w=0; w<windows.size(); w++){
          report<<"    <td><a id=\"a_"<<channels[c]->GetNameConv()<<"-"<<windows[w]<<"\" href=\"./"<<channels[c]->GetName()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONMAP-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\"><img id=\"img_"<<channels[c]->GetNameConv()<<"-"<<windows[w]<<"\" src=\"./"<<channels[c]->GetName()<<"/";
          if(GO_thumb)
            report<<"th"<<channels[c]->GetNameConv()<<"_OMICRONMAP-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\" alt=\""<<channels[c]->GetName()<<" "<<windows[w]<<"s\" /></a></td>"<<endl;
          else
            report<<channels[c]->GetNameConv()<<"_OMICRONMAP-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\" alt=\""<<channels[c]->GetName()<<" "<<windows[w]<<"s\" width=\""<<th_w<<"\" /></a></td>"<<endl;
        }
      }
      else if(GO_OutProducts.find("white")!=string::npos){
        for(unsigned int w=0; w<windows.size(); w++){
          report<<"    <td><a id=\"a_"<<channels[c]->GetNameConv()<<"-"<<windows[w]<<"\" href=\"./"<<channels[c]->GetName()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONWHITETS-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\"><img id=\"img_"<<channels[c]->GetNameConv()<<"-"<<windows[w]<<"\" src=\"./"<<channels[c]->GetName()<<"/";
          if(GO_thumb)
            report<<"th"<<channels[c]->GetNameConv()<<"_OMICRONWHITETS-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\" alt=\""<<channels[c]->GetName()<<" "<<windows[w]<<"s\" /></a></td>"<<endl;
          else
            report<<channels[c]->GetNameConv()<<"_OMICRONWHITETS-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\" alt=\""<<channels[c]->GetName()<<" "<<windows[w]<<"s\" width=\""<<th_w<<"\" /></a></td>"<<endl;
        }
      }
      else if(GO_OutProducts.find("timeseries")!=string::npos){
        for(unsigned int w=0; w<windows.size(); w++){
          report<<"    <td><a id=\"a_"<<channels[c]->GetNameConv()<<"-"<<windows[w]<<"\" href=\"./"<<channels[c]->GetName()<<"/"<<channels[c]->GetNameConv()<<"_OMICRONCONDTS-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\"><img id=\"img_"<<channels[c]->GetNameConv()<<"-"<<windows[w]<<"\" src=\"./"<<channels[c]->GetName()<<"/";
          if(GO_thumb)
            report<<"th"<<channels[c]->GetNameConv()<<"_OMICRONCONDTS-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\" alt=\""<<channels[c]->GetName()<<" "<<windows[w]<<"s\" /></a></td>"<<endl;
          else
            report<<channels[c]->GetNameConv()<<"_OMICRONCONDTS-"<<channels[c]->GetChunkLoad(0)<<"-"<<windows[w]<<"."<<form<<"\" alt=\""<<channels[c]->GetName()<<" "<<windows[w]<<"s\" width=\""<<th_w<<"\" /></a></td>"<<endl;
        }
      }
      else{}

      report<<"  </tr>"<<endl;
      report<<"</table>"<<endl;
    }
    
    report<<"</div>"<<endl;
    report<<"<hr />"<<endl;
    report<<"<!-- End channel "<<channels[c]->GetName()<<" -->"<<endl;
    report<<endl;
  }
  report<<"<!-- End channel report -->"<<endl;
  report<<endl;

  // index footer
  report<<"<!-- Begin footer -->"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td><a href=\"mailto:florent.robinet@ijclab.in2p3.fr\">florent.robinet@ijclab.in2p3.fr</a></td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<!-- End footer -->"<<endl;
  report<<"</body>"<<endl;
  report<<"</html>"<<endl;
  report.close();
  
  return;
}



////////////////////////////////////////////////////////////////////////////////////
void Omicron::MakeHtmlInit(void){
////////////////////////////////////////////////////////////////////////////////////

  // import material
  char *omicron_html_c = getenv("OMICRON_HTML");
  char *gwollum_pix_c = getenv("GWOLLUM_PIX");
  string omicron_html = ".";
  string gwollum_pix = ".";
  if(omicron_html_c!=NULL) omicron_html= (string)omicron_html_c;
  if(gwollum_pix_c!=NULL) gwollum_pix= (string)gwollum_pix_c;
  error_code ec;
  if(!CopyFile(ec, omicron_html+"/style/style."+tile->GetCurrentStyle()+".css", MainDir+"/style.css", true)){
    cerr<<"Omicron::MakeHtml: cannot copy style css sheet ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, gwollum_pix+"/gwollum_logo_min_trans.gif", MainDir+"/icon.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy gwollum logo ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, omicron_html+"/pics/led-blue.gif", MainDir+"/led-blue.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy blue led ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, omicron_html+"/pics/led-green.gif", MainDir+"/led-green.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy green led ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, omicron_html+"/pics/led-red.gif", MainDir+"/led-red.gif", true)){
    cerr<<"Omicron::MakeHtml: cannot copy red led ("<<ec<<")"<<endl;
  }
  RemoveFileOrDirectory(ec, MainDir+"/logo.gif");
  if(!GwollumOptions::GetOptionValueU("OUTPUT", "NOLOGO", 0)){
    if(!CopyFile(ec, omicron_html+"/pics/omicronlogo."+tile->GetCurrentStyle()+".gif", MainDir+"/logo.gif", true)){
      cerr<<"Omicron::MakeHtml: cannot copy omicron logo ("<<ec<<")"<<endl;
    }
  }
  GwollumOptions::WriteTextFile(MainDir+"/omicron.parameters.txt");
  
  // index header & scripts
  ofstream report((MainDir+"/"+GwollumOptions::GetOptionValueS("OUTPUT", "HTMLMAINPAGE", 0)).c_str());
  report<<"<html>"<<endl;
  report<<"<!-- Begin header -->"<<endl;
  report<<"<head>"<<endl;
  report<<"<title>Omicron Report (analysis in progress)</title>"<<endl;
  report<<"<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"<<endl;
  report<<"<link rel=\"icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"</head>"<<endl;
  report<<"<!-- End header -->"<<endl;
  report<<"<body>"<<endl;
  report<<endl;

  // index title
  report<<"<h1>Omicron Report (analysis in progress)</h1>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;

  // index summary
  report<<"<!-- Begin summary -->"<<endl;
  report<<"<h2>Summary</h2>"<<endl;
  report<<"<table>"<<endl;
  char *login = getlogin();
  report<<"  <tr><td>Omicron version:</td><td>"<<(string)O_PROJECT_VERSION<<": <a href=\"https://virgo.docs.ligo.org/virgoapp/Omicron/\" target=\"_blank\">documentation</a>, <a href=\"https://git.ligo.org/virgo/virgoapp/Omicron\" target=\"_blank\">gitlab repository</a></td></tr>"<<endl;
  if(login!=NULL)
    report<<"  <tr><td>Omicron run by:</td><td>"<<(string)login<<"</td></tr>"<<endl;
  else
    report<<"  <tr><td>Omicron run by:</td><td>unknown</td></tr>"<<endl;
  report<<"  <tr><td>Processing Date:</td><td>"<<mon->GetProcessDate()<<"</td></tr>"<<endl;
  report<<"  <tr><td>Number of channels:</td><td>"<<channels.size()<<"</td></tr>"<<endl;
  if(GwollumOptions::GetOptionValueU("PARAMETER", "STRICTMODE", 0)==1)
    report<<"  <tr><td>Run in STRICT mode:</td><td>YES</td></tr>"<<endl;
  else
    report<<"  <tr><td>Run in STRICT mode:</td><td>NO</td></tr>"<<endl;
  report<<"  <tr><td>Configuration:</td><td><a href=\"./omicron.parameters.txt\">omicron.parameters.txt</a></td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<"<!-- End summary -->"<<endl;
  report<<endl;

  // search parameters
  report<<"<!-- Begin parameters -->"<<endl;
  report<<"<h2>Parameters</h2>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td>Timing:</td><td> chunks of "<<tile->GetTimeRange()<<" sec, overlapping by "<<tile->GetOverlapDuration()<<" sec</td></tr>"<<endl;
  report<<"  <tr><td>Sampling frequency:</td><td>"<<channels[0]->GetWorkingFrequency()<<" Hz</td></tr>"<<endl;
  report<<"  <tr><td>Frequency range:</td><td>"<<tile->GetFrequencyMin()<<" &rarr; "<<tile->GetFrequencyMax()<<" Hz</td></tr>"<<endl;
  report<<"  <tr><td>Q range:</td><td>"<<tile->GetQ(0)<<" &rarr; "<<tile->GetQ(tile->GetQN()-1)<<"</td></tr>"<<endl;
  report<<"  <tr><td>Tiling maximal mismatch:</td><td>"<<tile->GetMismatchMax()*100.0<<" %</td></tr>"<<endl;
  if(GO_OutProducts.find("triggers")!=string::npos)
    report<<"  <tr><td>SNR threshold (triggers):</td><td>SNR &gt; "<<tile->GetSnrTriggerThr()<<"</td></tr>"<<endl;
  if(GO_OutProducts.find("map")!=string::npos)
    report<<"  <tr><td>SNR threshold (maps):</td><td>SNR &gt; "<<tile->GetSnrMapThr()<<"</td></tr>"<<endl;
  if(GwollumOptions::GetOptionValueS("PARAMETER", "CLUSTERING", 0).compare(""))
    report<<"  <tr><td>Trigger clustering:</td><td> dt = "<<channels[0]->GetClusterizeDt()<<" sec</td></tr>"<<endl;
  else
    report<<"  <tr><td>Trigger clustering:</td><td>NONE</td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<"<!-- End parameters -->"<<endl;
  report<<endl;

  // Injection parameters
  report<<"<!-- Begin injections -->"<<endl;
  report<<"<h2>Injections</h2>"<<endl;
  if(GO_InjSg){
    report<<"<h3>Sinusoidal Gaussian waveforms:</h3>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><td>Frequency range:</td><td>"<<oinj->GetFrequencyMin()<<" &rarr; "<<oinj->GetFrequencyMax()<<" Hz</td></tr>"<<endl;
    report<<"  <tr><td>Q range:</td><td>"<<oinj->GetQMin()<<" &rarr; "<<oinj->GetQMax()<<"</td></tr>"<<endl;
    report<<"  <tr><td>Amplitude range:</td><td>"<<scientific<<oinj->GetAmplitudeMin()<<" &rarr; "<<oinj->GetAmplitudeMax()<<"</td></tr>"<<endl;
    report<<"  <tr><td>Time range /chunk center:</td><td>"<<oinj->GetTimeMin()<<" &rarr; "<<oinj->GetTimeMax()<<" s</td></tr>"<<endl;
    report<<"</table>"<<endl;
  }
  if(FFL_inject!=NULL){
    report<<"<h3>Injection channels:</h3>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><td>FFL data:</td><td>"<<FFL_inject->GetInputFfl()<<"</td></tr>"<<endl;
    report<<"</table>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><th>Main channel</th><th>Injection channel</th><th>Injection factor</th></tr>"<<endl;
    for(unsigned int c=0; c<channels.size(); c++) report<<"  <tr><td>"<<channels[c]->GetName()<<"</td><td>"<<GO_InjChan[c]<<"</td><td>"<<GO_InjFact[c]<<"</td></tr>"<<endl;
    report<<"</table>"<<endl;
  }
  if(inject!=NULL){
    report<<"<h3>Software injections:</h3>"<<endl;
    report<<"<table>"<<endl;
    report<<"  <tr><td>Injection file(s):</td><td>"<<inject->GetInputFilePattern()<<"</td></tr>"<<endl;
    report<<"</table>"<<endl;
  }
  if((!GO_InjSg) && (FFL_inject==NULL) && (inject==NULL))
    report<<"<p>No injection was performed</p>"<<endl;
  report<<"<hr />"<<endl;
  report<<"<!-- End injections -->"<<endl;
  report<<endl;
  
  
  // index footer
  report<<"<!-- Begin footer -->"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td><a href=\"mailto:florent.robinet@ijclab.in2p3.fr\">florent.robinet@ijclab.in2p3.fr</a></td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<!-- End footer -->"<<endl;
  report<<"</body>"<<endl;
  report<<"</html>"<<endl;
  report.close();

  return;
}
