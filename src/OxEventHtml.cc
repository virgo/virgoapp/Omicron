/**
 * @file 
 * @brief See OxEvent.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxEvent.h"

////////////////////////////////////////////////////////////////////////////////////
void OxEvent::MakeHtmlEventTable(ofstream &aReport){
////////////////////////////////////////////////////////////////////////////////////

  tm utc;
  string det0 = OxOmicron::detector[0]->GetDetectorPrefix();
  string det1 = OxOmicron::detector[1]->GetDetectorPrefix();

   // gravitational-wave time delay
  double dt_gw = GetEventTimeDelay()-GetSliceTimeDelay(GetEventSliceIndex());
  
  aReport<<"<table class=\"oxevents\">"<<endl;

  // start time
  GPSToUTC(&utc, OxOmicron::GetOmicronStartTime(0));
  aReport<<"  <tr><td>Event time t("<<det0<<"):</td><td>GPS = "<<OxOmicron::GetOmicronStartTime(0)<<" ("<<asctime(&utc)<<") &rarr; ";
  GPSToUTC(&utc, OxOmicron::GetOmicronEndTime(0));
  aReport<<"GPS = "<<OxOmicron::GetOmicronEndTime(0)<<" ("<<asctime(&utc)<<")</td></tr>"<<endl;

  // end time
  aReport<<"  <tr><td>Event time t("<<det1<<"):</td><td>GPS = "<<OxOmicron::GetOmicronStartTime(1)<<" ("<<asctime(&utc)<<") &rarr; ";
  GPSToUTC(&utc, OxOmicron::GetOmicronEndTime(1));
  aReport<<"GPS = "<<OxOmicron::GetOmicronEndTime(1)<<" ("<<asctime(&utc)<<")</td></tr>"<<endl;

  // Omicron analysis window
  aReport<<"  <tr><td>Omicron analysis time window ("<<det0<<"):</td><td>GPS = "<<OxOmicron::GetOmicronStartTime(0)-omicron[0]->GetOverlapDuration()/2<<" &rarr; GPS = "<<OxOmicron::GetOmicronEndTime(0)+omicron[0]->GetOverlapDuration()/2<<"</td></tr>"<<endl;
  aReport<<"  <tr><td>Omicron analysis time window ("<<det1<<"):</td><td>GPS = "<<OxOmicron::GetOmicronStartTime(1)-omicron[1]->GetOverlapDuration()/2<<" &rarr; GPS = "<<OxOmicron::GetOmicronEndTime(1)+omicron[1]->GetOverlapDuration()/2<<"</td></tr>"<<endl;
  
  // time delay between detectors
  aReport<<"  <tr><td>Time delay between detectors t("<<det1<<") - t("<<det0<<"):</td><td>"<<GetEventTimeDelay()<<" s (slice index = "<<GetEventSliceIndex()<<" / "<<GetSliceN()<<"), &delta;t_gw = "<<dt_gw<<" s</td></tr>"<<endl;

  // time delay uncertainty
  aReport<<"  <tr><td>Time delay uncertainty:</td><td>"<<TMath::Sqrt(GetEventTimeDelayVariance())<<"s</td></tr>"<<endl;

  // event frequency
  aReport<<"  <tr><td>Frequency:</td><td>"<<GetEventFrequency()<<" Hz</td></tr>"<<endl;

  // event Xi
  aReport<<"  <tr><td>&Xi;:</td><td>"<<GetEventIntegratedXi()<<"</td></tr>"<<endl;

   // event rank
  aReport<<"  <tr><td>Rank:</td><td>R_"<<ox_opt->GetOptionValueS("OXE", "RANKMETHOD", 0)<<" = "<<GetEventRank()<<"</td></tr>"<<endl;

  // amplitude ratio
  aReport<<"  <tr><td>Amplitude ratio:</td><td>"<<GetEventAmplitudeRatio()<<"</td></tr>"<<endl;

  // antenna factor ratio
  aReport<<"  <tr><td>Antenna factor ratio (min/max):</td><td>"<<GetEventAntennaFactorRatioMin()<<" / "<<GetEventAntennaFactorRatioMax()<<"</td></tr>";

  // number of rejected frequency rows
  aReport<<"  <tr><td>Number of rejected frequency rows:</td>";
  aReport<<"    <td>Q0: "<<GetEventFrequencyRowRejectedN(0)<<" / "<<OxOmicron::omicron[0]->GetBandN(0);
  for(unsigned int q=1; q<OxOmicron::omicron[0]->GetQN(); q++){      
    aReport<<", Q"<<q<<": "<<GetEventFrequencyRowRejectedN(q)<<" / "<<OxOmicron::omicron[0]->GetBandN(q);
  }
  aReport<<"</td>"<<endl;
  aReport<<"  </tr>"<<endl;

  //  Max SNR
  aReport<<"  <tr><td>Maximum signal-to-noise ratio ("<<det0<<"):</td><td>"<<TMath::Sqrt(OxOmicron::GetOmicronSnrSqMax(0))<<"</td></tr>";
  aReport<<"  <tr><td>Maximum signal-to-noise ratio ("<<det1<<"):</td><td>"<<TMath::Sqrt(OxOmicron::GetOmicronSnrSqMax(1))<<"</td></tr>";

  // SNR variance
  aReport<<"  <tr><td>SNR variance ("<<det0<<"):</td><td>"<<OxOmicron::GetOmicronSnrVariance(0)<<"</td></tr>"<<endl;
  aReport<<"  <tr><td>SNR variance ("<<det1<<"):</td><td>"<<OxOmicron::GetOmicronSnrVariance(1)<<"</td></tr>"<<endl;


  aReport<<"</table>"<<endl;
  aReport<<"<hr />"<<endl;
 
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxEvent::MakeHtmlSkyMapTable(ofstream &aReport){
////////////////////////////////////////////////////////////////////////////////////

  // write sky maps to disk (png)
  WriteSkyMaps();

  aReport<<"<table class=\"oxmaps\">"<<endl;
  aReport<<"  <tr>"<<endl;
  gwl_ss<<OxInit::GetOutputDirectory()<<"/OXSKYMAP0-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png";
  if(filesystem::is_regular_file(gwl_ss.str())==true){
    aReport<<"    <td><a href=\"./OXSKYMAP0-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png\"><img src=\"./OXSKYMAP0-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png\" /></a></td>"<<endl;
  }
  gwl_ss.clear(); gwl_ss.str("");
  gwl_ss<<OxInit::GetOutputDirectory()<<"/OXSKYMAP1-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png";
  if(filesystem::is_regular_file(gwl_ss.str())==true){
    aReport<<"    <td><a href=\"./OXSKYMAP1-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png\"><img src=\"./OXSKYMAP1-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png\" /></a></td>"<<endl;
  }
  gwl_ss.clear(); gwl_ss.str("");
  gwl_ss<<OxInit::GetOutputDirectory()<<"/OXSKYMAPR-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png";
  if(filesystem::is_regular_file(gwl_ss.str())==true){
    aReport<<"    <td><a href=\"./OXSKYMAPR-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png\"><img src=\"./OXSKYMAPR-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png\" /></a></td>"<<endl;
  }
  gwl_ss.clear(); gwl_ss.str("");
  aReport<<"  </tr>"<<endl;
  aReport<<"</table>"<<endl;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void OxEvent::MakeHtmlConfigTable(ofstream &aReport, const string aOutDir){
////////////////////////////////////////////////////////////////////////////////////

  // CHECKME overwrite the ResultsHtml options?
  ox_opt->WriteTextFile(aOutDir+"/report_"+OxInit::GetName()+"_options.txt");

  // dump omicron options in a txt file
  OxOmicron::DumpOmicronOptions(0, aOutDir+"/report_"+OxInit::GetName()+"_omicron_options_0.txt");
  OxOmicron::DumpOmicronOptions(1, aOutDir+"/report_"+OxInit::GetName()+"_omicron_options_1.txt");

  aReport<<"<table>"<<endl;
  aReport<<"  <tr><td>Ox option file:</td><td><a href=\"./report_"<<OxInit::GetName()<<"_options.txt\">"<<OxInit::GetName()<<"_options.txt</a></td></tr>"<<endl;
  aReport<<"  <tr><td>Omicron option files:</td><td><a href=\"./report_"<<OxInit::GetName()<<"_omicron_options_0.txt\">detector 0</a>, <a href=\"./report_"<<OxInit::GetName()<<"_omicron_options_1.txt\">detector 1</a></td></tr>"<<endl;
  aReport<<"  <tr><td>Detector channel names:</td><td>"<<omicron[0]->GetChannelName(0)<<", "<<omicron[1]->GetChannelName(0)<<"</td></tr>"<<endl;
  aReport<<"  <tr><td>Omicron analysis time window and overlap:</td><td>"<<omicron[0]->GetChunkDuration()<<" s, "<<omicron[0]->GetOverlapDuration()<<" s</td></tr>"<<endl;
  aReport<<"  <tr><td>Number of Q planes:</td><td>"<<omicron[0]->GetQN()<<" (Q = "<<omicron[0]->GetQ(0)<<" &rarr; Q = "<<omicron[0]->GetQ(omicron[0]->GetQN()-1)<<")</td></tr>"<<endl;
  aReport<<"  <tr><td>Omicron frequency range:</td><td>"<<omicron[0]->GetTileFrequencyMin()<<" Hz &rarr; "<<omicron[0]->GetTileFrequencyMax()<<" Hz</td></tr>"<<endl;
  aReport<<"  <tr><td>Light travel time between detectors:</td><td>"<<OxOmicron::GetLightTravelTime()<<" s</td></tr>"<<endl;
  aReport<<"  <tr><td>Slice extra-time (calibration uncertainty + slice veto):</td><td>"<<GetSliceHalfDuration()-OxOmicron::GetLightTravelTime()<<" s</td></tr>"<<endl;
  aReport<<"  <tr><td>Number of time-delay bins in a cross-correlation map:</td><td>"<<OxCorr::GetTimeDelayBinsN()<<"</td></tr>"<<endl;
  aReport<<"  <tr><td>Number of slices in a cross-correlation map:</td><td>"<<GetSliceN()<<"</td></tr>"<<endl;
  aReport<<"  <tr><td>Number of time-delay bins in a slice:</td><td>"<<GetSliceTimeDelayBinsN()<<"</td></tr>"<<endl;
  aReport<<"  <tr><td>SNR gating:</td><td>&lt; "<<TMath::Sqrt(snrsq_gate_0)<<" (detector 0), &lt; "<<TMath::Sqrt(snrsq_gate_1)<<" (detector 1)</td></tr>"<<endl;
  aReport<<"  <tr><td>Amplitude spectrograms:</td><td>normalization = "<<ox_opt->GetOptionValueD("OXC", "AMPLITUDE", 1)<<", power index = "<<ox_opt->GetOptionValueD("OXC", "AMPLITUDE", 0)<<"</td></tr>"<<endl;
  aReport<<"  <tr><td>White noise SNR variance:</td><td>"<<OxOmicron::GetWhiteNoiseSnrVariance()<<" (after applying a correction factor = "<<ox_opt->GetOptionValueD("OXO", "WHITENOISECORR", 0)<<")</td></tr>"<<endl;
  aReport<<"</table>"<<endl;

  return;
}
