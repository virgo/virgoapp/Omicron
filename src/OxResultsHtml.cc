/**
 * @file 
 * @brief See OxResults.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxResults.h"

////////////////////////////////////////////////////////////////////////////////////
bool OxResults::MakeHtml(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxResults::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxResults::"<<__func__<<": the OxResults object is corrupted"<<endl;
    return false;
  }

  // initiate web report
  ofstream report = OxInit::CreateHtml("index.html", "Omicron-X report");

  // output directory
  string outdir = OxInit::GetOutputDirectory();

  // dump list of input Ox files
  string oxfilelistname = outdir+"/"+OxInit::GetName()+"_file_list.txt";
  ofstream oxfilelist(oxfilelistname.c_str());
  for(unsigned int f=0; f<OxInit::GetOxFileN(); f++){
    if(results_found[f]==true) oxfilelist<<"results=1 "<<OxInit::GetOxFileName(f)<<endl;
    else oxfilelist<<"results=0 "<<OxInit::GetOxFileName(f)<<endl;
  }
  oxfilelist.close();
  
  //****************** configuration *********************
  report<<"<h2>Ox analysis configuration</h2>"<<endl;
  OxEvent::MakeHtmlConfigTable(report, outdir);
  report<<"<hr />"<<endl;
  report<<endl;
  //******************************************************

  //****************** analysis results *********************
  tm utc;
  report<<"<h2>Ox analysis results</h2>"<<endl;

  // analysis results
  report<<"<table>"<<endl;
  report<<"  <tr><td>List of input Ox files:</td><td><a href=\"./"<<GetFileNameFromPath(oxfilelistname)<<"\">"<<GetFileNameFromPath(oxfilelistname)<<"</a> ("<<OxInit::GetOxFileN()<<" files)</td></tr>"<<endl;
  report<<"  <tr><td>Analysis result file:</td><td><a href=\"./"<<GetFileNameFromPath(outfilename)<<"\">"<<GetFileNameFromPath(outfilename)<<"</a></td></tr>"<<endl;
  report<<"  <tr><td>Slice selected for the the fakeground:</td><td>slice index="<<fake_slice_index<<" ("<<GetSliceTimeDelay(fake_slice_index)<<" s), time lag = "<<fake_lag<<" s</td></tr>"<<endl;
  report<<"</table>"<<endl;

  // segments
  report<<"<table class=\"oxsegments\">"<<endl;
  report<<"  <tr><th></th><th>Foreground</th><th>Fakeground</th><th>Background</th></tr>"<<endl;
  report<<"  <tr><td>Number of processed ox events</td><td>"<<GetProcessedEventsN(oxresult_foreground)<<"</td><td>"<<GetProcessedEventsN(oxresult_fakeground)<<"</td><td>"<<GetProcessedEventsN(oxresult_background)<<"</td></tr>"<<endl;
  for(unsigned int d=0; d<2; d++){
    report<<"  <tr><td>Start Date (detector "<<d<<")</td><td>";
    for(unsigned int r=0; r<oxresult_n; r++){
      if(segments[d][r]->GetN()>0){
        GPSToUTC(&utc, segments[d][r]->GetFirst());
        report<<asctime(&utc)<<" UTC</td><td>";
      }
      else report<<"-</td><td>";
    }
    report<<"</tr>"<<endl;
    report<<"  <tr><td>End Date (detector "<<d<<")</td><td>";
    for(unsigned int r=0; r<oxresult_n; r++){
      if(segments[d][r]->GetN()>0){
        GPSToUTC(&utc, segments[d][r]->GetLast());
        report<<asctime(&utc)<<" UTC</td><td>";
      }
      else report<<"-</td><td>";
    }
    report<<"</tr>"<<endl;
    report<<"  <tr><td>Start GPS (detector "<<d<<")</td><td>";
    for(unsigned int r=0; r<oxresult_n; r++){
      if(segments[d][r]->GetN()>0){
        report<<(unsigned int)segments[d][r]->GetFirst()<<"</td><td>";
      }
      else report<<"-</td><td>";
    }
    report<<"</tr>"<<endl;
    report<<"  <tr><td>End GPS (detector "<<d<<")</td><td>";
    for(unsigned int r=0; r<oxresult_n; r++){
      if(segments[d][r]->GetN()>0){
        report<<(unsigned int)segments[d][r]->GetLast()<<"</td><td>";
      }
      else report<<"-</td><td>";
    }
    report<<"</tr>"<<endl;
    report<<"  <tr><td>Used livetime (detector "<<d<<")</td><td>"<<segments[d][oxresult_foreground]->GetLiveTime()<<" s</td><td>"<<segments[d][oxresult_fakeground]->GetLiveTime()<<" s</td><td>"<<segments[d][oxresult_background]->GetLiveTime()<<" s</td></tr>"<<endl;
  }
  report<<"  <tr><td>Cumulative livetime</td><td>"<<livetime[oxresult_foreground]<<" s</td><td>"<<livetime[oxresult_fakeground]<<" s</td><td>"<<livetime[oxresult_background]<<" s</td></tr>"<<endl;
  report<<"  <tr><td></td><td>";
  report<<livetime[oxresult_foreground]/31557600<<" years, "<<(livetime[oxresult_foreground]%31557600)/86400<<" days, "<<(livetime[oxresult_foreground]%86400)/3600<<" hours, "<<(livetime[oxresult_foreground]%3600)/60<<" min, "<<livetime[oxresult_foreground]%60<<" s</td><td>";
  report<<livetime[oxresult_fakeground]/31557600<<" years, "<<(livetime[oxresult_fakeground]%31557600)/86400<<" days, "<<(livetime[oxresult_fakeground]%86400)/3600<<" hours, "<<(livetime[oxresult_fakeground]%3600)/60<<" min, "<<livetime[oxresult_fakeground]%60<<" s</td><td>";
  report<<livetime[oxresult_background]/31557600<<" years, "<<(livetime[oxresult_background]%31557600)/86400<<" days, "<<(livetime[oxresult_background]%86400)/3600<<" hours, "<<(livetime[oxresult_background]%3600)/60<<" min, "<<livetime[oxresult_background]%60<<" s</td><td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<endl;

  // detection plot
  report<<"<table>"<<endl;
  report<<"  <tr>"<<endl;
  report<<"    <td><a href=\"./oxr_ev_detection.png\"><img src=\"./oxr_ev_detection.png\" width=\"800\"></a></td>"<<endl;
  report<<"  </tr>"<<endl;
  report<<"</table>"<<endl;
  report<<endl;

  // list of loudest events (foreground)
  report<<"<h3>List of loudest "<<OxResultTypeName[oxresult_foreground]<<" events</h3>"<<endl;
  MakeHtmlLoudest(report, oxresult_foreground);
  
  // list of loudest events (fakeground)
  report<<"<h3>List of loudest "<<OxResultTypeName[oxresult_fakeground]<<" events</h3>"<<endl;
  MakeHtmlLoudest(report, oxresult_fakeground);
  
  report<<"<hr />"<<endl;
  report<<endl;
  //**********************************************************

  //****************** background studies **********************
  report<<"<h2>Background studies</h2>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr><td>Detection threshold:</td><td>R &gt; "<<GetRankDetectionThreshold()<<"</td></tr>"<<endl;
  report<<"  <tr><td>False-alarm rate above that detection threshold:</td><td>"<<RankToRate(oxresult_background, GetRankDetectionThreshold(), 1)<<"</td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr>"<<endl;
  report<<"    <td><a href=\"./oxr_ev_rank_background.png\"><img src=\"./oxr_ev_rank_background.png\" width=\"100%\"></a></td>"<<endl;
  report<<"    <td><a href=\"./oxr_ev_freq_background.png\"><img src=\"./oxr_ev_freq_background.png\" width=\"100%\"></a></td>"<<endl;
  report<<"    <td><a href=\"./oxr_ev_rank_freq_background.png\"><img src=\"./oxr_ev_rank_freq_background.png\" width=\"100%\"></a></td>"<<endl;
  report<<"  </tr>"<<endl;
  report<<"</table>"<<endl;
  report<<endl;

  // list of loudest events (background)
  report<<"<h3>List of loudest "<<OxResultTypeName[oxresult_background]<<" events</h3>"<<endl;
  MakeHtmlLoudest(report, oxresult_background);
  report<<"<hr />"<<endl;
  report<<endl;
  //**********************************************************

  //****************** injections studies **********************
  report<<"<h2>Injection studies</h2>"<<endl;
  OxSim::MakeHtmlResults(report);
  report<<"<hr />"<<endl;
  report<<endl;
  //**********************************************************

  // close web report
  OxInit::CloseHtml(report);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void OxResults::MakeHtmlLoudest(ofstream &aReport, const OxResultType aResultType){
////////////////////////////////////////////////////////////////////////////////////

  unsigned int chunk_start_0 = 0;
  unsigned int chunk_start_1 = 0;
  unsigned int seg_index_0 = 0;
  unsigned int seg_index_1 = 0;
  unsigned int psd_length0 = 0;
  unsigned int psd_length1 = 0;

  // loop over loudest events
  for(int l=0; l<ev_loudest_entry[aResultType]->GetSize(); l++){

    // load event
    //cout<<aResultType<<" "<<l<<" "<<ev_loudest_entry[aResultType]->At(l)<<endl;
    if(OxEvent::GetEntry(ev_loudest_entry[aResultType]->At(l))<=0) break;

    monit->Monitor::SetMessageType(monitor_status);
    *monit<<"OxResults::"<<__func__<<": loudest event det0 = "<<OxOmicron::GetOmicronStartTime(0)<<", det1 = "<<OxOmicron::GetOmicronStartTime(1)<<endl;

    // header for the event
    aReport<<"<h4 class=\"oxloudest\">#"<<l<<" <a href=\"javascript:void(0)\" name=\"loudest_"<<aResultType<<"_"<<l<<"\" onclick=\"toggle('id_loudest_"<<aResultType<<"_"<<l<<"')\">[more/less]</a> detector 0: "<<OxOmicron::GetOmicronStartTime(0)<<"-"<<OxOmicron::GetOmicronEndTime(0)<<" | detector 1: "<<OxOmicron::GetOmicronStartTime(1)<<"-"<<OxOmicron::GetOmicronEndTime(1)<<" | R = "<<OxEvent::GetEventRank()<<" | FAR = "<<RankToRate(oxresult_background, OxEvent::GetEventRank(), 1)<<" Hz | IFAR = "<<1.0/RankToRate(oxresult_background, OxEvent::GetEventRank(), 1)/(double)O_SECONDS_IN_ONE_YEAR<<" yrs</h4>"<<endl;

    // event details
    aReport<<"<div id=\"id_loudest_"<<aResultType<<"_"<<l<<"\" style=\"visibility:hidden;height:0;\">"<<endl;

    // dump event table
    OxEvent::MakeHtmlEventTable(aReport);

    // dump map table
    if(l<(int)ox_opt->GetOptionValueU("OXR", "NLOUDESTMAP", aResultType)){

      // omicron chunk times
      chunk_start_0 = GetOmicronChunkStartTime(0);
      chunk_start_1 = GetOmicronChunkStartTime(1);

      // get segment index for that event --> PSD length
      if(segments[0][aResultType]->IsInsideSegment(seg_index_0, chunk_start_0)==false){
        psd_length0 = 0;
      }
      else
        psd_length0 = segments[0][aResultType]->GetEnd(seg_index_0)-chunk_start_0;
      if(segments[1][aResultType]->IsInsideSegment(seg_index_1, chunk_start_1)==false){
        psd_length1 = 0;
      }
      else
        psd_length1 = segments[1][aResultType]->GetEnd(seg_index_1)-chunk_start_1;
      
      // make omicron maps
      if((OxOmicron::Process(0, chunk_start_0, psd_length0)==true) && (OxOmicron::Process(1, chunk_start_1, psd_length1)==true)){

        // make cross-correlation maps
        if((OxCorr::Transform(0)==true) && (OxCorr::Transform(1)==true) && (OxCorr::Process()==true)){
          OxCorr::WriteMaps(OxEvent::GetEventTimeDelay());
        }
      }
      
      // print map tables
      OxCorr::MakeHtmlMapTable(aReport, (OxOmicron::GetOmicronStartTime(0) + OxOmicron::GetOmicronEndTime(0))/2, (OxOmicron::GetOmicronStartTime(1) + OxOmicron::GetOmicronEndTime(1))/2);
      OxOmicron::MakeHtmlMapTable(aReport, 0, (OxOmicron::GetOmicronStartTime(0) + OxOmicron::GetOmicronEndTime(0))/2);
      OxOmicron::MakeHtmlMapTable(aReport, 1, (OxOmicron::GetOmicronStartTime(1) + OxOmicron::GetOmicronEndTime(1))/2);

      // sky maps
      MakeHtmlSkyMapTable(aReport);

    }
    
    aReport<<"</div>"<<endl;
  }

  return;
}
