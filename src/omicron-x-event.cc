/**
 * @file 
 * @brief Program to display an Ox event in a web report.
 * @details This program generates a web page displaying all the characteristics of an Ox event. An Ox event is defined by three parameters:
 * - A start time for the first detector (detector 0). This time is used to set the start time of the Omicron analysis chunk.
 * - A start time for the second detector (detector 1). This time is used to set the start time of the Omicron analysis chunk.
 * - A slice index which indicates the slice in the cross-correlation map which defines the Ox event.
 *
 * The Ox analysis is configured with an Ox option file (see OxInit and other Ox classes for the list of options). A single Omicron chunk is analyzed for both detectors. The resulting spectrograms are cross-correlated with the OxCorr engine. The selected event slice properties are then saved, as well as tmie-frequency maps as png files. All the results are displayed in a web page saved in the output directory.
 * @snippet this omicron-x-event-usage
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxSim.h"


using namespace std;

/**
 * @brief Prints the program usage message.
 */
void PrintUsage(void){
  //! [omicron-x-event-usage]
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"omicron-x-event [option file] [GPS 0] ([GPS 1]) ([slice index])"<<endl; 
  cerr<<endl;
  cerr<<"[option file]"<<endl;
  cerr<<"Path to the Ox option file"<<endl;
  cerr<<endl;
  cerr<<"[GPS 0/1]"<<endl;
  cerr<<"GPS start time of the Omicron analysis chunk for detector 0 and 1"<<endl;
  cerr<<"   Optionnally, it is possible to use data above the analysis chunk to init the PSD buffers."<<endl;
  cerr<<"   To achieve that, add \"+[T]\" after the GPS time, where [T] a the number of seconds."<<endl;
  cerr<<"If [GPS 1] is not provided, [GPS 1] = [GPS 0]"<<endl;
  cerr<<endl;
  cerr<<"[slice index]"<<endl;
  cerr<<"Slice index in the cross-correlation map. If not given, use the first slice in the map."<<endl;
  cerr<<endl;
  cerr<<"See also: https://virgo.docs.ligo.org/virgoapp/Omicron/omicron-x-event_8cc.html#details"<<endl;
  cerr<<endl;
  //! [omicron-x-event-usage]
  return;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return OmicronPrintVersion();
  }

  // number of arguments
  if(argc<3){
    PrintUsage();
    return -1;
  }

  // get list of options
  string optionfile = "";
  unsigned int gps0 = 0;
  unsigned int psdl0 = 0;
  unsigned int gps1 = 0;
  unsigned int psdl1 = 0;
  unsigned int slice_index = 0;
  vector<string> gps_psdl;
  
  // option file
  optionfile = (string)argv[1];
  if(filesystem::is_regular_file(optionfile)==false){
    cerr<<"The option file "<<optionfile<<" cannot be found"<<endl;
    return -2;
  }

  // GPS 0
  if(((string)(argv[2])).find("+")==string::npos){
    gps0 = stoul(argv[2]);
  }
  else{
    gps_psdl = SplitString((string)(argv[2]), '+');
    if(gps_psdl.size()!=2){
      cerr<<"The option string"<<argv[2]<<" cannot be interpreted"<<endl;
      return -2;
    }
    gps0 = stoul(gps_psdl[0]);
    psdl0 = stoul(gps_psdl[1]);
  }

  // GPS 1
  if(argc>3){
    if(((string)(argv[3])).find("+")==string::npos){
      gps1 = stoul(argv[3]);
    }
    else{
      gps_psdl = SplitString((string)(argv[3]), '+');
      if(gps_psdl.size()!=2){
        cerr<<"The option string"<<argv[3]<<" cannot be interpreted"<<endl;
        return -2;
      }
      gps1 = stoul(gps_psdl[0]);
      psdl1 = stoul(gps_psdl[1]);
    }
  }
  else{
    gps1 = gps0;
    psdl1 = psdl0;
  }
  
  // slice index
  if(argc>4) slice_index = stoul(argv[4]);

  // create Ox object
  OxSim *ox = new OxSim(optionfile, gps0, gps1);

  // run analysis
  if(ox->OxOmicron::Process(0, gps0, psdl0)==false) return 1;
  if(ox->OxOmicron::Process(1, gps1, psdl1)==false) return 2;
  if(ox->OxCorr::Transform(0)==false) return 3;
  if(ox->OxCorr::Transform(1)==false) return 4;
  if(ox->OxCorr::Process()==false) return 5;
  if(ox->OxEvent::MakeEvent(slice_index, 0)==false) return 6;
  if(ox->OxCorr::WriteMaps(ox->OxEvent::GetEventTimeDelay())==false) return 7;
  if(ox->MakeHtml()==false) return 8;

  delete ox;
    
  return 0;
}
