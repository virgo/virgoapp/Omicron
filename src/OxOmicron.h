/**
 * @file 
 * @brief Omicron for Ox analyses.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __OxOmicron__
#define __OxOmicron__
#include "OxInit.h"

using namespace std;

/**
 * @brief Expected SNR variance for a white noise processed with Omicron. 
 * @details For white noise, the squared norm of the $Q$-transform coefficients follow an exponential distribution with a mean value of 2. The Omicron tile SNR is derived from this assumption. The Omicron tile SNR variance can be predicted from this assumption: the SNR variance is \f$2e^{-1} - 0.5e^{-2}\pi\f$. 
 */
#define WHITE_NOISE_VARIANCE 0.5231747165490665540943382438854314386844635009765625

/**
 * @brief Omicron for Ox analyses.
 * @details This class is designed to manage the Omicron analysis for Ox. Two Omicron objects are created by the constructor, one for each detector: detector 0 and detector 1. Call Process() to run the Omicron analysis and generate spectrograms.
 *
 * The Omicron options can either be provided by two option text files or by a list of Ox files: see OxInit. The results of the Omicron analysis is saved in TTree in Ox files: see CreateOutputFile().
 *
 * When working with 2 detectors, gravitational-wave sources can be localized on a ring across the sky. This ring is defined by the detection time delay between the 2 detectors, defined as: \f$\delta t_{gw} = t_1 - t_0\f$. The maximum time delay \f$\delta t_{gw} = \delta t_{01}\f$ is given by the light travel time between the two detectors: \f$-\delta t_{01} \le \delta t_{gw} \le \delta t_{01}\f$. When initializing this class, sky rings are pre-computed for a collection of \f$N_R = 2n_r+1\f$ pre-defined time delays. They are indexed by \f$r\f$ ranging from \f$0\f$ to \f$N_R\f$, \f$r=0\f$ being associated to \f$\delta t_{gw}=-\delta t_{01}\f$, \f$r=n_r\f$ to \f$\delta t_{gw}=0\f$ and \f$r=2n_r\f$ to \f$\delta t_{gw}=+\delta t_{01}\f$.
 * Each ring is discretized in a series of points separated by a constant angular distance. For each point, the detector antenna factors are computed at the time of the Omicron analysis window. Then, for each ring, the minimum and maximum antenna factor values are saved: see ScanSkyRings().
 */
class OxOmicron: public OxInit{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
  /**
   * @brief Constructor of the OxOmicron class.
   * @details The list of options is given in a text file. They are identified with a set of tag and key words:
   * - `OXO OPTIONS`: path to the Omicron option files for the first and second detectors. 
   * - `OXO FFL0`: path to the FFL file for the first detector. This option is used to overload the FFL file which is obtained from Ox files (if any).
   * - `OXO FFL1`: path to the FFL file for the second detector. This option is used to overload the FFL file which is obtained from Ox files (if any).
   * - `OXO INJGEN`: path to the InjGen file with injections. This option is used to overload the injection file which is obtained from Ox files (if any).
   * - `OXO SKYRINGS`: this option parameterizes how sky rings are pre-computed. The first parameter \f$n_R\f$ is the number of rings to cover the range of time delays from \f$\delta t_{gw}=0\f$ (excluded) to the maximum light-travel time (included) between detectors. The second parameter is the number of points over the largest ring (\f$\delta t_{gw}=0\f$). Default = 100 for all parameters.
   * - `OXO WHITENOISECORR`: this option provides a multiplicative factor to correct the theoretical variance of the Omicron SNR for white noise: ::WHITE_NOISE_VARIANCE. Default = 1.0 (no correction).
   *
   * @sa OxInit::OxInit() for additional options.
   *
   * Two Omicron objects are created with CreateOmicron().
   * @warning The two Omicron objects must have the same parameter set: see CheckOmicron().
   *
   * If Ox files are provided (see OxInit::OxInit()), the Omicron results can also be read with this class.
   *
   * @param[in] aOptionFile Path to the option file.
   * @param[in] aGpsRef0 Reference GPS time to intiate the Omicron object for the first detector.
   * @param[in] aGpsRef1 Reference GPS time to intiate the Omicron object for the second detector.
   */
  OxOmicron(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1);

  /**
   * @brief Destructor of the OxOmicron class.
   */
  virtual ~OxOmicron(void);

  /**
     @}
  */

  /**
   * @brief Runs the Omicron analysis.
   * @details The Omicron analysis of the chunk starting at a given GPS time is performed:
   * - Omicron::NewChannel(): loads the Omicron channel.
   * - Omicron::DefineNewChunk(): sets the chunk timing as requested.
   * - Omicron::LoadData(): loads the data time series.
   * - Omicron::Condition(): conditions the data time series.
   * - Omicron::Project(): computes the Q transform.
   *
   * The analysis results are saved in the Omicron TTree: FillTree(). Moreover, the antenna factor ratio are computed all over the sky with ScanSkyRings() (only for detector 0).
   *
   * @returns true if all the processing steps were successful.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @param[in] aTimeStart Chunk GPS time start [s].
   * @param[in] aPsdLengthReset Use this flag to reset the PSD buffer. A positive value [s] indicates how much data, from `aTimeStart`, is used to initialize the PSD buffer. If `aPsdLengthReset` is smaller or equal to the chunk duration, the PSD buffer is initialized with the current segment. If `aPsdLengthReset` is larger than the duration of the current segment, `aPsdLengthReset` is considered to initialize the PSD buffer.
  */
  bool Process(const unsigned int aDetectorIndex, const unsigned int aTimeStart, const unsigned int aPsdLengthReset);

  /**
   * @brief Writes Omicron spectrograms as png files in the output directory.
   * @details Calls Omicron::WriteOutput(). Only SNR maps are saved to disk as png files. This behavior is the result of some Omicron being forced at intialization: see CreateOmicron().
   * @note If the spectrograms were already printed, this function exits immediately.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   */
  bool WriteMaps(const unsigned int aDetectorIndex);
  
  /**
   * @brief Returns a sky ring as a TGraph.
   * @details One sky ring is selected based on a time delay \f$\delta t_{gw} = t_1 - t_0\f$. The sky ring with the closest time-delay is selected. It is returned as a TGraph where the x-axis is the right ascension [rad] and the y-axis is the declination [rad].
   * @note The TGraph object must be deleted by the user.
   * @param[in] aTimeDelay Time delay \f$\delta t_{gw} = t_1-t_0\f$.
   */
  TGraph* GetSkyRing(const double aTimeDelay);
  
  /**
   * @brief Returns a sky ring as a TGraph.
   * @details The sky ring is returned as a TGraph where the x-axis is the right ascension [rad] and the y-axis is the declination [rad].
   * @note The TGraph object must be deleted by the user.
   * @param[in] aSkyRingIndex Sky ring index \f$r\f$ from 0 to \f$N_R\f$.
   */
  TGraph* GetSkyRing(const unsigned int aSkyRingIndex);
  
  /**
   * @brief Creates an ouput file.
   * @details A new ROOT file is opened in the output directory to save the Ox analysis results (OxInit::CreateOutputFile()). The Omicron option TTrees are written in the file with the names "omicron0_options" and "omicron1_options" for the first and second detector respectively. The Omicron TTree "oxo" is initialized in the ouput file.
   * @sa OxInit::CreateOutputFile() for the file management.
   * @param[out] aOutFileName Output ROOT file name.
   * @param[in] aFileId File ID used to name the ouput Ox file.
   * @param[in] aTime Time used to name the ouput file.
   */
  bool CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime);

  /**
   * @brief Returns the white noise SNR variance.
   */
  inline double GetWhiteNoiseSnrVariance(void){
    return whitenoise_snr_variance;
  };
  
  /**
   * @brief Returns the Omicron chunk duration [s].
   */
  inline unsigned int GetOmicronChunkDuration(void){
    return omicron[0]->GetChunkDuration();
  };
  
  /**
   * @brief Returns the Omicron overlap duration [s].
   */
  inline unsigned int GetOmicronOverlapDuration(void){
    return omicron[0]->GetOverlapDuration();
  };
  
  /**
   * @brief Returns the Ox analysis duration [s].
   * @details The Ox analysis duration \f$\Delta_t\f$is the duration of Omicron spectrograms excluding the overlap between analysis chunks.
   */
  inline unsigned int GetAnalysisDuration(void){
    return omicron[0]->GetChunkDuration()-omicron[0]->GetOverlapDuration();
  };

  /**
   * @brief Returns the maximum light travel time between the two detectors [s].
   */
  inline double GetLightTravelTime(void){ return skyring_timedelays[2*skyring_n]; };

  /**
   * @brief Returns the number of Omicron analysis results in the input Ox files.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline Long64_t GetEntries(const unsigned int aDetectorIndex){ return oxo_tree_read[aDetectorIndex]->GetEntries(); };

  /**
   * @brief Loads an entry in the Omicron results in the input Ox files.
   * @returns The function returns the number of bytes read from the input buffer. If entry does not exist the function returns 0. If an I/O error occurs, the function returns -1.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @param[in] aEntry Entry number.
   * @pre The detector index must be valid.
   */
  inline int GetEntry(const unsigned int aDetectorIndex, Long64_t aEntry){ return oxo_tree_read[aDetectorIndex]->GetEntry(aEntry); };

  /**
   * @brief Returns the current spectrogram GPS time (chunk center) [s].
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline unsigned int GetOmicronTime(const unsigned int aDetectorIndex){ return oxo_start_time[aDetectorIndex] + GetAnalysisDuration()/2; };

  /**
   * @brief Returns the current spectrogram GPS start time [s].
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline unsigned int GetOmicronStartTime(const unsigned int aDetectorIndex){ return oxo_start_time[aDetectorIndex]; };

  /**
   * @brief Returns the current spectrogram GPS end time [s].
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline unsigned int GetOmicronEndTime(const unsigned int aDetectorIndex){ return oxo_start_time[aDetectorIndex] + GetAnalysisDuration(); };

  /**
   * @brief Returns the current Omicron chunk GPS start time [s].
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline unsigned int GetOmicronChunkStartTime(const unsigned int aDetectorIndex){ return oxo_start_time[aDetectorIndex] - omicron[aDetectorIndex]->GetOverlapDuration()/2; };

  /**
   * @brief Returns the current Omicron chunk GPS end time [s].
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline unsigned int GetOmicronChunkEndTime(const unsigned int aDetectorIndex){ return oxo_start_time[aDetectorIndex] - omicron[aDetectorIndex]->GetOverlapDuration()/2; };

  /**
   * @brief Returns the maximum SNR squared in the current spectrogram.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline double GetOmicronSnrSqMax(const unsigned int aDetectorIndex){ return oxo_snrsq[aDetectorIndex]; };

  /**
   * @brief Returns the variance of SNRs in the current spectrogram.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @pre The detector index must be valid.
   */
  inline double GetOmicronSnrVariance(const unsigned int aDetectorIndex){ return oxo_snrvar[aDetectorIndex]; };

  /**
   * @brief Returns the expected Omicron amplitude variance under the white-noise hypothesis.
   * @details The distribution of amplitudes in an Omicron spectrogram can be analytically predicted for a white-noise time series. The distribution of Q-transform coefficients (squared norm) is an exponential distribution with a mean equals to 2. This distribution can be turned into a signal-to-noise ratio, and, taking into account the noise amplitude in a frequency band, the amplitude distribution can be derived. This function returns the variance of amplitudes along a frequency row under the white-noise hypothesis.
   * @sa GetWhiteNoiseSnrVariance()
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @param[in] aQindex Q plane index (must be valid!).
   * @param[in] aFrequencyIndex Frequency row index (must be valid!).
   */
  inline double GetWhiteNoiseAmplitudeVariance(const unsigned int aDetectorIndex, const unsigned int aQindex, const unsigned int aFrequencyIndex){
    return GetWhiteNoiseSnrVariance()*omicron[aDetectorIndex%2]->GetBandNoiseAmplitude(aQindex, aFrequencyIndex)*omicron[aDetectorIndex%2]->GetBandNoiseAmplitude(aQindex, aFrequencyIndex);
  };

  /**
   * @brief Dumps Omicron options in a text file.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @param[in] aOutFile Path to the output text file.
   */
  inline void DumpOmicronOptions(const unsigned int aDetectorIndex, const string aOutFile){ return omicron[aDetectorIndex%2]->GwollumOptions::WriteTextFile(aOutFile); };

  /**
   * @brief Returns the number of sky rings \f$N_R = 2n_R + 1\f$.
   */
  inline unsigned int GetRingN(void){ return 2*skyring_n+1; };

  /**
   * @brief Returns the time delay associated to a sky ring: \f$\delta t_{gw}=t_1 - t_0\f$ [s].
   * @details The sky rings are indexed from 0 to \f$N_R = 2n_R + 1\f$. The sky ring with index 0 corresponds to \f$\delta t_{gw}=-\delta t_{01}\f$ which is the maximum time delay associated to the light travel from detector 1 to detector 0. The sky ring with index\f$N_R-1 = 2n_R\f$ corresponds to \f$\delta t_{gw}=+\delta t_{01}\f$ which is the maximum time delay associated to the light travel from detector 0 to detector 1. The sky ring with index\f$n_R\f$ corresponds to \f$\delta t_{gw}=0\f$ which is the central sky ring between the two detectors.
   * @param[in] aSkyRingIndex Sky ring index \f$r\f$ from 0 to \f$N_R\f$. Must be valid!
   */
  inline double GetRingTimeDelay(const unsigned int aSkyRingIndex){
    return skyring_timedelays[aSkyRingIndex];
  };

  /**
   * @brief Returns the number of points along a sky ring.
   * @details The number of points on a ring is used to estimate the minimum and maximum antenna factors for each detectors: see ScanSkyRings(). The number of points is set to guarantee a fixed distance between two consecutive points.
   * The number of points is maximal for \f$r=n_R\f$ for which \f$\delta t_{gw}=0\f$. This is the number provided with the option `OXO/SKYRINGS`. For \f$r=0\f$ (\f$\delta t_{gw}=-\delta t_{01}\f$) or \f$r=N_R-1=2n_R\f$ (\f$\delta t_{gw}=+\delta t_{01}\f$), there is one single point.
   * @param[in] aSkyRingIndex Sky ring index \f$r\f$ from 0 to \f$N_R\f$.
   */
  inline unsigned int GetRingPointsN(const unsigned int aSkyRingIndex){
    if(aSkyRingIndex>=skyring_n)
      return (unsigned int)(TMath::Max(1.0, (double)skyring_np*TMath::Sin((double)(2*skyring_n-aSkyRingIndex)/(double)skyring_n * TMath::Pi()/2.0)));
    else
      return (unsigned int)(TMath::Max(1.0, (double)skyring_np*TMath::Sin((double)aSkyRingIndex/(double)skyring_n * TMath::Pi()/2.0)));
  };

protected:
  
  Omicron *omicron[2];           ///< Omicron objects (detector 0 and 1).
  Streams *detector[2];          ///< Detector streams.
  TTree *oxo_tree[2];            ///< TTree: Omicron results. Do not delete as it is owned by the output file.
  TChain *oxo_tree_read[2];      ///< TChain: Omicron results (read-only).
  double oxo_snrvar[2];          ///< TTree: overall SNR variance: calculated in OxCorr.
  vector <unsigned int> proc_maps[2]; ///< List of produced maps (map time).

  /**
   * @brief Returns an estimate for the minimum and maximum antenna factor ratio for a given time delay between the two detectors.
   * @details This ratio is defined as:
   * \f[
   {\cal{R}}_{gw} = \sqrt{\frac{F^2_{+1}(t_0, \theta_S, \varphi_S, \Psi_S)+F^2_{\times 1}(t_0, \theta_S, \varphi_S, \Psi_S)}{F^2_{+0}(t_0, \theta_S, \varphi_S, \Psi_S)+F^2_{\times 0}(t_0, \theta_S, \varphi_S, \Psi_S)}}
   \f]
   * All the sky rings compatible with the time delay and its uncertainty are scanned. The minimum and maximim antenna factor ratio is returned.
   * @warning The ScanSkyRings() must be called first.
   *
   * @param[out] aRmin Minimum antenna factor ratio.
   * @param[out] aRmax Maximum antenna factor ratio.
   * @param[in] aTimeDelay Time delay \f$\delta t_{gw}=t_1-t_0\f$ between detector 1 and detector 0 [s].
   * @param[in] aTimeDelayUncertainty Uncertainty on the time delay [s].
   */
  void GetAntennaFactorRatioMinMax(double &aRmin, double &aRmax,
                                   const double aTimeDelay, const double aTimeDelayUncertainty);

  /**
   * @brief Generates an html table displaying the Omicron spectrograms.
   * @details The Omicron spectrograms for all Q values are displayed in a single row.
   * @warning The spectrograms must be previously generated with WriteOmicronMaps(). The requested map time must match the time of generated spectrograms.
   *
   * @param[in,out] aReport Output file stream.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @param[in] aMapTime Central GPS time of the spectrogram.
   */  
  void MakeHtmlMapTable(ofstream &aReport, const unsigned int aDetectorIndex, const unsigned int aMapTime);

  /**
   * @brief Fills the output tree with the current Omicron results.
   * @returns The function returns the number of bytes committed to the individual branches.
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   */  
  inline int FillTree(const unsigned int aDetectorIndex){
    if(oxo_tree[aDetectorIndex]!=NULL) return oxo_tree[aDetectorIndex]->Fill();
    return 0;
  };

private:

  double whitenoise_snr_variance;///< White noise SNR variance (after correction).

  // TTREE
  unsigned int oxo_start_time[2];///< TTree: Omicron start time (removing padding).
  double oxo_snrsq[2];           ///< TTree: Maximum squared SNR.

  // SKY RINGS
  unsigned int skyring_n;        ///< Number of sky rings in the sky to cover strictly positive time delays.
  unsigned int skyring_np;       ///< Number of points to cover the null time delay ring (max).
  double *skyring_timedelays;    ///< List of time delays (2 OxOmicron::skyring_n + 1).
  double *skyring_af_ratio_min;  ///< Minimum antenna factor ratio (1/0) along one sky ring (ratio of \f$F_{+}^2 + F_{\times}^2\f$).
  double *skyring_af_ratio_max;  ///< Maximum antenna factor ratio (1/0) along one sky ring (ratio of \f$F_{+}^2 + F_{\times}^2\f$).
  unsigned int skyring_start_time;///< GPS time associated to the sky rings. Detector 0 is used as a reference.
  TH1D** skyring_af_ratio;       ///< Antenna factor ratio histograms /ring.

  /**
   * @brief Generates sky rings and scan the antenna factor ratio along the rings.
   * @details All the sky rings are scanned. Each ring is discretized. The number of points on each ring is determined by GetRingPointsN(). For each sky point, the antenna factor (\f${\cal{F}}^2 = F_{+}^2 + F_{\times}^2\f$) is calculated for each detector at a reference time given by the analysis window of detector 0. The antenna factor ratio \f$\f${\cal{F}}_1/\f${\cal{F}}_0\f$ is histogramed fro each ring. Moreover the minimum and maximum value for each ring is saved.
   * @note The calculation is performed only if the reference time for detector 0 has changed. This function exits immediately otherwise.
   */
  void ScanSkyRings(void);

  /**
   * @brief Creates the Omicron object for a given detector.
   * @details If no Ox files are provided, the omicron object is created using the option text file provided with `OXO/OPTIONS`. With Ox files, the Omicron object is created using the Omicron options saved in the Ox files.
   *
   * @note The list of Omicron options are dumped in a text file (`[NAME].omicron.[0/1].[GPS].opt`) in the output directory.
   * @warning Some omicron options, when provided as a text file, are forced to some pre-determined values. This is necessary to post-process Ox events.
   *
   * @param[in] aDetectorIndex Detector index: 0 = first detector, 1 = second detector.
   * @param[in] aGpsRef Reference GPS time to initiate the Omicron object.
   */
  Omicron* CreateOmicron(const unsigned int aDetectorIndex, const unsigned int aGpsRef);

 /**
   * @brief Checks the consistency between the 2 Omicron objects.
   * @details Conditions to meet:
   * - The Omicron objects must be valid
   * - There must be exactly 1 channel per Omicron object.
   * - The analysis timing must be the same.
   * - The tiling structure must be the same.
   * - The sampling frequency must be the same.
   *
   * @returns true if the omicron objects are consistent.
   */
  bool CheckOmicron(void);

  ClassDef(OxOmicron,0)  
};


#endif


