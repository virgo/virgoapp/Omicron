/**
 * @file 
 * @brief Omicron cross-correlation analysis.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __OxAnalysis__
#define __OxAnalysis__

#include "OxResults.h"

using namespace std;

/**
 * @brief Ox cross-correlation analysis.
 * @details This class is designed to run a cross-correlation analysis between time-frequency maps produced with Omicron.
 *
 * After the OxAnalysis object is constructed, run the Omicron cross-correlation analysis with Process().
 */
class OxAnalysis{

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the OxAnalysis class.
   * @details The Ox analysis is initialized. Options are read and the OxEvent object is created.
   *
   * The list of options is given in a text file. They are identified with a set of tag and key words:
   * - `OX NPERFILE`: Number of analysis windows to save in an Ox file (default = 10). The first detector serves as a reference.
   * - `OX LAGS`: List of lag indices to process. This option is interpreted differently depending on the number of option parameters:
   *   - 0 parameters (missing option): only process the lag `l=0`.
   *   - 1 parameter `L0`: process only one lag with the index `l=L0`.
   *   - 2 parameters `L0 L1`: process all the lags between `l=L0` and `l=L1`, `l=L1` being included.
   *   - N parameters `L0 L1 ... LN`: process the lags `l=L0` `l=L1` ... and `l=LN`.
   *
   * For other options, see OxResults::OxResults().
   * @param[in] aOptionFile Path to the option file.
   * @param[in] aGpsRef0 Reference GPS time to intiate the Omicron object for the first detector.
   * @param[in] aGpsRef1 Reference GPS time to intiate the Omicron object for the second detector.
   */
  OxAnalysis(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1);

  /**
   * @brief Destructor of the OxAnalysis class.
   */
  virtual ~OxAnalysis(void);
  /**
     @}
  */

  /**
   * @brief Runs the cross-correlation analysis over a list of time segments.
   * @details The Omicron analysis over the two detectors is performed one lag at a time. The list of input segments for detector 0 is processed sequentially, chunk by chunk. The corresponding chunk for detector 1 is considered only if it is inside the input segment list for detector 1. The follwing processing functions are called:
   * - OxOmicron::Process() for both detectors.
   * - OxOmicron::Transform() for both detectors.
   * - OxCorr::Process().
   * - OxEvent::SaveEvents().
   *
   * The Ox files are saved in the output directory.
   *
   * @param[in] aInSeg0 List of valid time segments for detector 0.
   * @param[in] aInSeg1 List of valid time segments for detector 1.
   */
  bool Process(Segments *aInSeg0, Segments *aInSeg1);

  /**
   * @brief Post-processing of Ox files.
   * @details First, an Ox result file is open with OxResults::CreateOutputFile(). Ox events are processed with OxResults::Process(). The result of this analysis is saved in the new output file.
   *
   * Finally, OxResults::SavePlots() and OxResults::MakeHtml() are called to save results in the output directory.
   */
  bool PostProcess(void);

  /**
   * @brief Returns the class status.
   */
  inline bool GetStatus(void){ return monit->GetStatus()&&ox_results->GetStatus(); };


private:

  GwollumOptions *ox_opt;     ///< List of options.
  OxResults *ox_results;      ///< Ox results.
  Monitor *monit;             ///< Monitoring.


  ClassDef(OxAnalysis,0)
};

#endif


