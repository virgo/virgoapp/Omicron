/**
 * @file 
 * @brief See OxEvent.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OxEvent.h"

ClassImp(OxEvent)

////////////////////////////////////////////////////////////////////////////////////
OxEvent::OxEvent(const string aOptionFile, const unsigned int aGpsRef0, const unsigned int aGpsRef1):
OxCorr(aOptionFile, aGpsRef0, aGpsRef1){
////////////////////////////////////////////////////////////////////////////////////

  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxEvent::"<<__func__<<endl;

  // random seed
  gRandom->SetSeed(0);
  
  // list of options
  OxInit::DefineOption("OXE", "SLICEDELTA", 0.0, 2);
  OxInit::DefineOption("OXE", "SNRGATE", 1000.0, 2);
  OxInit::DefineOption("OXE", "RANKMETHOD", "X", 1);

  // add options from the text file
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxEvent::"<<__func__<<": add OXE options"<<endl;
  OxInit::AddOptions();

  // overload some of the options (from text file)
  // OXE/SLICEDELTA keep the TTree value
  OxInit::OverloadOption("OXE", "SNRGATE");
  OxInit::OverloadOption("OXE", "RANKMETHOD");

  // save SNR gates for performance reasons
  snrsq_gate_0 = ox_opt->GetOptionValueD("OXE", "SNRGATE", 0);
  snrsq_gate_1 = ox_opt->GetOptionValueD("OXE", "SNRGATE", 1);
  snrsq_gate_0 *= snrsq_gate_0;
  snrsq_gate_1 *= snrsq_gate_1;

  // save rank method for performance reasons
  rank_method = ox_opt->GetOptionValueS("OXE", "RANKMETHOD", 0);

  // amplitude ratio container
  aratio_q = new double [OxOmicron::omicron[0]->GetQN()];
  
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxEvent::"<<__func__<<": make slices"<<endl;

  // make slices
  slice_n = 0;
  slice_nbins = 0;
  slice_firstbin = NULL;
  slice_ctn_binmax = new int* [OxOmicron::omicron[0]->GetQN()];
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
    slice_ctn_binmax[q] = new int [OxOmicron::omicron[0]->GetBandN(q)];
    for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){
      slice_ctn_binmax[q][f] = 0;
    }
  }
  slice_delta_cal = TMath::Abs(ox_opt->GetOptionValueD("OXE", "SLICEDELTA", 0));
  slice_delta_veto = TMath::Abs(ox_opt->GetOptionValueD("OXE", "SLICEDELTA", 1));
  MakeSlices();

  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxEvent::"<<__func__<<": create output TTrees"<<endl;

  // Ox event TTree
  oxe_tree = NULL;
  oxe_aindex = 0;
  oxe_sindex = 0;
  oxe_dt = 0.0;
  oxe_dt_var = 0.0;
  oxe_oxo_0 = 0;
  oxe_oxo_1 = 0;
  oxe_frequency = 0.0;
  oxe_aratio = 0.0;
  oxe_afratio_min = 0.0;
  oxe_afratio_max = 1.0e20;
  oxe_Xi = 0.0;
  oxe_n_sveto_q = new unsigned int [OxOmicron::omicron[0]->GetQN()];
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
    oxe_n_sveto_q[q] = 0;
  }
  
  monit->Monitor::SetMessageType(monitor_info_3);
  *monit<<"OxEvent::"<<__func__<<": prepare input TTrees"<<endl;

  // get Ox event TChain (read-only)
  oxe_tree_read = new TChain("oxe");
  for(unsigned int f=0; f<OxInit::GetOxFileN(); f++){
    if(oxe_tree_read->AddFile(OxInit::GetOxFileName(f).c_str(), 0)!=1) monit->Monitor::SetStatus(false);
  }
  oxe_tree_read->SetBranchAddress("oxe_aindex",       &oxe_aindex);
  oxe_tree_read->SetBranchAddress("oxe_sindex",       &oxe_sindex);
  oxe_tree_read->SetBranchAddress("oxe_dt",           &oxe_dt);
  oxe_tree_read->SetBranchAddress("oxe_dt_var",       &oxe_dt_var);
  oxe_tree_read->SetBranchAddress("oxe_oxo_0",        &oxe_oxo_0);
  oxe_tree_read->SetBranchAddress("oxe_oxo_1",        &oxe_oxo_1);
  oxe_tree_read->SetBranchAddress("oxe_frequency",    &oxe_frequency);
  oxe_tree_read->SetBranchAddress("oxe_aratio",       &oxe_aratio);
  oxe_tree_read->SetBranchAddress("oxe_afratio_min",  &oxe_afratio_min);
  oxe_tree_read->SetBranchAddress("oxe_afratio_max",  &oxe_afratio_max);
  oxe_tree_read->SetBranchAddress("oxe_Xi",           &oxe_Xi);
  oxe_tree_read->SetBranchAddress("oxe_n_sveto_q",    &(oxe_n_sveto_q[0]));
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxEvent::"<<__func__<<": number of entries = "<<GetEntries()<<endl;

  // check consistency with OxOmicron TTrees
  // this required for the mapping between trees: see GetEntry()
  if(oxe_tree_read->GetNtrees()!=oxo_tree_read[0]->GetNtrees()){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxEvent::"<<__func__<<": mismatch between oxe and oxo_0"<<endl;
  }

  // fill option tree
  ox_opt->FillTree();
}

////////////////////////////////////////////////////////////////////////////////////
OxEvent::~OxEvent(void){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxEvent::"<<__func__<<endl;

  delete [] aratio_q;
  delete [] slice_firstbin;
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){
    delete [] slice_ctn_binmax[q];
  }
  delete [] slice_ctn_binmax;
  delete [] oxe_n_sveto_q;
  delete oxe_tree_read;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxEvent::CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxEvent::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxEvent::"<<__func__<<": the OxEvent object is corrupted"<<endl;
    return false;
  }
  
  // create file
  if(OxCorr::CreateOutputFile(aOutFileName, aFileId, aTime)==false) return false;

  // create oxevent tree so it is owned by the file
  // note: from now on, the file is managed by OxInit (saving and closing)
  oxe_tree = new TTree("oxe", "oxe");
  oxe_tree->Branch("oxe_aindex", &oxe_aindex, "oxe_aindex/b");
  oxe_tree->Branch("oxe_sindex", &oxe_sindex, "oxe_sindex/i");
  oxe_tree->Branch("oxe_dt", &oxe_dt, "oxe_dt/D");
  oxe_tree->Branch("oxe_dt_var", &oxe_dt_var, "oxe_dt_var/D");
  oxe_tree->Branch("oxe_oxo_0", &oxe_oxo_0, "oxe_oxo_0/L");
  oxe_tree->Branch("oxe_oxo_1", &oxe_oxo_1, "oxe_oxo_1/L");
  oxe_tree->Branch("oxe_frequency", &oxe_frequency, "oxe_frequency/D");
  oxe_tree->Branch("oxe_aratio", &oxe_aratio, "oxe_aratio/D");
  oxe_tree->Branch("oxe_afratio_min", &oxe_afratio_min, "oxe_afratio_min/D");
  oxe_tree->Branch("oxe_afratio_max", &oxe_afratio_max, "oxe_afratio_max/D");
  oxe_tree->Branch("oxe_Xi", &oxe_Xi, "oxe_Xi/D");
  gwl_ss<<"oxe_n_sveto_q["<<OxOmicron::omicron[0]->GetQN()<<"]/i";
  oxe_tree->Branch("oxe_n_sveto_q", &(oxe_n_sveto_q[0]), gwl_ss.str().c_str());
  gwl_ss.clear(); gwl_ss.str("");

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxEvent::MakeEvent(const unsigned int aSliceIndex, const UChar_t aAnalysisIndex){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxEvent::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxEvent::"<<__func__<<": the OxEvent object is corrupted"<<endl;
    return false;
  }
  if(aSliceIndex>=GetSliceN()){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxEvent::"<<__func__<<": the slice index is incorrect"<<endl;
    return false;
  }

  // analysis index
  oxe_aindex = aAnalysisIndex;
  
  // compute event parameters
  ComputeEvent(aSliceIndex);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool OxEvent::SaveEvents(const UChar_t aAnalysisIndex){
////////////////////////////////////////////////////////////////////////////////////
  monit->Monitor::SetMessageType(monitor_info_2);
  *monit<<"OxEvent::"<<__func__<<endl;
  if(monit->GetStatus()==false){
    monit->Monitor::SetMessageType(monitor_fatal);
    *monit<<"OxEvent::"<<__func__<<": the OxEvent object is corrupted"<<endl;
    return false;
  }

  if(oxe_tree==NULL){
    monit->Monitor::SetMessageType(monitor_error);
    *monit<<"OxEvent::"<<__func__<<": no output tree (CreateOutputFile())"<<endl;
    return false;
  }

  // for efficiency reasons, do not call MakeEvent() in a loop
  // (but the code should be the same!)

  // analysis index
  oxe_aindex = aAnalysisIndex;
  
  // save Ox events in the TTree
  for(unsigned int s=0; s<GetSliceN(); s++){

    // compute event parameters
    ComputeEvent(s);

    // fill tree
    FillTree();
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
int OxEvent::GetEntry(const Long64_t aEntry){
////////////////////////////////////////////////////////////////////////////////////

  // empty tree
  if(GetEntries()==0) return 0;
  
  // load event
  int nbytes = oxe_tree_read->GetEntry(aEntry);
  if(nbytes<=0) return nbytes;

  // get tree number
  // NOTE: empty trees are not counted
  int tree_number = oxe_tree_read->GetTreeNumber();

  // load corresponding OxOmicron entry
  // the number of oxe trees is the same for oxo_0 and oxo_1
  int nbytes_oxo_0 = OxOmicron::GetEntry(0, OxOmicron::oxo_tree_read[0]->GetTreeOffset()[tree_number]+oxe_oxo_0);
  if(nbytes_oxo_0<=0) return nbytes_oxo_0;
  int nbytes_oxo_1 = OxOmicron::GetEntry(1, OxOmicron::oxo_tree_read[1]->GetTreeOffset()[tree_number]+oxe_oxo_1);
  if(nbytes_oxo_1<=0) return nbytes_oxo_1;

  return nbytes + nbytes_oxo_0 + nbytes_oxo_1;
}


////////////////////////////////////////////////////////////////////////////////////
double OxEvent::GetEventRank(void){
////////////////////////////////////////////////////////////////////////////////////

  // SNR gating
  if(OxOmicron::GetOmicronSnrSqMax(0)>=snrsq_gate_0) return 0.0;
  if(OxOmicron::GetOmicronSnrSqMax(1)>=snrsq_gate_1) return 0.0;

  // no time delay
  if(GetEventTimeDelayVariance()<=0.0) return 0.0;

  // Amplitude cross-correlation ranking statistic r = A
  if(!rank_method.compare("A")){
    if(GetEventAmplitudeRatio()<GetEventAntennaFactorRatioMin()) return 0.0;
    if(GetEventAmplitudeRatio()>GetEventAntennaFactorRatioMax()) return 0.0;
  }

  return GetEventIntegratedXi();
}

////////////////////////////////////////////////////////////////////////////////////
void OxEvent::WriteSkyMaps(void){
////////////////////////////////////////////////////////////////////////////////////

  // time delay at the center of the slice
  double slice_dt = GetSliceTimeDelay(oxe_sindex);

  // best-matching sky ring
  TGraph *g_ring = GetSkyRing(GetEventTimeDelay()-slice_dt);
  g_ring->SetMarkerStyle(7);
  g_ring->SetMarkerColor(2);

  // print map (first detector)
  TH2D* h2_map_0 = OxOmicron::detector[0]->GetSkyAntennaFactor(OxOmicron::GetOmicronStartTime(0));
  GwollumPlot::SetLogx(0);
  GwollumPlot::SetLogy(0);
  GwollumPlot::SetLogz(0);
  GwollumPlot::Draw(h2_map_0, "COLZ");
  GwollumPlot::Draw(g_ring, "PSAME");
  gwl_ss<<OxInit::GetOutputDirectory()<<"/OXSKYMAP0"<<"-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png";
  GwollumPlot::Print(gwl_ss.str());
  gwl_ss.clear(); gwl_ss.str("");

  // print map (second detector)
  TH2D* h2_map_1 = OxOmicron::detector[1]->GetSkyAntennaFactor(GetOmicronStartTime(0));
  GwollumPlot::Draw(h2_map_1, "COLZ");
  GwollumPlot::Draw(g_ring, "PSAME");
  gwl_ss<<OxInit::GetOutputDirectory()<<"/OXSKYMAP1"<<"-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png";
  GwollumPlot::Print(gwl_ss.str());
  gwl_ss.clear(); gwl_ss.str("");

  // print ratio map
  h2_map_1->Divide(h2_map_0);
  h2_map_1->SetTitle(("Antenna factor ratio ("+OxOmicron::detector[1]->GetDetectorPrefix()+"/"+OxOmicron::detector[0]->GetDetectorPrefix()+")").c_str());
  h2_map_1->GetZaxis()->SetTitle(("Antenna factor ratio ("+OxOmicron::detector[1]->GetDetectorPrefix()+"/"+OxOmicron::detector[0]->GetDetectorPrefix()+")").c_str());
  h2_map_1->GetZaxis()->SetRangeUser(0,2);
  GwollumPlot::Draw(h2_map_1, "COLZ");
  GwollumPlot::Draw(g_ring, "PSAME");
  gwl_ss<<OxInit::GetOutputDirectory()<<"/OXSKYMAPR"<<"-"<<OxOmicron::GetOmicronStartTime(0)<<"-s"<<oxe_sindex<<".png";
  GwollumPlot::Print(gwl_ss.str());
  gwl_ss.clear(); gwl_ss.str("");
                                                      
  delete h2_map_0;
  delete h2_map_1;
  delete g_ring;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
int OxEvent::FillTree(void){
////////////////////////////////////////////////////////////////////////////////////

  // link event with omicron parameters (last entry)
  oxe_oxo_0 = oxo_tree[0]->GetEntries()-1;
  oxe_oxo_1 = oxo_tree[1]->GetEntries()-1;

  return oxe_tree->Fill();
}

////////////////////////////////////////////////////////////////////////////////////
void OxEvent::ComputeEvent(const unsigned int aSliceIndex){
////////////////////////////////////////////////////////////////////////////////////

  // init parameters
  oxe_sindex = aSliceIndex;
  oxe_dt = 0.0;
  oxe_dt_var = 0.0;
  oxe_frequency = 0.0;
  oxe_Xi = 0.0;
  oxe_aratio = 0.0;
  oxe_afratio_min = 0.0;
  oxe_afratio_max = 1.0e20;

  // locals
  double zeta_bar = 0.0;
  double xi = 0.0;
  double xi_max = 0.0;
  double dt_max = 0.0;
  double dt_gw = 0.0;
  double weight_dt = 0.0;
  double sum_of_weights_dt = 0.0;
  double weight_f = 0.0;
  double sum_of_weights_f = 0.0;
  double weight_xi = 0.0;
  double sum_of_weights_xi = 0.0;
  double omicron_tile_duration = 0.0;
  double var_amp_0 = 0.0;
  double var_amp_1 = 0.0;
  double var_ampnoise_0 = 0.0;
  double var_ampnoise_1 = 0.0;
  double var_diff_0 = 0.0;
  double var_diff_1 = 0.0;
  double xi_max_q = 0.0;

  // time delay at the center of the slice
  double slice_dt = GetSliceTimeDelay(aSliceIndex);

  //***** First, we compute the event parameters
  
  // loop over Q planes
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){

    // init
    oxe_n_sveto_q[q] = 0;
    xi_max_q = -2.0;
    aratio_q[q] = 0.0;
    
    // loop over frequency rows
    for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){

      // init
      xi_max = -2.0;
      slice_ctn_binmax[q][f] = -1;
      
      // loop over time-delay bins in the slice --> get the maximum |xi|
      for(unsigned int t=slice_firstbin[aSliceIndex]; t<slice_firstbin[aSliceIndex]+slice_nbins; t++){
        xi = TMath::Abs(OxCorr::GetXi(q, f, t));
        if(xi > xi_max){
          slice_ctn_binmax[q][f] = (int)t;
          xi_max = xi;
        }
      }

      // xi at maximum
      xi_max = OxCorr::GetXi(q, f, slice_ctn_binmax[q][f]);
            
      // the maximum is negative
      if(xi_max<0){
        //slice_ctn_binmax[q][f] = -1;
        oxe_n_sveto_q[q]++;
        continue;
      }

      // get time delay at maximum
      dt_max = OxCorr::GetTimeDelay((unsigned int)slice_ctn_binmax[q][f]);

      // GW time delay (from the center of the slice)
      dt_gw = dt_max - slice_dt;

      // apply slice veto
      if(dt_gw > OxOmicron::GetLightTravelTime() + slice_delta_cal){
        //slice_ctn_binmax[q][f] = -2;
        oxe_n_sveto_q[q]++;
        continue;
      }
      if(dt_gw < -OxOmicron::GetLightTravelTime() - slice_delta_cal){
        //slice_ctn_binmax[q][f] = -3;
        oxe_n_sveto_q[q]++;
        continue;
      }
      
      // test noise variance (0)
      var_amp_0 = OxCorr::GetAmplitudeVariance(0, q, f);
      var_ampnoise_0 = OxOmicron::GetWhiteNoiseAmplitudeVariance(0, q, f);
      var_diff_0 = var_amp_0 - var_ampnoise_0;
      if(var_diff_0<=0.0){
        //slice_ctn_binmax[q][f] = -5;
        oxe_n_sveto_q[q]++;
        continue;
      }

      // test noise variance (1)
      var_amp_1 = OxCorr::GetAmplitudeVariance(1, q, f);
      var_ampnoise_1 = OxOmicron::GetWhiteNoiseAmplitudeVariance(1, q, f);
      var_diff_1 = var_amp_1 - var_ampnoise_1;
      if(var_diff_1<=0.0){
        //slice_ctn_binmax[q][f] = -4;
        oxe_n_sveto_q[q]++;
        continue;
      }

      // frequency: weighted mean over frequencies and Q planes
      weight_f = xi_max;
      sum_of_weights_f += weight_f;
      oxe_frequency += OxOmicron::omicron[0]->GetBandFrequency(q, f)*weight_f;

      // get zeta_bar
      zeta_bar = OxCorr::GetZetaBar(q, f, slice_ctn_binmax[q][f]);
      
      // max xi --> amplitude ratio estimator for that Q plane
      if(xi_max>xi_max_q){
        xi_max_q = xi_max;

        // use the best variance deviation from noise
        if(var_diff_0/var_amp_0 > var_diff_1/var_amp_1){
          aratio_q[q] = zeta_bar / var_diff_0;
        }
        else{
          aratio_q[q] = var_diff_1 / zeta_bar;
        }
      }
            
      // apply uncertainty veto
      if(omicron_tile_duration/2.0 > OxOmicron::GetLightTravelTime() + slice_delta_cal + slice_delta_veto){
        //slice_ctn_binmax[q][f] = -6;
        oxe_n_sveto_q[q]++;
        continue;
      }

      // fill data points for event display
      OxCorr::g_timedelay_valid[q]->SetPoint(f, dt_max, OxOmicron::omicron[0]->GetBandFrequency(q,f));

      // Omicron tile duration --> time uncertainty
      omicron_tile_duration = (double)OxOmicron::omicron[0]->GetChunkDuration() / (double)OxOmicron::omicron[0]->GetBandTileN(q,f);

      // time delay: weighted mean over frequencies and Q planes
      weight_dt = 4.0 * xi_max / omicron_tile_duration / omicron_tile_duration;
      sum_of_weights_dt += weight_dt;
      oxe_dt += dt_max*weight_dt;
      oxe_dt_var += weight_dt*weight_dt*omicron_tile_duration*omicron_tile_duration/4.0;
    }
  }

  // amplitude ratio = median of all Q planes
  oxe_aratio = GetMedian(OxOmicron::omicron[0]->GetQN(), aratio_q);

  // weighted average for the frequency
  if(sum_of_weights_f>0.0){
    oxe_frequency /= sum_of_weights_f;
  }
  else{
    oxe_frequency = 0.0;
  }

  // weighted average for the time delay
  if(sum_of_weights_dt>0.0){
    oxe_dt /= sum_of_weights_dt;
    oxe_dt_var /= (sum_of_weights_dt*sum_of_weights_dt);
    if(oxe_dt_var<0.0) oxe_dt_var = 0.0;
  }
  else{
    oxe_dt = slice_dt; // CHECKME: right choice?
    oxe_dt_var = -1.0;
  }

  // get time delay bin for this event
  unsigned int dt_bin = GetTimeDelayBinIndex(oxe_dt);

  // get antenna factor ratio min/max
  // FIXME: hard-coded +/-3 sigma!
  OxOmicron::GetAntennaFactorRatioMinMax(oxe_afratio_min, oxe_afratio_max, oxe_dt-slice_dt, 3.0*TMath::Sqrt(oxe_dt_var));

  //***** Second step: xi integration at oxe_dt
    
  // loop over Q planes
  for(unsigned int q=0; q<OxOmicron::omicron[0]->GetQN(); q++){

    // loop over frequency rows
    for(unsigned int f=0; f<OxOmicron::omicron[0]->GetBandN(q); f++){

      // weighted average for Xi
      xi = OxCorr::GetXi(q, f, slice_ctn_binmax[q][f]);
      weight_xi = TMath::Abs(xi);
      sum_of_weights_xi += weight_xi;

      // get time delay at maximum
      dt_max = OxCorr::GetTimeDelay((unsigned int)slice_ctn_binmax[q][f]);

      // GW time delay (from the center of the slice)
      dt_gw = dt_max - slice_dt;

      // apply slice veto
      if(dt_gw > OxOmicron::GetLightTravelTime() + slice_delta_cal) continue;
      if(dt_gw < -OxOmicron::GetLightTravelTime() - slice_delta_cal) continue;

      // test variance
      //var_diff_0 = OxCorr::GetAmplitudeVariance(0, q, f)-OxOmicron::GetWhiteNoiseAmplitudeVariance(0, q, f);
      //if(var_diff_0<0.0) continue;
      //var_diff_1 = OxCorr::GetAmplitudeVariance(1, q, f)-OxOmicron::GetWhiteNoiseAmplitudeVariance(1, q, f);
      //if(var_diff_1<0.0) continue;

      // xi
      xi = OxCorr::GetXi(q, f, dt_bin);
      
      // integrate xi
      oxe_Xi += xi * weight_xi;
    }
  }
  
  // weighted average
  if(sum_of_weights_xi>0.0){
      oxe_Xi /= sum_of_weights_xi;
  }
  else{
    oxe_Xi = 0.0;
  }

  return;
}


////////////////////////////////////////////////////////////////////////////////////
void OxEvent::MakeSlices(void){
////////////////////////////////////////////////////////////////////////////////////

  // slice half duration [s]
  double slice_half_duration = OxOmicron::GetLightTravelTime() + slice_delta_cal + slice_delta_veto;
  if(slice_half_duration>(double)GetAnalysisDuration()/2.0){
    monit->Monitor::SetMessageType(monitor_warning);
    *monit<<"OxEvent::"<<__func__<<": the analysis duration is too short to contain one slice --> use a single slice"<<endl;
    slice_half_duration = (double)GetAnalysisDuration()/2.0;
  }
  
  // total number of time-delay bins in a map
  unsigned int ntbins = OxCorr::GetTimeDelayBinsN();

  // number of time bins in a slice
  slice_nbins = 2*(unsigned int)TMath::Ceil(slice_half_duration/((double)OxOmicron::GetAnalysisDuration()/(double)ntbins));

  // number of full slices in the second half of the analysis window
  unsigned int n_fullslice = (ntbins/2-slice_nbins/2)/slice_nbins;
  
  // number of slices in a map: use an odd number to have a slice at the center of a map
  slice_n = 2*n_fullslice + 1;
  
  // first time-delay bin index of each slice
  slice_firstbin = new unsigned int [slice_n];
  for(unsigned int s=0; s<slice_n; s++)
    slice_firstbin[s] = GetSliceUnusedTimeDelayBinsN()/2 + s*slice_nbins;

  // print info
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxEvent::"<<__func__<<": number of slices in a cross-correlation map = "<<GetSliceN()<<endl;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxEvent::"<<__func__<<": number of time-delay bins in a slice = "<<GetSliceTimeDelayBinsN()<<endl;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxEvent::"<<__func__<<": number of unsused time-delay bins = "<<GetSliceUnusedTimeDelayBinsN()<<endl;
  monit->Monitor::SetMessageType(monitor_info_4);
  *monit<<"OxEvent::"<<__func__<<": slice width = +-"<<GetSliceHalfDuration()<<" s"<<endl;

  return;
}
