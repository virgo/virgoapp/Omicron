/**
 * @file 
 * @brief See Oomicron.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oomicron.h"

ClassImp(Omicron)


////////////////////////////////////////////////////////////////////////////////////
Omicron::Omicron(const string aOptionFile, const bool aOneChannel, const unsigned int aGpsRef, const bool aStrict):
GwollumOptions(){ 
////////////////////////////////////////////////////////////////////////////////////

  PrintAsciiLogo();
  gErrorIgnoreLevel = kError;

  // Read options
  ReadOptions(aOptionFile, aStrict);

  // make components
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": process options"<<endl;
  MakeOptions();
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": prepare FFL object"<<endl;
  MakeFfl(aGpsRef);
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": prepare trigger objects"<<endl;
  MakeTriggers();
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": preprare tiling object"<<endl;
  MakeTiling();
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": prepapre spectrum objects"<<endl;
  MakeSpectrum(aOneChannel);
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": prepare injections"<<endl;
  MakeInjections(aGpsRef);

  // chunk FFT
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": prepare FFT plan"<<endl;
  offt = new fft(tile->GetTimeRange()*GetSampleFrequency(), GwollumOptions::GetOptionValueS("PARAMETER", "FFTPLAN", 0), "r2c");
 
  // data containers
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": prepare data containers"<<endl;
  PsdVect = new double [(spectrum1[0]->GetDataBufferLength()+tile->GetOverlapDuration())*GetSampleFrequency()];
  ChunkVect = new double [offt->GetSize_t()];
  TukeyWindow = GetTukeyWindow(offt->GetSize_t(),
                               (double)tile->GetOverlapDuration()/(double)tile->GetTimeRange());

  // default output directory: main dir
  for(unsigned int c=0; c<channels.size(); c++){
    channels[c]->SetOutputDirectory(GO_MainDir);
  }

  // omicron injection file
  oinjfile.open((GO_MainDir+"/oinjections.txt").c_str());
  oinjfile<<"# Time # Frequency # Q # Amplitude # Phase # True SNR # sigma_t # sigma_f"<<endl;
  oinjfile.precision(5);
  
  // default plottime offset
  SetPlotTimeOffset(0.0);

  // requested segments
  inSegments = new Segments();
  inChunksN = 0;
  chanindex = -1;
}

////////////////////////////////////////////////////////////////////////////////////
Omicron::~Omicron(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;

  // print additional products
  if(GetStatus()&&GO_OutProducts.find("summary")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": save summary"<<endl;
    SaveSummary();
  }
  if(GetStatus()&&GO_OutProducts.find("option")!=string::npos){
    SaveOptions();
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": save options"<<endl;
  }
  if(GetStatus()&&GO_OutProducts.find("html")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": save html report"<<endl;
    MakeHtml();
  }
  
  for(unsigned int c=0; c<channels.size(); c++){
    delete channels[c];
  }
  for(unsigned int c=0; c<channels.size()/one_channel; c++){
    delete spectrum1[c];
    delete spectrum2[c];
  }
  delete [] spectrum1;
  delete [] spectrum2;
  delete spectrumw;
  if(FFL_inject!=FFL) delete FFL_inject;
  delete FFL;
  delete inject;
  delete oinj;
  delete tile;
  delete [] PsdVect;
  delete [] ChunkVect;
  delete [] TukeyWindow;
  delete offt;
  delete inSegments;
  delete mon;
  
  if(oinjfile.is_open()) oinjfile.close();
  GO_InjChan.clear();
  GO_InjFact.clear();
  channels.clear();
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::InitSegments(Segments *aInSeg, Segments *aOutSeg){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  if(aInSeg==NULL){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": the input segment is NULL"<<endl;
    return false;
  }
  if(!aInSeg->GetStatus()){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": the input segment is corrupted"<<endl;
    return false;
  }
  if(!aInSeg->GetN()){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": there is no input segment"<<endl;
    return false;
  }

  // save input segments
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": save input segments"<<endl;
  if(!inSegments->AddSegments(aInSeg)) return false;
  
  // set sequence structure
  inChunksN += tile->SetSegments(aInSeg, aOutSeg);
  
  // update channel list
  if(FFL!=NULL){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": extract channels (FFL)"<<endl;
    if(!FFL->ExtractChannels(aInSeg->GetStart(0))) return false;
  }
  if((FFL_inject!=NULL) && (FFL_inject!=FFL)){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": extract injection channels (FFL)"<<endl;
    if(!FFL_inject->ExtractChannels(aInSeg->GetStart(0))) return false;
  }

  // print segment info
  mon->Monitor::SetMessageType(monitor_info_4);
  *mon<<"Omicron::"<<__func__<<": number of input segments   = "<<aInSeg->GetN()<<endl;
  mon->Monitor::SetMessageType(monitor_info_4);
  *mon<<"Omicron::"<<__func__<<": input livetime     = "<<aInSeg->GetLiveTime()<<endl;
  if(aOutSeg!=NULL){
    mon->Monitor::SetMessageType(monitor_info_4);
    *mon<<"Omicron::"<<__func__<<": number of output segments  = "<<aOutSeg->GetN()<<endl;
    mon->Monitor::SetMessageType(monitor_info_4);
    *mon<<"Omicron::"<<__func__<<": output livetime    = "<<aOutSeg->GetLiveTime()<<endl;
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::MakeDirectories(const double aId){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  std::error_code ec;

  string maindir_prev = MainDir;
  MainDir = GO_MainDir;
  
  // create subdirectory
  if(aId!=0.0){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": create main subdirectory"<<endl;
    gwl_ss<<MainDir<<"/"<<setprecision(3)<<fixed<<aId;
    MainDir = gwl_ss.str();
    gwl_ss.clear(); gwl_ss.str("");
    for(unsigned int n=0; n<3; n++){
      if(!CreateDirectory(ec, MainDir)){
        mon->Monitor::SetMessageType(monitor_error);
        *mon<<"Omicron::"<<__func__<<": the output directory "<<MainDir<<" cannot be created: err="<<ec<<" (try="<<n<<")"<<endl;
        sleep(3);
        continue;
      }
      break;
    }
    if(!filesystem::is_directory(MainDir)){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": the output directory "<<MainDir<<" does not exist"<<endl;
      return false;
    }
  }

  // prelimiary html page
  if(GetStatus()&&GO_OutProducts.find("html")!=string::npos) MakeHtmlInit();
  
  // channel directories
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": create channel directories"<<endl;
  for(unsigned int c=0; c<channels.size(); c++){
    channels[c]->SetOutputDirectory(MainDir+"/"+channels[c]->GetName());
    for(unsigned int n=0; n<3; n++){
      if(!CreateDirectory(ec, channels[c]->GetOutputDirectory())){
         mon->Monitor::SetMessageType(monitor_error);
        *mon<<"Omicron::"<<__func__<<": the channel output directory "<<channels[c]->GetOutputDirectory()<<" cannot be created: err="<<ec<<" (try="<<n<<")"<<endl;
        sleep(3);
        continue;
      }
      break;
    }
    if(!filesystem::is_directory(channels[c]->GetOutputDirectory())){
      mon->Monitor::SetMessageType(monitor_fatal);
      *mon<<"Omicron::"<<__func__<<": the channel output directory "<<channels[c]->GetOutputDirectory()<<" does not exist"<<endl;
      return false;
    }
  }
  
  // update omicron injection file
  if(oinjfile.is_open()){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": open injection report file"<<endl;
    oinjfile.close();
    RemoveFileOrDirectory(ec, maindir_prev+"/oinjections.txt");
    oinjfile.open((MainDir+"/oinjections.txt").c_str());
    oinjfile<<"# Time # Frequency # Q # Amplitude # Phase # True SNR # sigma_t # sigma_f"<<endl;
    oinjfile.precision(5);
  }
    
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::NewChunk(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  bool newseg;
  
  // load new chunk
  if(!tile->NewChunk(newseg)) return false;
  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": chunk "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<" is loaded"<<endl;

  // new segment --> init PSD buffer
  if(newseg){
    mon->Monitor::SetMessageType(monitor_status);
    *mon<<"Omicron::"<<__func__<<": start new segment --> reset PSD buffers"<<endl;
    for(unsigned int c=0; c<GetChannelsN()/one_channel; c++){
      spectrum1[c]->Reset();
      spectrum2[c]->Reset();
    }
    
    // init PSD buffers
    if(FFL!=NULL){

      // only one Spectrum object --> init with current channel
      if((one_channel==GetChannelsN())&&(chanindex>=0)){
        if(InitPsdBuffer(chanindex, 0)==false){
          mon->Monitor::SetMessageType(monitor_warning);
          *mon<<"Omicron::"<<__func__<<": cannot initialize PSD buffers"<<endl;
        }
      }
      // N Spectrum objects --> init all Spect objects
      else if(one_channel==1){
        for(unsigned int c=0; c<GetChannelsN(); c++){
          if(InitPsdBuffer(c, 0)==false){
            mon->Monitor::SetMessageType(monitor_warning);
            *mon<<"Omicron::"<<__func__<<": cannot initialize PSD buffers"<<endl;
          }
        }
      }
      else{
        mon->Monitor::SetMessageType(monitor_error);
        *mon<<"Omicron::"<<__func__<<": no channel is called --> cannot init PSD buffers"<<endl;
      }
    }
  }
  
  // generate SG parameters
  if(GO_InjSg) oinj->MakeWaveform();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::DefineNewChunk(const unsigned int aTimeStart, const unsigned int aTimeEnd, const unsigned int aPsdLengthReset){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  if(aTimeEnd-aTimeStart!=tile->GetTimeRange()){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": the input chunk ("<<aTimeEnd-aTimeStart<<" s) is not the right duration"<<endl;
    return false;
  }

  // set segment for this chunk
  Segments *Stmp = new Segments((double)aTimeStart, (double)aTimeEnd);
  if(!InitSegments(Stmp)){
    delete Stmp;
    return false;
  }
  delete Stmp;

  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": chunk "<<aTimeStart<<"-"<<aTimeEnd<<" is loaded"<<endl;

  // set tiling for this segment
  bool dummy; // not used
  if(!tile->NewChunk(dummy)) return false;
    
  // reset PSD buffer
  if(aPsdLengthReset>0){
    for(unsigned int c=0; c<GetChannelsN()/one_channel; c++){
      spectrum1[c]->Reset();
      spectrum2[c]->Reset();
    }

    // init PSD buffer
    if((FFL!=NULL)){

      // with the current segment
      if(aPsdLengthReset<=tile->GetTimeRange()){

        // only one Spectrum object --> init with current channel
        if((one_channel==GetChannelsN())&&(chanindex>=0)){
          if(InitPsdBuffer(chanindex, 0)==false) return false;
        }
        // N Spectrum objects --> init all Spect objects
        else if(one_channel==1){
          for(unsigned int c=0; c<GetChannelsN(); c++){
            if(InitPsdBuffer(c, 0)==false) return false;
          }
        }
        else{
          mon->Monitor::SetMessageType(monitor_error);
          *mon<<"Omicron::"<<__func__<<": no channel is called --> cannot init PSD buffers"<<endl;
          return false;
        }
      }

      // or with a user-defined value
      else{

        // only one Spectrum object --> init with current channel
        if((one_channel==GetChannelsN())&&(chanindex>=0)){
          if(InitPsdBuffer(chanindex, aPsdLengthReset)==false) return false;
        }
        // N Spectrum objects --> init all Spect objects
        else if(one_channel==1){
         for(unsigned int c=0; c<GetChannelsN(); c++){
            if(InitPsdBuffer(c, aPsdLengthReset)==false) return false;
          }
        }
        else{
          mon->Monitor::SetMessageType(monitor_error);
          *mon<<"Omicron::"<<__func__<<": no channel is called --> cannot init PSD buffers"<<endl;
          return false;
        }
      }
    }
  }

  // generate SG parameters
  if(GO_InjSg) oinj->MakeWaveform();

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::NewChannel(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // load new channel
  chanindex++;

  // last channel
  if(chanindex==(int)GetChannelsN()){
    chanindex=-1;
    mon->Monitor::SetMessageType(monitor_status);
    *mon<<"Omicron::"<<__func__<<": no more channels to load"<<endl;
    return false; 
  }

  // new channel
  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": channel "<<channels[chanindex]->GetName()<<" ("<<chanindex+1<<"/"<<GetChannelsN()<<") is loaded"<<endl;

  // reset chunk numbers
  channels[chanindex]->ResetChunk();
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::LoadData(double **aDataVector, unsigned int &aSize){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  if(FFL==NULL){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": this function can only be used with a valid FFL object"<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }
  if(tile->GetChunkTimeCenter()==0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no chunk called yet"<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }
  if(chanindex<0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no channel called yet"<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }

  if(GO_InjChan.size()){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": load data vector and add injections"<<endl;
  }
  else{
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": load data vector"<<endl;
  }

  // load data (Outils.cc)
  if(LoadData(aDataVector, aSize, tile->GetChunkTimeStart(), tile->GetChunkTimeEnd(), (unsigned int)chanindex)==false){
    return false;
  }

  // monitor
  channels[chanindex]->ChunkLoad(tile->GetChunkTimeCenter());
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
int Omicron::Condition(const unsigned int aInVectSize, double *aInVect, bool &aIsFlat){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return -1;
  }

  // no channels / chunks
  if(chanindex<0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no channel called yet"<<endl;
    return -2;
  }
  if(tile->GetChunkTimeCenter()==0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no chunk called yet"<<endl;
    return -3;
  }

  // Input data checks
  if(aInVect==NULL){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": the input vector is NULL ("<<channels[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
    return 1;
  }

  // input vector is flat
  aIsFlat = false;
  if(aInVectSize>0&&IsFlat(aInVectSize, aInVect)){
    aIsFlat = true;
    mon->Monitor::SetMessageType(monitor_warning);
    *mon<<"Omicron::"<<__func__<<": the input vector is flat ("<<channels[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
  }
  
  // test native sampling (and update if necessary)
  unsigned int nativesampling = aInVectSize/(tile->GetTimeRange());
  if(!channels[chanindex]->SetNativeFrequency(nativesampling)) return 2;

  // add sg injections
  if(GO_InjSg){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": add SineGaus injections"<<endl;
    for(unsigned int d=0; d<aInVectSize; d++)
      aInVect[d] += oinj->GetWaveform(d, nativesampling);
  }

  // transform data vector
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": transform data vector"<<endl;
  if(!channels[chanindex]->Transform(aInVectSize, aInVect,
                                     offt->GetSize_t(), ChunkVect)) return 3;

  // apply Tukey Window
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": apply Tukey window"<<endl;
  for(unsigned int i=0; i<offt->GetSize_t(); i++)
    ChunkVect[i] *= TukeyWindow[i];

  // double-whiten the data
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": double-whiten the chunk"<<endl;
  if(Whiten(chanindex, false)==false) return 4;
  
  // compute tiling power
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": compute tiling power"<<endl;
  tile->SetPower(spectrum1[chanindex/one_channel], spectrum2[chanindex/one_channel]);

  // save sg injection parameters
  // must be done here because the spectra are needed
  if(GO_InjSg) SaveSG();

  // monitor
  channels[chanindex]->ChunkCondition(tile->GetChunkTimeCenter());
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////
long unsigned int Omicron::Project(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return 0;
  }

  // no channels/ chunks
  if(chanindex<0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no channel called yet"<<endl;
    return 0;
  }
  if(tile->GetChunkTimeCenter()==0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no chunk called yet"<<endl;
    return 0;
  }

  // project and save the number of triggers
  channels[chanindex]->SetChunkTriggersN(tile->ProjectData(offt));

  // monitor
  channels[chanindex]->ChunkProject(tile->GetChunkTimeCenter());
  
  return channels[chanindex]->GetChunkTriggersN();
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::WriteOutput(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }
   
  // no channels/ chunks
  if(chanindex<0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no channel called yet"<<endl;
    return false;
  }
  if(tile->GetChunkTimeCenter()==0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no chunk called yet"<<endl;
    return false;
  }

  //*** ASD
  if(GO_OutProducts.find("asd")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": write ASD"<<endl;
    SaveAPSD("ASD");
  }
  
  //*** PSD
  if(GO_OutProducts.find("psd")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": write PSD"<<endl;
    SaveAPSD("PSD");
  }
    
  //*** CONDITIONNED TS
  if(GO_OutProducts.find("timeseries")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": write conditionned time-series"<<endl;
    SaveTS(false);
  }

  //*** WHITENED TS
  if(GO_OutProducts.find("white")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": write whitened data"<<endl;
    offt->Backward();// Back in time domain
    // IMPORTANT: after that, the frequency-domain vector of offt is corrupted (r2c)
    // apply FFT normalization
    for(unsigned int i=0; i<offt->GetSize_t(); i++)
      offt->SetRe_t(i, offt->GetRe_t(i)*(double)channels[chanindex]->GetWorkingFrequency()/(double)offt->GetSize_t());
    SaveTS(true);

    if(GO_OutProducts.find("whitepsd")!=string::npos){
      unsigned int dstart = (tile->GetCurrentOverlapDuration()-tile->GetOverlapDuration()/2)*channels[chanindex]->GetWorkingFrequency(); // start of 'sane' data
      unsigned int dsize = (tile->GetTimeRange()-tile->GetCurrentOverlapDuration())*channels[chanindex]->GetWorkingFrequency(); // size of 'sane' data
      if(spectrumw->LoadData(dsize, offt->GetRe_t(), dstart)) SaveWPSD();
    }
  }
  
  //*** MAPS
  if(GO_OutProducts.find("map")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": write maps"<<endl;
    channels[chanindex]->AddMapSNR(tile->SaveMaps(channels[chanindex]->GetOutputDirectory(),
                                                  channels[chanindex]->GetNameConv()+"_OMICRON",
                                                  GO_OutFormat, toffset, GO_thumb));
  }

  //*** TRIGGERS
  string chunktfile = "";
  if(GO_OutProducts.find("triggers")!=string::npos){
    mon->Monitor::SetMessageType(monitor_info_3);
    *mon<<"Omicron::"<<__func__<<": write triggers"<<endl;
    double triggerrate = 0.0;
    if((!ExtractTriggers(triggerrate)) || (FlushTriggers()<0)){// extract triggers
      chunktfile = "";
      return false;
    }
    // write triggers to disk
    chunktfile = GetFileNameFromPath(WriteTriggers());
  }

  // monitor
  // -- do not include output segment selection --
  channels[chanindex]->ChunkWrite(tile->GetChunkTimeCenter(), chunktfile);
  channels[chanindex]->AddChunkProcSegment(tile->GetChunkTimeStart()+tile->GetCurrentOverlapDuration()-tile->GetOverlapDuration()/2,
                                           tile->GetChunkTimeEnd()-tile->GetOverlapDuration()/2);
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::ExtractTriggers(double &aTriggerRate){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  // no channels/ chunks
  if(chanindex<0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no channel called yet"<<endl;
    return false;
  }
  if(tile->GetChunkTimeCenter()==0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no chunk called yet"<<endl;
    return false;
  }

  // trigger rate: evaluated over the chunk excluding nominal overlaps/2
  aTriggerRate = (double)(channels[chanindex]->GetChunkTriggersN())/(double)(tile->GetTimeRange()-tile->GetOverlapDuration());

  mon->Monitor::SetMessageType(monitor_info_4);
  *mon<<"Omicron::"<<__func__<<": extract "<<channels[chanindex]->GetChunkTriggersN()<<" triggers"<<endl;

  // check against trigger rate limit
  if(aTriggerRate>GO_RateMax){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": the maximum trigger rate ("<<GO_RateMax<<" Hz) is exceeded ("<<channels[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
    return false;
  }

  // save tiles above SNR threshold
  if(!tile->SaveTriggers(channels[chanindex])){
    channels[chanindex]->TriggerBuffer::Reset();
    channels[chanindex]->Triggers::Reset();
    return false;
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
Long64_t Omicron::FlushTriggers(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return -1;
  }
  
  // no channels/ chunks
  if(chanindex<0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no channel called yet"<<endl;
    return -1;
  }
  if(tile->GetChunkTimeCenter()==0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no chunk called yet"<<endl;
    return -1;
  }

  // flush buffer if any
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": flush triggers"<<endl;
  if(!channels[chanindex]->Flush()) return -1;

  // sort triggers
  mon->Monitor::SetMessageType(monitor_info_3);
  *mon<<"Omicron::"<<__func__<<": sort triggers"<<endl;
  if(!channels[chanindex]->SortTriggers()) return -1;

  mon->Monitor::SetMessageType(monitor_info_4);
  *mon<<"Omicron::"<<__func__<<": found "<<channels[chanindex]->Triggers::GetTriggerN()<<" triggers"<<endl;

  // clustering if any
  if(GwollumOptions::GetOptionValueS("PARAMETER", "CLUSTERING", 0).compare("")){
    if(!channels[chanindex]->Clusterize()) return -1;
    mon->Monitor::SetMessageType(monitor_info_4);
    *mon<<"Omicron::"<<__func__<<": found "<<channels[chanindex]->GetClusterN()<<" clusters"<<endl;
    return (Long64_t)channels[chanindex]->GetClusterN();
  }

  return channels[chanindex]->Triggers::GetTriggerN();
}

////////////////////////////////////////////////////////////////////////////////////
string Omicron::WriteTriggers(const bool aUseLVDir){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return "";
  }
  
  // no channels/ chunks
  if(chanindex<0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no channel called yet"<<endl;
    return "";
  }
  if(tile->GetChunkTimeCenter()==0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": no chunk called yet"<<endl;
    return "";
  }

  // write triggers to disk
  if(!aUseLVDir)
    return channels[chanindex]->Write(channels[chanindex]->GetOutputDirectory(), GO_OutFormat);

  unsigned int gps_5=0;
  if(channels[chanindex]->GetN()) gps_5 = (unsigned int) (channels[chanindex]->GetFirst() / 100000);
  else return "";

  gwl_ss<< MainDir << "/" << channels[chanindex]->GetNamePrefix() << "/" << channels[chanindex]->GetNameSuffixUnderScore() << "_OMICRON/" << gps_5;
  string lvdir = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  std::error_code ec;
  if(!CreateDirectory(ec, lvdir)) return "";
  return channels[chanindex]->Write(lvdir, GO_OutFormat);  
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::InitPsdBuffer(const unsigned int aChannelIndex, const unsigned int aPsdLengthForce){
////////////////////////////////////////////////////////////////////////////////////

  // segment start
  unsigned int tstart = tile->GetChunkTimeStart();
  unsigned int tstart0 = tstart;

  // locals
  unsigned int tend;
  unsigned int dsize;
  double *dvector = NULL;
  unsigned int nativesampling;
  
  // keep adding chunks to fill the PSD buffer
  while(spectrum1[aChannelIndex/one_channel]->IsBufferFull()==false){

    // chunk end
    tend = tstart + tile->GetTimeRange();

    // test the chunk end
    if(aPsdLengthForce>0){// --> user defined
      if(tend > tstart0 + aPsdLengthForce) break;
    }
    else{// stop if outside the current segment
      if(tend>tile->GetSegmentEnd()) break;
    }

    // get chunk data vector for this chunk
    // note: a FFL object is required!
    if(LoadData(&dvector, dsize, tstart, tend, aChannelIndex)==false){
      delete [] dvector;
      return false;
    }
    
    // note: we do not include software injection (Oinject) as they are generated on the fly for each chunk
  
    // test native sampling (and update if necessary)
    nativesampling = dsize/tile->GetTimeRange();
    if(!channels[aChannelIndex]->SetNativeFrequency(nativesampling)){
      delete [] dvector;
      return false;
    }

    // transform data vector --> fill ChunkVect
    if(channels[chanindex]->Transform(dsize, dvector, offt->GetSize_t(), ChunkVect)==false){
      delete [] dvector;
      return false;
    }
    delete [] dvector;
    
    // whiten the chunk and update both Spectrum objects
    if(Whiten(aChannelIndex, true)==false){
      return false;
    }

    // next chunk
    tstart = tend - tile->GetOverlapDuration();
  }

  return true;
}




