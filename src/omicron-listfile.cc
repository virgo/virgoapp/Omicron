/**
 * @file 
 * @brief Program to search for omicron trigger files.
 * @details `omicron-listfile` is a command line program to search omicron trigger files in standard places.
 * There are 2 standard directories where omicron trigger files can be stored:
 * - The offline area pointed by the environment variable `$OMICRON_TRIGGERS`
 * - The online area pointed by the environment variable `$OMICRON_TRIGGERS_ONLINE`
 *
 * The trigger files must be organized in a standard directory structure as described in the
 * [DetChar veto trigger conventions](https://dcc.ligo.org/LIGO-T1500286).
 *
 * This program scans files in the offline area first. This is complemented by files in the online area (with no overlap).
 *
 * The program must be given a timing and a channel name.
 * @verbatim
omicron-listfile [channel name] [GPS]
 @endverbatim
 * This command returns the file for this `[GPS]` time.
 *
 * @verbatim
omicron-listfile [channel name] [GPS START] [GPS END]
 @endverbatim
 * This command returns the list of file covering the time range `[GPS START]-[GPS END]`
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OmicronUtils.h"

using namespace std;

void printhelp(void){
    cerr<<endl;
    cerr<<"This program prints the list of Omicron trigger files."<<endl;
    cerr<<endl;
    cerr<<"Usage:"<<endl; 
    cerr<<endl;
    cerr<<"CASE1: omicron-listfile [channel name] [GPS start time] [GPS stop time]"<<endl; 
    cerr<<"|____ prints the list of trigger files associated to given channel between 2 GPS times."<<endl; 
    cerr<<endl;
    cerr<<"CASE2: omicron-listfile [channel name] [GPS time]"<<endl; 
    cerr<<"|____ prints the trigger file associated to a given channel containing a GPS time."<<endl; 
    cerr<<endl;
    return;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return OmicronPrintVersion();
  }

  // check the command line
  if(argc<3){
    printhelp();
    return -1;
  }

  // channel name
  string channelname = (string)argv[1];

  // timing
  unsigned int start = atoi(argv[2]);
  unsigned int stop=start;  
  if(argc>3) stop = atoi(argv[3]);

  cout<<GetOmicronFilePattern(channelname,start,stop)<<endl;
  
  return 0;
}

