/**
 * @file 
 * @brief See Oomicron.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oomicron.h"

/**
 * @brief html color codes. 
 */
static const string colorcode[17] = {"#1b02e6","#0220e6","#0257e6","#028ee6","#01c5e6","#01e6cf","#01e698","#01e660","#01e629","#10e601","#7fe601","#b6e601","#e6de00","#e6a600","#e66f00","#e63800","#e60000"};


////////////////////////////////////////////////////////////////////////////////////
void Omicron::PrintStatusInfo(void){
////////////////////////////////////////////////////////////////////////////////////
  mon->Monitor::SetMessageType(monitor_info_2);
  *mon<<"Omicron::"<<__func__<<endl;
  if(GetStatus()==false){
    mon->Monitor::SetMessageType(monitor_fatal);
    *mon<<"Omicron::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  if(!inSegments->GetN()) return;

  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": requested start    = "<<(unsigned int)inSegments->GetFirst()<<endl;
  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": requested end      = "<<(unsigned int)inSegments->GetLast()<<endl;
  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": requested livetime = "<<(unsigned int)inSegments->GetLiveTime()<<" s"<<endl;
  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": number of channels = "<<channels.size()<<endl;
  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": number of chunks   = "<<inChunksN<<endl;
  mon->Monitor::SetMessageType(monitor_status);
  *mon<<"Omicron::"<<__func__<<": processing time    = "<<mon->GetProcessingTime()<<" s ("<<mon->GetProcessingTimeString()<<")"<<endl;

  for(unsigned int c=0; c<channels.size(); c++){
    mon->Monitor::SetMessageType(monitor_status);
    *mon<<"Omicron::"<<__func__<<": "<<channels[c]->GetName()<<" number of loaded chunks      = "<<channels[c]->GetChunkLoadN()<<endl;
    mon->Monitor::SetMessageType(monitor_status);
    *mon<<"Omicron::"<<__func__<<": "<<channels[c]->GetName()<<" number of conditioned chunks = "<<channels[c]->GetChunkConditionN()<<endl;
    mon->Monitor::SetMessageType(monitor_status);
    *mon<<"Omicron::"<<__func__<<": "<<channels[c]->GetName()<<" number of projected chunks   = "<<channels[c]->GetChunkProjectN()<<endl;
    mon->Monitor::SetMessageType(monitor_status);
    *mon<<"Omicron::"<<__func__<<": "<<channels[c]->GetName()<<" number of written chunks     = "<<channels[c]->GetChunkWriteN()<<endl;

    if(channels[c]->GetChunkProcSegment()->GetN()){
      mon->Monitor::SetMessageType(monitor_status);
      *mon<<"Omicron::"<<__func__<<": "<<channels[c]->GetName()<<" start_out    = "<<(unsigned int)channels[c]->GetChunkProcSegment()->GetFirst()<<endl;
      mon->Monitor::SetMessageType(monitor_status);
      *mon<<"Omicron::"<<__func__<<": "<<channels[c]->GetName()<<" end_out      = "<<(unsigned int)channels[c]->GetChunkProcSegment()->GetLast()<<endl;
      mon->Monitor::SetMessageType(monitor_status);
      *mon<<"Omicron::"<<__func__<<": "<<channels[c]->GetName()<<" livetime_out = "<<(unsigned int)channels[c]->GetChunkProcSegment()->GetLiveTime()<<" s ("<<channels[c]->GetChunkProcSegment()->GetLiveTime()/inSegments->GetLiveTime()*100.0<<"%)"<<endl;
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::IsFlat(const unsigned int aInVectSize, double *aInVect){
////////////////////////////////////////////////////////////////////////////////////
  double vals = aInVect[0];
  if(aInVect[0]==aInVect[aInVectSize-1]){
    for(unsigned int i=0; i<aInVectSize; i++)
      if(aInVect[i]!=vals) return false;
    return true;
  }
  
  return false;
}

////////////////////////////////////////////////////////////////////////////////////
string Omicron::GetColorCode(const double aSnrRatio){
////////////////////////////////////////////////////////////////////////////////////
  if(aSnrRatio<0) return "";
  double inc = 0.1;

  if(aSnrRatio>=17.0*inc) return colorcode[16];// saturation

  return colorcode[((unsigned int)floor(aSnrRatio/inc))%17];
}

////////////////////////////////////////////////////////////////////////////////////
vector <string> Omicron::GetChannels(void){
////////////////////////////////////////////////////////////////////////////////////
  vector <string> chanlist;
  for(unsigned int c=0; c<channels.size(); c++) chanlist.push_back(channels[c]->GetName());
  return chanlist;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveOptions(void){
////////////////////////////////////////////////////////////////////////////////////
  GwollumOptions::FillTree();
  GwollumOptions::WriteTree("gwloptions", MainDir+"/omicron.parameters.root");
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveSummary(void){
////////////////////////////////////////////////////////////////////////////////////
  osummaryfile.open((MainDir+"/summary.txt").c_str());

  osummaryfile<<"# requested GPS start # requested GPS end # requested livetime [s] # number of chunks"<<endl;
  osummaryfile<<(unsigned int)inSegments->GetFirst()<<" "<<(unsigned int)inSegments->GetLast()<<" "<<(unsigned int)inSegments->GetLiveTime()<<" "<<inChunksN<<endl;
  osummaryfile<<endl;
  
  osummaryfile<<"# channel name # data # condition # projection # write # processed livetime [s]"<<endl;
  for(unsigned int c=0; c<channels.size(); c++)
    osummaryfile<<channels[c]->GetName()<<" "<<channels[c]->GetChunkLoadN()<<" "<<channels[c]->GetChunkConditionN()<<" "<<channels[c]->GetChunkProjectN()<<" "<<channels[c]->GetChunkWriteN()<<" "<<channels[c]->GetChunkProcSegment()->GetLiveTime()<<endl;

  osummaryfile.close();

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveSG(void){
////////////////////////////////////////////////////////////////////////////////////
  if(oinjfile.is_open()){
    oinjfile<<fixed<<(double)tile->GetChunkTimeCenter()+oinj->GetTime()<<" ";
    oinjfile<<fixed<<oinj->GetFrequency()<<" ";
    oinjfile<<fixed<<oinj->GetQ()<<" ";
    oinjfile<<scientific<<oinj->GetAmplitude()<<" ";
    oinjfile<<fixed<<oinj->GetPhase()<<" ";
    oinjfile<<fixed<<oinj->GetTrueSNR(spectrum1[chanindex/one_channel], spectrum2[chanindex/one_channel])<<" ";
    oinjfile<<fixed<<oinj->GetSigmat()<<" ";
    oinjfile<<fixed<<oinj->GetSigmaf()<<" ";
    oinjfile<<endl;
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveAPSD(const string aType){
////////////////////////////////////////////////////////////////////////////////////

  // averaging factor
  double factor = 2.0;

  // plots
  TGraph *GAPSD1;
  TGraph *GAPSD2;
  TGraph **G1, **G2; // periodograms
  G1 = new TGraph* [spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(1)];
  G2 = new TGraph* [spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(0)+spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(1)];

  // ASD
  if(!aType.compare("ASD")){

    // averaging factor
    factor = TMath::Sqrt(factor);

    // first whitening
    GAPSD1 = spectrum1[chanindex/one_channel]->GetASD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("ASD_1");
    for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0); i++){
      gwl_ss<<"periodogram1_0_"<<i;
      G1[i] = spectrum1[chanindex/one_channel]->GetAmplitudePeriodogram(0,i);
      G1[i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
    for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(1); i++){
      gwl_ss<<"periodogram1_1_"<<i;
      G1[spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+i] = spectrum1[chanindex/one_channel]->GetAmplitudePeriodogram(1,i);
      G1[spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }

    // second whitening
    GAPSD2 = spectrum2[chanindex/one_channel]->GetASD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("ASD_2");
    for(unsigned int i=0; i<spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(0); i++){
      gwl_ss<<"periodogram2_0_"<<i;
      G2[i] = spectrum2[chanindex/one_channel]->GetAmplitudePeriodogram(0,i);
      G2[i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
    for(unsigned int i=0; i<spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(1); i++){
      gwl_ss<<"periodogram2_1_"<<i;
      G2[spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(0)+i] = spectrum2[chanindex/one_channel]->GetAmplitudePeriodogram(1,i);
      G2[spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(0)+i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
  }

  //PSD
  else{

    // first whitening
    GAPSD1 = spectrum1[chanindex/one_channel]->GetPSD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("PSD_1");
    for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0); i++){
      gwl_ss<<"periodogram1_0_"<<i;
      G1[i] = spectrum1[chanindex/one_channel]->GetPeriodogram(0,i);
      G1[i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
    for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(1); i++){
      gwl_ss<<"periodogram1_1_"<<i;
      G1[spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+i] = spectrum1[chanindex/one_channel]->GetPeriodogram(1,i);
      G1[spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }

    // second whitening
    GAPSD2 = spectrum2[chanindex/one_channel]->GetPSD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("PSD_2");
    for(unsigned int i=0; i<spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(0); i++){
      gwl_ss<<"periodogram2_0_"<<i;
      G2[i] = spectrum2[chanindex/one_channel]->GetPeriodogram(0,i);
      G2[i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
    for(unsigned int i=0; i<spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(1); i++){
      gwl_ss<<"periodogram2_1_"<<i;
      G2[spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(0)+i] = spectrum2[chanindex/one_channel]->GetPeriodogram(1,i);
      G2[spectrum2[chanindex/one_channel]->GetNSubSegmentsMax(0)+i]->SetName(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
  }
  
  // combine the 2 A/PSD
  for(unsigned int i=0; i<(unsigned int)GAPSD2->GetN(); i++)
    GAPSD2->GetY()[i] *= (GAPSD1->GetY()[i]/factor);
   
  // combine the 2 periodograms
  for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(1); i++){
    for(unsigned int j=0; j<(unsigned int)G1[i]->GetN(); j++)
      G2[i]->GetY()[j] *= (G1[i]->GetY()[j]/factor);
    delete G1[i];
  }
  delete [] G1;

  // cosmetics
  tile->SetLogx(1);
  tile->SetLogy(1);
  tile->SetGridx(1);
  tile->SetGridy(1);
  GAPSD2->GetXaxis()->SetMoreLogLabels();
  GAPSD2->SetLineWidth(1);
  GAPSD2->GetYaxis()->SetTitleOffset(1.4);
  GAPSD2->GetXaxis()->SetLabelSize(0.045);
  GAPSD2->GetYaxis()->SetLabelSize(0.045);
  GAPSD2->GetXaxis()->SetTitleSize(0.045);
  GAPSD2->GetYaxis()->SetTitleSize(0.045);
  GAPSD2->GetYaxis()->SetRangeUser(GAPSD2->GetYaxis()->GetXmin()/10.0,
                                   GAPSD2->GetYaxis()->GetXmax()*2.0);
  GAPSD1->SetLineWidth(1);
  if(aType.compare("ASD")){
    GAPSD2->GetHistogram()->SetYTitle("Power [Amp^{2}/Hz]");
    GAPSD2->SetTitle((channels[chanindex]->GetName()+": Power spectrum density").c_str());
  }
  else{
    GAPSD2->GetHistogram()->SetYTitle("Amplitude [Amp/#sqrt{Hz}]");
    GAPSD2->SetTitle((channels[chanindex]->GetName()+": Amplitude spectrum density").c_str());
  }

  // draw
  tile->Draw(GAPSD2,"AP");
  for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(1); i++){
    G2[i]->SetLineColor(12);
    tile->Draw(G2[i],"Lsame");
  }
  tile->Draw(GAPSD2,"PLsame");
  tile->RedrawAxis();
  tile->RedrawAxis("g");
  tile->Draw(GAPSD1,"PLsame");

  // set new name
  gwl_ss<<aType;
  gwl_ss<<"_"<<channels[chanindex]->GetName()<<"_"<<tile->GetChunkTimeCenter();
  GAPSD2->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();

  // ROOT
  if(GO_OutFormat.find("root")!=string::npos){
    gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRON"<<aType<<"-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    TFile *fpsd=new TFile((gwl_ss.str()).c_str(),"RECREATE");
    gwl_ss.str(""); gwl_ss.clear();
    fpsd->cd();
    GAPSD2->Write();
    for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(1); i++){
      G2[i]->Write();
    }
    fpsd->Close();
  }

  // Graphix
  vector <string> form;
  if(GO_OutFormat.find("gif")!=string::npos) form.push_back("gif");
  if(GO_OutFormat.find("png")!=string::npos) form.push_back("png");
  if(GO_OutFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(GO_OutFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(GO_OutFormat.find("xml")!=string::npos) form.push_back("xml");
  if(GO_OutFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(GO_OutFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(GO_OutFormat.find("svg")!=string::npos) form.push_back("svg"); 
  if(form.size()){
    for(unsigned int f=0; f<form.size(); f++){
      gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRON"<<aType<<"-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<"."<<form[f];
      tile->Print(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
  }

  // cleaning
  form.clear();
  delete GAPSD1;
  delete GAPSD2;
  for(unsigned int i=0; i<spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(0)+spectrum1[chanindex/one_channel]->GetNSubSegmentsMax(1); i++)
    delete G2[i];
  delete [] G2;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveTS(const bool aWhite){
////////////////////////////////////////////////////////////////////////////////////

  // create graph 
  TGraph *GDATA = new TGraph(offt->GetSize_t());
 
  // whitened data
  if(aWhite){
    gwl_ss<<"whitets_"<<channels[chanindex]->GetNameConv()<<"_"<<tile->GetChunkTimeCenter();
    for(unsigned int i=0; i<offt->GetSize_t(); i++)
      GDATA->SetPoint(i,(double)tile->GetChunkTimeStart()+(double)i/(double)(channels[chanindex]->GetWorkingFrequency()),offt->GetRe_t(i));
  }
  // conditioned data
  else{
    gwl_ss<<"ts_"<<channels[chanindex]->GetNameConv()<<"_"<<tile->GetChunkTimeCenter();
    for(unsigned int i=0; i<offt->GetSize_t(); i++)
      GDATA->SetPoint(i,(double)tile->GetChunkTimeStart()+(double)i/(double)(channels[chanindex]->GetWorkingFrequency()),ChunkVect[i]);
  }
 
  // plot name
  GDATA->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();
     
  // cosmetics
  tile->SetLogx(0);
  tile->SetLogy(0);
  tile->SetGridx(1);
  tile->SetGridy(1);
  GDATA->GetHistogram()->SetXTitle("Time [s]");
  GDATA->GetHistogram()->SetYTitle("Amplitude");
  if(aWhite)
    GDATA->SetTitle((channels[chanindex]->GetName()+": amplitude whitened data time series").c_str());
  else
    GDATA->SetTitle((channels[chanindex]->GetName()+": amplitude conditionned time series").c_str());
  GDATA->SetLineWidth(1);
  GDATA->GetXaxis()->SetNoExponent();
  GDATA->GetYaxis()->SetTitleOffset(1.4);
  GDATA->GetXaxis()->SetLabelSize(0.045);
  GDATA->GetYaxis()->SetLabelSize(0.045);
  GDATA->GetXaxis()->SetTitleSize(0.045);
  GDATA->GetYaxis()->SetTitleSize(0.045);
  GDATA->GetXaxis()->SetNdivisions(4,5,0);
  tile->Draw(GDATA,"APL");
 
  // ROOT
  if(GO_OutFormat.find("root")!=string::npos){
    if(aWhite)
      gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    else
      gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    TFile * fdata=new TFile((gwl_ss.str()).c_str(),"RECREATE");
    gwl_ss.str(""); gwl_ss.clear();
    fdata->cd();
    GDATA->Write();
    fdata->Close();
  }

  vector <unsigned int> windows = tile->GetPlotTimeWindows();
 
  // WAV
  if(GO_OutFormat.find("wav")!=string::npos){
    // adjust volume
    double volume = 0;
    if(GDATA->GetN()) volume=65500.0/GDATA->GetMaximum();

    // loop over windows
    for(int w=(int)windows.size()-1; w>=0; w--){
      if(aWhite)
	gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<".wav";
      else
	gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<".wav";
      
      // n samples
      unsigned int sound_nstart = TMath::Max((unsigned int)0,(unsigned int)(((double)tile->GetChunkTimeCenter()+toffset-(double)windows[w]/2.0-(double)tile->GetChunkTimeStart())*(double)channels[chanindex]->GetWorkingFrequency()));
      unsigned int sound_n = TMath::Min(GDATA->GetN()*windows[w]/(tile->GetChunkTimeEnd()-tile->GetChunkTimeStart()), GDATA->GetN()-sound_nstart);

      // generate wav file
      MakeStereoSoundFile(gwl_ss.str(),
			  sound_n,
			  channels[chanindex]->GetWorkingFrequency(), GDATA->GetY(), GDATA->GetY(), volume,
			  sound_nstart);
      gwl_ss.str(""); gwl_ss.clear();

    }
  }
 
  // Graphix
  vector <string> form;
  if(GO_OutFormat.find("gif")!=string::npos) form.push_back("gif");
  if(GO_OutFormat.find("png")!=string::npos) form.push_back("png");
  if(GO_OutFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(GO_OutFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(GO_OutFormat.find("xml")!=string::npos) form.push_back("xml");
  if(GO_OutFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(GO_OutFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(GO_OutFormat.find("svg")!=string::npos) form.push_back("svg"); 
  if(form.size()){
    
    // zoom
    for(int w=(int)windows.size()-1; w>=0; w--){
      GDATA->GetXaxis()->SetLimits(tile->GetChunkTimeCenter()+toffset-(double)windows[w]/2.0,tile->GetChunkTimeCenter()+toffset+(double)windows[w]/2.0);
      for(unsigned int f=0; f<form.size(); f++){
	if(aWhite)
	  gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
	else
	  gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
	tile->Print(gwl_ss.str().c_str());
	gwl_ss.str(""); gwl_ss.clear();

        // thumbnails
        if(GO_thumb){
          if(aWhite)
            gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/th"+channels[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
          else
            gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/th"+channels[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
          tile->Print(gwl_ss.str().c_str(), (double)O_THUMBNAIL_RATIO);
          gwl_ss.str(""); gwl_ss.clear();
        }
      }
    }
  }
      
  form.clear();
  delete GDATA;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveWPSD(void){
////////////////////////////////////////////////////////////////////////////////////

  // extract PSD
  TGraph *GPSD = spectrumw->GetPSD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
  
  // cosmetics
  tile->SetLogx(1);
  tile->SetLogy(1);
  tile->SetGridx(1);
  tile->SetGridy(1);
  tile->Draw(GPSD,"APL");
  tile->RedrawAxis();
  tile->RedrawAxis("g");
  GPSD->SetLineWidth(1);
  GPSD->GetXaxis()->SetMoreLogLabels();
  GPSD->GetYaxis()->SetTitleOffset(1.4);
  GPSD->GetXaxis()->SetLabelSize(0.045);
  GPSD->GetYaxis()->SetLabelSize(0.045);
  GPSD->GetXaxis()->SetTitleSize(0.045);
  GPSD->GetYaxis()->SetTitleSize(0.045);
  GPSD->GetHistogram()->SetXTitle("Frequency [Hz]");
  GPSD->GetHistogram()->SetYTitle("Power [Amp^{2}/Hz]");
  GPSD->SetTitle((channels[chanindex]->GetName()+": Power spectrum density").c_str());

  // set new name
  gwl_ss<<"PSDW_"<<channels[chanindex]->GetNameConv()<<"_"<<tile->GetChunkTimeCenter();
  GPSD->SetName(gwl_ss.str().c_str());
  gwl_ss.str(""); gwl_ss.clear();

  // ROOT
  if(GO_OutFormat.find("root")!=string::npos){
    gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONWPSD-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    TFile *fpsd=new TFile((gwl_ss.str()).c_str(),"RECREATE");
    gwl_ss.str(""); gwl_ss.clear();
    fpsd->cd();
    GPSD->Write();
    fpsd->Close();
  }

  // Graphix
  vector <string> form;
  if(GO_OutFormat.find("gif")!=string::npos) form.push_back("gif");
  if(GO_OutFormat.find("png")!=string::npos) form.push_back("png");
  if(GO_OutFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(GO_OutFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(GO_OutFormat.find("xml")!=string::npos) form.push_back("xml");
  if(GO_OutFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(GO_OutFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(GO_OutFormat.find("svg")!=string::npos) form.push_back("svg"); 
  if(form.size()){
    for(unsigned int f=0; f<form.size(); f++){
      gwl_ss<<channels[chanindex]->GetOutputDirectory()<<"/"+channels[chanindex]->GetNameConv()<<"_OMICRONWPSD-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<"."<<form[f];
      tile->Print(gwl_ss.str().c_str());
      gwl_ss.str(""); gwl_ss.clear();
    }
  }
  
  form.clear();
  delete GPSD;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::LoadData(double **aDataVector, unsigned int &aSize,
                       const unsigned int aTimeStart, const unsigned int aTimeEnd,
                       const unsigned int aChannelIndex){
////////////////////////////////////////////////////////////////////////////////////

  // get data vector
  *aDataVector = FFL->GetData(aSize, channels[aChannelIndex]->GetName(), aTimeStart, aTimeEnd);
  if(aSize<=0){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": cannot load "<<channels[aChannelIndex]->GetName()<<" data between "<<aTimeStart<<" and "<<aTimeEnd<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }

  // test native sampling (and update if necessary)
  unsigned int nativesampling = aSize/(aTimeEnd-aTimeStart);
  if(!channels[aChannelIndex]->SetNativeFrequency(nativesampling)){
    mon->Monitor::SetMessageType(monitor_error);
    *mon<<"Omicron::"<<__func__<<": cannot set native frequency "<<nativesampling<<" Hz for "<<channels[aChannelIndex]->GetName()<<" ("<<aTimeStart<<" - "<<aTimeEnd<<")"<<endl;
    delete [] *aDataVector;
    *aDataVector=NULL; aSize=0;
    return false;
  }

  // add software injections if any
  inject->Inject(channels[aChannelIndex], aSize, *aDataVector, aTimeStart);
    
  // get injection vector and inject it
  if(GO_InjChan.size()){
    unsigned int dsize_inj;
    double *dvector_inj = FFL_inject->GetData(dsize_inj, GO_InjChan[aChannelIndex], aTimeStart, aTimeEnd);
    if(dsize_inj<=0){
      mon->Monitor::SetMessageType(monitor_error);
      *mon<<"Omicron::"<<__func__<<": cannot load "<<GO_InjChan[aChannelIndex]<<" data between "<<aTimeStart<<" and "<<aTimeEnd<<endl;
      delete [] *aDataVector;
      *aDataVector=NULL; aSize=0;
      return false;
    }

    // size mismatch
    if(dsize_inj!=aSize){
      mon->Monitor::SetMessageType(monitor_error);
      *mon<<"Omicron::"<<__func__<<": the sampling of the injection channel is not the same as the sampling of the main channel ("<<GO_InjChan[aChannelIndex]<<" "<<aTimeStart<<"-"<<aTimeEnd<<")"<<endl;
      cerr<<"Omicron::LoadData: the sampling of the injection channel is not the same as the sampling of the main channel ("<<GO_InjChan[aChannelIndex]<<" "<<aTimeStart<<"-"<<aTimeEnd<<")"<<endl;
      delete [] *aDataVector; *aDataVector=NULL; aSize=0;
      delete [] dvector_inj;
      return false;
    }

    // add injections in the data
    for(unsigned int d=0; d<aSize; d++) (*aDataVector)[d] += (GO_InjFact[aChannelIndex]*dvector_inj[d]);
    delete [] dvector_inj;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::Whiten(const unsigned int aChannelIndex, const bool aMute){
////////////////////////////////////////////////////////////////////////////////////

  // update first spectrum (if enough data)
  unsigned int dstart = (tile->GetCurrentOverlapDuration()-tile->GetOverlapDuration()/2)*channels[aChannelIndex]->GetWorkingFrequency(); // start of 'sane' data
  unsigned int dsize = (tile->GetTimeRange()-tile->GetCurrentOverlapDuration())*channels[aChannelIndex]->GetWorkingFrequency(); // size of 'sane' data
  if(!spectrum1[aChannelIndex/one_channel]->AddData(dsize, ChunkVect, dstart, aMute)){
    mon->Monitor::SetMessageType(monitor_warning);
    *mon<<"Omicron::"<<__func__<<": this chunk is not used for PSD(1) estimation ("<<channels[aChannelIndex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
  }
  
  // fft-forward the chunk data
  if(!offt->Forward(ChunkVect)) return false;

  // 1st whitening (+ FFT normalization)
  WhitenFrequency(spectrum1[aChannelIndex/one_channel], aChannelIndex, 1.0/(double)offt->GetSize_t());

  // back in the time domain
  offt->Backward();

  // update second spectrum
  double *rvec = offt->GetRe_t();
  if(!spectrum2[aChannelIndex/one_channel]->AddData(dsize, rvec, dstart, aMute)){
    mon->Monitor::SetMessageType(monitor_warning);
    *mon<<"Omicron::"<<__func__<<": this chunk is not used for PSD(2) estimation ("<<channels[aChannelIndex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
  }
  delete [] rvec;
  
  // fft-forward the chunk data
  offt->Forward();

  // 2nd whitening (+ FFT normalization)
  WhitenFrequency(spectrum2[aChannelIndex/one_channel], aChannelIndex, 1.0/(double)channels[aChannelIndex]->GetWorkingFrequency());

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::WhitenFrequency(Spectrum *aSpec, const unsigned int aChannelIndex, const double aNorm){
////////////////////////////////////////////////////////////////////////////////////

  unsigned int i=0; // frequency index
 
  // zero-out DC
  offt->SetRe_f(i,0.0);
  offt->SetIm_f(i,0.0);
  i++;
  
  // zero-out below highpass frequency
  unsigned int n = (unsigned int)(channels[aChannelIndex]->GetHighPassFrequency()*(double)tile->GetTimeRange());
  for(; i<n; i++){
    offt->SetRe_f(i,0.0);
    offt->SetIm_f(i,0.0);
  }
 
  // normalize data by the ASD
  double asdval;
  for(; i<offt->GetSize_f(); i++){
    asdval = aSpec->GetAmplitude((double)i/(double)tile->GetTimeRange())/sqrt(2.0);
    if(asdval<=0){
      offt->SetRe_f(i,0.0);
      offt->SetIm_f(i,0.0);
      continue;
    }
    offt->SetRe_f(i, offt->GetRe_f(i) * aNorm / asdval);
    offt->SetIm_f(i, offt->GetIm_f(i) * aNorm / asdval);
  }

  return;
}

