/**
 * @file 
 * @brief Ox initialization.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __OxInit__
#define __OxInit__
#include "Oomicron.h"

using namespace std;

/**
 * @brief Ox analysis initialization.
 * @details This class is designed to initialize an Ox analysis with a set of options. The options are provided in a text file, called the Ox option file. New options can be defined with the following sequence:
 * - OxInit::DefineOption() to define a new option by a set of key and tag words.
 * - OxInit::AddOptions() to read the options in the text file. Note that the priority is given to options listed in input Ox files provided with the `OXI/OXFILELIST` option.
 * - OxInit::OverloadOption() to give the priority to options in the text file.
 *
 * This class also manages the output Ox files. Call CreateOutputFile() to open a new file. After that all the data products are owned by the file. CreateOutputFile() can be called multiple times in a row to save the analysis results in multiple files. For the last file in the analysis sequence, close it with CloseOutputFile().
 *
 * This class also provides functions to create a web report for an Ox analysis.
 */
class OxInit{

public:
  
  /**
   * @name Constructors and destructors
   @{
  */
  
  /**
   * @brief Constructor of the OxInit class.
   * @details The list of options is given in a text file. They are identified with a set of tag and key words:
   * - `OXI OXFILELIST`: list of file patterns pointing to Ox files.
   * - `OXI NAME`: analysis name. If a list of Ox files is provided, the name is extracted from the files.
   * - `OXI OUTDIR`: path to the output directory. If the output directory does not exist, it is created.
   * - `OXI VERBOSITY`: verbosity level (0 = silent).
   *
   * If a list of Ox files is provided, the options are downloaded from the Ox files. Some options are overwritten when provided in the text file:
   * - `OXI OXFILELIST`
   * - `OXI OUTDIR`
   * - `OXI VERBOSITY`
   *
   * @param[in] aOptionFile Path to the Ox option text file.
   */
  OxInit(const string aOptionFile);

  /**
   * @brief Destructor of the OxInit class.
   */
  virtual ~OxInit(void);

  /**
     @}
  */

  /**
   * @brief Returns the object name.
   */
  inline string GetName(void){ return ox_name; }; 

  /**
   * @brief Returns the number of input Ox files.
   * @details This the number of files matching the patterns listed with `OXI/OXFILELIST` containing Ox options.
   */
  inline unsigned int GetOxFileN(void){ return ox_nfiles; }; 

  /**
   * @brief Returns the path to the output directory.
   */
  inline string GetOutputDirectory(void){ return ox_outdir; }; 

  /**
   * @brief Closes the current ouput Ox file (if any).
   * @details All the ROOT objects are saved in the file before closing.
   */
  void CloseOutputFile(void);

  /**
   * @brief Creates a new ouput Ox file.
   * @details A new ROOT file is opened in the output directory to save the Ox analysis results. The file name is "[NAME]_[ID]-[TIME].root", where [TIME] and [ID] must be provided by the user. [NAME] is the name provided in the Ox option file. The file remains open.
   *
   * @warning If [TIME]=0, the [TIME] variable is incremented (+1) to guarantee the output Ox file does not already exist.
   *
   * The option TTree is saved in the Ox file.
   * @note If an Ox file is already open, it is closed before opening a new file (saving all the pending objects).
   *
   * @param[out] aOutFileName Output ROOT Ox file name.
   * @param[in] aFileId File ID used to name the ouput Ox file.
   * @param[in] aTime Time used to name the ouput Ox file.
   */
  bool CreateOutputFile(string &aOutFileName, const string aFileId, const unsigned int aTime);

  /**
   * @brief Attach a TTree to the output Ox file.
   * @param[in] aTree Pointer to the TTree.
   * @pre The output Ox file must exist: see CreateOutputFile().
   */
  inline void AttachTree(TTree* aTree){ aTree->SetDirectory(ox_outfile); };

  /**
   * @brief Returns the class status.
   */
  inline bool GetStatus(void){ return monit->GetStatus(); };

  /**
   * @brief Returns the verbosity level.
   */
  inline unsigned int GetVerbosityLevel(void){ return monit->GetVerbosityLevel(); };

protected:
  
  Monitor *monit;                ///< Class monitor.
  GwollumOptions *ox_opt;        ///< List of options.

  /**
   * @brief Creates an html report.
   * @details A new html file is created in the output directory. It is intiated with a header, a title, and a summary table.
   * @returns The output stream to continue to populate the report.
   * @param[in] aIndexFileName File name for the index.
   * @param[in] aTitle Page title.
   */
  ofstream CreateHtml(const string aIndexFileName="index.html", const string aTitle="Omicron-X report");
  
  /**
   * @brief Closes an html report.
   * @details The html report is finished with a footer and the file stream is closed.
   */
  void CloseHtml(ofstream &aReport);
  
  /**
   * @brief Returns the path to the Ox file.
   * @param[in] aFileIndex File index: must be valid.
   */
  inline string GetOxFileName(const unsigned int aFileIndex){ return ox_filename[aFileIndex]; }; 

  /**
   * @brief Defines an option (integer).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  inline void DefineOption(const string aTag, const string aKey, const int aDefaultValue, const unsigned int aSize){
    OxInit::ox_opt_text->Add(aTag, aKey, aDefaultValue, aSize);
  };

  /**
   * @brief Defines an option (unsigned integer).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  inline void DefineOption(const string aTag, const string aKey, const unsigned int aDefaultValue, const unsigned int aSize){
    OxInit::ox_opt_text->Add(aTag, aKey, aDefaultValue, aSize);
  };

  /**
   * @brief Defines an option (double).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  inline void DefineOption(const string aTag, const string aKey, const double aDefaultValue, const unsigned int aSize){
    OxInit::ox_opt_text->Add(aTag, aKey, aDefaultValue, aSize);
  };

  /**
   * @brief Defines an option (string).
   * @param[in] aTag Option tag.
   * @param[in] aKey Option aKey.
   * @param[in] aDefaultValue Default value.
   * @param[in] aSize Number of values for this option. Use 0 for no limits on the size.
   */
  inline void DefineOption(const string aTag, const string aKey, const string aDefaultValue, const unsigned int aSize){
    OxInit::ox_opt_text->Add(aTag, aKey, aDefaultValue, aSize);
  };

  /**
   * @brief Adds options from the text Ox option file.
   * @details Options (and values) in the text file are added if:
   * 1. No Ox files are provided.
   * 2. The option is missing in the Ox files
   * 3. New options were defined with DefineOption().
   */
  void AddOptions(void);

  /**
   * @brief Overloads an option using the text file as a reference.
   * @param[in] aTag Option tag.
   * @param[in] aKey Option key.
   */
  void OverloadOption(const string aTag, const string aKey);

private:

  string ox_opt_filepath;        ///< Path to the option text file.
  GwollumOptions *ox_opt_text;   ///< List of options extracted from a txt file.
  string ox_name;                ///< Object name.
  string ox_outdir;              ///< Path to the output directory.
  TFile *ox_outfile;             ///< Output file.
  unsigned int ox_nfiles;        ///< Number of input Ox files matching the patterns (without checking content).
  string *ox_filename;           ///< List of Ox file names.
  
  /**
   * @brief Gets options from a list of Ox files.
   * @details When a list of Ox files is provided, this function lists all the files matching the file patterns. From these files, the Ox options are extracted. If a file does not contain Ox options, the file is ignored.
   *
   * The list of options must be uniform across all the Ox files.
   */
  void GetOptionsFromOxFiles(void);

  ClassDef(OxInit,0)  
};


#endif


