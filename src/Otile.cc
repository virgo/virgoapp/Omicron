/**
 * @file 
 * @brief See Otile.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Otile.h"

ClassImp(Otile)

////////////////////////////////////////////////////////////////////////////////////
vector <double> ComputeQs(const double aQMin, const double aQMax, const double aMaximumMismatch){
////////////////////////////////////////////////////////////////////////////////////  
  
  // adjust input parameters
  double qmin=aQMin;
  double qmax=aQMax;
  if(fabs(qmax)<fabs(qmin)){ qmin=fabs(qmax); qmax=fabs(qmin); }
  if(qmin<TMath::Sqrt(11.0)) qmin=TMath::Sqrt(11.0);
  if(qmax<TMath::Sqrt(11.0)) qmax=TMath::Sqrt(11.0);
  if(qmax<qmin){ qmin=qmax; qmax=qmin; }
  
  // number of planes
  double QCumulativeMismatch = TMath::Log(qmax/qmin)/TMath::Sqrt(2.0);
  double mismatchstep = 2.0*TMath::Sqrt(aMaximumMismatch/3.0);
  int n = (int)ceil(QCumulativeMismatch/mismatchstep);
  if(n<=0) n=1;
  double Qmismatchstep = QCumulativeMismatch/(double)n;
  
  // compute Q values
  vector <double> qs;
  for(int i=0; i<n; i++){
    qs.push_back(qmin * TMath::Exp(TMath::Sqrt(2.0) * (0.5+(double)i) * Qmismatchstep));
  }
  return qs;
}

////////////////////////////////////////////////////////////////////////////////////
Otile::Otile(const unsigned int aTimeRange, const unsigned int aTimeOverlap,
	     const double aQMin, const double aQMax, 
	     const double aFrequencyMin, const double aFrequencyMax, 
	     const unsigned int aSampleFrequency, const double aMaximumMismatch,
             const unsigned int aFullMapNt,
	     const string aPlotStyle, const unsigned int aVerbosity):
Osequence(aTimeRange, aTimeOverlap, aVerbosity),
  GwollumPlot("otile", aPlotStyle){
////////////////////////////////////////////////////////////////////////////////////
 
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;

  // save parameters
  MaximumMismatch = aMaximumMismatch;
  
  // compute Q values
  vector <double> Qs = ComputeQs(aQMin, aQMax, MaximumMismatch);
  nq = Qs.size();
 
  // create Q planes
  Osequence::mon->SetMessageType(monitor_info_3);
  *Osequence::mon<<"Otile::"<<__func__<<": creating "<<nq<<" Q-planes"<<endl;
  qplanes = new Oqplane* [nq];
  t_snrmax = new unsigned int* [nq];
  f_snrmax = new unsigned int* [nq];
  for(unsigned int q=0; q<nq; q++){
    qplanes[q] = new Oqplane(Qs[q], aSampleFrequency,
                             aFrequencyMin, aFrequencyMax,
                             Osequence::GetTimeRange(), MaximumMismatch);
    Osequence::mon->SetMessageType(monitor_info_4);
    *Osequence::mon<<"Otile::"<<__func__<<": q-plane #"<<q<<", Q = "<<qplanes[q]->GetQ()<<endl;
    Osequence::mon->SetMessageType(monitor_info_4);
    *Osequence::mon<<"Otile::"<<__func__<<": q-plane #"<<q<<", time range = "<<qplanes[q]->GetTimeRange()<<" s"<<endl;
    Osequence::mon->SetMessageType(monitor_info_4);
    *Osequence::mon<<"Otile::"<<__func__<<": q-plane #"<<q<<", frequency range = "<<qplanes[q]->GetFrequencyMin()<<" - "<<qplanes[q]->GetFrequencyMax()<<" Hz"<<endl;
    Osequence::mon->SetMessageType(monitor_info_4);
    *Osequence::mon<<"Otile::"<<__func__<<": q-plane #"<<q<<", number of frequency rows = "<<qplanes[q]->GetBandN()<<endl;
    Osequence::mon->SetMessageType(monitor_info_4);
    *Osequence::mon<<"Otile::"<<__func__<<": q-plane #"<<q<<", number of tiles = "<<qplanes[q]->GetTileN()<<endl;
  }
  Qs.clear();

  // check timing
  if(qplanes[0]->GetTimeRange()!=Osequence::GetTimeRange()){
    Osequence::mon->SetMessageType(monitor_fatal);
    *Osequence::mon<<"Otile::"<<__func__<<": timing mismatch (tiling/sequence)"<<endl;
  }
  
  // set default parameters
  SetSnrThr();
  SetMapFill();
  SetRangez();

  // create full maps
  pwin.clear();
  FullMapNt = aFullMapNt;
  fullmap = NULL;
  for(unsigned int q=0; q<nq; q++){
    t_snrmax[q] = NULL;
    f_snrmax[q] = NULL;
  }
  vector<unsigned int> win_whatever;
  SetPlotTimeWindows(win_whatever);
   
  // Newtonian chirp track
  chirp = new TF1("chirp", "pow([0]*(x-[1]), -3.0/8.0)", -(double)GetTimeRange()/2.0, -0.0001);
  chirp->SetLineColor(0);
  chirp->SetLineWidth(1);
  chirp->SetNpx(1000);
  SetChirp();
}

////////////////////////////////////////////////////////////////////////////////////
Otile::~Otile(void){
////////////////////////////////////////////////////////////////////////////////////
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;
  for(unsigned int w=0; w<pwin.size(); w++) delete fullmap[w];
  delete [] fullmap;
  for(unsigned int q=0; q<nq; q++){
    delete [] t_snrmax[q];
    delete [] f_snrmax[q];
    delete qplanes[q];
  }
  delete [] qplanes;
  delete [] t_snrmax;
  delete [] f_snrmax;
  delete chirp;
}
 
////////////////////////////////////////////////////////////////////////////////////
long unsigned int Otile::GetTileN(const double aPadding){
////////////////////////////////////////////////////////////////////////////////////
  long unsigned int nt = 0;
  for(unsigned int q=0; q<nq; q++){
    nt+=qplanes[q]->GetTileN(aPadding);
  }
  return nt;  
}

////////////////////////////////////////////////////////////////////////////////////
double* Otile::GetBands(const unsigned int aQindex){
////////////////////////////////////////////////////////////////////////////////////
  if(aQindex<nq) return qplanes[aQindex]->GetBands();
  unsigned int nfbins = GetBandN(nq);
  double *fbins = new double [nfbins+1];
  double FullMapFrequencyLogStep = log(GetFrequencyMax()/GetFrequencyMin()) / (double)nfbins;
  for(unsigned int f=0; f<=nfbins; f++) fbins[f] = GetFrequencyMin() * TMath::Exp((double)f*FullMapFrequencyLogStep);
  return fbins;
}

////////////////////////////////////////////////////////////////////////////////////
void Otile::SetPlotTimeWindows(vector <unsigned int> aWindows){
////////////////////////////////////////////////////////////////////////////////////
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;
  if(GetStatus()==false){
    Osequence::mon->SetMessageType(monitor_fatal);
    *Osequence::mon<<"Otile::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  // clean up existing full maps
  for(unsigned int w=0; w<pwin.size(); w++) delete fullmap[w];
  delete [] fullmap;
  
  // time windows
  pwin.clear();
  if(aWindows.size()==0) pwin.push_back(Osequence::GetTimeRange() - Osequence::GetOverlapDuration());
  else{
    for(unsigned int w=0; w<aWindows.size(); w++){
      if(aWindows[w]==0) pwin.push_back(Osequence::GetTimeRange() - Osequence::GetOverlapDuration());
      else if(aWindows[w]>Osequence::GetTimeRange()) pwin.push_back(Osequence::GetTimeRange());
      else pwin.push_back(aWindows[w]);
    }
    std::sort(pwin.begin(), pwin.end());
  }

  // create full map
  fullmap = new TH2D* [pwin.size()];
  double *fbins = GetBands(nq);
  double *tbins;
  for(unsigned int w=0; w<pwin.size(); w++){
    gwl_ss<<"fullmap_"<<pwin[w];
    tbins = new double [GetBandTileN(nq, 0)+1];
    for(unsigned int t=0; t<=GetBandTileN(nq, 0); t++)
      tbins[t] = -(double)pwin[w]/2.0 + (double)t/(double)GetBandTileN(nq, 0)*(double)pwin[w];
    fullmap[w] = new TH2D(gwl_ss.str().c_str(),"Full map",
                          GetBandTileN(nq, 0), tbins,
                          GetBandN(nq), fbins);
    delete [] tbins;
    gwl_ss.clear(); gwl_ss.str("");
    fullmap[w]->SetDirectory(0);
    fullmap[w]->GetXaxis()->SetTitle("Time [s]");
    fullmap[w]->GetYaxis()->SetTitle("Frequency [Hz]");
    fullmap[w]->GetXaxis()->SetNoExponent();
    fullmap[w]->GetYaxis()->SetMoreLogLabels();
    fullmap[w]->GetXaxis()->SetNdivisions(4,5,0);
    fullmap[w]->GetXaxis()->SetLabelSize(0.045);
    fullmap[w]->GetYaxis()->SetLabelSize(0.045);
    fullmap[w]->GetZaxis()->SetLabelSize(0.045);
    fullmap[w]->GetXaxis()->SetTitleSize(0.045);
    fullmap[w]->GetYaxis()->SetTitleSize(0.045);
    fullmap[w]->GetZaxis()->SetTitleSize(0.045);
    fullmap[w]->GetYaxis()->SetTitleOffset(1.4);
  }
  delete [] fbins;

  for(unsigned int q=0; q<nq; q++){
    delete [] t_snrmax[q];
    delete [] f_snrmax[q];
    t_snrmax[q] = new unsigned int [pwin.size()];
    f_snrmax[q] = new unsigned int [pwin.size()];
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Otile::DrawMapTiling(const unsigned int aQindex){
////////////////////////////////////////////////////////////////////////////////////  
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;
  if(GetStatus()==false){
    Osequence::mon->SetMessageType(monitor_fatal);
    *Osequence::mon<<"Otile::"<<__func__<<": corrupted object"<<endl;
    return;
  }

  qplanes[aQindex]->FillMap("display", qplanes[aQindex]->GetTimeMin(), qplanes[aQindex]->GetTimeMax());
  GwollumPlot::Draw(qplanes[aQindex]->tfmap, "COL");

  /* add horizontal lines (paper plots)
  qplanes[aQindex]->GetXaxis()->SetRangeUser(-0.1,0.1);
  TLine *lf;
  for(int f=0; f<qplanes[aQindex]->GetNBands(); f++){
    lf = new TLine(-0.1,qplanes[aQindex]->GetBandStart(f),0.1,qplanes[aQindex]->GetBandStart(f));
    lf->SetLineStyle(3);
    lf->SetLineColor(2);
    Draw(lf,"same");
  }
  */
  return;
}

////////////////////////////////////////////////////////////////////////////////////
long unsigned int Otile::ProjectData(fft *aDataFft){
////////////////////////////////////////////////////////////////////////////////////
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;
  if(GetStatus()==false){
    Osequence::mon->SetMessageType(monitor_fatal);
    *Osequence::mon<<"Otile::"<<__func__<<": corrupted object"<<endl;
    return 0;
  }

  long unsigned int nt = 0;// number of tiles above threshold
  
  // project onto q planes, removing half the overlap on both sides
  for(unsigned int p=0; p<nq; p++)
    nt+=qplanes[p]->ProjectData(aDataFft, (double)(Osequence::GetOverlapDuration()/2));
  
  return nt;
}

////////////////////////////////////////////////////////////////////////////////////
Segments* Otile::GetTileSegments(TH1D *aSnrThreshold, const double aPadding){
////////////////////////////////////////////////////////////////////////////////////
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;
  if(GetStatus()==false){
    Osequence::mon->SetMessageType(monitor_fatal);
    *Osequence::mon<<"Otile::"<<__func__<<": corrupted object"<<endl;
    return NULL;
  }

  Segments *seg= new Segments();
  
  // add tile segments for each plane
  for(unsigned int p=0; p<nq; p++)
    qplanes[p]->AddTileSegments(seg, aSnrThreshold, (double)Osequence::GetChunkTimeCenter(), aPadding);
    
  return seg;
}

////////////////////////////////////////////////////////////////////////////////////
bool Otile::SaveTriggers(TriggerBuffer *aTriggers){
////////////////////////////////////////////////////////////////////////////////////
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;
  if(GetStatus()==false){
    Osequence::mon->SetMessageType(monitor_fatal);
    *Osequence::mon<<"Otile::"<<__func__<<": corrupted object"<<endl;
    return false;
  }

  if(!aTriggers->Segments::GetStatus()){
    Osequence::mon->SetMessageType(monitor_error);
    *Osequence::mon<<"Otile::"<<__func__<<": the trigger Segments object is corrupted"<<endl;
    return false;
  }
    
  // output active segments (remove 1/2 overlap)
  Segments *seg = Osequence::GetChunkOut();

  // corrupted segments
  if((seg==NULL)||(seg->GetStatus()==false)){
    Osequence::mon->SetMessageType(monitor_error);
    *Osequence::mon<<"Otile::"<<__func__<<": the chunk segments are corrupted"<<endl;
    return false;
  }
  
  // no livetime --> nothing to do
  if(seg->GetLiveTime()==0.0){
    delete seg;
    return true;
  }

  Osequence::mon->SetMessageType(monitor_info_4);
  *Osequence::mon<<"Otile::"<<__func__<<": saving triggers for "<<aTriggers->GetName()<<" in "<<seg->GetFirst()<<"-"<<seg->GetLast()<<endl;

  // if buffer, segments must be saved first
  if(aTriggers->GetSize()){
    if(!aTriggers->SetSegments(seg)){
      Osequence::mon->SetMessageType(monitor_error);
      *Osequence::mon<<"Otile::"<<__func__<<": cannot save trigger segments"<<endl;
      delete seg;
      return false;
    }
  }
  
  // save triggers for each Q plane
  for(unsigned int p=0; p<nq; p++){
    if(!qplanes[p]->SaveTriggers(aTriggers, (double)Osequence::GetChunkTimeCenter(), seg)){
      Osequence::mon->SetMessageType(monitor_error);
      *Osequence::mon<<"Otile::"<<__func__<<": cannot save trigger for q-plane #"<<p<<endl;
      delete seg;
      return false;
    }
  }
    
  // save trigger segments (if no buffer)
  if(aTriggers->GetSize()==0){
    aTriggers->Segments::AddSegments(seg);
  }
  
  delete seg;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
double Otile::SaveMaps(const string aOutdir, const string aName, const string aFormat,
                       const double aTimeOffset, const bool aThumb){
////////////////////////////////////////////////////////////////////////////////////
  Osequence::mon->SetMessageType(monitor_info_2);
  *Osequence::mon<<"Otile::"<<__func__<<endl;
  if(GetStatus()==false){
    Osequence::mon->SetMessageType(monitor_fatal);
    *Osequence::mon<<"Otile::"<<__func__<<": corrupted object"<<endl;
    return -1.0;
  }

  if(!filesystem::is_directory(aOutdir)){
    Osequence::mon->SetMessageType(monitor_error);
    *Osequence::mon<<"Otile::"<<__func__<<": the directory "<<aOutdir<<" is missing"<<endl;
    return -1.0;
  }
   
  if(fullmap==NULL){
    Osequence::mon->SetMessageType(monitor_error);
    *Osequence::mon<<"Otile::"<<__func__<<": the fullmap does not exist, SetPlotTimeWindows() must be called first"<<endl;
    return -1.0;
  }

  Osequence::mon->SetMessageType(monitor_info_4);
  *Osequence::mon<<"Otile::"<<__func__<<": saving maps for "<<aName<<" centered on "<<(double)Osequence::GetChunkTimeCenter()+aTimeOffset<<endl;

  // get SNR-max bins /qplane and /window
  Osequence::mon->SetMessageType(monitor_info_3);
  *Osequence::mon<<"Otile::"<<__func__<<": search for tiles with the maximum SNR"<<endl;
  unsigned int tstart, tend;
  double snr2max, snr2;
  for(unsigned int q=0; q<nq; q++){

    // loop over windows
    for(unsigned int w=0; w<pwin.size(); w++){

      // init
      snr2max = -1.0;
      t_snrmax[q][w] = 0;
      f_snrmax[q][w] = 0;

      // loop over frequency rows
      for(unsigned int f=0; f<qplanes[q]->GetBandN(); f++){

        // window starting/ending tile index
	tstart=qplanes[q]->GetTimeTileIndex(f, aTimeOffset-(double)pwin[w]/2.0);
	tend=qplanes[q]->GetTimeTileIndex(f, aTimeOffset+(double)pwin[w]/2.0);

	for(unsigned int t=tstart; t<tend; t++){
	  snr2=qplanes[q]->GetTileSnrSq(t, f);
	  if(snr2>snr2max){
	    snr2max = snr2;
	    t_snrmax[q][w] = t;
	    f_snrmax[q][w] = f;
	  }
	}
      }
    }
  }

  // get qplane with SNR-max /window (for full map)
  Osequence::mon->SetMessageType(monitor_info_3);
  *Osequence::mon<<"Otile::"<<__func__<<": get qplane with maximum SNR /window"<<endl;
  unsigned int *q_snrmax = new unsigned int [pwin.size()];
  for(unsigned int w=0; w<pwin.size(); w++){
    snr2max = -1.0;
    q_snrmax[w] = 0;
    for(unsigned int q=0; q<nq; q++){
      snr2=qplanes[q]->GetTileSnrSq(t_snrmax[q][w], f_snrmax[q][w]);
      if(snr2>snr2max){
	snr2max = snr2;
	q_snrmax[w] = q;
      }
    }
  }

  // maximum SNR in the first window
  double snr_max_w0 = TMath::Sqrt(qplanes[q_snrmax[0]]->GetTileSnrSq(t_snrmax[q_snrmax[0]][0], f_snrmax[q_snrmax[0]][0]));

  // apply map SNR threshold (first window only!)
  if(qplanes[q_snrmax[0]]->GetTileSnrSq(t_snrmax[q_snrmax[0]][0], f_snrmax[q_snrmax[0]][0])<SnrThr_map*SnrThr_map){
    // below threshold: exit
    Osequence::mon->SetMessageType(monitor_warning);
    *Osequence::mon<<"Otile::"<<__func__<<": map "<<aName<<" (+-"<<pwin[0]/2.0<<"s) is below SNR threshold -> do not save"<<endl;
    delete [] q_snrmax;
    return snr_max_w0;
  }

  // graphix
  vector <string> form;
  if(aFormat.find("gif")!=string::npos) form.push_back("gif");
  if(aFormat.find("png")!=string::npos) form.push_back("png");
  if(aFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(aFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(aFormat.find("xml")!=string::npos) form.push_back("xml");
  if(aFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(aFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(aFormat.find("svg")!=string::npos) form.push_back("svg");
  if(aFormat.find("root")!=string::npos) form.push_back("root");
  
  // no graphical format --> exit
  if(form.size()==0){
    delete [] q_snrmax;
    return snr_max_w0;
  }
  
  // fill maps
  Osequence::mon->SetMessageType(monitor_info_3);
  *Osequence::mon<<"Otile::"<<__func__<<": fill maps"<<endl;
  for(unsigned int q=0; q<nq; q++)
    qplanes[q]->FillMap(mapfill,
                        aTimeOffset-(double)pwin.back()/2.0,
                        aTimeOffset+(double)pwin.back()/2.0);

  // set chirp parameters
  if(chirpt<=0) chirp->SetParameter(1, (double)Osequence::GetChunkTimeCenter()+aTimeOffset);
  else chirp->SetParameter(1, chirpt);

  // cosmetics
  GwollumPlot::SetLogx(0);
  GwollumPlot::SetLogy(1);
  GwollumPlot::SetGridx(0);
  GwollumPlot::SetGridy(0);

  // special case of root files
  TFile *frootmap = NULL;
  if(aFormat.find("root")!=string::npos){
    gwl_ss<<aOutdir<<"/"<<aName<<"MAPQ-"<<Osequence::GetChunkTimeCenter()<<".root";
    frootmap = new TFile(gwl_ss.str().c_str(), "RECREATE");
    gwl_ss.clear(); gwl_ss.str("");
  }
  
  // save maps for each Q plane
  for(unsigned int q=0; q<nq; q++){
    Osequence::mon->SetMessageType(monitor_info_3);
    *Osequence::mon<<"Otile::"<<__func__<<": draw map #"<<q<<endl;

    // apply time offset
    MapApplyOffset(qplanes[q]->tfmap, (double)Osequence::GetChunkTimeCenter());

    // last window zoom
    qplanes[q]->tfmap->GetXaxis()->SetRange((double)Osequence::GetChunkTimeCenter()+aTimeOffset-(double)pwin.back()/2.0,
                                     (double)Osequence::GetChunkTimeCenter()+aTimeOffset+(double)pwin.back()/2.0);
    // plot title
    gwl_ss<<aName<<": Q="<<fixed<<setprecision(3)<<qplanes[q]->GetQ();
    qplanes[q]->tfmap->SetTitle(gwl_ss.str().c_str());
    gwl_ss.clear(); gwl_ss.str("");

    // draw map
    GwollumPlot::Draw(qplanes[q]->tfmap,"COLZ");

    // save map in ROOT file
    if(aFormat.find("root")!=string::npos){
      gwl_ss<<"map"<<q;
      if(qplanes[q]->Omap::Write(frootmap, gwl_ss.str())==0)
        cerr<<"Otile::SaveMaps: cannot save map Q"<<q<<endl;
      gwl_ss.clear(); gwl_ss.str("");
    }
    
    // loop over time windows
    Osequence::mon->SetMessageType(monitor_info_3);
    *Osequence::mon<<"Otile::"<<__func__<<": save map #"<<q<<endl;
    for(unsigned int w=0; w<pwin.size(); w++){
      
      // zoom in
      qplanes[q]->tfmap->GetXaxis()->SetRangeUser((double)Osequence::GetChunkTimeCenter()+aTimeOffset-(double)pwin[w]/2.0,
                                                  (double)Osequence::GetChunkTimeCenter()+aTimeOffset+(double)pwin[w]/2.0);
      
      // loudest tile info
      if(!mapfill.compare("amplitude"))
        gwl_ss<<"Loudest: GPS="<<fixed<<setprecision(3)<<qplanes[q]->GetTileTime(t_snrmax[q][w],f_snrmax[q][w])
              <<", f="<<qplanes[q]->GetBandFrequency(f_snrmax[q][w])<<" Hz, "
              <<mapfill<<"="<<scientific<<qplanes[q]->GetTileContent(t_snrmax[q][w],f_snrmax[q][w]);
      else
        gwl_ss<<"Loudest: GPS="<<fixed<<setprecision(3)<<qplanes[q]->GetTileTime(t_snrmax[q][w],f_snrmax[q][w])
              <<", f="<<qplanes[q]->GetBandFrequency(f_snrmax[q][w])<<" Hz, "
              <<mapfill<<"="<<qplanes[q]->GetTileContent(t_snrmax[q][w],f_snrmax[q][w]);
      GwollumPlot::AddText(gwl_ss.str(), 0.01,0.01,0.03);
      gwl_ss.clear(); gwl_ss.str("");
      
      // set Z-axis range
      if(vrange[0]<vrange[1]) qplanes[q]->tfmap->GetZaxis()->SetRangeUser(vrange[0],vrange[1]);
      else qplanes[q]->tfmap->GetZaxis()->UnZoom();
      
      // save plot
      for(unsigned int f=0; f<form.size(); f++){
        if(!form[f].compare("root")) continue;
	gwl_ss<<aOutdir<<"/"<<aName<<"MAPQ"<<q<<"-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
	GwollumPlot::Print(gwl_ss.str());
	gwl_ss.clear(); gwl_ss.str("");
	if(aThumb){ //thumbnail
	  gwl_ss<<aOutdir<<"/th"<<aName<<"MAPQ"<<q<<"-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
	  GwollumPlot::Print(gwl_ss.str(), (double)O_THUMBNAIL_RATIO);
	  gwl_ss.clear(); gwl_ss.str("");
	}
      }

      // save plot with chirp
      if(GetChirpMass()>0){
        chirp->SetRange((double)Osequence::GetChunkTimeCenter()+aTimeOffset-pwin[w]/2.0, chirp->GetParameter(1)-0.00001);
      
	GwollumPlot::Draw(chirp,"LSAME");
	for(unsigned int f=0; f<form.size(); f++){
          if(!form[f].compare("root")) continue;
          gwl_ss<<aOutdir<<"/"<<aName<<"MAPQ"<<q<<"C-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
	  GwollumPlot::Print(gwl_ss.str());
	  gwl_ss.clear(); gwl_ss.str("");
	  if(aThumb){ //thumbnail
	    gwl_ss<<aOutdir<<"/th"<<aName<<"MAPQ"<<q<<"C-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
	    GwollumPlot::Print(gwl_ss.str(), (double)O_THUMBNAIL_RATIO);
	    gwl_ss.clear(); gwl_ss.str("");
	  }
	}
	GwollumPlot::UnDraw(chirp);
      }
      
    }
    
    // unzoom
    qplanes[q]->tfmap->GetXaxis()->UnZoom();
    MapApplyOffset(qplanes[q]->tfmap, -(double)Osequence::GetChunkTimeCenter());
  }

  // save map in ROOT file
  if(frootmap!=NULL){
    frootmap->Close();
  }

  // full map
  Osequence::mon->SetMessageType(monitor_info_3);
  *Osequence::mon<<"Otile::"<<__func__<<": fill and save full map"<<endl;
  for(unsigned int w=0; w<pwin.size(); w++){

    // get full map
    FillFullMap(w, aTimeOffset);
    fullmap[w]->SetTitle(aName.c_str());
    fullmap[w]->GetZaxis()->SetTitle(StringToUpper(mapfill).c_str());
    MapApplyOffset(fullmap[w], (double)Osequence::GetChunkTimeCenter()+aTimeOffset);

    // draw map
    GwollumPlot::Draw(fullmap[w],"COLZ");
        
    // loudest tile
    if(!mapfill.compare("amplitude"))
      gwl_ss<<"Loudest: GPS="<<fixed<<setprecision(3)<<(double)Osequence::GetChunkTimeCenter()+qplanes[q_snrmax[w]]->GetTileTime(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w])
            <<", f="<<qplanes[q_snrmax[w]]->GetBandFrequency(f_snrmax[q_snrmax[w]][w])<<" Hz, "
            <<mapfill<<"="<<scientific<<qplanes[q_snrmax[w]]->GetTileContent(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w]);
    else gwl_ss<<"Loudest: GPS="<<fixed<<setprecision(3)<<(double)Osequence::GetChunkTimeCenter()+qplanes[q_snrmax[w]]->GetTileTime(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w])
               <<", f="<<qplanes[q_snrmax[w]]->GetBandFrequency(f_snrmax[q_snrmax[w]][w])<<" Hz, "
               <<mapfill<<"="<<qplanes[q_snrmax[w]]->GetTileContent(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w]);
    GwollumPlot::AddText(gwl_ss.str(), 0.01,0.01,0.03);
    gwl_ss.clear(); gwl_ss.str("");
    
    // set vertical range
    if(vrange[0]<vrange[1]) fullmap[w]->GetZaxis()->SetRangeUser(vrange[0],vrange[1]);
    else fullmap[w]->GetZaxis()->UnZoom();
        
    // save plot
    for(unsigned int f=0; f<form.size(); f++){
      gwl_ss<<aOutdir<<"/"<<aName<<"MAP"<<"-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
      GwollumPlot::Print(gwl_ss.str());
      gwl_ss.clear(); gwl_ss.str("");
      if(aThumb){ //thumbnail
	gwl_ss<<aOutdir<<"/th"<<aName<<"MAP"<<"-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
	GwollumPlot::Print(gwl_ss.str(), (double)O_THUMBNAIL_RATIO);
	gwl_ss.clear(); gwl_ss.str("");
      }
    }

    // save plot with chirp
    if(GetChirpMass()>0){
      chirp->SetRange((double)Osequence::GetChunkTimeCenter()+aTimeOffset-pwin[w]/2.0, chirp->GetParameter(1)-0.00001);
      GwollumPlot::Draw(chirp,"LSAME");
      for(unsigned int f=0; f<form.size(); f++){
	gwl_ss<<aOutdir<<"/"<<aName<<"MAP"<<"C-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
	GwollumPlot::Print(gwl_ss.str());
	gwl_ss.clear(); gwl_ss.str("");
	if(aThumb){ //thumbnail
	  gwl_ss<<aOutdir<<"/th"<<aName<<"MAP"<<"C-"<<Osequence::GetChunkTimeCenter()<<"-"<<pwin[w]<<"."<<form[f];
	  GwollumPlot::Print(gwl_ss.str(), (double)O_THUMBNAIL_RATIO);
	  gwl_ss.clear(); gwl_ss.str("");
	}
      }
    }

    MapApplyOffset(fullmap[w], -(double)Osequence::GetChunkTimeCenter()-aTimeOffset);
  }

  // clean up and exit
  delete [] q_snrmax;
  return snr_max_w0;
}

////////////////////////////////////////////////////////////////////////////////////
void Otile::FillFullMap(const unsigned int aWindowIndex, const double aTimeOffset){
////////////////////////////////////////////////////////////////////////////////////  

  if(fullmap==NULL){
    Osequence::mon->SetMessageType(monitor_error);
    *Osequence::mon<<"Otile::"<<__func__<<": the fullmap does not exist, SetPlotTimeWindows() must be called first"<<endl;
    return;
  }
  
  unsigned int tstart, tend, ttstart, ttend, ffstart, ffend;
  double content, snr2;

  // reset fullmap
  fullmap[aWindowIndex]->Reset();

  // loop over q planes
  for(unsigned int q=0; q<nq; q++){

    // loop over frequency band of that q plane
    for(unsigned int f=0; f<qplanes[q]->GetBandN(); f++){

      // first and last frequency bin of full map to consider
      ffstart = fullmap[aWindowIndex]->GetYaxis()->FindBin(qplanes[q]->GetBandStart(f));
      ffend = fullmap[aWindowIndex]->GetYaxis()->FindBin(qplanes[q]->GetBandEnd(f));

      // qplane time bin indexes to sweep
      tstart = TMath::Max(0, qplanes[q]->GetTimeTileIndex(f, fullmap[aWindowIndex]->GetXaxis()->GetBinLowEdge(1)+aTimeOffset));
      tend = TMath::Min((int)qplanes[q]->GetBandTileN(f)-1, qplanes[q]->GetTimeTileIndex(f, fullmap[aWindowIndex]->GetXaxis()->GetBinUpEdge(fullmap[aWindowIndex]->GetNbinsX()-1)+aTimeOffset));

      // loop over relevant qplane time bins
      for(unsigned int t=tstart; t<=tend; t++){

	// first and last time bin of full map to consider
	ttstart = fullmap[aWindowIndex]->GetXaxis()->FindBin(qplanes[q]->GetTileTimeStart(t,f)-aTimeOffset);
	ttend = fullmap[aWindowIndex]->GetXaxis()->FindBin(qplanes[q]->GetTileTimeEnd(t,f)-aTimeOffset);
	
	snr2=qplanes[q]->GetTileSnrSq(t,f);
	content=qplanes[q]->GetTileContent(t,f);

	// loop over full map bins overlapping with qplane bins
	for(unsigned int tt=ttstart; tt<=ttend; tt++){
	  for(unsigned int ff=ffstart; ff<=ffend; ff++){
	    if(snr2>fullmap[aWindowIndex]->GetBinError(tt,ff)){// update content if higher SNR
	      fullmap[aWindowIndex]->SetBinContent(tt,ff,content);
	      fullmap[aWindowIndex]->SetBinError(tt,ff,snr2);
	    }
          }
        }
      }
    }
    
  }
  
  return;
}
