The Omicron package is designed to perform a Q-transform of time series. It also contains the `omicron-x` application to search for unmodeled gravitational-waves with Omicron.

For a full documentation, see [the Omicron doxygen page](https://virgo.docs.ligo.org/virgoapp/Omicron/).
